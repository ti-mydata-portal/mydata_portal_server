package com.hanafn.openapi.portal.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.RoleEnum;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.enums.etcEnum;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.BillingRequest;
import com.hanafn.openapi.portal.views.dto.FileDownloadResponse;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.UserRequest;
import com.hanafn.openapi.portal.views.dto.nice.ServiceCategoryEnum;
import com.hanafn.openapi.portal.views.repository.ApiRepository;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.repository.BillingRepository;
import com.hanafn.openapi.portal.views.repository.ProductRepository;
import com.hanafn.openapi.portal.views.repository.SecurityAuditRepository;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.repository.SettlementRepository;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.BillingVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;
import com.hanafn.openapi.portal.views.vo.RequestApiVO;

import lombok.extern.slf4j.Slf4j;

@Component("statXls")
@Slf4j
public class ExcelUtil {

    @Autowired
    ApiRepository apiRepository;
    @Autowired
    SecurityAuditRepository securityAuditRepository;
    @Autowired
    SettlementRepository settlementRepository;
    @Autowired
    BillingRepository billingRepository;
    @Autowired
    AppsRepository appsRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    SettingRepository settingRepository;
    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    CommonUtil commonUtil;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;

    // 헤더 스타일
    private void setHeaderStyle(CellStyle headStyle) {
        // 가는 경계선
        headStyle.setBorderTop(BorderStyle.THIN);
        headStyle.setBorderBottom(BorderStyle.THIN);
        headStyle.setBorderLeft(BorderStyle.THIN);
        headStyle.setBorderRight(BorderStyle.THIN);

        // 배경색은 노란색
        headStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.YELLOW.getIndex());
        headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // 데이터는 가운데 정렬
        headStyle.setAlignment(HorizontalAlignment.CENTER);
    }

    // 헤더 생성
    private void setHeader(Row row, Cell targetCell, CellStyle headStyle) {
        Cell cell = targetCell;
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("해당월");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("앱명");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("API명");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용건수");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("총금액");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("할인율");
        cell = row.createCell(6);
        cell.setCellStyle(headStyle);
        cell.setCellValue("최종요금");
        cell = row.createCell(7);
        cell.setCellStyle(headStyle);
        cell.setCellValue("비고");
    }

    // 데이터영역 스타일
    private void setBodyStyle(CellStyle bodyStyle) {
        bodyStyle.setBorderTop(BorderStyle.THIN);
        bodyStyle.setBorderBottom(BorderStyle.THIN);
        bodyStyle.setBorderLeft(BorderStyle.THIN);
        bodyStyle.setBorderRight(BorderStyle.THIN);
    }

    // 데이터영역 생성
    private void setBody(Row row, Cell targetCell, CellStyle bodyStyle, RequestApiVO data) {
        Cell cell = targetCell;
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getMsDate());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getApiNm());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUseCnt());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getTotalCost());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getDiscountRate());
        cell = row.createCell(6);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getFinalCost());
        cell = row.createCell(7);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getComment());
    }

    /*** 보안감사 - 관리포털 계정관리
     * @return*/
    public FileDownloadResponse excelAccount(HfnUserRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털계정관리");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setAccountHeader(row, cell, headStyle);

        List<HfnUserVO> list = securityAuditRepository.selectAdminAccountList(request);

        // 데이터 부분 생성
        for(HfnUserVO data : list) {
            row = sheet.createRow(rowNo++);
            setAccountBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털계정관리.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("ssl 오류1", e);
        } catch (Exception e) {
            log.error("ssl 오류2", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        FileDownloadResponse data = new FileDownloadResponse();
        data.setSelCnt(list.size());

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
            return data;
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    // 헤더 생성
    private void setAccountHeader(Row row, Cell targetCell, CellStyle headStyle) {
        Cell cell = targetCell;
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용자ID");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용자");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스카테고리");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("권한");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("접속IP");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("부서");
        cell = row.createCell(6);
        cell.setCellStyle(headStyle);
        cell.setCellValue("연락처");
        cell = row.createCell(7);
        cell.setCellStyle(headStyle);
        cell.setCellValue("상태");
        cell = row.createCell(8);
        cell.setCellStyle(headStyle);
        cell.setCellValue("잠금여부");
    }

    // 데이터영역 생성
    private void setAccountBody(Row row, Cell targetCell, CellStyle bodyStyle, HfnUserVO data) {
        Cell cell = targetCell;
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getHfnId());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(ServiceCategoryEnum.resolve(data.getHfnCd()).getName());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(RoleEnum.resolve(data.getRoleCd()).getKor());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getIpAddr());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getDeptNm());
        cell = row.createCell(6);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserTel());
        cell = row.createCell(7);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserStatCd());
        cell = row.createCell(8);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getLoginLock());
    }

    /*** 보안감사 - 관리포털 접속이력
     * @return*/
    public FileDownloadResponse excelConnHis(PortalLogRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털접속이력");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setConnHisHeader(row, cell, headStyle);

        List<PortalLogVO> list = securityAuditRepository.selectConnectionHistory(request);

        // 데이터 부분 생성
        for(PortalLogVO data : list) {
            row = sheet.createRow(rowNo++);
            setConnHisBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털접속이력.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        FileDownloadResponse data = new FileDownloadResponse();
        data.setSelCnt(list.size());

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
            return data;
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setConnHisHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("접속IP");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("거래명");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("거래일시");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용자ID");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스카테고리");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용자");
        cell = row.createCell(6);
        cell.setCellStyle(headStyle);
        cell.setCellValue("인증결과");
        cell = row.createCell(7);
        cell.setCellStyle(headStyle);
        cell.setCellValue("실패사유");
    }

    private void setConnHisBody(Row row, Cell cell, CellStyle bodyStyle, PortalLogVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getIpAddress());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getTrxNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getRegDttm());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getHfnId());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(!StringUtils.isEmpty(data.getHfnCd()) ? ServiceCategoryEnum.resolve(data.getHfnCd()).getName() : "");
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserNm());
        cell = row.createCell(6);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getProcStatCd());
        cell = row.createCell(7);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getOutputCtnt());
    }

    /*** 보안감사 - 개인정보감사,관리자 행위 이력
     * @return*/
    public FileDownloadResponse excelActivityHis(PortalLogRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setActivityHisHeader(row, cell, headStyle);

        List<PortalLogVO> list = securityAuditRepository.selectActivityHistory(request);

        // 데이터 부분 생성
        for(PortalLogVO data : list) {
            row = sheet.createRow(rowNo++);
            setActivityHisBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            if ("pinfo".equals(request.getSearchMenu())){
                filename = URLEncoder.encode("개인정보감사.xls","UTF-8");
            } else {
                filename = URLEncoder.encode("관리자행위이력.xls","UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        FileDownloadResponse data = new FileDownloadResponse();
        data.setSelCnt(list.size());

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
            return data;
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setActivityHisHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용시간");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용자ID");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스카테고리");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용자");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("접속IP");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용메뉴");
        cell = row.createCell(6);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사용기능");
        cell = row.createCell(7);
        cell.setCellStyle(headStyle);
        cell.setCellValue("조회건수");
    }

    private void setActivityHisBody(Row row, Cell cell, CellStyle bodyStyle, PortalLogVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getRegDttm());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getHfnId());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(ServiceCategoryEnum.resolve(data.getHfnCd()).getName());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserNm());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getIpAddress());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getTrxNm());
        cell = row.createCell(6);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(commonUtil.menuCnvrs(data.getMenuFunc()));
        cell = row.createCell(7);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getCnt());
    }

    /*** 수수료정산 - 관리포털 결제내역
     * @return*/
    public FileDownloadResponse excelPayment(BillingRequest.PaymentRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털결제내역");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setPaymentHeader(row, cell, headStyle);

        List<PayInfoVO> list = billingRepository.selectPayInfoNice(request);

        // 데이터 부분 생성
        for(PayInfoVO data : list) {
            row = sheet.createRow(rowNo++);
            setPaymentBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털결제내역.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        FileDownloadResponse data = new FileDownloadResponse();
        data.setSelCnt(list.size());

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
            return data;
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setPaymentHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("결제일");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("앱명칭");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("결제상품수");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("결제금액");
    }

    private void setPaymentBody(Row row, Cell cell, CellStyle bodyStyle, PayInfoVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAuthDate());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getPrdCnt());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAmt());
    }

    /*** 수수료정산 - 관리포털 정산내역 */
    public void excelSettlement(BillingRequest.SettlementRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털정산내역");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setSettlementHeader(row, cell, headStyle);

        List<AppUseVO> list = billingRepository.selectAppUseInfo(request);

        // 데이터 부분 생성
        for(AppUseVO data : list) {
            row = sheet.createRow(rowNo++);
            setSettlementBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털정산내역.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setSettlementHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("앱명칭");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("상품");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("시도건수");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("과금건수");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("과금금액");
    }

    private void setSettlementBody(Row row, Cell cell, CellStyle bodyStyle, AppUseVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppNm());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getPrdNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getReqCnt());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getResCnt());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getPayAmt());
    }

    /*** 수수료정산 - 청구관리 */
    public void excelBilling(BillingRequest request, HttpServletResponse response) {

        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털정산내역");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setBillingHeader(row, cell, headStyle);

        List<BillingVO> list = billingRepository.selectBillingList(request);

        // 데이터 부분 생성
        for(BillingVO data : list) {
            row = sheet.createRow(rowNo++);
            setBillingBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털정산내역.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }

    }

    private void setBillingHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스카테고리");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("앱");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("청구일");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("청구기간");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("청구금액");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("수납금액");
        cell = row.createCell(6);
        cell.setCellStyle(headStyle);
        cell.setCellValue("미냅금액");
        cell = row.createCell(7);
        cell.setCellStyle(headStyle);
        cell.setCellValue("메모");
    }

    private void setBillingBody(Row row, Cell cell, CellStyle bodyStyle, BillingVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(ServiceCategoryEnum.resolve(data.getPrdCtgy()).getName());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getBillingDate());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getBillingStDt()+ " ~ " +data.getBillingEnDt());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getBillingAmount());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getReceiptAmount());
        cell = row.createCell(6);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUnpaidAmount());
        cell = row.createCell(7);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getMemo());
    }

    /** APP목록 다운로드 **/
    public void excelApp(AppsRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털APP목록");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setAppHeader(row, cell, headStyle);

        List<AppsVO> list = appsRepository.selectAppList(request);

        // 데이터 부분 생성
        for(AppsVO data : list) {
            if (StringUtils.equals(data.getUserNmEncrypted(), "Y")) {
                try {
                    if (data.getRegUser() != null && !"".equals(data.getRegUser())) {
                        String decryptedUserNm = cryptoUtil.decrypt(data.getRegUser());
                        data.setRegUser(CommonUtil.maskingName(decryptedUserNm));
                        log.debug("복호화 - 이름: {}", decryptedUserNm);
                    }
                } catch (UnsupportedEncodingException e) {
                    log.error(e.getMessage());
                    throw new BusinessException("E026", messageSource.getMessage("E026"));
                } catch (Exception e) {
                    log.error(e.getMessage());
                    throw new BusinessException("E026", messageSource.getMessage("E026"));
                }
            }
            row = sheet.createRow(rowNo++);
            setAppBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털APP목록.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setAppHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("이용자");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("사업자번호");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("앱이름");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("시작일자");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("종료일자");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("앱상태");
        cell = row.createCell(6);
        cell.setCellStyle(headStyle);
        cell.setCellValue("승인상태");
    }

    private void setAppBody(Row row, Cell cell, CellStyle bodyStyle, AppsVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getRegUser());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getCopRegNo());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppNm());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppSvcStDt());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getAppSvcEnDt());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(etcEnum.resolveByCd(data.getAppStatCd()).getKor());
        cell = row.createCell(6);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(etcEnum.resolveByCd(data.getAppAplvStatCd()).getKor());
    }

    /** API목록 다운로드 **/
    public void excelApi(AdminRequest.ApiRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털API목록");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setApiHeader(row, cell, headStyle);

        List<ApiVO> list = apiRepository.selectApiList(request);

        // 데이터 부분 생성
        for(ApiVO data : list) {
            row = sheet.createRow(rowNo++);
            setApiBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털API목록.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setApiHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스카테고리");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("API명");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("URL");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("메서드");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("프로토콜");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("상태");
    }

    private void setApiBody(Row row, Cell cell, CellStyle bodyStyle, ApiVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(ServiceCategoryEnum.resolve(data.getHfnCd()).getName());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getApiNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getApiUrl());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getApiMthd());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getApiProcType());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(etcEnum.resolveByCd(data.getApiStatCd()).getKor());
    }

    /** 상품목록 다운로드 **/
    public void excelPrd(ProductRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털상품목록");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setPrdHeader(row, cell, headStyle);

        List<ProductVO> list = productRepository.selectPrdList(request);

        // 데이터 부분 생성
        for(ProductVO data : list) {
            row = sheet.createRow(rowNo++);
            setPrdBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털상품목록.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setPrdHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스카테고리");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("상품종류");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("상품명");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("서비스코드");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("등록일");
    }

    private void setPrdBody(Row row, Cell cell, CellStyle bodyStyle, ProductVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(ServiceCategoryEnum.resolve(data.getPrdCtgy()).getName());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(etcEnum.resolveByCd(data.getPrdKind()).getKor());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getPrdNm());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getServiceCd());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getRegDttm());
    }
    /** 이용자목록 다운로드 **/
    public void excelUser(UserRequest request, HttpServletResponse response) {
        // 워크북 생성
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("관리포털이용자목록");
        Row row = null;
        Cell cell = null;
        int rowNo = 0;

        // 테이블 헤더용 스타일
        CellStyle headStyle = wb.createCellStyle();
        setHeaderStyle(headStyle);

        // 데이터용 스타일
        CellStyle bodyStyle = wb.createCellStyle();
        setBodyStyle(bodyStyle);

        // 헤더 생성
        row = sheet.createRow(rowNo++);
        setUserHeader(row, cell, headStyle);

        List<UserVO> list = settingRepository.adminSelectUserList(request);

        // 데이터 부분 생성
        for(UserVO data : list) {

            // 이름, 전화번호, 이메일 복호화
            if (data.getUserNm() != null && !"".equals(data.getUserNm())) {
                String decryptedUserNm = null;
                try {
                    decryptedUserNm = cryptoUtil.decrypt(data.getUserNm());
                } catch (UnsupportedEncodingException e) {
                    log.error("filename encoding error.", e);
                } catch (Exception e) {
                    log.error("filename encoding error.", e);
                }
                data.setUserNm(CommonUtil.maskingName(decryptedUserNm));
                log.debug("복호화 - 이름: {}", decryptedUserNm);
            }

            if (data.getUserTel() != null && !"".equals(data.getUserTel())) {
                String decryptedUserTel = null;
                try {
                    decryptedUserTel = cryptoUtil.decrypt(data.getUserTel());
                } catch (UnsupportedEncodingException e) {
                    log.error("filename encoding error.", e);
                } catch (Exception e) {
                    log.error("filename encoding error.", e);
                }
                data.setUserTel(CommonUtil.maskingPhone(decryptedUserTel));
                log.debug("복호화 - 전화번호: {}", decryptedUserTel);
            }

            if (data.getUserEmail() != null && !"".equals(data.getUserEmail())) {
                String decryptedUserEmail = null;
                try {
                    decryptedUserEmail = cryptoUtil.decrypt(data.getUserEmail());
                } catch (UnsupportedEncodingException e) {
                    log.error("filename encoding error.", e);
                } catch (Exception e) {
                    log.error("filename encoding error.", e);
                }
                data.setUserEmail(CommonUtil.maskingEmail(decryptedUserEmail));
                log.debug("복호화 - 이메일: {}", decryptedUserEmail);
            }

            row = sheet.createRow(rowNo++);
            setUserBody(row, cell, bodyStyle, data);
        }

        // 컨텐츠 타입과 파일명 지정
        String filename = "";
        try {
            filename = URLEncoder.encode("관리포털이용자목록.xls","UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("filename encoding error.", e);
        } catch (Exception e) {
            log.error("filename encoding error.", e);
        }

        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attatchment; filename = "+ filename);

        try {
            // 엑셀 출력
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        } catch(Exception e) {
            log.error("ExcelUtils write Error: " + e.toString());
            throw new BusinessException("ExcelUtils write Error", e.getCause());
        }
    }

    private void setUserHeader(Row row, Cell cell, CellStyle headStyle) {
        cell = row.createCell(0);
        cell.setCellStyle(headStyle);
        cell.setCellValue("아이디");
        cell = row.createCell(1);
        cell.setCellStyle(headStyle);
        cell.setCellValue("이름");
        cell = row.createCell(2);
        cell.setCellStyle(headStyle);
        cell.setCellValue("전화번호");
        cell = row.createCell(3);
        cell.setCellStyle(headStyle);
        cell.setCellValue("이메일");
        cell = row.createCell(4);
        cell.setCellStyle(headStyle);
        cell.setCellValue("본인인증");
        cell = row.createCell(5);
        cell.setCellStyle(headStyle);
        cell.setCellValue("가입일시");
    }

    private void setUserBody(Row row, Cell cell, CellStyle bodyStyle, UserVO data) {
        cell = row.createCell(0);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserId());
        cell = row.createCell(1);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserNm());
        cell = row.createCell(2);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserTel());
        cell = row.createCell(3);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getUserEmail());
        cell = row.createCell(4);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getSendCd());
        cell = row.createCell(5);
        cell.setCellStyle(bodyStyle);
        cell.setCellValue(data.getRegDttm());
    }
}
