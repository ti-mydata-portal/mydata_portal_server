package com.hanafn.openapi.portal.util;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Qualifier("other")
public class Crypto implements ISecurity{
	
    @Override
    public String encrypt(String str) throws GeneralSecurityException, UnsupportedEncodingException, BusinessException {
    	String enStr = "";
    	//nStr = DguardManager.getInstance().Encrypt(str);
        return enStr;
    }
    
    @Override
    public String encrypt(String key, String str) throws GeneralSecurityException, UnsupportedEncodingException {
        return this.encrypt(str);
    }
    
    @Override
    public String decrypt(String str) throws GeneralSecurityException, UnsupportedEncodingException {
        String rtn = "";
        //rtn = DguardManager.getInstance().Decrypt(str);
        return rtn;
    }

    @Override
    public String decrypt(String key, String str) throws GeneralSecurityException, UnsupportedEncodingException {
        return this.decrypt(str);
    }
}
