package com.hanafn.openapi.portal.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class ElasticsearchRestClientConfig extends AbstractElasticsearchConfiguration {
	@Value("${spring.elasticsearch.rest.uris}")
	private String elauri;
	//private String elauri = "127.0.0.1:9200";

	@Override
	@Bean
	public RestHighLevelClient elasticsearchClient() {
    	log.info("Es uri : [{}]", elauri);
		//System.out.println("Es uri : " +elauri);
		final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
				.connectedTo(elauri)
//				.withBasicAuth("elastic", "elasticti!23")
				.build();

		return RestClients.create(clientConfiguration).rest();
	}
	 
}
