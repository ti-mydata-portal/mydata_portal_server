package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.TokenVO;

import lombok.Data;

@Data
public class TokenResponse {
	private List<TokenVO> list;
}
