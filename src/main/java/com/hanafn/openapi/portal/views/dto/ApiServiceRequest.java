package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class ApiServiceRequest {
	private String seqNo;
	private String modDivCd;
	private String apiSvrStatCd;
	private String beforeData;
	private String afterData;
	private String minVer;
	private String curVer;
	private String modUser;
	private String modUserNm;
	private String modDttm;
}
