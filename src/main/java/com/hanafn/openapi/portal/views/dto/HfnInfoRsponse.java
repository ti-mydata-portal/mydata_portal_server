package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.HfnInfoVO;

import lombok.Data;

@Data
public class HfnInfoRsponse {
    private List<HfnInfoVO> list;
}
