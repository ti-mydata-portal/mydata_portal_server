package com.hanafn.openapi.portal.views.vo;

import lombok.Data;

import java.util.List;

import org.apache.ibatis.type.Alias;

@Data
@Alias("apiColumnList")
public class ApiColumnListVO {
	private String seqNo;
    private String apiId;
    private String clmListCd;
    private String clmCd;
	private String clmNm;
	private String clmReqDiv;
	private Long clmOrd;
	private String clmType;
    private String clmCtnt;
    private String clmDefRes;
    private String clmValue;
    private String clmLength;
    private String required;
    private String masking;
    private String logVisible;
    private String sqlInjection;
    private String nonidYn;
    private String hideYn;
    private List<ApiColumnListVO> apiColumnList;
}
