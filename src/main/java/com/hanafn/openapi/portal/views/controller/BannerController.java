package com.hanafn.openapi.portal.views.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.views.dto.BannerRequest;
import com.hanafn.openapi.portal.views.dto.BannerResponse;
import com.hanafn.openapi.portal.views.repository.BannerRepository;
import com.hanafn.openapi.portal.views.service.BannerService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/banner")
@Slf4j
@RequiredArgsConstructor
public class BannerController {

    private final BannerService service;
    private final BannerRepository repository;
    
    @Autowired
    CommonUtil commonUtil;
    
    @Autowired
    FileService fileService;
    
    @Autowired
    MessageSourceAccessor messageSource;

    @PostMapping("/selectBannerList")
    public ResponseEntity<?> selectBannerList(@Valid @RequestBody BannerRequest request, @CurrentUser UserPrincipal currentUser) {
        log.debug("###################### 개발자공간-약LIST ######################");

        BannerResponse.BannerListResponse data = service.selectBannerList(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectBannerDetail")
    public ResponseEntity<?> selectBannerDetail(@Valid @RequestBody BannerRequest.BannerDetailRequest request, @CurrentUser UserPrincipal currentUser) {
        log.debug("###################### 개발자공간-약LIST ######################");

        BannerResponse data = service.selectBanner(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectBannerUser")
    public ResponseEntity<?> selectBannerUser( @CurrentUser UserPrincipal currentUser, @RequestBody BannerRequest request) {
        log.debug("###################### 개발자공간-약LIST ######################");

        BannerResponse.BannerListResponse data = service.selectBannerUser(request);

        return ResponseEntity.ok(data);
    }

    /** 앱 별 항목암호화 키 관리 : 키 파일 저장하기 **/
    @PostMapping("/insertBanner")
    public ResponseEntity<?> insertBanner
    (
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "subject", required = false) String subject,
            @RequestParam(value = "ctnt", required = false) String ctnt,
            @RequestParam(value = "imgFileNm", required = false) String fileNm,
            @RequestParam(value = "img", required = false) MultipartFile file,
            @RequestParam(value = "orderNo", required = false) String orderNo,
            @RequestParam(value = "dispYn", required = false) String dispYn,
            @RequestParam(value = "url", required = false) String url,
            @RequestParam(value = "bannerGb", required = false) String bannerGb
    ) {
        if(commonUtil.superAdminCheck(currentUser)) {
        	
        	BannerRequest.BannerInsertRequest request = new BannerRequest.BannerInsertRequest();
        	
            if( file != null && !file.isEmpty() ) {
//                if( appsRepository.checkCryptoKey(appKey) > 0 ) {
//                    throw new BusinessException("CR03", messageSource.getMessage("CR03"));
//                }

                String data = "";
                try {
                    data = fileService.filePhotoSave(file);
                } catch (IOException e) {
                    log.info("첨부 파일 저장하기 에러 : " + e.toString());
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }

                if(data != null && data != "" && !data.equals("")) {
                    request.setBkImg(data);
                } else {
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }
            }
            request.setCtnt(ctnt);
            request.setSubject(subject);
            request.setOrderNo(Integer.parseInt(orderNo));
            request.setDispYn(dispYn);
            request.setRegId(currentUser.getUserKey());
            request.setUrl(url);
            request.setBannerGb(bannerGb);
            
            repository.insertBanner(request);

            return ResponseEntity.ok(new SignUpResponse(true,"Banner Insert Successfully"));
            
        } else {
            throw new BusinessException("C003", messageSource.getMessage("C003"));
        }
    }

    @PostMapping("/deleteBanner")
    public ResponseEntity<?> deleteTerms(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BannerRequest.BannerDetailRequest request) {

    	repository.deleteBanner(request);
        return ResponseEntity.ok(new SignUpResponse(true, "Terms Delete successfully"));
    }

    @PostMapping("/updateBanner")
    public ResponseEntity<?> updateTerms
    (
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "subject", required = false) String subject,
            @RequestParam(value = "ctnt", required = false) String ctnt,
            @RequestParam(value = "bkImg", required = false) String bkImg,
            @RequestParam(value = "imgFileNm", required = false) String fileNm,
            @RequestParam(value = "img", required = false) MultipartFile file,
            @RequestParam(value = "orderNo", required = false) String orderNo,
            @RequestParam(value = "dispYn", required = false) String dispYn,
            @RequestParam(value = "url", required = false) String url,
            @RequestParam(value = "seqNo", required = false) int seqNo,
            @RequestParam(value = "bannerGb", required = false) String bannerGb
    ) {
    	if(commonUtil.superAdminCheck(currentUser)) {
        	
        	BannerRequest.BannerUpdateRequest request = new BannerRequest.BannerUpdateRequest();
        	
            if( file != null && !file.isEmpty() ) {
//                if( appsRepository.checkCryptoKey(appKey) > 0 ) {
//                    throw new BusinessException("CR03", messageSource.getMessage("CR03"));
//                }

                String data = "";
                try {
                	fileService.fileDelete(bkImg);
                    data = fileService.filePhotoSave(file);
                } catch (IOException e) {
                    log.info("첨부 파일 저장하기 에러 : " + e.toString());
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }

                if(data != null && data != "" && !data.equals("")) {
                    request.setBkImg(data);
                } else {
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }
            }
            request.setCtnt(ctnt);
            request.setSubject(subject);
            request.setOrderNo(Integer.parseInt(orderNo));
            request.setDispYn(dispYn);
            request.setModId(currentUser.getUserKey());
            request.setUrl(url);
            request.setSeqNo(seqNo);
            request.setBannerGb(bannerGb);
            
            repository.updateBanner(request);

            return ResponseEntity.ok(new SignUpResponse(true,"Banner Insert Successfully"));
            
        } else {
            throw new BusinessException("C003", messageSource.getMessage("C003"));
        }
    }
}
