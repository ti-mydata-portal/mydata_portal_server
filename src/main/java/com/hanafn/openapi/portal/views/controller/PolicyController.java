package com.hanafn.openapi.portal.views.controller;

import javax.validation.Valid;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.views.dto.*;
import com.hanafn.openapi.portal.views.service.ApiService;
import com.hanafn.openapi.portal.views.vo.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppUsageRatioRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.PolicyResponse;
import com.hanafn.openapi.portal.admin.views.vo.TokenPolicyVO;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.views.service.PolicyService;
import com.hanafn.openapi.portal.views.service.ApiService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PolicyController {

    @Autowired
    PolicyService policyService;

    /***********************************************************************************************/
    /********************************************* API *********************************************/
    /***********************************************************************************************/
    @PostMapping("/apiPolicy")
    public ApiPolicyVO apiPolicy(@Valid @RequestBody ApiPolicyRequest request) {
        return policyService.selectApiPolicy(request);
    }

    @PostMapping("/apiPolicyRegist")
    public ResponseEntity<?> apiPolicyRegist(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiPolicyRequest request) {
        if (StringUtils.isBlank(request.getGubun())) {
            request.setRegUser(currentUser.getUserKey());
            policyService.insertApiPolicy(request);
        } else {
            request.setModUser(currentUser.getUserKey());
            policyService.updateApiPolicy(request);
        }
        return ResponseEntity.ok(true);
    }

    /***********************************************************************************************/
    /********************************* APP정책 : 인증실패 제어 (토큰발급) *********************************/
    /***********************************************************************************************/
    @PostMapping("/getTokenPolicyList")
    public ResponseEntity<?> getTokenPolicyList(@Valid @RequestBody TokenPolicyRequest request) {
        PolicyResponse list = policyService.getTokenPolicyList(request);

        return ResponseEntity.ok(list);
    }

    @PostMapping("/getTokenPolicyDetail")
    public TokenPolicyVO getTokenPolicyDetail(@Valid @RequestBody TokenPolicyRequest request) {
        return policyService.getTokenPolicyDetail(request);
    }

    @PostMapping("/getTokenPolicyRegistration")
    public ResponseEntity<?> getTokenPolicyRegistration(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TokenPolicyRequest request) {
        policyService.getTokenPolicyRegistration(currentUser, request);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/getTokenPolicyReset")
    public ResponseEntity<?> getTokenPolicyReset(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TokenPolicyRequest request) {
        request.setModUser(currentUser.getUserKey());
        policyService.getTokenPolicyReset(request);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/getTokenPolicyUseSetting")
    public ResponseEntity<?> getTokenPolicyUseSetting(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TokenPolicyRequest request) {
        request.setRegUser(currentUser.getUserKey());
        request.setModUser(currentUser.getUserKey());
        policyService.getTokenPolicyUseSetting(request);
        return ResponseEntity.ok(true);
    }

    /***********************************************************************************************/
    /********************************************* 상품 *********************************************/
    /***********************************************************************************************/
    @PostMapping("/selectPrdPolicy")
    public PrdPolicyVO selectPrdPolicy(@Valid @RequestBody PrdPolicyRequest request) {
        return policyService.selectPrdPolicy(request);
    }

    @PostMapping("/insertPrdPolicy")
    public ResponseEntity<?> insertPrdPolicy(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PrdPolicyRequest request) {
        policyService.insertPrdPolicy(request);
        return ResponseEntity.ok(true);
    }


    /***********************************************************************************************/
    /******************************** APP정책 : 랜덤 사용량 제어 (토큰발급) ********************************/
    /***********************************************************************************************/
    @PostMapping("/selectAppUsageRatioPolicy")
    public AppUsageRatioVO selectAppUsageRatioPolicy(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppUsageRatioRequest request) {
        return policyService.selectAppUsageRatioPolicy(request);
    }

    @PostMapping("/insertAppUsageRatioPolicy")
    public ResponseEntity<?> insertAppUsageRatioPolicy(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppUsageRatioRequest request) {
        request.setRegUser(currentUser.getUserKey());
        policyService.insertAppUsageRatioPolicy(request);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/updateAppUsageRatioPolicy")
    public ResponseEntity<?> updateAppUsageRatioPolicy(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppUsageRatioRequest request) {
        request.setModUser(currentUser.getUserKey());
        policyService.deleteAppUsageRatioPolicy(request);
        request.setRegUser(currentUser.getUserKey());
        policyService.insertAppUsageRatioPolicy(request);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/deleteAppUsageRatioPolicy")
    public ResponseEntity<?> deleteAppUsageRatioPolicy(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppUsageRatioRequest request) {
        request.setModUser(currentUser.getUserKey());
        policyService.deleteAppUsageRatioPolicy(request);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/getRedisSet")
    public ResponseEntity<?> getRedisSet(@Valid @RequestBody PolicyRequest request) {
        PolicyResponse data = RedisCommunicater.getRedis(request.getPattern());

        return ResponseEntity.ok(data);
    }
    // 신규
    // API 정보 조회
    @PostMapping("/policyGetApiList")
    public ResponseEntity<?> policyGetApiList() {
        List<ApiVO> data = policyService.policyGetApiList();
        return ResponseEntity.ok(data);
    }
    // 클라이언트ID 조회
    @PostMapping("/policyGetClientIdList")
    public ResponseEntity<?> policyGetClientIdList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest.policyAppRequest request) {
        List<ServiceAppVO> data = policyService.policyGetClientIdList(request);
        return ResponseEntity.ok(data);
    }
    // 서버IP 조회
    @PostMapping("/policyGetServerIpList")
    public ResponseEntity<?> policyGetServerIpList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest.policyServerIpRequest request) {
        List<ServiceAppIpVO> data = policyService.policyGetServerIpList(request);
        return ResponseEntity.ok(data);
    }
    // 정책등록 regPolicyService
    @PostMapping("/regPolicyService")
    public ResponseEntity<?> regPolicyService(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServicePolicyRequest request) {
        request.setRegUser(currentUser.getUserKey());
        policyService.regPolicyService(request);
        return ResponseEntity.ok(new SignUpResponse(true, "policy registered successfully"));
    }
    // 정책 조회 getPolicyServiceList
    @PostMapping("/getPolicyServiceList")
    public ResponseEntity<?> getPolicyServiceList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServicePolicyRequest request) {
        PolicyServiceResponsePaging data = policyService.getPolicyServiceList(request);
        return ResponseEntity.ok(data);
    }
    // 정책 상태 변경 updPolicyServiceStatus
    @PostMapping("/updPolicyServiceStatus")
    public ResponseEntity<?> updPolicyServiceStatus(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServicePolicyRequest request) {
        policyService.updPolicyServiceStatus(request);
        return ResponseEntity.ok(new SignUpResponse(true, "policy status upd successfully"));
    }
    // 정책 수정 조회
    @PostMapping("/getPolicyServiceDetail")
    public ResponseEntity<?> getPolicyServiceDetail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServicePolicyRequest request) {
        PolicyServiceVO data = policyService.getPolicyServiceDetail(request);
        return ResponseEntity.ok(data);
    }
    // 정책 수정 updPolicyService
    @PostMapping("/updPolicyService")
    public ResponseEntity<?> updPolicyService(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServicePolicyRequest request) {
        policyService.updPolicyService(request);
        return ResponseEntity.ok(new SignUpResponse(true, "policy upd successfully"));
    }
    // 정책 삭제 delPolicyService
    @PostMapping("/delPolicyService")
    public ResponseEntity<?> delPolicyService(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServicePolicyRequest request) {
        policyService.delPolicyService(request);
        return ResponseEntity.ok(new SignUpResponse(true, "policy del upd successfully"));
    }
    // 레디스 정보 조회
    @PostMapping("/getRedisData")
    public ResponseEntity<?> getRedisData(@Valid @RequestBody ServicePolicyRequest request) {
        PolicyResponse data = policyService.getRedisData(request);
        return ResponseEntity.ok(data);
    }
}