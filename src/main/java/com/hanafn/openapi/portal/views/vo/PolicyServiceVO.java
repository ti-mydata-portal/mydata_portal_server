package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("policyservice")
public class PolicyServiceVO {
    private String seqNo;
    private String statCd;
    private String userKey;
    private String useorgNm;
    private String cnlKey;
    private String apiId;
    private String apiNm;
    private String apiUrl;
    private String maxUser;
    private String ltdTimeFm;
    private String ltdTime;
    private String ltdCnt;
    private String regDttm;
}
