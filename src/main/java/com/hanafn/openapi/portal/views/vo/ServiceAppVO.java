package com.hanafn.openapi.portal.views.vo;
import org.apache.ibatis.type.Alias;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Alias("serviceapp")
public class ServiceAppVO {
    private String appKey; // 서비스키
    private String appNm; // 서비스명
    private String appStatCd; // 서비스상태
    private String appClientId; // 클라이언트ID
    private String appScr; // 시크릿키
    private String appSvcStDt; // 서비스시작일
    private String appSvcEnDt; // 서비스종료일
    private String appCtnt; // 서비스설명
    private String userKey; // 이용기관코드
    private String hostNm; // 호스트명
    private String useDefresYn; // 대응답사용여부
    private String appMemo; // 메모
    private String hisSeqNo; // 히스토리넘버
    private String regUser; // 등록자
    private String regDttm; // 등록일시
    private String modUser; // 수정자
    private String modDttm; // 수정일시
    private String useorgNm; // 기관명
    private String cnlKey; // 허용IP/
    private String appScheme;
    private String redirectUri;

    private List<HashMap> clientIPs;
    private List<HashMap> callbackList;
    private List<HashMap> schemeList;
}
