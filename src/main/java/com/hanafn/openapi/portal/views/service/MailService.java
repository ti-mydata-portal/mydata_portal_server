package com.hanafn.openapi.portal.views.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.views.dto.MailRequest;
import com.hanafn.openapi.portal.views.repository.ProductRepository;
import com.hanafn.openapi.portal.views.repository.SettingRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class MailService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

	@Autowired
	ProductRepository repository;
	
	@Autowired
	SettingRepository settingRepository;
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public List<String> selectPrdUseUser(MailRequest request){
		return repository.selectPrdUseUser(request);
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public List<String> selectPrdUseUserSms(MailRequest request){
		return repository.selectPrdUseUserSms(request);
	}
	
	public String selectMailSender(MailRequest.MailSender request) {
		return settingRepository.selectMailSender(request);
	}
	
}