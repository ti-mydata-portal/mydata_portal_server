package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.QnaVO;

import lombok.Data;

@Data
public class QnaResponse {
    private List<QnaVO> qnaList;
    private QnaVO qna;

    private int totCnt;
    private int selCnt;
    private int pageIdx;
    private int pageSize;
}
