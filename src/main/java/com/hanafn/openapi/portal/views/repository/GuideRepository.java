package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdSettReqeust;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.views.dto.ApiDevGuideRequest;
import com.hanafn.openapi.portal.views.dto.AppsAllRequest;
import com.hanafn.openapi.portal.views.vo.ApiCtgrVO;
import com.hanafn.openapi.portal.views.vo.ApiDevGuideVO;

@Mapper
public interface GuideRepository {
    List<ApiDevGuideVO> selectApiDevGuideList(ApiDevGuideRequest apiDevGuideRequest);
    List<ApiDevGuideVO> selectApiDevGuideAllList(ApiDevGuideRequest apiStatModRequest);
    List<AppsVO> selectAppsAll(AppsAllRequest appsAllRequest);
    List<AppsVO> selectAppsAllNice(AppsAllRequest appsAllRequest);
    List<ProductVO> selectProdList(PrdSettReqeust request);
    List<ApiCtgrVO> selectDevGuidesApiCtgrAll(ApiDevGuideRequest apiDevGuideRequest);
    String selectEnckeyByEntrCd(String value);
}
