package com.hanafn.openapi.portal.views.dto;

import lombok.Data;
@Data
public class StatisticsRequest {
    private int pageIdx;
    private int pageSize;
    private int pageOffset = 0;
    private String searchType;
    private String searchApiType;
    private String searchUserKey;
    private String searchStDt;
    private String searchEnDt;
    @Data
    public static class statisticsDetailRequest {
        private String trxDate; // 일자
        private String apiType; // apiType
        private String type;
        private String statDate;
        private String orgCode;
        private String clientId;
        private String tmSlot;
    }
    @Data
    public static class statisticsUseorgDetailRequest {
        private String statDate; // 일자
        private String userKey; // apiType
    }
    @Data
    public static class statisticsMydataRequest {
        private int pageIdx;
        private int PageSize;
        private int PageOffset = 0;
        private String searchType;
        private String searchUserKey;
        private String searchApiType;
        private String searchStDt;
        private String searchEnDt;
        private String clientId;
        private String type;
    }
    @Data
    public static class statisticsBatchRequest {
        private String regDate;
        private String tmSlot;
    }
}
