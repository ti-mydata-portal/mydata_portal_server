package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;

import lombok.Data;

@Data
public class HfnUserRsponsePaging {
    private int pageIdx;
    private int pageSize;

    private int totCnt;
    private int selCnt;
    private List<HfnUserVO> list;
}