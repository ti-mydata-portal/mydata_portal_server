package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.views.dto.BannerRequest;
import com.hanafn.openapi.portal.views.vo.BannerVO;

@Mapper
public interface BannerRepository {
	List<BannerVO> selectBannerList(BannerRequest req);
	BannerVO selectBanner(BannerRequest.BannerDetailRequest req);
	List<BannerVO> selectBannerUser(BannerRequest request);
	
	int insertBanner(BannerRequest.BannerInsertRequest req);
	int deleteBanner(BannerRequest.BannerDetailRequest req);
	int updateBanner(BannerRequest.BannerUpdateRequest req);
}