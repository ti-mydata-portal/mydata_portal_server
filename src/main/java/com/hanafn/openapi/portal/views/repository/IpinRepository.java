package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdPayReqeust;
import com.hanafn.openapi.portal.views.dto.IpinRequest;
import com.hanafn.openapi.portal.views.dto.NicePayRequest;
import com.hanafn.openapi.portal.views.vo.IpinVO;
import com.hanafn.openapi.portal.views.vo.PayBoVO;
import com.hanafn.openapi.portal.views.vo.PayPrdBoVO;

@Mapper
public interface IpinRepository {
	IpinVO selectCI(IpinRequest req);
	IpinVO selectCertUserId(IpinRequest req);
	
	IpinVO selectCertResNo(IpinRequest.IpinUpdatePw req);
	int updateEmptyResNo(IpinRequest.IpinUpdatePw req);
	int updateUserPwd(IpinRequest.IpinUpdatePw req);
	int updateUserPwdHis(IpinRequest.IpinUpdatePw req);
	int selectCertCnt(IpinRequest req);
	void insertAdminCert(AdminRequest.SndCertMgntRequest sndCertMgntRequest);
	
	int updateCertUserIdResNo(IpinRequest.IpinUpdateReqNo request);
	
	String selectOverDI(AdminRequest.SndCertMgntRequest sndCertMgntRequest);
	String selectMoid();
	
	void insertPayAuthRequest(NicePayRequest reqeust);
	void insertPrdPay(PrdPayReqeust reqeust);
	void updatePrdPayStat(NicePayRequest.NicePayReusltRequest request);
	void updatePayResult(NicePayRequest.NicePayReusltRequest request);
	void updateBoPrd(NicePayRequest.NicePayBoRequest request);
	PayBoVO selectPayInfo(NicePayRequest.NicePayReusltRequest request);
	List<PayPrdBoVO> selectPayPrdInfo(NicePayRequest.NicePayReusltRequest request);
	int selectBefPayCnt(NicePayRequest.NicePayReusltRequest request);
	
	void updateAppPrdUse(NicePayRequest.NicePayReusltRequest request);
}