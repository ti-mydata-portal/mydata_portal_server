package com.hanafn.openapi.portal.views.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.views.dto.BannerRequest;
import com.hanafn.openapi.portal.views.dto.BannerResponse;
import com.hanafn.openapi.portal.views.dto.TermsRequest;
import com.hanafn.openapi.portal.views.repository.BannerRepository;
import com.hanafn.openapi.portal.views.vo.BannerVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class BannerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BannerService.class);

	@Autowired
	BannerRepository repository;
	
	@Autowired
	FileService fileService;
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public BannerResponse.BannerListResponse selectBannerList(BannerRequest request){
		List<BannerVO> list = repository.selectBannerList(request);
		
		BannerResponse.BannerListResponse res = new BannerResponse.BannerListResponse();
		res.setBannerList(list);
		
		return res;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public BannerResponse selectBanner(BannerRequest.BannerDetailRequest request){
		BannerVO terms = repository.selectBanner(request);
		
		BannerResponse res = new BannerResponse();
		res.setBanner(terms);
		
		return res;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public BannerResponse.BannerListResponse selectBannerUser(BannerRequest request){
		List<BannerVO> list = repository.selectBannerUser(request);
		
		BannerResponse.BannerListResponse res = new BannerResponse.BannerListResponse();
		res.setBannerList(list);
		for(BannerVO vo : list) {
			String path = vo.getBkImg();
			
			vo.setBkImg(path.replace(fileService.getPhotoFileDir(), ""));
			
		}
		
		
		return res;
	}
}