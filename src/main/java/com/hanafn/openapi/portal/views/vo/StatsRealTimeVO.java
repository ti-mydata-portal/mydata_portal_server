package com.hanafn.openapi.portal.views.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("statsRealTime")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatsRealTimeVO {
	private String trxDt;
	private String userId;
	private String userNm;
	private String appKey;
	private String appNm;
	private String prdId;
	private String prdNm;
	private String prdCd;
	private String apiNm;
	private String apiUrl;
	private String apiId;
	private String copRegNo;
	private String time;
	private String apiTrxCnt;
	private String avgProcTerm;
	private String maxProcTerm;
	private String apiError;

}
