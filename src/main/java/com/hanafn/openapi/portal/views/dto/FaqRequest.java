package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class FaqRequest {
    private int seqNo;
    private String faqGb;
    private String statCd;
    private String subject;
    private String ctnt;
    private String regId;
    private String regDttm;
    private String modId;
    private String modDttm;

    private int pageIdx;
    private int pageSize;
    private int pageOffset;
    private String searchFaq;
    
    @Data
    public static class FaqListRequest {
        private String searchFaq;
        private String statCd;
        private String faqGb;
        private int pageIdx;
        private int pageSize;
        private int pageOffset;
    }
}
