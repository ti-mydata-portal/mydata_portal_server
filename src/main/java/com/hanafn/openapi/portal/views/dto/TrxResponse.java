package com.hanafn.openapi.portal.views.dto;

import com.hanafn.openapi.portal.views.vo.TrxVO;
import lombok.Data;

import java.util.List;

@Data
public class TrxResponse {
    private int totCnt;
    private int selCnt;
    private List<TrxVO> trxList;
}