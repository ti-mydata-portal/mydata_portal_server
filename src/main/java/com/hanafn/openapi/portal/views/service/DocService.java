package com.hanafn.openapi.portal.views.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.views.dto.DocRequest;
import com.hanafn.openapi.portal.views.dto.DocResponse;
import com.hanafn.openapi.portal.views.repository.DocRepository;
import com.hanafn.openapi.portal.views.vo.DocVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class DocService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DocService.class);

	@Autowired
	DocRepository repository;
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public DocResponse.DocListResponse selectDocList(DocRequest request){
		List<DocVO> list = repository.selectDocList(request);
		
		DocResponse.DocListResponse res = new DocResponse.DocListResponse();
		res.setDocList(list);
		
		return res;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public DocResponse selectDoc(DocRequest.DocDetailRequest request){
		DocVO terms = repository.selectDoc(request);
		
		DocResponse res = new DocResponse();
		res.setDoc(terms);
		
		return res;
	} 
}