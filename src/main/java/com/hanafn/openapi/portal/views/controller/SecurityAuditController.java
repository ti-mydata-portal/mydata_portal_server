package com.hanafn.openapi.portal.views.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.CryptoUtil;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRsponsePaging;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.ExcelUtil;
import com.hanafn.openapi.portal.views.dto.FileDownloadResponse;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.HfnUserRsponsePaging;
import com.hanafn.openapi.portal.views.service.SecurityAuditService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class SecurityAuditController {

    @Autowired
    SecurityAuditService service;
    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    ExcelUtil excelUtil;
    @Autowired
    CommonUtil commonUtil;
    @Autowired
    CryptoUtil cryptoUtil;

    @PostMapping("/adminAccountList")
    public ResponseEntity<?> adminAccountList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest hfnUserRequest) throws Exception {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN")){
                if(!ga.getAuthority().contains("ROLE_SYS_ADMIN"))
                {
                    hfnUserRequest.setSearchHfnCd(currentUser.getHfnCd());
                }

                hfnUserRequest.setUserKey(currentUser.getUserKey());
                HfnUserRsponsePaging data = service.selectAdminAccountList(hfnUserRequest);
                
                for(HfnUserVO vo: data.getList()) {
                	vo.setHfnId(commonUtil.maskingId(vo.getHfnId()));
                	vo.setUserNm(commonUtil.maskingName(cryptoUtil.decrypt(vo.getUserNm())));
                }
                return ResponseEntity.ok(data);
            }
        }

        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    @PostMapping("/adminConnectionHistory")
    public ResponseEntity<?> adminConnectionHistory(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PortalLogRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_ADMIN")){
                PortalLogRsponsePaging data = service.selectConnectionHistory(request);
                return ResponseEntity.ok(data);
            }
        }

        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    @PostMapping("/adminActivityHistory")
    public ResponseEntity<?> adminActivityHistory(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PortalLogRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_ADMIN")){
                PortalLogRsponsePaging data = service.selectActivityHistory(request);
                return ResponseEntity.ok(data);
            }
        }

        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    @PostMapping("/excelAccount")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelDownload(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam(value = "searchHfncd", required = false) String searchHfncd,
            @RequestParam(value = "searchNm", required = false) String searchNm,
            @RequestParam(value = "searchRoleCd", required = false) String searchRoleCd,
            @RequestParam(value = "searchStatus", required = false) String searchStatus
    ) throws Exception {
        HfnUserRequest request = new HfnUserRequest();
        request.setSearchHfnCd(searchHfncd);
        request.setSearchNm(searchNm);
        request.setSearchRoleCd(searchRoleCd);
        request.setSearchStatus(searchStatus);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        FileDownloadResponse data = excelUtil.excelAccount(request, response);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/excelConnectionHis")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelConnectionHis(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam(value = "searchNm", required = false)  String searchNm,
            @RequestParam(value = "searchProcStatCd", required = false)  String searchProcStatCd,
            @RequestParam(value = "searchStDt", required = false)  String searchStDt,
            @RequestParam(value = "searchEnDt", required = false)  String searchEnDt
    ) throws Exception {
        PortalLogRequest request = new PortalLogRequest();
        request.setSearchNm(searchNm);
        request.setSearchProcStatCd(searchProcStatCd);
        request.setSearchStDt(searchStDt);
        request.setSearchEnDt(searchEnDt);

        FileDownloadResponse data = excelUtil.excelConnHis(request, response);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/excelActivityHis")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelActivityHis(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam(value = "searchNm", required = false)  String searchNm,
            @RequestParam(value = "searchOperator", required = false)  String searchOperator,
            @RequestParam(value = "searchTarget", required = false)  String searchTarget,
            @RequestParam(value = "searchMenu", required = false)  String searchMenu,
            @RequestParam(value = "searchStDt", required = false)  String searchStDt,
            @RequestParam(value = "searchEnDt", required = false)  String searchEnDt
    ) throws Exception {
        PortalLogRequest request = new PortalLogRequest();
        request.setSearchNm(searchNm);
        request.setSearchOperator(searchOperator);
        request.setSearchTarget(searchTarget);
        request.setSearchMenu(searchMenu);
        request.setSearchStDt(searchStDt);
        request.setSearchEnDt(searchEnDt);

        FileDownloadResponse data = excelUtil.excelActivityHis(request, response);

        return ResponseEntity.ok(data);
    }
}