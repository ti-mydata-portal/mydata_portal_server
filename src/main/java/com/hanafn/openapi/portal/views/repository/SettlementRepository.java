package com.hanafn.openapi.portal.views.repository;

import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.vo.AppPrdUseVO;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SettlementRepository {

    List<AppUseVO> selectAppUseInfo(SettlementRequest.AppUseInfoRequest request);
    List<AppPrdUseVO> getAppPrdUseInfo(SettlementRequest.AppUseInfoRequest request);
    
    List<AppUseVO> selectAppUseInfoPaging(SettlementRequest.AppUseInfoRequest request);
    List<AppPrdUseVO> getAppPrdUseInfoPaging(SettlementRequest.AppUseInfoRequest request);
    int countAppUseInfoPaging(SettlementRequest.AppUseInfoRequest request);
    int countAppPrdUseInfoPaging(SettlementRequest.AppUseInfoRequest request);
    
    List<PayInfoVO> getPayInfoNice(SettlementRequest.AppUseInfoRequest request);
    List<PayInfoVO> getPayInfoNiceALL(SettlementRequest.AppUseInfoRequest request);
    int countPayInfoNice(SettlementRequest.AppUseInfoRequest request);
    
    List<PayInfoVO> getPayDetailInfoNice(SettlementRequest.PayDetailRequest request);
}
