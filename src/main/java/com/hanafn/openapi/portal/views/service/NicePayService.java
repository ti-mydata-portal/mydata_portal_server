package com.hanafn.openapi.portal.views.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdPayReqeust;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.viewsv2.repository.AplvRepository;
import com.hanafn.openapi.portal.admin.viewsv2.repository.BoAppsRepository;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoApiVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.BoAppPrdVO;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvVO;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.DataEncrypt;
import com.hanafn.openapi.portal.util.DateUtil;
import com.hanafn.openapi.portal.util.NicePayUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.NicePayRequest;
import com.hanafn.openapi.portal.views.repository.IpinRepository;
import com.hanafn.openapi.portal.views.repository.ProductRepository;
import com.hanafn.openapi.portal.views.vo.PayBoVO;
import com.hanafn.openapi.portal.views.vo.PayPrdBoVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class NicePayService {
	private static final Logger LOGGER = LoggerFactory.getLogger(NicePayService.class);

	@Autowired
	IpinRepository repository;

	@Autowired
	AplvRepository aplvRepository;

	@Autowired
	BoAppsRepository boAppsRepository;

	@Autowired
	ProductRepository productRepository;

	@Value("${pay.backoffice.first.url}")
    private String boFirstUrl;
	
	@Value("${pay.backoffice.addPay.url}")
    private String boAddPayUrl;
	
	@Value("${pay.merchant.key}")
	private String merchantKey;
	
	@Value("${pay.merchant.id}")
	private String merchantID;
	
	@Autowired
	NicePayUtil nicePayUtil;
	
	@Autowired
    MessageSourceAccessor messageSource;
	
	@Autowired
	@Qualifier("Crypto")
	ISecurity cryptoUtil;
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public void createAppPrdPay(NicePayRequest request){
		List<AppPrdInfoVO> appPrdInfo = request.getAppPrdInfo();
		
		for(AppPrdInfoVO vo : appPrdInfo) {
			PrdPayReqeust payReqeust = new PrdPayReqeust();
			
			if(vo.getPlanAmt() > 0) {
				payReqeust.setAppKey(request.getAppKey());
				payReqeust.setUserKey(request.getRegUser());
				payReqeust.setPrdId(vo.getPrdId());
				payReqeust.setMoid(request.getMoid());
				payReqeust.setUseCnt(vo.getBuyCnt());
				payReqeust.setAmtGbCd("");
				payReqeust.setPrdAmt(vo.getFeeAmount());
				payReqeust.setPayConfirmYn(request.getPayConfirmYn());
				payReqeust.setPayAgreeYn(request.getPayAgreeYn());
				payReqeust.setCreditReportYn(request.getCreditReportYn());
				payReqeust.setRegUser(request.getRegUser());

				// PORTAL_PRD_PAY_INFO
				repository.insertPrdPay(payReqeust);
			}
		}
		// PORTAL_PAY_HIS
		repository.insertPayAuthRequest(request);
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public void updateAppPrdPay(NicePayRequest.NicePayReusltRequest request, HttpServletRequest httpRequest) throws BusinessException, Exception{
		
		String resultCode = request.getResultCode();
		String payMethod = request.getPayMethod();
		boolean paySuccess = false;
		String conUrl = boFirstUrl;
		
		if(payMethod.equals("CARD")){
			if(resultCode.equals("3001")) paySuccess = true; // 신용카드(정상 결과코드:3001)
		}else if(payMethod.equals("BANK")){
			if(resultCode.equals("4000")) paySuccess = true; // 계좌이체(정상 결과코드:4000)
		}else if(payMethod.equals("CELLPHONE")){
			if(resultCode.equals("A000")) paySuccess = true; // 휴대폰(정상 결과코드:A000)
		}else if(payMethod.equals("VBANK")){
			if(resultCode.equals("4100")) paySuccess = true; // 가상계좌(정상 결과코드:4100)
		}else if(payMethod.equals("SSG_BANK")){
			if(resultCode.equals("0000")) paySuccess = true; // SSG은행계좌(정상 결과코드:0000)
		}else if(payMethod.equals("CMS_BANK")){
			if(resultCode.equals("0000")) paySuccess = true; // 계좌간편결제(정상 결과코드:0000)
		}
		if(paySuccess) {
			repository.updatePrdPayStat(request);
            // UPDATE PORTAL_PRD_PAY_INFO
            // SET PAY_STAT_CD = 'OK'
            // WHERE MOID = #{moid}

			repository.updateAppPrdUse(request);
            // UPDATE PORTAL_APP_PRD_INFO
            // SET USE_FL = 'Y'
            //    ,USE_ST_DT = 	NULL
            //    ,USE_END_DT = NULL
            //    ,MOD_USER = #{modUser}
            //    ,MOD_DTTM = NOW()
            //  WHERE APP_KEY =(SELECT APP_KEY
            //  FROM PORTAL_PRD_PAY_INFO
            //  WHERE MOID = #{moid}
            //  GROUP BY APP_KEY)
            //  AND USE_FL = 'WAIT'     > 포털에서 WAIT로 세팅하는 케이스 없음
		}

		repository.updatePayResult(request);
        // UPDATE PORTAL_PAY_HIS

		PayBoVO payBo = repository.selectPayInfo(request);
		String appClientId = payBo.getAppClientId();

		List<PayPrdBoVO> payPrdBo = repository.selectPayPrdInfo(request);

		Map<String, Object> param = new HashMap<String, Object>();
		List<AppPrdInfoVO> prdlist = request.getAppPrdInfo();
		
    	if(payPrdBo != null && payPrdBo.size() > 0) {
    		for(PayPrdBoVO vo : payPrdBo) {
    			conUrl = boAddPayUrl;
    			param.put("userKey", request.getModUser());
    	    	param.put("appKey", payBo.getAppKey());
    	    	
    	    	param.put("payMethod", payBo.getPayMethod());
    	    	param.put("paidAmount", payBo.getAmt());
    	    	param.put("paidAt", payBo.getAuthDate());
    	    	param.put("pgTid", payBo.getTxTid());
    	    	param.put("pgMoID", payBo.getMoid());
    	    	param.put("buyerTitle", payBo.getGoodsName());
    	    	param.put("prdCtgy", payBo.getPrdCtgy());
    	    	
    	    	String userNm = payBo.getBuyerName();
    	    	// 이름
                if (userNm != null && !"".equals(userNm)) {
                    String decryptedUserNm = cryptoUtil.decrypt(userNm);
                    param.put("buyerName", decryptedUserNm);
                }
                
                String email = payBo.getBuyerEmail();
                // 이메일 
                if (email != null && !"".equals(email)) {
                    String decryptedEmail = cryptoUtil.decrypt(email);
                    param.put("buyerEmail", decryptedEmail);
                }
                
                String tel = payBo.getBuyerTel();
                // 연락처
                if (tel != null && !"".equals(tel)) {
                    String decryptedTel = cryptoUtil.decrypt(tel);
                    param.put("buyerTel", decryptedTel);
                }
    	    	
    	    	param.put("buyerpostCode", "");
    	    	param.put("instCnt", payBo.getCardQuota());
    	    	
    	    	List<Map<String, Object>> prdList = new ArrayList<Map<String, Object>>();
    			
    			Map<String, Object> prdInfo = new HashMap<String, Object>();
    			prdInfo.put("prdCtgy", vo.getPrdCtgy());
    			prdInfo.put("prdKind", vo.getPrdKind());
    			prdInfo.put("serviceCd", vo.getServiceCd());
    			
    			for(AppPrdInfoVO prdVo : prdlist) {
    				if(prdVo.getSeqNo() != null && !"".equals(prdVo.getSeqNo()) && prdVo.getSeqNo().equals(vo.getSeqNo())) {
    					prdInfo.put("useCnt", prdVo.getBuyCnt());
    					prdInfo.put("prdAmt", prdVo.getPlanAmt());
    					prdInfo.put("planCd", prdVo.getPlanCd());
    					prdInfo.put("term", prdVo.getTerm());
    					prdInfo.put("prdId", prdVo.getPrdId());
    				}
    			}
    			
    			prdInfo.put("lgcyCd", vo.getSvcCd());
    			
    			if(!(vo.getMppCd1() != null && !"".equals(vo.getMppCd1())) && !(vo.getMppCd2() != null && !"".equals(vo.getMppCd2())) && !(vo.getMppCd3() != null && !"".equals(vo.getMppCd3()))) {
    				
    				conUrl = boFirstUrl;
    				
    				param.put("appNm", payBo.getAppNm());
    				param.put("prdKind", payBo.getPrdKind());
    				param.put("appCtnt", payBo.getAppCtnt());
    		    	param.put("appSvcNm", payBo.getAppSvcNm());
    		    	param.put("appSvcUrl", payBo.getAppSvcUrl());
    		    	param.put("copRegNo", payBo.getCopRegNo());
    		    	param.put("copNm", payBo.getCopNm());
    		    	
    		    	param.put("bizCdtn", payBo.getBizCdtn());
    		    	param.put("bizType", payBo.getBizType());
    		    	param.put("copAddr1", payBo.getCopAddr1());
    		    	param.put("copAddr2", payBo.getCopAddr2());
    		    	param.put("copAddrNo", payBo.getCopAddrNo());
    		    	
    		    	String ownNm = payBo.getOwnNm();
                    if (ownNm != null && !"".equals(ownNm)) {
                        String decryptedOwnNm = cryptoUtil.decrypt(ownNm);
                        param.put("ownNm", decryptedOwnNm);
                    }
    		    	
                    String dirNm = payBo.getDirNm();
                    if (dirNm != null && !"".equals(dirNm)) {
                        String decryptedDirNm = cryptoUtil.decrypt(dirNm);
                        param.put("dirNm", decryptedDirNm);
                    }
    		    	
    		    	param.put("dirTelNo1", payBo.getDirTelNo1());
    		    	
                    String dirTel2 = payBo.getDirTelNo2();
                    if (dirTel2 != null && !"".equals(dirTel2)) {
                        String decryptedTel2 = cryptoUtil.decrypt(dirTel2);
                        param.put("dirTelNo2", decryptedTel2.substring(0,3));
                        param.put("dirTelNo3", decryptedTel2.substring(3));
                    }

                    String dirEmail = payBo.getDirEmail();
                    if (dirEmail != null && !"".equals(dirEmail)) {
                        String decryptedDirEmail = cryptoUtil.decrypt(dirEmail);
                        param.put("dirEmail", decryptedDirEmail);
                    }

    		    	param.put("dirDomain", payBo.getDirDomain());
    			}else {
    				prdInfo.put("mppCd1", vo.getMppCd1());
    				prdInfo.put("mppCd2", vo.getMppCd2());
    				prdInfo.put("mppCd3", vo.getMppCd3());
    			}
    			
    			prdList.add(prdInfo);
    			
    			param.put("list", prdList);
    			
    			ObjectMapper om = new ObjectMapper();
    			
    			BufferedReader br = null;
    			HttpURLConnection hcon = null;
    			URL url;
    			StringBuffer result = new StringBuffer();
    	    	
    	    	try {
    				
    	    		String jsonParam = om.writeValueAsString(param);
    				url = new URL(conUrl);
    				hcon = (HttpURLConnection) url.openConnection();

    				if(hcon != null) {
    					hcon.setRequestMethod("POST");
    					hcon.setDoOutput(true);              //항상 갱신된내용을 가져옴.
    					hcon.setRequestProperty("Content-Type", "application/json;charset=euc-kr");
    					hcon.setRequestProperty("Authorization", httpRequest.getHeader("Authorization"));

						OutputStream os;

						os = hcon.getOutputStream();
						os.write(jsonParam.getBytes());
						os.flush();

						int code = hcon.getResponseCode();
						String message = hcon.getResponseMessage();
						if(code != 200) {
							Map<String, Object> cancelMap = this.payCancel(request,messageSource.getMessage("E084"));
							
							if(cancelMap.get("ResultCode") != null && !cancelMap.get("ResultCode").equals("") && cancelMap.get("ResultCode").equals("2001")) {
								throw new BusinessException(cancelMap.get("ResultCode")+"", messageSource.getMessage("E084"));
							}else {
								throw new BusinessException(cancelMap.get("ResultCode")+"", messageSource.getMessage("E084")+"("+cancelMap.get("ResultCode") + ":" + cancelMap.get("ResultMsg")+")");
							}
						}

						br = new BufferedReader(new InputStreamReader(hcon.getInputStream()));

						String temp;
						while ((temp = br.readLine()) != null) {
							result.append(temp);
						}

						Map<String, Object> resultMap = om.readValue(result.toString(), Map.class);
						
						log.info("******************************요금제 통신 시작 ************************");
						log.info("input_param" + jsonParam);
						log.info("URL : " + conUrl);
						log.info("result : " + result);
						
						log.info("******************************요금제 통신 끝 ************************");

						if(resultMap != null && resultMap.get("resultCode").equals("0000")) {

							List<Map<String, Object>> prdResultList = (List<Map<String, Object>>) resultMap.get("list");

							for(Map<String, Object> prd : prdResultList) {
								NicePayRequest.NicePayBoRequest boRequest = new NicePayRequest.NicePayBoRequest();
								boRequest.setAppKey((String)resultMap.get("appKey"));
								boRequest.setMppCd1((String)prd.get("mappingCode1"));
								boRequest.setMppCd2((String)prd.get("mappingCode2"));
								boRequest.setMppCd3((String)prd.get("mappingCode3"));
								boRequest.setMppUser((String)prd.get("mappingUser"));
								boRequest.setServiceCd((String)prd.get("serviceCd"));
								boRequest.setRestCnt((int)prd.get("remainCount"));
								boRequest.setUseStDt((String)prd.get("startDate"));
								boRequest.setUseEndDt((String)prd.get("endDate"));

								repository.updateBoPrd(boRequest);

								//반복해서 수행되더라도 무시.
								RedisCommunicater.productCodeRedisSet(appClientId, vo.getPrdCd(), "true");
								setRedisProductLimit(boRequest.getAppKey(), vo.getPrdId(), appClientId, String.valueOf ((int)prd.get("remainCount")));

								//mppCD1 저장
								ProductRequest productRequest = new ProductRequest();
								productRequest.setPrdCtgy(vo.getPrdCtgy());
								productRequest.setPrdId(Integer.parseInt(vo.getPrdId()));
								List<ApiVO> apiList = productRepository.selectPrdApiInfo(productRequest);
								for (ApiVO apivo : apiList) {
									RedisCommunicater.appPrdMppCd1RedisSet(vo.getPrdCtgy(), appClientId, apivo.getApiUrl(), boRequest.getMppCd1());
								}
							}

							//redis product set
							setRedisProduct(appClientId, vo.getPrdId(), vo.getPrdCd(), vo.getPrdCtgy());
						}else {
							String errorCode = "E084";
							String cancelMsg = "";
							if(resultMap != null) {
								log.error((String) resultMap.get("resultCode") + ":" + resultMap.get("message"));

								if (resultMap.get("resultCode").equals("0002")) {
									errorCode = "E085";
									cancelMsg = messageSource.getMessage("E085");    //
								} else {
									errorCode = "E084";
									cancelMsg = messageSource.getMessage("E084");    // 취소사유
								}
							}
							
							Map<String, Object> cancelMap = this.payCancel(request,cancelMsg);
							
							if(cancelMap.get("ResultCode") != null && !cancelMap.get("ResultCode").equals("") && cancelMap.get("ResultCode").equals("2001")) {
								throw new BusinessException(errorCode, messageSource.getMessage(errorCode));
							}else {
								throw new BusinessException(errorCode, messageSource.getMessage(errorCode)+"("+cancelMap.get("ResultCode") + ":" + cancelMap.get("ResultMsg")+")");
							}
						}
					} else {
						throw new BusinessException("HttpUrlConnection Error");
					}
    			} catch (JsonProcessingException e) {
    				log.error(e.getMessage());
					throw new BusinessException(e.getMessage());
    			} catch (MalformedURLException e) {
    				log.error(e.getMessage());
					throw new BusinessException(e.getMessage());
				} catch (IOException e) {
					log.error(e.getMessage());
					throw new BusinessException(e.getMessage());
				} catch (BusinessException e) {
					log.error(e.getMessage());
					throw e;
				}finally {
    	    		if(hcon != null) {
						hcon.disconnect();
						try {
							if(br != null) {
								br.close();
							}
						} catch (IOException e) {
							log.error(e.getMessage());
						}
					}

    			}
    		}
    	}
	}
	
	private Map<String,Object> payCancel(NicePayRequest.NicePayReusltRequest request, String cancelMsg) throws Exception {
		String tid 					= request.getTxTid();
		String cancelAmt 			= request.getAmt(); 
		String partialCancelCode 	= "0"; 	// 부분취소여부
		String mid 					= merchantID;	// 상점 ID
		String moid					= request.getMoid();	// 주문번호
		
		DataEncrypt sha256Enc 	= new DataEncrypt();
		String meKey 		= merchantKey; // 상점키
		String ediDate			= nicePayUtil.getyyyyMMddHHmmss();
		String signData 		= sha256Enc.encrypt(mid + cancelAmt + ediDate + merchantKey);
		
		StringBuffer requestData = new StringBuffer();
		requestData.append("TID=").append(tid).append("&");
		requestData.append("MID=").append(mid).append("&");
		requestData.append("Moid=").append(moid).append("&");
		requestData.append("CancelAmt=").append(cancelAmt).append("&");
		requestData.append("CancelMsg=").append(URLEncoder.encode(cancelMsg, "euc-kr")).append("&");
		requestData.append("PartialCancelCode=").append(partialCancelCode).append("&");
		requestData.append("EdiDate=").append(ediDate).append("&");
		requestData.append("CharSet=").append("utf-8").append("&");
		requestData.append("SignData=").append(signData);
		String resultJsonStr = nicePayUtil.connectToServer(requestData.toString(), "https://webapi.nicepay.co.kr/webapi/cancel_process.jsp");
		
		String resultCode 	= "";
		String resultMsg 	= "";
		String resultCancelAmt 	= "";
		String cancelDate 	= "";
		String cancelTime = "";
		String resultTid 		= "";

		if("9999".equals(resultJsonStr)){
			resultCode 	= "9999";
			resultMsg	= "통신실패";
		}else{
			HashMap resultData = nicePayUtil.jsonStringToHashMap(resultJsonStr);
			resultCode 	= (String)resultData.get("ResultCode");	// 결과코드 (취소성공: 2001, 취소성공(LGU 계좌이체):2211)
			resultMsg 	= (String)resultData.get("ResultMsg");	// 결과메시지
			resultCancelAmt 	= (String)resultData.get("CancelAmt");	// 취소금액
			cancelDate 	= (String)resultData.get("CancelDate");	// 취소일
			cancelTime 	= (String)resultData.get("CancelTime");	// 취소시간
			resultTid 		= (String)resultData.get("TID");		// 거래아이디 TID
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("ResultCode", resultCode);
		result.put("ResultMsg", resultMsg);
		
		return result;
	}

	/**
	 * redis set client_id + product_id, client_id+apiURL
	 * @param clientId
	 * @param prdId
	 * @param prdCtgy
	 * @throws Exception
	 */
	private void setRedisProduct(String clientId, String prdId, String prdCd, String prdCtgy) throws Exception {
		try {
			//API 목록 조회
			PrdAplvVO prdAplvVO = new PrdAplvVO();
			prdAplvVO.setPrdCtgy(prdCtgy);
			prdAplvVO.setPrdId(Integer.parseInt(prdId));

			List<BoApiVO> apiList = aplvRepository.selectPrdApiList(prdAplvVO);

			//APP PRD Redis set
			RedisCommunicater.productCodeRedisSet(clientId, prdCd, "true");

			//APP API redis set
			for (BoApiVO apiInfo : apiList) {
				RedisCommunicater.appApiRedisSet(
						clientId, apiInfo.getApiUrl(), "true");
			}
		} catch (NumberFormatException ne) {
			LOGGER.error("redis set err.. client id + product id", ne);
		} catch (Exception e) {
			LOGGER.error("redis set err.. client id + product id", e);
		}
	}

	/*
	선불상품 한도 저장
	 */
	public void setRedisProductLimit(String appKey, String prdId, String appClientId, String restCnt) {
		try {
			//만료일이 현재일 이전이면 redis 접근권한 false
			String useFl;
			BoAppPrdVO vo = new BoAppPrdVO();
			vo.setAppKey(appKey);
			vo.setPrdId(prdId);
			//db 갱신
			AppPrdInfoVO appPrdInfoVo = boAppsRepository.selectAppPrdListBySvcCd(vo);
			if (appPrdInfoVo == null)
				LOGGER.error(String.format("상품을 찾을 수 없습니다. app=%s, prd=%s", appKey, prdId));

			String today = DateUtil.getCurrentDate();

			if (appPrdInfoVo.getLimitYn().equals("Y")) { //gw에서 한도측정 사용
				RedisCommunicater.appPrdLimitRedisSet(appClientId, appPrdInfoVo.getPrdCd(), restCnt, "0");
			}else { //gw에서 한도측정 미사용. 한도건수를 제거함.
				RedisCommunicater.appPrdLimitRedisDel(appClientId, appPrdInfoVo.getPrdCd());
			}
		} catch (BusinessException be) {
			LOGGER.error("redis update err.." , be);
		} catch (Exception e) {
			LOGGER.error("redis update err..", e);
		}
	}
}