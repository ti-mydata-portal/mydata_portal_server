package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class ServicePolicyRequest {
    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;
    private String seqNo;
    private String userKey;
    private String statCd;
    private String cleintId;
    private String cnlKey;
    private String useorgNm;
    private String apiId;
    private String apiUrl;
    private String ltdOptions;
    private String maxUser;
    private String ltdTimeFm;
    private String ltdTime;
    private String bfltdTime;
    private String ltdCnt;
    private String limitedValue;
    private String regUser;

}
