package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.views.vo.ApiCtgrVO;

import lombok.Data;

@Data
public class ApiRsponsePaging {
    private int pageIdx;
    private int pageSize;
    
    private int totCnt;
    private int selCnt;
    private List<ApiVO> list;
    private List<ApiCtgrVO> ctgrList;
}
