package com.hanafn.openapi.portal.views.controller;

import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.CommExcelUtil;
import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.dto.SettlementResponse;
import com.hanafn.openapi.portal.views.service.SettlementNiceService;
import com.hanafn.openapi.portal.views.vo.AppPrdUseVO;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/settlement")
@Slf4j
@RequiredArgsConstructor
public class SettlementController {

    private final SettlementNiceService settlementService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);
    
    @Autowired
    CommExcelUtil excelUtil;

    // 후불 사용 정보(이용자포털)
    @PostMapping("/getAfterUseInfoNice")
    public ResponseEntity<?> getAfterUseInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	
    	String [] prdKinds = {"20","21"};
   	 
    	request.setSearchPrdKind(prdKinds);
    	
        return ResponseEntity.ok(settlementService.getUseInfoNicePaging(request));
    }
    
    @PostMapping("/getAfterAppUseInfoPaging")
    public ResponseEntity<?> getAfterAppUseInfoPaging(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	
    	String [] prdKinds = {"20","21"};
   	 
    	request.setSearchPrdKind(prdKinds);
    	
        return ResponseEntity.ok(settlementService.getAppUseInfoPaging(request));
    }
    
    @PostMapping("/getAfterUseInfoPaging")
    public ResponseEntity<?> getAfterUseInfoPaging(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	
    	String [] prdKinds = {"20","21"};
   	 
    	request.setSearchPrdKind(prdKinds);
    	
        return ResponseEntity.ok(settlementService.getUseInfoPaging(request));
    }
    
    // 선불 사용 정보(이용자포털)
    @PostMapping("/getBeforeUseInfoNice")
    public ResponseEntity<?> getBeforeUseInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	String [] prdKinds = {"10","11"};
    	
    	request.setSearchPrdKind(prdKinds);
    	
        return ResponseEntity.ok(settlementService.getUseInfoNicePaging(request));
    }
    
    @PostMapping("/getBeforeAppUseInfoPaging")
    public ResponseEntity<?> getBeforeAppUseInfoPaging(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	
    	String [] prdKinds = {"10","11"};
   	 
    	request.setSearchPrdKind(prdKinds);
    	
        return ResponseEntity.ok(settlementService.getAppUseInfoPaging(request));
    }
    
    @PostMapping("/getBeforeUseInfoPaging")
    public ResponseEntity<?> getBeforeUseInfoPaging(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	
    	String [] prdKinds = {"10","11"};
   	 
    	request.setSearchPrdKind(prdKinds);
    	
        return ResponseEntity.ok(settlementService.getUseInfoPaging(request));
    }
    
    // 결제 정보(이용자포털)
    @PostMapping("/getPayInfoNice")
    public ResponseEntity<?> getPayInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.AppUseInfoRequest request) throws Exception {
    	
        return ResponseEntity.ok(settlementService.getPayInfoNice(request));
    }
    
    // 결제 정보(이용자포털)
    @PostMapping("/getPayDetailInfoNice")
    public ResponseEntity<?> getPayDetailInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.PayDetailRequest request) throws Exception {
    	request.setUserKey(currentUser.getUserKey());
        return ResponseEntity.ok(settlementService.getPayDetailInfoNice(request));
    }
    
    // NICEAPI 선결제APP이용내역 엑셀다운로드
    @PostMapping("/beforeUseAppExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> beforeUseAppExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String searchUserKey,
                                           @RequestParam String searchAppKey,
                                           @RequestParam String searchPrdId,
                                           @RequestParam String searchStDtText,
                                           @RequestParam String searchEnDtText
                                            ) throws Exception {
        SettlementRequest.AppUseInfoRequest request = new SettlementRequest.AppUseInfoRequest();
        request.setSearchStDtText(searchStDtText);
        request.setSearchEnDtText(searchEnDtText);
        request.setSearchUserKey(searchUserKey);
        request.setSearchAppKey(searchAppKey);
        request.setSearchPrdId(searchPrdId);
        
        String [] prdKinds = {"10","11"};
    	request.setSearchPrdKind(prdKinds);
        
        SettlementResponse res = settlementService.getUseInfoNice(request);
        
        List<AppUseVO> appAppUseVo = res.getAppUseList();
        List<AppPrdUseVO> appPrdUseVo = res.getAppPrdUseList();
        
        String[] header1 = {"앱명칭", "상품","시도건수", "과금건수", "과금금액"};
        String[] header2 = {"사용일자", "앱명칭", "상품","시도건수", "과금건수", "과금금액"};
        String[] sheetNms = {"APP 사용내역 합계", "사용내역"};

        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header1);
        headers.add(header2);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(appAppUseVo);
        bodys.add(appPrdUseVo);
        
        String[] exportColumn1 = {"appNm", "prdNm", "reqCnt", "resCnt", "payAmt"};
        String[] exportColumn2 = {"useDt", "appNm", "prdNm", "reqCnt", "resCnt", "payAmt"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        exportColumns.add(exportColumn2);
        
        excelUtil.excelDown(2, sheetNms, headers, bodys, exportColumns, response);
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    // NICEAPI 후정산APP사용내역 엑셀다운로드
    @PostMapping("/afterUseAppExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> afterUseAppExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String searchUserKey,
                                           @RequestParam String searchAppKey,
                                           @RequestParam String searchPrdId,
                                           @RequestParam String searchStDtText,
                                           @RequestParam String searchEnDtText
                                            ) throws Exception {
        SettlementRequest.AppUseInfoRequest request = new SettlementRequest.AppUseInfoRequest();
        request.setSearchStDtText(searchStDtText);
        request.setSearchEnDtText(searchEnDtText);
        request.setSearchUserKey(searchUserKey);
        request.setSearchAppKey(searchAppKey);
        request.setSearchPrdId(searchPrdId);
        
        String [] prdKinds = {"20","21"};
    	request.setSearchPrdKind(prdKinds);
        
        SettlementResponse res = settlementService.getUseInfoNice(request);
        
        List<AppUseVO> appAppUseVo = res.getAppUseList();
        List<AppPrdUseVO> appPrdUseVo = res.getAppPrdUseList();
        
        String[] header1 = {"앱명칭", "상품","시도건수", "과금건수", "과금금액"};
        String[] header2 = {"사용일자", "앱명칭", "상품","시도건수", "과금건수", "과금금액"};
        String[] sheetNms = {"APP 사용내역 합계", "사용내역"};

        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header1);
        headers.add(header2);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(appAppUseVo);
        bodys.add(appPrdUseVo);
        
        String[] exportColumn1 = {"appNm", "prdNm", "reqCnt", "resCnt", "payAmt"};
        String[] exportColumn2 = {"useDt", "appNm", "prdNm", "reqCnt", "resCnt", "payAmt"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        exportColumns.add(exportColumn2);
        
        excelUtil.excelDown(2, sheetNms, headers, bodys, exportColumns, response);
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    
    // NICEAPI 결재내역 다운로드
    @PostMapping("/payInfoExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> payInfoExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String searchUserKey
                                            ) throws Exception {
        SettlementRequest.AppUseInfoRequest request = new SettlementRequest.AppUseInfoRequest();
        request.setSearchUserKey(searchUserKey);
        
        SettlementResponse.PayInfoResponse res = settlementService.getPayInfoNice(request);

        List<PayInfoVO> payInfoList = res.getPayInfoList();
        
        String[] header1 = {"결제일", "앱명칭","결제상품수", "결제금액", "주문번호"};
        String[] sheetNms = {"결제내역"};

        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header1);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(payInfoList);
        
        String[] exportColumn1 = {"authDate", "appNm", "prdCnt", "amt", "moid"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response);
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
}
