package com.hanafn.openapi.portal.views.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

import org.apache.ibatis.type.Alias;

@Data
@Alias("statMd4")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatMd4VO {

	private String tmSlot;
	private String statusCnt;
	private List<StatMd5VO> statusList;
}
