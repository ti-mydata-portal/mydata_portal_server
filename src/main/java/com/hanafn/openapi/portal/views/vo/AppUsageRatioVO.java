package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("appUsageRatio")
public class AppUsageRatioVO {

    /**
     * OPENAPI_PORTAL_APP_POLICY_INFO 앱 관련 정책 정보
     */

    private String seqNo;
    private String appNm;
    private String appKey;
    private String ratio;
    private String regUser;
    private String regDttm;
    private String modUser;
    private String modDttm;
}