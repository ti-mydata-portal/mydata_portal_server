package com.hanafn.openapi.portal.views.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.views.dto.TermsRequest;
import com.hanafn.openapi.portal.views.dto.TermsResponse;
import com.hanafn.openapi.portal.views.repository.TermsRepository;
import com.hanafn.openapi.portal.views.vo.TermsInstVO;
import com.hanafn.openapi.portal.views.vo.TermsVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class TermsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TermsService.class);

	@Autowired
	TermsRepository termsRepository;
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public TermsResponse.TermsListResponse selectTermsList(TermsRequest termsRequest){
		List<TermsVO> list = termsRepository.selectTermsList(termsRequest);
		
		TermsResponse.TermsListResponse termsListResponse = new TermsResponse.TermsListResponse();
		termsListResponse.setTermsList(list);
		
		return termsListResponse;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public TermsResponse selectTerms(TermsRequest.TermsDetailRequest termsRequest){
		TermsVO terms = termsRepository.selectTerms(termsRequest);
		
		TermsResponse termsResponse = new TermsResponse();
		termsResponse.setTerms(terms);
		
		return termsResponse;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public TermsResponse selectTermsUser(TermsRequest.TermsUserRequest termsRequest){
		TermsVO terms = termsRepository.selectTermsUser(termsRequest);
		List<TermsInstVO> termsInstList = termsRepository.selectTermsInstList(termsRequest);
		
		TermsResponse termsResponse = new TermsResponse();
		termsResponse.setTerms(terms);
		termsResponse.setTermsInstList(termsInstList);
		
		return termsResponse;
	}
}