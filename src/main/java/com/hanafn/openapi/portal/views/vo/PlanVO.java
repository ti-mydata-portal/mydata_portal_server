package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("plan")
public class PlanVO {

	private String term;
    private int price;
    private String plannm;
    private int count;
    private String plancd;
}
