package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.TermsInstVO;
import com.hanafn.openapi.portal.views.vo.TermsVO;

import lombok.Data;

@Data
public class TermsResponse {

	private TermsVO terms;
	private List<TermsInstVO> termsInstList; 
	
	@Data
    public static class TermsListResponse{
		private List<TermsVO> termsList;
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }

}
