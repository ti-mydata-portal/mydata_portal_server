package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("statsMydata")
public class StatsMydataVO {
    private String statDate;
    private String type;
    private String orgCode;
    private String clientId;
    private String apiType;
    private String apiTypeNm;
    private String tmSlot;
    private String successApiCnt;
    private String failApiCnt;
    private String maxApiCnt;
    private String userKey;
    private String userNm;
    private String appNm;
    private String tm;
    private String tokenNew;
    private String tokenRenew;
    private String tokenRevoke;
    private String tokenOwn;


    private String rspMed;
    private String rspAvg;
    private String rspTotal;
    private String rspStdev;
    private String statusCode;
    private String apiCallCnt;
    private String status200;
    private String status302;
    private String status400;
    private String status401;
    private String status403;
    private String status404;
    private String status429;
    private String status500;
}
