package com.hanafn.openapi.portal.views.vo;

import java.util.List;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@Alias("statMd1")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatMd1VO {

	private String type;
	private String statDate;
	private String mydataSvcCnt;
	private List<StatMd2VO> mydataSvcList;
}
