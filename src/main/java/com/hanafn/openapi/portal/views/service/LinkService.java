package com.hanafn.openapi.portal.views.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.UUIDUtils;
import com.hanafn.openapi.portal.views.dto.ApiTimesampRequest;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.dto.ServiceAppRequest;
import com.hanafn.openapi.portal.views.dto.StatisticsRequest;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.repository.StatsRepository;
import com.hanafn.openapi.portal.views.vo.StatMd1VO;
import com.hanafn.openapi.portal.views.vo.StatMd2VO;
import com.hanafn.openapi.portal.views.vo.StatMd3VO;
import com.hanafn.openapi.portal.views.vo.StatMd4VO;
import com.hanafn.openapi.portal.views.vo.StatMd5VO;
import com.hanafn.openapi.portal.views.vo.UseorgVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class LinkService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LinkService.class);

	@Autowired
    SettingNiceService settingService;
	
	@Autowired
	AppsService appsService;
	
	@Autowired
    SettingRepository settingRepository;
	
	@Autowired
    StatsRepository statRepository;
	
	@Autowired
    GWCommunicater gWCommunicater;
	
	public void tokenReg(UserPrincipal currentUser, String orgCode, Map<String, Object> request, Map<String, Object> response) throws Exception{
		LinkRequest.TokenRequest token = new LinkRequest.TokenRequest();
		
		token.setIssueOrgCd(orgCode);
		token.setGrantType((String)request.get("grant_type"));
		token.setClientId((String)request.get("client_id"));
		token.setScr((String)request.get("client_secret"));
		token.setToken((String)response.get("access_token"));
		token.setStatCd("Y");
		token.setScope((String)request.get("scope"));
		token.setExpiresIn((String)response.get("expires_in"));
		
		int cnt = settingRepository.selectTokenCnt(token);
		
		if(cnt > 0) {
			settingRepository.updateToken(token);
		}else {
			settingRepository.insertToken(token);
		}
		
		RedisCommunicater.oauthTokenKeySet(orgCode, token.getToken());
	}
	
	public void useOrgReg(String strJson, UserPrincipal currentUser) throws Exception{
		
		ObjectMapper om = new ObjectMapper();
    	
    	Map<String, Object> data = om.readValue(strJson, Map.class);
    	
    	ApiTimesampRequest timeRequest = new ApiTimesampRequest();
		timeRequest.setApiUrl("/mgmts/orgs");
		timeRequest.setReqTimestamp((String)data.get("search_timestamp"));
		settingService.updateSearchTime(timeRequest);

    	int orgCnt = Integer.parseInt((String)data.get("org_cnt"));
    	
    	if(orgCnt > 0) {
    		List<Map<String, Object>> orgData = (List<Map<String, Object>>)data.get("org_list");
    		
    		for(Map<String, Object> info : orgData) {
    			String opType = (String)info.get("op_type");
    			if(opType != null && !"".equals(opType) && "I".equals(opType)) {
    				UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
        			
        	    	request.setUserKey((String)info.get("org_code"));
        	    	request.setUseorgId((String)info.get("org_code"));
        	    	request.setUseorgGb((String)info.get("org_type"));
        	    	
        	    	if(info.containsKey("org_name")) {
        	    		request.setUseorgNm((String)info.get("org_name"));
        	    	}
        	    	
        	    	if(info.containsKey("org_regno")) {
        	    		request.setBrn((String)info.get("org_regno"));
        	    	}
        	    	
        	    	if(info.containsKey("corp_regno")) {
        	    		request.setUseorgNo((String)info.get("corp_regno"));
        	    	}
        	    	
        	    	if(info.containsKey("serial_num")) {
        	    		request.setSerialNum((String)info.get("serial_num"));
        	    	}
        	    	
        	    	if(info.containsKey("address")) {
        	    		request.setUseorgAddr((String)info.get("address"));
        	    	}
        	    	
        	    	if(info.containsKey("domain")) {
        	    		request.setUseorgDomain((String)info.get("domain"));
        	    	}

        	    	if(info.containsKey("relay_org_code")) {
        	    		request.setRelorgCd((String)info.get("relay_org_code"));
        	    	}
        	    	
        	    	if(info.containsKey("industry")) {
        	    		request.setEntrCd((String)info.get("industry"));
        	    	}
        	    	
        	    	if(info.containsKey("auth_type")) {
        	    		request.setProvCertCd((String)info.get("auth_type"));
        	    	}
        	    	
        	    	if(info.containsKey("cert_issuer_dn")) {
        	    		request.setCertIssuerDn((String)info.get("cert_issuer_dn"));
        	    	}
        	    	
        	    	if(info.containsKey("cert_oid")) {
        	    		request.setCertOid((String)info.get("cert_oid"));
        	    	}
        	    	
        	    	if(info.containsKey("is_rcv_org")) {
        	    		if(info.get("cert_oid") != null && !"".equals(info.get("cert_oid")) && "true".equals(info.get("cert_oid"))) {
        	    			request.setIsRcvOrg("Y");
        	    		}else {
        	    			request.setIsRcvOrg("N");
        	    		}
        	    	}
        	    	
        	    	request.setRegUser(currentUser.getUserKey());
        	    	request.setModUser(currentUser.getUserKey());

					if(info.containsKey("domain_ip_cnt")) {
						int domainIpCnt = Integer.parseInt((String)info.get("domain_ip_cnt"));

						if(domainIpCnt > 0) {
							List<Map<String, String>> domainIPs = new ArrayList<Map<String, String>>();

							List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("domain_ip_list");

							for(Map<String, Object> ipInfo : ipData) {
								Map<String, String> ipMap = new HashMap<String, String>();
								ipMap.put("domainIp", (String)ipInfo.get("domain_ip"));
								domainIPs.add(ipMap);
							}
							request.setUseorgDomainIPs(domainIPs);
						} else {
							request.setUseorgDomainIPs(new ArrayList<Map<String, String>>());
						}
					}else {

						request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
					}

        	    	if(info.containsKey("ip_cnt")) {
        	    		int ipCnt = Integer.parseInt((String)info.get("ip_cnt"));

        	    		if(ipCnt > 0) {
        	    			List<Map<String, String>> clientIPs = new ArrayList<Map<String, String>>();

        	    			List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("ip_list");

        	    			for(Map<String, Object> ipInfo : ipData) {
        	    				Map<String, String> ipMap = new HashMap<String, String>();
        	    				ipMap.put("cnlKey", (String)ipInfo.get("ip"));
        	    			}
        	    			request.setClientIPs(clientIPs);
        	    		} else {
							request.setClientIPs(new ArrayList<Map<String,String>>());
						}
        	    	}else {
        	    		request.setClientIPs(new ArrayList<Map<String,String>>());
        	    	}

        	        settingService.insertUseOrg(request, false);
    			}else if("M".equals(opType)) {
    				UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
    				request.setUserKey((String)info.get("org_code"));
        	    	request.setUseorgId((String)info.get("org_code"));
        	    	request.setUseorgGb((String)info.get("org_type"));
        	    	
        	    	if(info.containsKey("org_name")) {
        	    		request.setUseorgNm((String)info.get("org_name"));
        	    	}
        	    	
        	    	if(info.containsKey("org_regno")) {
        	    		request.setBrn((String)info.get("org_regno"));
        	    	}
        	    	
        	    	if(info.containsKey("corp_regno")) {
        	    		request.setUseorgNo((String)info.get("corp_regno"));
        	    	}
        	    	
        	    	if(info.containsKey("serial_num")) {
        	    		request.setSerialNum((String)info.get("serial_num"));
        	    	}
        	    	
        	    	if(info.containsKey("address")) {
        	    		request.setUseorgAddr((String)info.get("address"));
        	    	}
        	    	
        	    	if(info.containsKey("domain")) {
        	    		request.setUseorgDomain((String)info.get("domain"));
        	    	}
        	    	
        	    	if(info.containsKey("relay_org_code")) {
        	    		request.setRelorgCd((String)info.get("relay_org_code"));
        	    	}
        	    	
        	    	if(info.containsKey("industry")) {
        	    		request.setEntrCd((String)info.get("industry"));
        	    	}
        	    	
        	    	if(info.containsKey("auth_type")) {
        	    		request.setProvCertCd((String)info.get("auth_type"));
        	    	}
        	    	
        	    	if(info.containsKey("cert_issuer_dn")) {
        	    		request.setCertIssuerDn((String)info.get("cert_issuer_dn"));
        	    	}
        	    	
        	    	if(info.containsKey("cert_oid")) {
        	    		request.setCertOid((String)info.get("cert_oid"));
        	    	}
        	    	
        	    	if(info.containsKey("is_rcv_org")) {
        	    		if(info.get("cert_oid") != null && !"".equals(info.get("cert_oid")) && "true".equals(info.get("cert_oid"))) {
        	    			request.setIsRcvOrg("Y");
        	    		}else {
        	    			request.setIsRcvOrg("N");
        	    		}
        	    	}
        	    	
        	    	request.setModUser(currentUser.getUserKey());
        	    	request.setRegUser(currentUser.getUserKey());
        	    	
					if(info.containsKey("domain_ip_cnt")) {
						int domainIpCnt = Integer.parseInt((String)info.get("domain_ip_cnt"));

						if(domainIpCnt > 0) {
							List<Map<String, String>> domainIPs = new ArrayList<Map<String, String>>();

							List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("domain_ip_list");

							for(Map<String, Object> ipInfo : ipData) {
								Map<String, String> ipMap = new HashMap<String, String>();
								ipMap.put("domainIp", (String)ipInfo.get("domain_ip"));
								domainIPs.add(ipMap);
							}
							request.setUseorgDomainIPs(domainIPs);
						} else {
							request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
						}
					}else {
						request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
					}

        	    	if(info.containsKey("ip_cnt")) {
        	    		int ipCnt = Integer.parseInt((String)info.get("ip_cnt"));
        	    		
        	    		if(ipCnt > 0) {
        	    			List<Map<String, String>> clientIPs = new ArrayList<Map<String, String>>();
        	    			
        	    			List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("ip_list");
        	    			
        	    			for(Map<String, Object> ipInfo : ipData) {
        	    				Map<String, String> ipMap = new HashMap<String, String>();
        	    				ipMap.put("cnlKey", (String)ipInfo.get("ip"));
        	    				clientIPs.add(ipMap);
        	    			}
        	    			request.setClientIPs(clientIPs);
        	    		} else {
							request.setClientIPs(new ArrayList<Map<String,String>>());
						}
        	    	}else {
        	    		request.setClientIPs(new ArrayList<Map<String,String>>());
        	    	}
        	    	
        	    	settingService.useOrgUpd(request, false);
    			}else {
    				UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
    				request.setUserKey((String)info.get("org_code"));
    				request.setUseorgId((String)info.get("org_code"));
    				request.setUseorgStatCd("DEL");
    				request.setModUser(currentUser.getUserId());
    		        settingService.useOrgStatusUpd(request, false);
    			}
    		}
    	}
	}
	
	public void useOrgRegRcv(String strJson, UserPrincipal currentUser) throws Exception{
		
		ObjectMapper om = new ObjectMapper();
    	
    	Map<String, Object> data = om.readValue(strJson, Map.class);
    	
    	ApiTimesampRequest timeRequest = new ApiTimesampRequest();
		timeRequest.setApiUrl("/mgmts/rcv_orgs");
		timeRequest.setReqTimestamp((String)data.get("search_timestamp"));
		settingService.updateSearchTime(timeRequest);

    	int orgCnt = Integer.parseInt((String)data.get("org_cnt"));
    	
    	if(orgCnt > 0) {
    		List<Map<String, Object>> orgData = (List<Map<String, Object>>)data.get("org_list");
    		
    		for(Map<String, Object> info : orgData) {
    			String opType = (String)info.get("op_type");
    			
    			ServiceAppRequest.ServiceAppRegistRequest requestApp = new ServiceAppRequest.ServiceAppRegistRequest();
		    	
    	        requestApp.setUserKey((String)info.get("org_code"));
    	        requestApp.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
    	        requestApp.setAppStatCd("OK");
    	        requestApp.setAppClientId((String)info.get("client_id"));
    	        requestApp.setAppScr((String)info.get("client_secret"));
    	        
    			if(opType != null && !"".equals(opType) && "I".equals(opType)) {
    				UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
        			
        	    	request.setUserKey((String)info.get("org_code"));
        	    	request.setUseorgId((String)info.get("org_code"));
        	    	request.setUseorgNm((String)info.get("org_code"));
        	    	request.setUseorgGb("99");
        	    	
        	    	request.setRegUser(currentUser.getUserKey());
        	    	request.setModUser(currentUser.getUserKey());
        	    	
        	        settingService.insertUseOrg(request, false);
        	        
        	        requestApp.setAppKey(UUIDUtils.generateUUID());
    				appsService.serviceAppReg(requestApp, false);
    				
    				gWCommunicater.oauthRegService(requestApp.getAppClientId(), requestApp.getAppScr(), requestApp.getUserKey(), "", "", "");
    			}else if("M".equals(opType)) {
    				UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
    				
    				request.setUserKey((String)info.get("org_code"));
        	    	request.setUseorgId((String)info.get("org_code"));
        	    	request.setUseorgNm((String)info.get("org_code"));
        	    	request.setUseorgGb("99");
        	    	
        	    	request.setModUser(currentUser.getUserKey());
        	    	request.setRegUser(currentUser.getUserKey());
        	    	
        	    	settingService.useOrgUpd(request, false);
        	    	
        	        String appKey = appsService.selectAppKey(requestApp);
    				
    				if(appKey != null && !"".equals(appKey)) {
    					requestApp.setAppKey(appKey);
	    				appsService.serviceAppUpd(requestApp, false);
    				}
    				
    				gWCommunicater.oauthRegService(requestApp.getAppClientId(), requestApp.getAppScr(), requestApp.getUserKey(), "", "", "");
    			}else {
    				UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
    				request.setUserKey((String)info.get("org_code"));
    				request.setUseorgId((String)info.get("org_code"));
    				request.setUseorgStatCd("DEL");
    				request.setModUser(currentUser.getUserId());
    				
    		        settingService.useOrgStatusUpd(request, false);
    		        String appKey = appsService.selectAppKey(requestApp);
    				
    				if(appKey != null && !"".equals(appKey)) {
    					requestApp.setAppKey(appKey);
	    				ServiceAppRequest requestDel = new ServiceAppRequest();
	    				requestDel.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
	    				requestDel.setAppStatCd("DEL");
	    				appsService.serviceAppStatusUpd(requestDel, false);
    				}
    			}
    		}
    	}
	}
	
	public void appReg(String strJson, UserPrincipal currentUser) throws Exception{
		ObjectMapper om = new ObjectMapper();
    	
    	Map<String, Object> data = om.readValue(strJson, Map.class);
    	
    	ApiTimesampRequest timeRequest = new ApiTimesampRequest();
		timeRequest.setApiUrl("/mgmts/service");
		timeRequest.setReqTimestamp((String)data.get("search_timestamp"));
		settingService.updateSearchTime(timeRequest);

		int orgCnt = Integer.parseInt((String)data.get("org_cnt"));
    	
    	if(orgCnt > 0) {
    		List<Map<String, Object>> orgData = (List<Map<String, Object>>)data.get("org_list");
    		
    		for(Map<String, Object> orgInfo : orgData) {
				int serviceCnt = Integer.parseInt((String)orgInfo.get("service_cnt"));
    			
    			if(serviceCnt > 0) {
	    			List<Map<String, Object>> serviceData = (List<Map<String, Object>>)orgInfo.get("service_list");
	    			
	    			for(Map<String, Object> serviceInfo : serviceData) {
	    				
	    				List<String> callbacks = new ArrayList<String>();
	    				String sCallback = "";
	    				List<String> schemes = new ArrayList<>();
	    				String sSchemes = "";
	    				
		    			ServiceAppRequest.ServiceAppRegistRequest request = new ServiceAppRequest.ServiceAppRegistRequest();
				    	
				    	request.setUserKey((String)orgInfo.get("org_code"));
				        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
				        request.setAppStatCd("OK");
				        request.setAppClientId((String)serviceInfo.get("client_id"));
				        request.setAppNm((String)serviceInfo.get("service_name"));
				        
				        if(serviceInfo.containsKey("client_secret")) {
				        	request.setAppScr((String)serviceInfo.get("client_secret"));
				        }
				        
				        if(serviceInfo.containsKey("redirect_uri_cnt")) {
							int redirectUriCnt = Integer.parseInt((String)serviceInfo.get("redirect_uri_cnt"));
				        	
				        	if(redirectUriCnt > 0) {
				        		List<Map<String, String>> redirectUris = new ArrayList<Map<String, String>>();
	        	    			
	        	    			List<Map<String, Object>> redirectUriData = (List<Map<String, Object>>)serviceInfo.get("redirect_uri_list");
	        	    			
	        	    			for(Map<String, Object> ruInfo : redirectUriData) {
	        	    				Map<String, String> ruMap = new HashMap<String, String>();
	        	    				ruMap.put("redirectUri", (String)ruInfo.get("redirect_uri"));
	        	    				callbacks.add((String)ruInfo.get("redirect_uri"));
	        	    				redirectUris.add(ruMap);
	        	    				
	        	    			}
	        	    			sCallback = String.join(",", callbacks);
	        	    			request.setClientIPs(redirectUris);
				        	} else {
				        		sCallback = "";
								request.setCallbackList(new ArrayList<Map<String,String>>());
							}
				        }else {
				        	sCallback = "";
				        	request.setCallbackList(new ArrayList<Map<String,String>>());
				        }
				        
				        if(serviceInfo.containsKey("app_scheme_cnt")) {
							int appSchemeCnt = Integer.parseInt((String)serviceInfo.get("app_scheme_cnt"));
							
				        	if(appSchemeCnt > 0) {
				        		List<Map<String, String>> appSchemes = new ArrayList<Map<String, String>>();
	        	    			
	        	    			List<Map<String, Object>> appSchemeData = (List<Map<String, Object>>)serviceInfo.get("app_scheme_list");
	        	    			
	        	    			for(Map<String, Object> asInfo : appSchemeData) {
	        	    				Map<String, String> asMap = new HashMap<String, String>();
	        	    				asInfo.put("appScheme", (String)asInfo.get("app_scheme"));
	        	    				schemes.add((String)asMap.get("app_scheme"));
	        	    				appSchemes.add(asMap);
	        	    			}
	        	    			sSchemes = String.join(",", schemes);
	        	    			request.setSchemeList(appSchemes);
				        	} else {
								request.setSchemeList(new ArrayList<Map<String,String>>());
							}
				        }else {
				        	request.setSchemeList(new ArrayList<Map<String,String>>());
				        }
				        
				        String opType = (String)serviceInfo.get("op_type");
		    			if(opType != null && !"".equals(opType) && "I".equals(opType)) {
		    				request.setAppKey(UUIDUtils.generateUUID());
		    				appsService.serviceAppReg(request, false);
		    				gWCommunicater.oauthRegService(request.getAppClientId(), request.getAppScr(), request.getUserKey(), sCallback, sSchemes, appsService.selUseorgGb(request));
		    			} else if("M".equals(opType)) {
		    				String appKey = appsService.selectAppKey(request);
		    				
		    				if(appKey != null && !"".equals(appKey)) {
		    					request.setAppKey(appKey);
			    				appsService.serviceAppUpd(request, false);
		    				}
		    				gWCommunicater.oauthRegService(request.getAppClientId(), request.getAppScr(), request.getUserKey(), sCallback, sSchemes, appsService.selUseorgGb(request));
		    			} else {
		    				String appKey = appsService.selectAppKey(request);
		    				
		    				if(appKey != null && !"".equals(appKey)) {
			    				request.setAppKey(appKey);
			    				ServiceAppRequest requestDel = new ServiceAppRequest();
			    				requestDel.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
			    				requestDel.setAppStatCd("DEL");
			    				appsService.serviceAppStatusUpd(requestDel, false);
		    				}
		    			}
				        
	    			}
    			}
    		}
    		
    	}
	}
	
	public Map<String, Object> statistics(String strJson) throws Exception {
		ObjectMapper om = new ObjectMapper();
		
		Map<String, Object> data = om.readValue(strJson, Map.class);
		
		StatisticsRequest.statisticsDetailRequest request = new StatisticsRequest.statisticsDetailRequest(); 
		
		request.setType((String)data.get("type"));
		request.setStatDate((String)data.get("stat_date"));
		
		Map<String, Object> main = statRepository.selectMainStatInfo(request);
		
		if(main != null && main.isEmpty()) {
			main.put("type", request.getType());
			
			List<Map<String, Object>> list2 = statRepository.selectSub2StatInfo(request);
			
			if(list2 != null && list2.size() > 0) {
				main.put("mydata_svc_list", list2);
				
				for(Map<String, Object> vo2 : list2) {
					request.setOrgCode((String)vo2.get("org_code"));
					request.setClientId((String)vo2.get("client_id"));
					List<Map<String, Object>> list3 = statRepository.selectSub3StatInfo(request);
					
					if(list3 != null && list3.size() > 0) {
						vo2.put("api_type_list", list3);
						
						for(Map<String, Object> vo3 : list3) {
							request.setApiType((String)vo3.get("api_type"));
							List<Map<String, Object>> list4 = statRepository.selectSub4StatInfo(request);
							
							vo3.put("tm_slot_list", list4);
						}
					}
				}
			}
		}else {
			main = new HashMap<String, Object>();
			main.put("type", request.getType());
			main.put("stat_date", request.getStatDate());
			main.put("mydata_svc_cnt", "0");
		}
		
		return main;
		
	}
	
	public Map<String,Object> apis(String strJson) throws Exception {
		
		Map<String, Object> vo = settingRepository.selectVerInfo();
		List<Map<String, Object>> voSub = settingRepository.selectApiList();
		
		vo.put("api_list", voSub);
		
		return vo;
		
	}
}