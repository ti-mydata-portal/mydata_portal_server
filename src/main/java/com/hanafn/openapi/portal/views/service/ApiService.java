package com.hanafn.openapi.portal.views.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiAllListRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiColumnListRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.CtgrApiAllListRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiTagVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.cmct.SwaggerCommunicater;
import com.hanafn.openapi.portal.cmct.dto.SwaggerResponse;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.InnerTransUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.ApiColumnRequest;
import com.hanafn.openapi.portal.views.dto.ApiDetailRsponse;
import com.hanafn.openapi.portal.views.dto.ApiRegistRequest;
import com.hanafn.openapi.portal.views.dto.ApiResponse;
import com.hanafn.openapi.portal.views.dto.ApiRsponsePaging;
import com.hanafn.openapi.portal.views.dto.ApiServiceRequest;
import com.hanafn.openapi.portal.views.dto.ApiServiceResponse;
import com.hanafn.openapi.portal.views.dto.ApiStatModRequest;
import com.hanafn.openapi.portal.views.dto.ApiTagRequest;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.dto.StatsRsponse;
import com.hanafn.openapi.portal.views.repository.ApiRepository;
import com.hanafn.openapi.portal.views.vo.ApiColumnListVO;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;
import com.hanafn.openapi.portal.views.vo.ApiServiceVO;
import com.hanafn.openapi.portal.views.vo.ApiStatModHisVO;
import com.hanafn.openapi.portal.views.vo.StatsRealTimeVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class ApiService {
	private final String apiStatCdDel="DEL";
	private final String apiStatCdClose="CLOSE";
	private final String apiStatCdOk="OK";
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiService.class);

	@Autowired
	ApiRepository apiRepository;

	@Autowired
	MessageSourceAccessor messageSource;
	
	@Autowired
	@Qualifier("Crypto")
	ISecurity cryptoUtil;
	
	@Value("${bt.tran.api.url}")
	private String btTranApiUrl;
    
    @Autowired
    public InnerTransUtil innerTransUtil;

	public ApiDetailRsponse selectApi(AdminRequest.ApiRequest apiRequest) throws Exception{
		ApiVO apiInfo = apiRepository.selectApi(apiRequest);

		ApiColumnRequest apiColumnRequest = new ApiColumnRequest();
		apiColumnRequest.setApiId(apiRequest.getApiId());

		apiColumnRequest.setClmReqDiv("REQUEST");
		List<ApiColumnVO> apiColumnRequestList = apiRepository.selectApiColumnList(apiColumnRequest);
		settingApiColumn(apiColumnRequestList);	//컬럼 타입이 LIST일 경우 하위 리스트 셋팅

		apiColumnRequest.setClmReqDiv("RESPONSE");
		List<ApiColumnVO> apiColumnResponseList = apiRepository.selectApiColumnList(apiColumnRequest);
		settingApiColumn(apiColumnResponseList); //컬럼 타입이 LIST일 경우 하위 리스트 셋팅

		List<ApiStatModHisVO> apiStatModHisList = apiRepository.selectApiStatModHisList(apiRequest);
		
		for(ApiStatModHisVO vo : apiStatModHisList) {
			if(vo.getRegUser() != null && !"".equals(vo.getRegUser())) {
				vo.setRegUser(cryptoUtil.decrypt(vo.getRegUser()));
			}
		}

		ApiDetailRsponse apiDetailResponse = new ApiDetailRsponse();
		apiDetailResponse.setApiId(apiInfo.getApiId());
		apiDetailResponse.setApiNm(apiInfo.getApiNm());
		apiDetailResponse.setApiStatCd(apiInfo.getApiStatCd());
		apiDetailResponse.setApiSvc(apiInfo.getApiSvc());
		apiDetailResponse.setApiVer(apiInfo.getApiVer());
		apiDetailResponse.setApiUri(apiInfo.getApiUri());
		apiDetailResponse.setApiUrl(apiInfo.getApiUrl());
		apiDetailResponse.setGwType(apiInfo.getGwType());
		apiDetailResponse.setMsgType(apiInfo.getMsgType());
		apiDetailResponse.setEncodingType(apiInfo.getEncodingType());
		apiDetailResponse.setApiMthd(apiInfo.getApiMthd());
		apiDetailResponse.setUserKey(apiInfo.getUserKey());
		apiDetailResponse.setApiTosUrl(apiInfo.getApiTosUrl());
		apiDetailResponse.setHfnCd(apiInfo.getHfnCd());
		apiDetailResponse.setHfnSvcCd(apiInfo.getHfnSvcCd());
		apiDetailResponse.setApiPubYn(apiInfo.getApiPubYn());
		apiDetailResponse.setApiCtnt(apiInfo.getApiCtnt());
		apiDetailResponse.setApiHtml(apiInfo.getApiHtml());
		apiDetailResponse.setApiProcType(apiInfo.getApiProcType());
		apiDetailResponse.setApiProcUrl(apiInfo.getApiProcUrl());
		apiDetailResponse.setCtgrCd(apiInfo.getCtgrCd());
		apiDetailResponse.setCtgrNm(apiInfo.getCtgrNm());
		apiDetailResponse.setSubCtgrCd(apiInfo.getSubCtgrCd());
		apiDetailResponse.setApiAuthType(apiInfo.getApiAuthType());
		apiDetailResponse.setApiContentProvider(apiInfo.getApiContentProvider());
		apiDetailResponse.setCnsntIngrpNm(apiInfo.getCnsntIngrpNm());
		apiDetailResponse.setCnsntIngrpCd(apiInfo.getCnsntIngrpCd());
		apiDetailResponse.setCnsntIntypCd(apiInfo.getCnsntIntypCd());
		apiDetailResponse.setBaseRateYn(apiInfo.getBaseRateYn());
		apiDetailResponse.setApiRscdYn(apiInfo.getApiRscdYn());
		apiDetailResponse.setApiRsKey(apiInfo.getApiRsKey());
		apiDetailResponse.setApiRsValue(apiInfo.getApiRsValue());
		apiDetailResponse.setDlyTermDiv(apiInfo.getDlyTermDiv());
		apiDetailResponse.setDlyTermDt(apiInfo.getDlyTermDt());
		apiDetailResponse.setDlyTermTm(apiInfo.getDlyTermTm());
		apiDetailResponse.setApiRequestList(apiColumnRequestList);
		apiDetailResponse.setApiResponseList(apiColumnResponseList);
		apiDetailResponse.setApiStatModHisList(apiStatModHisList);
		apiDetailResponse.setParentId(apiInfo.getParentId());
		apiDetailResponse.setHttpPrtcl(apiInfo.getHttpPrtcl());
		apiDetailResponse.setHttpMethod(apiInfo.getHttpMethod());
		apiDetailResponse.setHttpUrl(apiInfo.getHttpUrl());
		apiDetailResponse.setHttpCtOut(apiInfo.getHttpCtout());
		apiDetailResponse.setHttpRtOut(apiInfo.getHttpRtOut());
		apiDetailResponse.setTcpIp(apiInfo.getTcpIp());
		apiDetailResponse.setTcpType(apiInfo.getTcpType());
		apiDetailResponse.setTcpPort(apiInfo.getTcpPort());
		apiDetailResponse.setTcpSvc(apiInfo.getTcpSvc());
		apiDetailResponse.setTcpCtOut(apiInfo.getTcpCtOut());
		apiDetailResponse.setTcpRtOut(apiInfo.getTcpRtOut());
		apiDetailResponse.setTcpEnc(apiInfo.getTcpEnc());
		apiDetailResponse.setRegDttm(apiInfo.getRegDttm());
		apiDetailResponse.setRegUser(apiInfo.getRegUser());
		apiDetailResponse.setProcDttm(apiInfo.getProcDttm());
		apiDetailResponse.setProcUser(apiInfo.getProcUser());
		apiDetailResponse.setScope(apiInfo.getScope());
		apiDetailResponse.setApiType(apiInfo.getApiType());
		apiDetailResponse.setApiIndustry(apiInfo.getApiIndustry());
		apiDetailResponse.setApiBtId(apiInfo.getApiBtId());
		apiDetailResponse.setApiTransCycle(apiInfo.getApiTransCycle());
		apiDetailResponse.setContentType(apiInfo.getContentType());
		apiDetailResponse.setApiVerYn(apiInfo.getApiVerYn());
		apiDetailResponse.setCheckSchema(apiInfo.getCheckSchema());

		return apiDetailResponse;
	}
	
	public void settingApiColumnRequest(List<ApiColumnRequest> columnList) {
		for(ApiColumnRequest apiColumn : columnList){

			if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				AdminRequest.ApiColumnListRequest apiColumnListRequest = new AdminRequest.ApiColumnListRequest();
				apiColumnListRequest.setApiId(apiColumn.getApiId());
				apiColumnListRequest.setClmCd(apiColumn.getClmCd());
				apiColumnListRequest.setClmReqDiv(apiColumn.getClmReqDiv());

				List<ApiColumnListRequest> apiColumnList = apiRepository.selectApiColumnDetailRequestList(apiColumnListRequest);
				apiColumn.setApiColumnList(apiColumnList);
				this.settingApiSubColumnRequest(apiColumnList);
			}
		}
	}
	
	private void settingApiSubColumnRequest(List<ApiColumnListRequest> apiColumnList) {
		for(ApiColumnListRequest apiColumn : apiColumnList){
			AdminRequest.ApiColumnListRequest apiColumnListRequest = new AdminRequest.ApiColumnListRequest();
			
			apiColumnListRequest.setApiId(apiColumn.getApiId());
			apiColumnListRequest.setClmCd(apiColumn.getSeqNo());
			apiColumnListRequest.setClmReqDiv(apiColumn.getClmReqDiv());
			List<ApiColumnListRequest> apiSubColumnList = apiRepository.selectApiColumnDetailRequestList(apiColumnListRequest);
			apiColumn.setApiColumnList(apiSubColumnList);
			
			if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				this.settingApiSubColumnRequest(apiSubColumnList);
			}
		}
	}

	public void settingApiColumn(List<ApiColumnVO> columnList) {
		for(ApiColumnVO apiColumn : columnList){

			if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				AdminRequest.ApiColumnListRequest apiColumnListRequest = new AdminRequest.ApiColumnListRequest();
				apiColumnListRequest.setApiId(apiColumn.getApiId());
				apiColumnListRequest.setClmCd(apiColumn.getClmCd());
				apiColumnListRequest.setClmReqDiv(apiColumn.getClmReqDiv());

				List<ApiColumnListVO> apiColumnList = apiRepository.selectApiColumnDetailList(apiColumnListRequest);
				apiColumn.setApiColumnList(apiColumnList);
				this.settingApiSubColumn(apiColumnList);
			}
		}
	}
	
	private void settingApiSubColumn(List<ApiColumnListVO> apiColumnList) {
		for(ApiColumnListVO apiColumn : apiColumnList){
			AdminRequest.ApiColumnListRequest apiColumnListRequest = new AdminRequest.ApiColumnListRequest();
			
			apiColumnListRequest.setApiId(apiColumn.getApiId());
			apiColumnListRequest.setClmCd(apiColumn.getSeqNo());
			apiColumnListRequest.setClmReqDiv(apiColumn.getClmReqDiv());
			List<ApiColumnListVO> apiSubColumnList = apiRepository.selectApiColumnDetailList(apiColumnListRequest);
			apiColumn.setApiColumnList(apiSubColumnList);
			
			if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				this.settingApiSubColumn(apiSubColumnList);
			}
		}
	}

	public ApiRsponsePaging selectApiListNoPaging(AdminRequest.ApiRequest apiRequest){
		List<ApiVO> list = apiRepository.selectApiListNoPaging(apiRequest);

		for(ApiVO apiInfo : list){
			apiRequest.setApiId(apiInfo.getApiId());

			List<ApiTagVO> apiTagList = apiRepository.selectApiTagList(apiRequest);

			apiInfo.setApiTagList(apiTagList);
		}

		ApiRsponsePaging pagingData = new ApiRsponsePaging();
		pagingData.setList(list);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	public ApiRsponsePaging selectApiListPaging(AdminRequest.ApiRequest apiRequest){
		if(apiRequest.getPageIdx() == 0)
			apiRequest.setPageIdx(apiRequest.getPageIdx() + 1);

		if(apiRequest.getPageSize() == 0){
			apiRequest.setPageSize(20);
		}

		apiRequest.setPageOffset((apiRequest.getPageIdx()-1)*apiRequest.getPageSize());
		
		int totCnt = apiRepository.countApiList(apiRequest);
		List<ApiVO> list = apiRepository.selectApiList(apiRequest);

		ApiRsponsePaging pagingData = new ApiRsponsePaging();
		pagingData.setPageIdx(apiRequest.getPageIdx());
		pagingData.setPageSize(apiRequest.getPageSize());
		pagingData.setList(list);
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	/*
		URL 주소기준 중복체크
	 */
	public void apiDupCheck(ApiRegistRequest apiRegistRequest){

		int apiDupCheck = apiRepository.apiDupCheck(apiRegistRequest);

		if(apiDupCheck > 0){
			log.error(messageSource.getMessage("E013"));
			throw new BusinessException("E013",messageSource.getMessage("E013"));
		}
	}

	public void insertApi(ApiRegistRequest apiRegistRequest) throws Exception{

		apiRepository.insertApi(apiRegistRequest);
		apiRepository.insertApiHis(apiRegistRequest);
		insertApiColumn(apiRegistRequest);
        insertMapper(apiRegistRequest);
        
        ApiStatModRequest apiStatModRequest = new ApiStatModRequest();
        apiStatModRequest.setApiId(apiRegistRequest.getApiId());
        apiStatModRequest.setRegUserId(apiRegistRequest.getRegUserId());
		apiStatModRequest.setApiModDiv("REG");

		apiRepository.insertApiStatModHis(apiStatModRequest);

		RedisCommunicater.apiRedisSet(apiRegistRequest.getApiUrl(), "true");
		
		ObjectMapper op = new ObjectMapper();
		String apiJsonStrin = op.writeValueAsString(apiRegistRequest);
		
		LinkRequest dataRequst = new LinkRequest();
        dataRequst.setApiId(apiRegistRequest.getApiId());
        dataRequst.setOpType("I");
        
        RedisCommunicater.apiRedisSet(apiRegistRequest.getApiUrl(), "true");
        
        int sendYn = apiRepository.selectApiBtSendYn(dataRequst);
        
        if(sendYn > 0) {
        	Map<String, Object> sendData = apiRepository.selectApiMap(dataRequst);
            
            sendData.put("lnk_srvr_dv_cd", "");
            sendData.put("itf_mthd_dv_cd", "");
            
            innerTransUtil.sendBt(btTranApiUrl, sendData);
            
        }
	}
	
	private static String[] getIp(String str){
	    Pattern p =
	       Pattern.compile("((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})");
	    Matcher m = p.matcher(str);
	 
	    StringBuffer sb = new StringBuffer();
	    while(m.find()){
	        sb.append(m.group()+ " ");
	    }
	     
	    return m.reset().find() ? sb.toString().split(" ") : new String[0];
	}
	
	public void drApiUpd(String drIp) throws Exception{
		AdminRequest.ApiRequest apiRequest = new AdminRequest.ApiRequest();
		List<ApiVO> apiInfo = apiRepository.selectAllApi(apiRequest);
		
		if(apiInfo != null && apiInfo.size() > 0) {
			for(ApiVO vo : apiInfo) {
				
				String httpUrl = vo.getHttpUrl();
				String[] ips = getIp(httpUrl);
				
				if(httpUrl == null || "".equals(httpUrl)) {
					continue;
				}
				
				if(ips == null || ips.length <=0) {
					continue;
				}
				
				vo.setHttpUrl(httpUrl.replace(ips[0], drIp));

				ApiRegistRequest request = new ApiRegistRequest();
				
				ApiColumnRequest apiColumnRequest = new ApiColumnRequest();
				apiColumnRequest.setApiId(vo.getApiId());

				apiColumnRequest.setClmReqDiv("REQUEST");
				List<ApiColumnRequest> apiColumnRequestList = apiRepository.selectApiColumnRequestList(apiColumnRequest);
				settingApiColumnRequest(apiColumnRequestList);	//컬럼 타입이 LIST일 경우 하위 리스트 셋팅
				apiColumnRequest.setClmReqDiv("RESPONSE");
				List<ApiColumnRequest> apiColumnResponseList = apiRepository.selectApiColumnRequestList(apiColumnRequest);
				settingApiColumnRequest(apiColumnResponseList); //컬럼 타입이 LIST일 경우 하위 리스트 셋팅
				
				request.setApiId(vo.getApiId());
				request.setApiNm(vo.getApiNm());
				request.setApiStatCd(vo.getApiStatCd());
				request.setApiSvc(vo.getApiSvc());
				request.setApiVer(vo.getApiVer());
				request.setApiUri(vo.getApiUri());
				request.setApiUrl(vo.getApiUrl());
				request.setGwType(vo.getGwType());
				request.setMsgType(vo.getMsgType());
				request.setEncodingType(vo.getEncodingType());
				request.setApiMthd(vo.getApiMthd());
				request.setUserKey(vo.getUserKey());
				request.setApiTosUrl(vo.getApiTosUrl());
				request.setHfnCd(vo.getHfnCd());
				request.setHfnSvcCd(vo.getHfnSvcCd());
				request.setApiPubYn(vo.getApiPubYn());
				request.setApiCtnt(vo.getApiCtnt());
				request.setApiHtml(vo.getApiHtml());
				request.setApiProcType(vo.getApiProcType());
				request.setApiProcUrl(vo.getApiProcUrl());
				request.setCtgrCd(vo.getCtgrCd());
				request.setSubCtgrCd(vo.getSubCtgrCd());
				request.setApiAuthType(vo.getApiAuthType());
				request.setApiContentProvider(vo.getApiContentProvider());
				request.setCnsntIngrpNm(vo.getCnsntIngrpNm());
				request.setCnsntIngrpCd(vo.getCnsntIngrpCd());
				request.setCnsntIntypCd(vo.getCnsntIntypCd());
				request.setBaseRateYn(vo.getBaseRateYn());
				request.setApiRscdYn(vo.getApiRscdYn());
				request.setApiRsKey(vo.getApiRsKey());
				request.setApiRsValue(vo.getApiRsValue());
				request.setDlyTermDiv(vo.getDlyTermDiv());
				request.setDlyTermDt(vo.getDlyTermDt());
				request.setDlyTermTm(vo.getDlyTermTm());
				request.setParentId(vo.getParentId());
				request.setHttpPrtcl(vo.getHttpPrtcl());
				request.setHttpMethod(vo.getHttpMethod());
				request.setHttpUrl(vo.getHttpUrl());
				request.setHttpCtOut(vo.getHttpCtout());
				request.setHttpRtOut(vo.getHttpRtOut());
				request.setTcpIp(vo.getTcpIp());
				request.setTcpType(vo.getTcpType());
				request.setTcpPort(vo.getTcpPort());
				request.setTcpSvc(vo.getTcpSvc());
				request.setTcpCtOut(vo.getTcpCtOut());
				request.setTcpRtOut(vo.getTcpRtOut());
				request.setTcpEnc(vo.getTcpEnc());
				request.setScope(vo.getScope());
				request.setApiType(vo.getApiType());
				request.setApiIndustry(vo.getApiIndustry());
				request.setApiBtId(vo.getApiBtId());
				request.setApiTransCycle(vo.getApiTransCycle());
				request.setContentType(vo.getContentType());
				request.setApiVerYn(vo.getApiVerYn());
				request.setCheckSchema(vo.getCheckSchema());
				request.setApiRequestList(apiColumnRequestList);
				request.setApiResponseList(apiColumnResponseList);
				
				apiRepository.updateHttpUrl(request);
				insertMapper(request);
			}
		}
	}

	public void updateApi(ApiRegistRequest apiRegistRequest) throws Exception{

		apiRepository.updateApi(apiRegistRequest);
		apiRepository.insertApiHis(apiRegistRequest);

		AdminRequest.ApiRequest apiRequest = new AdminRequest.ApiRequest();
		apiRequest.setApiId(apiRegistRequest.getApiId());
		apiRequest.setRegUserName(apiRegistRequest.getRegUserName());

		ApiStatModRequest apiStatModRequest = new ApiStatModRequest();
		apiStatModRequest.setApiId(apiRegistRequest.getApiId());
		apiStatModRequest.setApiModDiv("MOD");
		apiStatModRequest.setRegUserName(apiRegistRequest.getRegUserName());
		apiStatModRequest.setRegUserId(apiRegistRequest.getRegUserId());

		apiRepository.insertApiStatModHis(apiStatModRequest);
		
		RedisCommunicater.apiRedisSet(apiRegistRequest.getApiUrl(), "true");

		apiRepository.deleteApiColumn(apiRegistRequest);
		apiRepository.deleteApiColumnList(apiRegistRequest);

		insertApiColumn(apiRegistRequest);
        insertMapper(apiRegistRequest);
        
        ObjectMapper op = new ObjectMapper();
		String apiJsonStrin = op.writeValueAsString(apiRegistRequest);
		
		LinkRequest dataRequst = new LinkRequest();
        dataRequst.setApiId(apiRegistRequest.getApiId());
        dataRequst.setOpType("M");
        
        RedisCommunicater.apiInfoRedisSet(apiRegistRequest.getApiUrl(), apiJsonStrin);
        
        int sendYn = apiRepository.selectApiBtSendYn(dataRequst);
        
        if(sendYn > 0) {
        	 Map<String, Object> sendData = apiRepository.selectApiMap(dataRequst);
        	 
        	innerTransUtil.sendBt(btTranApiUrl, sendData);
        }
	}

	public void insertTag(ApiRegistRequest apiRegistRequest){
		for(ApiTagRequest apiTag : apiRegistRequest.getApiTagList()){
			apiTag.setApiId(apiRegistRequest.getApiId());
			apiTag.setRegUserName(apiRegistRequest.getRegUserName());
			apiTag.setRegUserId(apiRegistRequest.getRegUserId());
			apiRepository.insertApiTag(apiTag);
		}
	}

	public void insertApiColumn(ApiRegistRequest apiRegistRequest){

		for(ApiColumnRequest apiColumn : apiRegistRequest.getApiRequestList()){
			apiColumn.setRegUserId(apiRegistRequest.getRegUserId());
			apiColumn.setApiId(apiRegistRequest.getApiId());
			apiColumn.setClmReqDiv("REQUEST");
			apiRepository.insertApiColumn(apiColumn);

			if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				this.insertSubList(apiColumn.getApiColumnList(), apiColumn.getClmCd(), apiRegistRequest, "REQUEST");
			}
		}

		for(ApiColumnRequest apiColumn : apiRegistRequest.getApiResponseList()){
			apiColumn.setRegUserId(apiRegistRequest.getRegUserId());
			apiColumn.setApiId(apiRegistRequest.getApiId());
			apiColumn.setClmReqDiv("RESPONSE");
			apiRepository.insertApiColumn(apiColumn);

			if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				this.insertSubList(apiColumn.getApiColumnList(), apiColumn.getClmCd(), apiRegistRequest, "RESPONSE");
			}
		}
	}
	
	private void insertSubList(List<AdminRequest.ApiColumnListRequest> list, String clmCd, ApiRegistRequest apiRegistRequest, String clmReqDiv) {
		for(AdminRequest.ApiColumnListRequest apiColumnListInfo : list){
			apiColumnListInfo.setRegUserId(apiRegistRequest.getRegUserId());
			apiColumnListInfo.setClmListCd(clmCd);
			apiColumnListInfo.setApiId(apiRegistRequest.getApiId());
			apiColumnListInfo.setClmReqDiv(clmReqDiv);
			apiRepository.insertApiColumnList(apiColumnListInfo);
			
			if(StringUtils.equals(apiColumnListInfo.getClmType(), "LIST")){
				this.insertSubList(apiColumnListInfo.getApiColumnList(), apiColumnListInfo.getSeqNo(), apiRegistRequest, clmReqDiv);
			}
		}
	}
	
	private String getSubSegment(AdminRequest.ApiColumnListRequest apiColumnListInfo) {
		StringBuffer result = new StringBuffer();
		
		if(StringUtils.equals(apiColumnListInfo.getClmType(), "LIST")){
			result.append("<segment id=\"")
			      .append(apiColumnListInfo.getClmCd())
			      .append("\"" )
			      .append(" type=\"array\"" )
			      .append(" key=\"")
			      .append(apiColumnListInfo.getClmCd())
			      .append("\">");
			
			for(AdminRequest.ApiColumnListRequest apiColumnListInfoSub : apiColumnListInfo.getApiColumnList()){
				result.append(this.getSubSegment(apiColumnListInfoSub));
			}
			
			result.append("</segment>");
		}else {
			if (apiColumnListInfo.getHideYn() == null)
				apiColumnListInfo.setHideYn("N");
			if(!"Y".equals(apiColumnListInfo.getHideYn())) {
				result.append("<segment id=\"")
				      .append(apiColumnListInfo.getClmCd())
				      .append("\"" )
				      .append(" type=\"")
				      .append(apiColumnListInfo.getClmType())
				      .append("\"" )
				      .append(" key=\"")
				      .append(apiColumnListInfo.getClmCd())
				      .append("\"" )
				      .append(" length=\"")
				      .append(apiColumnListInfo.getClmLength())
				      .append("\"");

	            if("Y".equals(apiColumnListInfo.getRequired())) result.append(" required=\"true\"");
	            if("Y".equals(apiColumnListInfo.getLogVisible())) result.append(" =\"true\"");
	            if("Y".equals(apiColumnListInfo.getSqlInjection())) result.append(" checkSqlInjection=\"true\"");
				if(!StringUtils.isEmpty(apiColumnListInfo.getClmValue())) result.append(" value=\"").append(apiColumnListInfo.getClmValue()).append("\"");
	            //result.append(" masking=\"").append(apiColumnListInfo.getMasking()).append("\"/>");
				result.append("/>");
			}
		}
		
		return result.toString();
	}

    public void insertMapper(ApiRegistRequest apiRegistRequest){

        StringBuilder result = new StringBuilder();
        result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" )
              .append("<api>" )
              .append("<name>")
              .append(apiRegistRequest.getApiUrl())
              .append("</name>")
              .append("<logLevel>info</logLevel>")
              .append("<checkSchema>");
              String checkSchema = apiRegistRequest.getCheckSchema();
              if(checkSchema != null && !"".equals(checkSchema) && "N".equals(checkSchema)) {
            	  result.append("false");
              }else {
            	  result.append("true");
              }
              result.append("</checkSchema>");
        if("HTTP".equals(apiRegistRequest.getApiProcType())) {
            result.append("<connection>" ).append(
                    "<protocol>").append(apiRegistRequest.getHttpPrtcl()).append("</protocol>" ).append(
                    "<method>").append(apiRegistRequest.getHttpMethod()).append("</method>" ).append(
                    "<url>").append(apiRegistRequest.getHttpUrl()).append("</url>" ).append(
                    "<headers><header><key>Content-Type</key><value>application/json</value></header>" ).append(
                    "<header><key>Accept</key><value>application/json</value></header></headers>" ).append(
                    "<connectTimeout>").append(apiRegistRequest.getHttpCtOut()).append("</connectTimeout>" ).append(
                    "<responseTimeout>").append(apiRegistRequest.getHttpRtOut()).append("</responseTimeout>" ).append(
                    "</connection>");
        } else {
            result.append("<connection>" ).append(
                    "<protocol>").append(apiRegistRequest.getApiProcType()).append("</protocol>" ).append(
                    "<ip>").append(apiRegistRequest.getTcpIp()).append("</ip>" ).append(
                    "<port>").append(apiRegistRequest.getTcpPort()).append("</port>" ).append(
                    "<type>").append(apiRegistRequest.getTcpType()).append("</type>" ).append(
                    "<connectTimeout>").append(apiRegistRequest.getTcpCtOut()).append("</connectTimeout>" ).append(
                    "<responseTimeout>").append(apiRegistRequest.getTcpRtOut()).append("</responseTimeout>" ).append(
                    "</connection>");
        }

        result.append("<mapper>" ).append(
                "<request>" ).append(
                "<from type=\"application/json\" dataPath=\"/\">");

        for(ApiColumnRequest apiColumn : apiRegistRequest.getApiRequestList()){
            if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
                result.append("<segment id=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" type=\"array\"" )
                      .append(" key=\"")
                      .append(apiColumn.getClmCd())
                      .append("\">");

                for(AdminRequest.ApiColumnListRequest apiColumnListInfo : apiColumn.getApiColumnList()){
					result.append(this.getSubSegment(apiColumnListInfo));
                }
                result.append("</segment>");
            } else {
            	if (apiColumn.getHideYn() == null)
            		apiColumn.setHideYn("N");

				if("Y".equals(apiColumn.getHideYn()))
					continue; //비공개인경우 표시하지 않음

                result.append("<segment id=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" type=\"")
                      .append(apiColumn.getClmType())
                      .append("\"" )
                      .append(" key=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" length=\"")
                      .append(apiColumn.getClmLength())
                      .append("\"");

                if("Y".equals(apiColumn.getRequired())) result.append(" required=\"true\"");
                if("Y".equals(apiColumn.getLogVisible())) result.append(" logVisible=\"true\"");
                if("Y".equals(apiColumn.getSqlInjection())) result.append(" checkSqlInjection=\"true\"");
				if(!StringUtils.isEmpty(apiColumn.getClmValue())) result.append(" value=\"").append(apiColumn.getClmValue()).append("\"");

				//result.append(" masking=\"").append(apiColumn.getMasking()).append("\"/>");
				result.append("/>");
            }
        }

        result.append("</from>");

        if("pv".equals(apiRegistRequest.getMsgType())) result.append("<to type=\"parameter/value\" dataPath=\"/\" charset=\""+apiRegistRequest.getEncodingType()+"\">");
        if("json".equals(apiRegistRequest.getMsgType())) result.append("<to type=\"application/json\" dataPath=\"/\" charset=\""+apiRegistRequest.getEncodingType()+"\">");
        if("fixed".equals(apiRegistRequest.getMsgType())) result.append("<to type=\"text/plain\" dataPath=\"/\" charset=\""+apiRegistRequest.getEncodingType()+"\">");


        for(ApiColumnRequest apiColumn : apiRegistRequest.getApiRequestList()){
            if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
                result.append("<segment id=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" type=\"array\"" )
                      .append(" key=\"")
                      .append(apiColumn.getClmCd())
                      .append("\">");
                for(AdminRequest.ApiColumnListRequest apiColumnListInfo : apiColumn.getApiColumnList()){
                    result.append(this.getSubSegment(apiColumnListInfo));
                }
                result.append("</segment>");
            } else {
                result.append("<segment id=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" type=\"")
                      .append(apiColumn.getClmType())
                      .append("\"" )
                      .append(" key=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" length=\"")
                      .append(apiColumn.getClmLength())
                      .append("\"");

                if("Y".equals(apiColumn.getRequired())) result.append(" required=\"true\"");
                if("Y".equals(apiColumn.getLogVisible())) result.append(" logVisible=\"true\"");
                if("Y".equals(apiColumn.getSqlInjection())) result.append(" checkSqlInjection=\"true\"");
				if(!StringUtils.isEmpty(apiColumn.getClmValue())) result.append(" value=\"").append(apiColumn.getClmValue()).append("\"");
				//result.append(" masking=\"").append(apiColumn.getMasking()).append("\"/>");
				result.append("/>");
            }
        }

        result.append("</to>" ).append(
                "</request>" ).append(
                "<response>");

        if("fixed".equals(apiRegistRequest.getMsgType())) {
            result.append("<from type=\"text/plain\" charset=\""+apiRegistRequest.getEncodingType()+"\" dataPath=\"/\">");
        } else {
            result.append("<from type=\"application/json\" charset=\""+apiRegistRequest.getEncodingType()+"\" dataPath=\"/\">");
        }

        for(ApiColumnRequest apiColumn : apiRegistRequest.getApiResponseList()){
            if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
                result.append("<segment id=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" type=\"array\"" )
                      .append(" key=\"")
                      .append(apiColumn.getClmCd())
                      .append("\">");
                
                for(AdminRequest.ApiColumnListRequest apiColumnListInfo : apiColumn.getApiColumnList()){
                    result.append(this.getSubSegment(apiColumnListInfo));
                }
                result.append("</segment>");
            } else {
                result.append("<segment id=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" type=\"")
                      .append(apiColumn.getClmType())
                      .append("\"" )
                      .append(" key=\"")
                      .append(apiColumn.getClmCd())
                      .append("\"" )
                      .append(" length=\"")
                      .append(apiColumn.getClmLength())
                      .append("\"");

                if("Y".equals(apiColumn.getRequired())) result.append(" required=\"true\"");
                if("Y".equals(apiColumn.getLogVisible())) result.append(" logVisible=\"true\"");
                if("Y".equals(apiColumn.getSqlInjection())) result.append(" checkSqlInjection=\"true\"");
                //result.append(" masking=\"").append(apiColumn.getMasking()).append("\"/>");
                result.append("/>");
            }
        }

        result.append("</from>" ).append(
                "<to type=\"application/json\" dataPath=\"/\">");
//		result.append("<segment id=\"RSP_CD\" type=\"string\" key=\"rsp_cd\" />");
        for(ApiColumnRequest apiColumn : apiRegistRequest.getApiResponseList()){
            if(StringUtils.equals(apiColumn.getClmType(), "LIST")){
				if (apiColumn.getHideYn() == null)
					apiColumn.setHideYn("N");

				if("Y".equals(apiColumn.getHideYn()))
					continue; //비공개인경우 표시하지 않음

                result.append("<segment id=\"").append(apiColumn.getClmCd()).append("\"" ).append(
                        " type=\"array\"" ).append(
                        " key=\"").append(apiColumn.getClmCd()).append("\">");
                for(AdminRequest.ApiColumnListRequest apiColumnListInfo : apiColumn.getApiColumnList()){
                    result.append(this.getSubSegment(apiColumnListInfo));
                }
                result.append("</segment>");
            } else {
				if (apiColumn.getHideYn() == null)
					apiColumn.setHideYn("N");

				if("Y".equals(apiColumn.getHideYn()))
					continue; //비공개인경우 표시하지 않음

                result.append("<segment id=\"").append(apiColumn.getClmCd()).append("\"" ).append(
                        " type=\"").append(apiColumn.getClmType()).append("\"" ).append(
                        " key=\"").append(apiColumn.getClmCd()).append("\"" ).append(
                        " length=\"").append(apiColumn.getClmLength()).append("\"");

                if("Y".equals(apiColumn.getRequired())) result.append(" required=\"true\"");
                if("Y".equals(apiColumn.getLogVisible())) result.append(" logVisible=\"true\"");
                if("Y".equals(apiColumn.getSqlInjection())) result.append(" checkSqlInjection=\"true\"");
                //result.append(" masking=\"").append(apiColumn.getMasking()).append("\"/>");
                result.append("/>");
            }
        }
        result.append("</to></response></mapper></api>");

        apiRegistRequest.setMapper(result.toString());
        apiRepository.insertMapper(apiRegistRequest);

        RedisCommunicater.mapperRedisSet("0100", apiRegistRequest.getApiUrl(), result.toString());
    }

	public void apiStatCdChange(ApiStatModRequest apiStatModRequest){

		if(apiStatModRequest.getDlyTermDt() != null){
			apiStatModRequest.setDlyTermDt(StringUtils.replace(apiStatModRequest.getDlyTermDt(), "-", ""));
			apiStatModRequest.setDlyTermTm(StringUtils.replace(apiStatModRequest.getDlyTermTm(), ":", "")+ "00");
		}

		AdminRequest.ApiRequest apiRequest = new AdminRequest.ApiRequest();
		apiRequest.setApiId(apiStatModRequest.getApiId());
		apiRequest.setRegUserId(apiStatModRequest.getRegUserId());
		apiRequest.setRegUserName(apiStatModRequest.getRegUserName());
		apiStatModRequest.setApiModDiv("STATSCHG");

		apiRepository.insertApiStatModHis(apiStatModRequest);

		ApiVO apiInfo = apiRepository.selectApiDetalInfo(apiRequest);

		if(StringUtils.equals(apiInfo.getApiStatCd(), "OK")) {
			//API 만료 날짜 업데이트
			apiRepository.updateApiDlyTerm(apiStatModRequest);

			if (StringUtils.equals(apiStatModRequest.getDlyTermDiv(), "IM")) {

				apiRequest.setApiStatCd("CLOSE");
				apiRepository.apiStatCdChange(apiRequest);

				RedisCommunicater.apiRedisSet(apiInfo.getApiUrl(), "false");
			}
		} else if (StringUtils.equals(apiInfo.getApiStatCd(), "CLOSE")){
			//API 만료 날짜 초기화
			apiStatModRequest.setDlyTermDiv(null);
			apiStatModRequest.setDlyTermDt(null);
			apiStatModRequest.setDlyTermTm(null);
			apiRepository.updateApiDlyTerm(apiStatModRequest);

			apiRequest.setApiStatCd("OK");
			apiRepository.apiStatCdChange(apiRequest);

			RedisCommunicater.apiRedisSet(apiInfo.getApiUrl(), "true");
		} else {
			log.error("잘못된 API STATCD 입니다 : " + apiInfo.getApiStatCd());
		}
	}

	public void apiDelete(ApiStatModRequest apiStatModRequest){

		// ModTime : Delete Time
		if(apiStatModRequest.getDlyTermDt() != null){
			apiStatModRequest.setDlyTermDt(StringUtils.replace(apiStatModRequest.getDlyTermDt(), "-", ""));
			apiStatModRequest.setDlyTermTm(StringUtils.replace(apiStatModRequest.getDlyTermTm(), ":", "")+ "00");
		}

		// 삭제 요청 객체 세팅
		ApiRequest apiRequest = new ApiRequest();
		apiRequest.setApiId(apiStatModRequest.getApiId());
		apiRequest.setRegUserName(apiStatModRequest.getRegUserName());
		apiRequest.setRegUserId(apiStatModRequest.getRegUserId());
		apiRequest.setApiStatCd(apiStatModRequest.getApiModDiv());

		// 삭제할 api 정보 get
		ApiVO apiInfo = apiRepository.selectApiDetalInfo(apiRequest);

		// 삭제검증 3. [ 해당 API의 상태가 현재 중지중이어야 한다. ]
		if(!StringUtils.equals(apiInfo.getApiStatCd(), apiStatCdClose)) {
			log.error(messageSource.getMessage("E050"));
			throw new BusinessException("E050", messageSource.getMessage("E050"));
		}

		// 삭제로직
		apiRepository.deleteApi(apiRequest);
		
		LinkRequest dataRequst = new LinkRequest();
        dataRequst.setApiId(apiStatModRequest.getApiId());
        dataRequst.setOpType("D");
        
        int sendYn = apiRepository.selectApiBtSendYn(dataRequst);
        
        if(sendYn > 0) {
	        Map<String, Object> sendData = apiRepository.selectApiMap(dataRequst);
	        
	        innerTransUtil.sendBt(btTranApiUrl, sendData);
        }
        
		// 로그남기기
		apiStatModRequest.setApiModDiv(apiStatCdDel);
		apiRepository.insertApiStatModHis(apiStatModRequest);

		RedisCommunicater.apiRedisDel(apiInfo.getApiUrl());
		RedisCommunicater.mapperRedisDel("0100", apiInfo.getApiUrl());
		RedisCommunicater.apiInfoRedisDel(apiInfo.getApiUrl());
	}

	public ApiResponse selectApiAllList(ApiAllListRequest apiAllListRequest){

		List<ApiVO> list = apiRepository.selectApiAllList(apiAllListRequest);

		ApiResponse data = new ApiResponse();
		data.setList(list);

		return data;
	}

	public ApiResponse searchCtgrApiAll(CtgrApiAllListRequest apiAllListRequest){

		List<ApiVO> list = apiRepository.searchCtgrApiAll(apiAllListRequest);

		ApiResponse data = new ApiResponse();
		data.setList(list);

		return data;
	}

	public ApiResponse swaggerList(AdminRequest.SwaggerRequest swaggerRequest) throws BusinessException {

		ApiResponse apiResponse = new ApiResponse();
		List<ApiVO> apiList = new ArrayList<>();

		SwaggerResponse swaggerResponse = SwaggerCommunicater.swaggerInfo(swaggerRequest.getApiSvc());

		Map<String, Object> paths = swaggerResponse.getPaths();
		Set pathsSet = paths.keySet();

		Iterator pathsIterator = pathsSet.iterator();
		while(pathsIterator.hasNext()){
			String key = (String)pathsIterator.next();
			//API 중복 체크

			String[] urlList = StringUtils.split(key, "/");
			if(urlList.length >= 2){

				String apiVer = urlList[0];
				String apiUrl = "";

				ApiRegistRequest apiRegistRequest = new ApiRegistRequest();
				apiRegistRequest.setApiSvc(swaggerRequest.getApiSvc());
				apiRegistRequest.setApiVer(apiVer);

				for(int i=1; i < urlList.length; i++){
					if(i == 1){
						apiUrl = urlList[i];
					}else{
						apiUrl += "/" + urlList[i];
					}
				}

				apiRegistRequest.setApiUri(apiUrl);

				int apiDupCheck = apiRepository.apiDupCheck(apiRegistRequest);
				Map<String, Object> uri = (Map<String, Object>)paths.get(key);

				Set uriSet = uri.keySet();
				Iterator uriIterator = uriSet.iterator();

				while (uriIterator.hasNext()) {
					String uriKey = (String) uriIterator.next();
					Map<String, Object> method = (Map<String, Object>)uri.get(uriKey);
					if (apiDupCheck == 0) {
						ApiVO apiInfo = new ApiVO();
						apiInfo.setApiNm((String)method.get("summary"));
						apiInfo.setApiSvc(swaggerRequest.getApiSvc());
						apiInfo.setApiVer(apiVer);
						apiInfo.setApiUri(apiUrl);
						apiInfo.setApiMthd(StringUtils.upperCase(uriKey));
						apiList.add(apiInfo);
					}
				}
			}
		}
		apiResponse.setList(apiList);
		return apiResponse;
	}

	public String colnumRequiredCheck(List<String> requiredList, String key){

		String requiredCheck = "N";

		for(String requiredInfo : requiredList){
			if(StringUtils.equals(requiredInfo, key)){
				requiredCheck = "Y";
				break;
			}
		}
		return requiredCheck;
	}

	public String colnumTypeCheck(String type){

		if (StringUtils.equals(type, "string")) {
			return "string";
		} else if (StringUtils.equals(type, "integer") || StringUtils.equals(type, "number")) {
			return "NUMBER";
		} else if (StringUtils.equals(type, "boolean")) {
			return "boolean";
		} else if (StringUtils.equals(type, "array")) {
			return "LIST";
		}else{
			return null;
		}
	}
	
	public List<ApiVO> selectApiCodeList(AdminRequest.apiCodeListRequest request) {
		return apiRepository.selectApiCodeList(request);
	}
	
	public StatsRsponse.RealTimeStatsResponse selectRealTimeList(AdminRequest.apiRealTimeRequest request) throws Exception {
		
		int totCnt = apiRepository.countRealTimeList(request);
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
		List<StatsRealTimeVO> realTimeList = apiRepository.selectRealTimeList(request);
		
		for(StatsRealTimeVO vo : realTimeList) {
			String userNm = vo.getUserNm();
			
			if (userNm != null && !"".equals(userNm)) {
				String decryptedUserNm = cryptoUtil.decrypt(userNm);
				vo.setUserNm(decryptedUserNm);
			}
		}
        data.setDayList(realTimeList);
        data.setTotCnt(totCnt);
        data.setSelCnt(realTimeList.size());
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
		
		return data;
	}
	
	private String name(String str) {
		String replaceString = str;

		String pattern = "";
		if (str.length() == 2) {
			pattern = "^(.)(.+)$";
		} else {
			pattern = "^(.)(.+)(.)$";
		}

		Matcher matcher = Pattern.compile(pattern).matcher(str);

		if (matcher.matches()) {
			replaceString = "";

			for (int i = 1; i <= matcher.groupCount(); i++) {
				String replaceTarget = matcher.group(i);
				if (i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');

					replaceString = replaceString + String.valueOf(c);
				} else {
					replaceString = replaceString + replaceTarget;
				}

			}
		}

		return replaceString;
	}
	
	public StatsRsponse.RealTimeStatsResponse selectRealTimeDetailList(AdminRequest.apiRealTimeRequest request) {
		
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		List<StatsRealTimeVO> realTimeList = apiRepository.selectRealTimeDetailList(request);
        data.setDayList(realTimeList);
		
		return data;
	}
	
	public StatsRsponse.RealTimeStatsResponse selectUserRealTimeList(AdminRequest.apiRealTimeRequest request) throws Exception {
		
		int totCnt = apiRepository.countUserRealTimeList(request);
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
		List<StatsRealTimeVO> realTimeList = apiRepository.selectUserRealTimeList(request);

        for(StatsRealTimeVO vo : realTimeList) {
            String userNm = vo.getUserNm();
            if (userNm != null && !"".equals(userNm)) {
                vo.setUserNm(cryptoUtil.decrypt(userNm));
            }
        }

		data.setDayList(realTimeList);
        data.setTotCnt(totCnt);
        data.setSelCnt(realTimeList.size());
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
		
		return data;
	}
	
	public StatsRsponse.RealTimeStatsResponse selectUserRealTimeDetailList(AdminRequest.apiRealTimeRequest request) {
		
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		List<StatsRealTimeVO> realTimeList = apiRepository.selectUserRealTimeDetailList(request);
        data.setDayList(realTimeList);
		
		return data;
	}
	
	public StatsRsponse.RealTimeStatsResponse selectPrdRealTimeList(AdminRequest.apiRealTimeRequest request) {
		
		int totCnt = apiRepository.countPrdRealTimeList(request);
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
		List<StatsRealTimeVO> realTimeList = apiRepository.selectPrdRealTimeList(request);
        data.setDayList(realTimeList);
        
        data.setTotCnt(totCnt);
        data.setSelCnt(realTimeList.size());
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
		
		return data;
	}
	
	public StatsRsponse.RealTimeStatsResponse selectPrdRealTimeDetailList(AdminRequest.apiRealTimeRequest request) {
		
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		List<StatsRealTimeVO> realTimeList = apiRepository.selectPrdRealTimeDetailList(request);
        data.setDayList(realTimeList);
		
		return data;
	}
	
    public StatsRsponse.RealTimeStatsResponse selectBizRealTimeList(AdminRequest.apiRealTimeRequest request) throws Exception{
		
		int totCnt = apiRepository.countBizRealTimeList(request);
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
		List<StatsRealTimeVO> realTimeList = apiRepository.selectBizRealTimeList(request);
		
		for(StatsRealTimeVO vo : realTimeList) {
			String userNm = vo.getUserNm();
			
			if (userNm != null && !"".equals(userNm)) {
				String decryptedUserNm = cryptoUtil.decrypt(userNm);
				vo.setUserNm(this.name(decryptedUserNm));
			}
		}
		
        data.setDayList(realTimeList);
        
        data.setTotCnt(totCnt);
        data.setSelCnt(realTimeList.size());
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
		
		return data;
	}
	
	public StatsRsponse.RealTimeStatsResponse selectBizRealTimeDetailList(AdminRequest.apiRealTimeRequest request) {
		
		StatsRsponse.RealTimeStatsResponse data = new StatsRsponse.RealTimeStatsResponse();
		List<StatsRealTimeVO> realTimeList = apiRepository.selectBizRealTimeDetailList(request);
        data.setDayList(realTimeList);
		
		return data;
	}
	
	public ApiServiceResponse selectApiSvcInfo() throws Exception {
		
		ApiServiceResponse data = new ApiServiceResponse();
		ApiServiceVO service = apiRepository.selectApiSvcInfo();
		List<ApiServiceVO> serviceHis = apiRepository.selectApiSvcInfoHis();
		
		for(ApiServiceVO vo : serviceHis) {
			if(vo.getModUserNm() != null && !"".equals(vo.getModUserNm())) {
				vo.setModUserNm(cryptoUtil.decrypt(vo.getModUserNm()));
			}
		}
		
        data.setApiSvrStatCd(service.getApiSvrStatCd());
        data.setMinVer(service.getMinVer());
        data.setCurVer(service.getCurVer());
        data.setHisList(serviceHis);
		
		return data;
	}
	
	public void updateApiSvcInfo(ApiServiceRequest request) {
		
		ApiServiceVO service = apiRepository.selectApiSvcInfo();
		boolean redisRegYn = false;
		
		String modDivCd = request.getModDivCd();
		if(modDivCd != null && !"".equals(modDivCd) && "01".equals(modDivCd)) {
			request.setBeforeData(service.getApiSvrStatCd());
			redisRegYn = true;
		}else if("02".equals(modDivCd)) {
			request.setBeforeData(service.getCurVer());
		}else if("03".equals(modDivCd)) {
			request.setBeforeData(service.getMinVer());
		}
		
		apiRepository.updateApiSvcInfo(request);
		apiRepository.insertApiSvcInfoHis(request);
		
		if(redisRegYn) {
			RedisCommunicater.apiServerUseYn(request.getApiSvrStatCd());
		}
	}
}