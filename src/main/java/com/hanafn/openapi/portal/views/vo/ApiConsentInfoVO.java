package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("apiConsentInfo")
public class ApiConsentInfoVO {

  private String commCdId;
  private String cdId;
  private String cdNm;
  private String cdEng;
}
