package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("serviceappIp")
public class ServiceAppIpVO {
    private String seqNo;
    private String userKey;
    private String cnlKey;
    private String regUser;
    private String regDttm;
    private String modUser;
    private String modDttm;
}
