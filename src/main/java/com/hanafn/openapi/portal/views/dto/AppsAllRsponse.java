package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.AppsVO;

import lombok.Data;

@Data
public class AppsAllRsponse {
    private List<AppsVO> list;

    @Data
    public static class devGuidesAppsAllRsponse{
        private String entrCd;
        private List<AppsVO> list;
    }
}
