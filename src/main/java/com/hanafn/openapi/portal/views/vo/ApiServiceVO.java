package com.hanafn.openapi.portal.views.vo;

import lombok.Data;

import java.util.List;

import org.apache.ibatis.type.Alias;

@Data
@Alias("apiService")
public class ApiServiceVO {
	private String seqNo;
	private String modDivCd;
	private String modDivNm;
	private String apiSvrStatCd;
	private String minVer;
	private String curVer;
	private String modUser;
	private String modUserNm;
	private String modDttm;
	private String beforeData;
	private String afterData;
}
