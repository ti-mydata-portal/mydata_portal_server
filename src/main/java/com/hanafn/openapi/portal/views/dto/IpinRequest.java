package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class IpinRequest {
    private String sencData;
    private String srtnMsg;
    private String resultEncData;
    private String di;
    private String userKey;
    private String certType;// 1 : 회원가입, 2 : 로그인, 3 : 아아디 찾기, 4 : 패스워드 찾기
    private String sendCd;
    
    @Data
    public static class IpinUpdateReqNo {
    	private String di;
    	private String resNo;
    }
    
    @Data
    public static class IpinUpdatePw {
    	private String userId;
    	private String userPwd;
    	private String resNo;
    	private String userKey;
    	private String seq;
    }
    
}
