package com.hanafn.openapi.portal.views.dto.nice;

import org.apache.commons.lang3.StringUtils;
import reactor.util.annotation.Nullable;

public enum ServiceCategoryEnum {

    NICEID("0100","나이스아이디","","","","", true),
    PARTNER("0200","제휴","","","","", true),
    MYDATA("0300","마이데이터","","","","", true),
    ZIKIMI("0400","지키미","","","","", true);

    private String value;
    private String name;
    private String appApiaAplvUrl;
    private String useorgAplvUrl;
    private String code;
    private String usePackageCd;
    private boolean useYn;

    ServiceCategoryEnum(String value, String name, String appApiaAplvUrl, String useorgAplvUrl, String code, String usePackageCd, boolean useYn) {
        this.value = value;
        this.name = name;
        this.appApiaAplvUrl = appApiaAplvUrl;
        this.useorgAplvUrl = useorgAplvUrl;
        this.code = code;
        this.usePackageCd = usePackageCd;
        this.useYn = useYn;
    }

    /**
     * Return the String value of HfnCompanyInfo
     */
    public String value() {
        return this.value;
    }

    /**
     * Return the Name of this HfnCompanyInfo
     */
    public String getName() {
        return this.name;
    }

    /**
     * Return the Key of this HfnCompanyInfo
     */
    public String getCode() {
    return this.code;
}

    public String getUsePackageCd() { return this.usePackageCd; }

    /**
     * Get the urls
     */
    public String getAppApiaAplvUrl() {
        return this.appApiaAplvUrl;
    }
    public String getUseorgAplvUrl() {
    return this.useorgAplvUrl;
}
    public boolean getUseYn() {
        return this.useYn;
    }

    /**
     *  Set the urls based by properties
     * @param url
     */
    private void setAppApiaAplvUrl(String url) {
            this.appApiaAplvUrl = url;
        }
        private void setUseorgAplvUrl(String url) {
            this.useorgAplvUrl = url;
        }
    
    @Nullable
    public static ServiceCategoryEnum resolve(String hfnCd) {
        for (ServiceCategoryEnum hfnCompanyInfo : ServiceCategoryEnum.values()) {
            if (StringUtils.equals(hfnCd, hfnCompanyInfo.value())) {
                return hfnCompanyInfo;
            }
        }
        return null;
    }

    @Nullable
    public static ServiceCategoryEnum resolveByHfnName(String hfnName) {
        for (ServiceCategoryEnum hfnCompanyInfo : ServiceCategoryEnum.values()) {
            if (StringUtils.equals(hfnName, hfnCompanyInfo.getCode())) {
                return hfnCompanyInfo;
            }
        }
        return null;
    }
}