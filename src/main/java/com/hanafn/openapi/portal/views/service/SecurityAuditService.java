package com.hanafn.openapi.portal.views.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRsponsePaging;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.HfnUserRsponsePaging;
import com.hanafn.openapi.portal.views.repository.SecurityAuditRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class SecurityAuditService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityAuditService.class);

    @Autowired
    SecurityAuditRepository repository;
    @Autowired
    MessageSourceAccessor messageSource;

    public HfnUserRsponsePaging selectAdminAccountList(HfnUserRequest request){
        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

        if(request.getPageSize() == 0){
            request.setPageSize(20);
        }

        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        int totCnt = repository.countAdminAccountList(request);
        List<HfnUserVO> list = repository.selectAdminAccountList(request);

        HfnUserRsponsePaging hfnUserRsponsePaging = new HfnUserRsponsePaging();
        hfnUserRsponsePaging.setPageIdx(request.getPageIdx());
        hfnUserRsponsePaging.setPageSize(request.getPageSize());
        hfnUserRsponsePaging.setList(list);
        hfnUserRsponsePaging.setTotCnt(totCnt);
        hfnUserRsponsePaging.setSelCnt(list.size());

        return hfnUserRsponsePaging;
    }

    public PortalLogRsponsePaging selectConnectionHistory(PortalLogRequest request) {
        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

        if(request.getPageSize() == 0){
            request.setPageSize(20);
        }

        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        int totCnt = repository.countConnectionHistory(request);
        List<PortalLogVO> list = repository.selectConnectionHistory(request);

        PortalLogRsponsePaging pagingData = new PortalLogRsponsePaging();
        pagingData.setPageIdx(request.getPageIdx());
        pagingData.setPageSize(request.getPageSize());
        pagingData.setList(list);
        pagingData.setTotCnt(totCnt);
        pagingData.setSelCnt(list.size());

        return pagingData;
    }

    public PortalLogRsponsePaging selectActivityHistory(PortalLogRequest request){
        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

        if(request.getPageSize() == 0){
            request.setPageSize(20);
        }

        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        int totCnt = repository.countActivityHistory(request);
        List<PortalLogVO> list = repository.selectActivityHistory(request);

        PortalLogRsponsePaging pagingData = new PortalLogRsponsePaging();
        pagingData.setPageIdx(request.getPageIdx());
        pagingData.setPageSize(request.getPageSize());
        pagingData.setList(list);
        pagingData.setTotCnt(totCnt);
        pagingData.setSelCnt(list.size());

        return pagingData;
    }
}
