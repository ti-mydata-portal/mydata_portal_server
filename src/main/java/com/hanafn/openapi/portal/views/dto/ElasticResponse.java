package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.views.vo.ElasticVO;

@Data
public class ElasticResponse {
    private List<ElasticVO> list;
    int totCnt;
}
