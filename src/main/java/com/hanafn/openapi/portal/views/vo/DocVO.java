package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("doc")
public class DocVO {

	private int seqNo;
	private String docCd;
	private String docNm;
	private String docPath;
	private String docFileNm;
	private String regUser;
	private String regId;
	private String regDttm;
	private String modUser;
	private String modId;
	private String modDttm;
}
