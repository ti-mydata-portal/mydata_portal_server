package com.hanafn.openapi.portal.views.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class SettlementRequest {

    private String hisSeq;
    private MultipartFile fileData;
    private String hfnCd;
    private String userKey;
    private String userNm;
    private String bilMonth;

    private String appKey;
    private String appNm;
    private String accCd;
    private String wdAmt;
    private String wdAcno;
    private String wdMemo;

    private String stDt;
    private String enDt;

    private String useorgCd;
    private String useorgNm;
    private String wdStatCd;
    private String regDttm;
    private String role;
    private String regUser;
    private String regUserId;

    private String seq;

    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;

    private String searchAplvStatCd;
    
    @Data
    public static class AppUseInfoRequest {
    	private String searchAppKey;
    	private String searchUserKey;
    	private String searchPrdId;
    	private String searchStDtText;
    	private String searchEnDtText;
    	private String[] searchPrdKind;
    	private int pageIdx = 0;
	    private int pageSize = 20;
	    private int pageOffset = 0;
	    private int pageIdxTot = 0;
	    private int pageSizeTot = 10;
	    private int pageOffsetTot = 0;
	    private int pageIdxUse = 0;
	    private int pageSizeUse = 10;
	    private int pageOffsetUse = 0;
    }
    
    @Data
    public static class PayDetailRequest {
    	private String userKey;
    	private String moid;
    }
}
