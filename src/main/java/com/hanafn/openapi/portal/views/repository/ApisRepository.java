package com.hanafn.openapi.portal.views.repository;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppReadInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.views.dto.ApisRequest;
import com.hanafn.openapi.portal.views.dto.DocRequest;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;
import com.hanafn.openapi.portal.views.vo.ApiCtgrVO;
import com.hanafn.openapi.portal.views.vo.ApiTestVO;
import com.hanafn.openapi.portal.views.vo.DocVO;

@Mapper
public interface ApisRepository {

	List<ApiCtgrVO> selectCtgrsList();
	List<ApiVO> selectApisList(ApisRequest request);
	ApiCtgrVO selectApis(ApisRequest request);
	List<ProductVO> selectProdList(ProductRequest request);
	List<ProductVO> selectProdListUser(ProductRequest request);
	List<ProductVO> selectMainProdList();
	
	AppsVO selectAppScrKey(AdminRequest.AppScrRequest request);
	List<AppReadInfoVO> selectAppReadUsers(AdminRequest.AppScrRequest request);
	void deleteAppReadUsers(AdminRequest.AppScrRequest request);
	List<AppReadInfoVO> selectAppReadUser(AdminRequest.AppReadUserRequest request);
	String getSubmitData(DocRequest.DocUserSubmitData request);
	List<DocVO> selectDocList(Map<String, Object> param);

	ApiVO selectApiId(AdminRequest.apiTestRequest request);
	ApiTestVO selectApiTestInfo();
	ApiTestVO selectApiTestAppInfo(AdminRequest.apiTestRequest request);
	
	String selectPrdService(ProductRequest request);
	
	List<ApiColumnVO> selectApiColumnList(AdminRequest.PrdApiRequest request);
	
	
}