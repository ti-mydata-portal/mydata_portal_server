package com.hanafn.openapi.portal.views.service;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppReadInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.ApiCtgrRsponse;
import com.hanafn.openapi.portal.views.dto.ApisRequest;
import com.hanafn.openapi.portal.views.dto.DocRequest;
import com.hanafn.openapi.portal.views.dto.DocResponse;
import com.hanafn.openapi.portal.views.dto.PlanResponse;
import com.hanafn.openapi.portal.views.dto.PrdApiResponse;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.repository.ApisRepository;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;
import com.hanafn.openapi.portal.views.vo.ApiCtgrVO;
import com.hanafn.openapi.portal.views.vo.DocVO;
import com.hanafn.openapi.portal.views.vo.PlanVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor
public class ApisService {

    private final ObjectMapper objectMapper;
    private final ApisRepository apisRepository;
    private final MessageSourceAccessor messageSource;
    
    @Value("${pay.backoffice.plan.url}")
	private String planUrl;
    
    @Autowired
	FileService fileService;
    
    @Autowired
    AppsRepository appsRepository;
    
    @Autowired
	AdminRepository adminRepository;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;

    public ApiCtgrRsponse.ApisResponse getApis(ApisRequest request) {
        try {
            ApiCtgrRsponse.ApisResponse apisResponse = new ApiCtgrRsponse.ApisResponse();
            ApiCtgrVO apiCtgrVO = apisRepository.selectApis(request);
            apisResponse = objectMapper.convertValue(apiCtgrVO, ApiCtgrRsponse.ApisResponse.class);
            apisResponse.setList(apisRepository.selectApisList(request));
            return apisResponse;
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }
    }
    
    public List<ApiCtgrVO> selectCtgrsList() {
        try {
        	List<ApiCtgrVO> apiCtgrList = apisRepository.selectCtgrsList();
            return apiCtgrList;
        } catch (BusinessException e) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }
    }
    
    public ProductResponse.ProductListResponse selectProdList(ProductRequest request){
    	List<ProductVO> prodList = apisRepository.selectProdListUser(request);
    	ProductResponse.ProductListResponse data  = new ProductResponse.ProductListResponse();
    	
    	data.setProdList(prodList);
    	
    	for(ProductVO vo : prodList) {
			String path = vo.getFilePath();
			String iconPath = vo.getIconPath();
			
			if(path != null && !"".equals(path)) {
				vo.setFilePath(path.replace(fileService.getPhotoFileDir(), ""));
			}
			
			if(iconPath != null && !"".equals(iconPath)) {
				vo.setIconPath(iconPath.replace(fileService.getPhotoFileDir(), ""));
			}
			
		}
    			
    	return data;
    }
    
    public ProductResponse.ProductListResponse selectMainProdList(){
    	List<ProductVO> prodList = apisRepository.selectMainProdList();
    	ProductResponse.ProductListResponse data  = new ProductResponse.ProductListResponse();
    	
    	data.setProdList(prodList);
    	
		for(ProductVO vo : prodList) {
			String path = vo.getFilePath();
			String iconPath = vo.getIconPath();
			
			if(path != null && !"".equals(path)) {
				vo.setFilePath(path.replace(fileService.getPhotoFileDir(), ""));
			}
			
			if(iconPath != null && !"".equals(iconPath)) {
				vo.setIconPath(iconPath.replace(fileService.getPhotoFileDir(), ""));
			}
			
		}
    			
    	return data;
    }
    
    public AdminResponse.AppsResponse selectAppScrKey(AdminRequest.AppScrRequest request) throws Exception {
    	
    	AppsVO vo = apisRepository.selectAppScrKey(request);
    	
    	AdminResponse.AppsResponse data = new AdminResponse.AppsResponse();
    	
    	String appScr = vo.getAppScr();
    	if(appScr != null && !"".equals(appScr)) {
    		data.setAppScr(cryptoUtil.decrypt(appScr));
    	}
    	
    	String appClientId = vo.getAppClientId();
		if(appClientId != null && !"".equals(appClientId)) {
    		data.setAppClientId(appClientId);
    	}
    	
    	return data;
    }
    
    public void deleteAppReadUsers(AdminRequest.AppScrRequest request) throws Exception {
    	apisRepository.deleteAppReadUsers(request);
    }
    
    public AdminResponse.AppsResponse selectAppReadUsers(AdminRequest.AppScrRequest request) throws Exception {
    	
    	List<AppReadInfoVO> list = apisRepository.selectAppReadUsers(request);
    	
    	AdminResponse.AppsResponse data = new AdminResponse.AppsResponse();
    	
    	for(AppReadInfoVO vo : list) {
    		String userNm = vo.getUserNm();
    		String userEmail = vo.getUserEmail();
    		String userTel = vo.getUserTel();
    		
    		// 이름
            if (userNm != null && !"".equals(userNm)) {
                String decryptedUserNm = cryptoUtil.decrypt(userNm);
                vo.setUserNm(decryptedUserNm);
            }
            
            if (userEmail != null && !"".equals(userEmail)) {
                String decryptedEmail = cryptoUtil.decrypt(userEmail);
                vo.setUserEmail(decryptedEmail);
            }
            
            if (userTel != null && !"".equals(userTel)) {
                String decryptedTel = cryptoUtil.decrypt(userTel);
                vo.setUserTel(decryptedTel);
            }
    	}
    	
    	data.setAppReadInfo(list);
    	
    	return data;
    }
    
    public AdminResponse.AppsResponse selectAppReadUser(AdminRequest.AppReadUserRequest request) throws Exception {

    	AdminRequest.AppScrRequest appScrRequest = new AdminRequest.AppScrRequest();
        appScrRequest.setAppKey(request.getAppKey());
        appScrRequest.setUserId(request.getUserId());
        appScrRequest.setAppKey(request.getAppKey());
        List<AppReadInfoVO> appReadInfo = apisRepository.selectAppReadUsers(appScrRequest);
        // SELECT T1.USER_KEY
        //       ,T2.USER_ID
        //       ,T2.USER_NM
        //       ,T2.USER_TEL
        //       ,T2.USER_EMAIL
        // FROM PORTAL_APP_READ_INFO T1
        //     ,PORTAL_USER_INFO T2
        // WHERE T1.USER_KEY = T2.USER_KEY
        // AND T1.APP_KEY = #{appKey}

        if (appReadInfo != null && appReadInfo.size() > 0) {
            log.error("이용자 본인 또는 이미 등록된 계정은 등록할 수 없습니다.");
            throw new BusinessException("E086", messageSource.getMessage("E086"));
        }

        // 이름
        if (request.getUserNm() != null && !"".equals(request.getUserNm())) {
            String encryptedUserNm  = cryptoUtil.encrypt(request.getUserNm().trim());
            request.setUserNm(encryptedUserNm);
        }

        // 휴대전화번호
        if (request.getUserTel() != null && !"".equals(request.getUserTel())) {
            String encryptedUserTel = cryptoUtil.encrypt(request.getUserTel().trim());
            request.setUserTel(encryptedUserTel);
        }

        // 이메일
        if (request.getUserEmail() != null && !"".equals(request.getUserEmail())) {
            String encryptedUserMail = cryptoUtil.encrypt(request.getUserEmail().trim());
            request.setUserEmail(encryptedUserMail);
        }

        List<AppReadInfoVO> userInfo = apisRepository.selectAppReadUser(request);

        if (userInfo != null) {
            for(AppReadInfoVO vo : userInfo) {

                if(StringUtils.equals(vo.getUserId(),request.getReqId())) {
                    log.error("이용자 본인 또는 이미 등록된 계정은 등록할 수 없습니다.");
                    throw new BusinessException("E086", messageSource.getMessage("E086"));
                }

                String userNm = vo.getUserNm();
                String userEmail = vo.getUserEmail();
                String userTel = vo.getUserTel();

                if (userNm != null && !"".equals(userNm)) {
                    String decryptedUserNm = cryptoUtil.decrypt(userNm);
                    vo.setUserNm(decryptedUserNm);
                }

                if (userEmail != null && !"".equals(userEmail)) {
                    String decryptedEmail = cryptoUtil.decrypt(userEmail);
                    vo.setUserEmail(decryptedEmail);
                }

                if (userTel != null && !"".equals(userTel)) {
                    String decryptedTel = cryptoUtil.decrypt(userTel);
                    vo.setUserTel(decryptedTel);
                }
            }

        }

        AdminResponse.AppsResponse data = new AdminResponse.AppsResponse();
    	data.setAppReadInfo(userInfo);
    	
    	return data;
    }
    
    public DocResponse.DocListResponse selectSubmitData(DocRequest.DocUserSubmitData request) throws Exception {
    	DocResponse.DocListResponse data = new DocResponse.DocListResponse();
    	
    	String submitData = apisRepository.getSubmitData(request);
    	
    	if(submitData != null && !"".equals(submitData)) { 
    		String [] arrSubmitData = submitData.split(",");
        	
        	List<String> listSubmitData = Arrays.asList(arrSubmitData);
        	
        	Map<String, Object> param = new HashMap<String, Object>();
        	
        	param.put("docList",listSubmitData);
        	
        	
        	List<DocVO> docList = apisRepository.selectDocList(param);
        	
        	data.setDocList(docList);
    	}
    	
    	return data;
    }
    
    public AdminResponse.AppsResponse selectAppPrdList(AppsRequest request) throws Exception {
    	AdminResponse.AppsResponse data = new AdminResponse.AppsResponse();
    	
    	List<AppPrdInfoVO> appPrdList = appsRepository.selectApPrdInfoDetailALL(request);
    	
    	data.setAppPrdInfo(appPrdList);
    	
    	return data;
    }
    
    public PrdApiResponse selectApiList(AdminRequest.PrdApiRequest request) throws Exception{
    	PrdApiResponse data = new PrdApiResponse();
    	
    	List<ApiVO> apiList = adminRepository.selectApiList(request);
    	request.setClmReqDiv("REQUEST");
    	List<ApiColumnVO> apiReqColumnList = apisRepository.selectApiColumnList(request);
    	request.setClmReqDiv("RESPONSE");
    	List<ApiColumnVO> apiResColumnList = apisRepository.selectApiColumnList(request);
    	data.setApiList(apiList);
    	data.setApiReqColumnList(apiReqColumnList);
    	data.setApiResColumnList(apiResColumnList);
    	
    	return data;
    }
    
    public PlanResponse selectPayPlanList(ProductRequest request, HttpServletRequest httpRequest) throws Exception{
    	PlanResponse data = new PlanResponse();
    	
    	Map<String,Object> param = new HashMap<String, Object>(); 
    	
    	String serviceCd = apisRepository.selectPrdService(request);
    	
    	ObjectMapper om = new ObjectMapper();
		
		BufferedReader br = null;
		HttpURLConnection hcon = null;
		URL url;
		StringBuffer result = new StringBuffer();
		
		param.put("searchNm", request.getSearchNm());
		if(serviceCd != null && !"".equals(serviceCd)) {
			param.put("serviceCd", serviceCd);
		}else {
			throw new BusinessException("9999", "상품의 서비스코드가 등록되지 않았습니다. 관리자에게 문의하세요.");
		}
		param.put("mchtCd", "NICEAPI");
    	
    	try {
			
    		String jsonParam = om.writeValueAsString(param);
			url = new URL(planUrl);
			hcon = (HttpURLConnection) url.openConnection();
			
			hcon.setRequestMethod("POST");
			hcon.setDoOutput(true);              //항상 갱신된내용을 가져옴.
			hcon.setRequestProperty("Content-Type", "application/json;charset=euc-kr");
			hcon.setRequestProperty("Authorization", httpRequest.getHeader("Authorization"));
			
			OutputStream os;
			
			os = hcon.getOutputStream();
			os.write(jsonParam.getBytes());
			os.flush();
			
			int code = hcon.getResponseCode();
			String message = hcon.getResponseMessage();
			if(code != 200 || code < 0) {
				throw new BusinessException(code+"",message);
			}
			
			br = new BufferedReader(new InputStreamReader(hcon.getInputStream()));
			
			String temp;
			while ((temp = br.readLine()) != null) {
				result.append(temp);
			}
			
			log.info("******************************요금제 통신 시작 ************************");
			
			log.info("URL : " + planUrl);
			log.info("result : " + result.toString());
			
			log.info("******************************요금제 통신 끝 ************************");
			
			
			Map<String, Object> resultMap = om.readValue(result.toString(), Map.class);
			
			if(resultMap != null && !resultMap.get("resultCode").equals("") && resultMap.get("resultCode").equals("0000")) {
//				
//				data.setPageIdx((int)resultMap.get("pageIdx"));
//				data.setPageSize((int)resultMap.get("pageSize"));
//				data.setTotCnt((int)resultMap.get("totCnt"));
//				data.setSelCnt((int)resultMap.get("selCnt"));
//				
				List<Map<String, Object>> planList = (List<Map<String, Object>>) resultMap.get("list");
				List<PlanVO> list = new ArrayList<PlanVO>();
				
				for(Map<String, Object> plan : planList) {
					PlanVO vo = new PlanVO();
					vo.setTerm((String)plan.get("term"));
					vo.setPrice((int)plan.get("price"));
					vo.setPlannm((String)plan.get("planNm"));
					vo.setCount((int)plan.get("count"));
					vo.setPlancd((String)plan.get("planCd"));
					
					list.add(vo);
				}
				
				data.setPlanList(list);
			}else {
				throw new BusinessException((String) resultMap.get("resultCode"), (String) resultMap.get("message"));
			}
		} catch (EOFException e) {
			log.error(e.getMessage());
		} catch (BusinessException e) {
			log.error(e.getMessage());
		} finally {
			
			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
    	
    	
    	return data;
    }
}
