package com.hanafn.openapi.portal.views.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/info")
@Slf4j
public class InfoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InfoController.class);

    @RequestMapping("/checkserver")
    public String checkServer() {
    	return "API_WEBSERVER_OK";
    }
}