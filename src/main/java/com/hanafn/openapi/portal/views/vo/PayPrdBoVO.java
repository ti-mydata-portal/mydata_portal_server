package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("payPrdBo")
public class PayPrdBoVO {
	
	private String prdCtgy;
	private String prdKind;
	private String serviceCd;
	private String appSvcUrl;
	private int useCnt;
	private int prdAmt;
	private String planCd;
	private String term;
	private String mppCd1;
	private String mppCd2;
	private String mppCd3;
	private String svcCd;
	private String seqNo;
	private String prdId;
	private String prdCd;
}
