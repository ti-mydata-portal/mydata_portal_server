package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class PolicyRequest {
    private String key;
    private String pattern;
}
