package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("billing")
public class BillingVO {
    private String prdCtgy;
    private String appKey;
    private String appNm;
    private String billingStDt;
    private String billingEnDt;
    private String billingDate;
    private String billingAmount;
    private String receiptDate;
    private String receiptAmount;
    private String receiptYn;
    private String unpaidAmount;
    private String memo;
    private String userNm;
    private String copNm;
    private String copRegNo;
    private String regUser;
    private String regDttm;
    private String modUser;
    private String modDttm;
}

