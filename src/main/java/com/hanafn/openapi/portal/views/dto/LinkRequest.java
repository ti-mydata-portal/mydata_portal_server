package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest.CommInfoInsert;

import lombok.Data;

@Data
public class LinkRequest {
	private String opType;
	private String appKey;
	private String userKey;
	private String apiId;
	
	@Data
	public static class TokenRequest {
		private String issueOrgCd;
		private String clientId;
		private String scr;
		private String token;
		private String grantType;
		private String statCd;
		private String scope;
		private String expiresIn;
		private String status;
	}
	
	@Data
	public static class TokenRequestInsert {
		private List<TokenRequest> tokenInfoList;
	}
}
