package com.hanafn.openapi.portal.views.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdSettReqeust;
import com.hanafn.openapi.portal.admin.viewsv2.dto.CommonResponse;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.ExcelUtil;
import com.hanafn.openapi.portal.views.dto.AppsAllRequest;
import com.hanafn.openapi.portal.views.dto.AppsAllRsponse;
import com.hanafn.openapi.portal.views.dto.BillingRequest;
import com.hanafn.openapi.portal.views.dto.BillingResponse;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.repository.BillingRepository;
import com.hanafn.openapi.portal.views.service.BillingService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/billing")
@Slf4j
@RequiredArgsConstructor
public class BillingController {

    private final BillingService service;
    private final BillingRepository repository;

    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    ExcelUtil excelUtil;

    /* NICE 결제내역 목록 조회 */
    @PostMapping("/selectPayInfoNice")
    public ResponseEntity<?> selectPayInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BillingRequest.PaymentRequest request) {
        return ResponseEntity.ok(service.selectPayInfoNice(request));
    }

    /* NICE 결제내역 상세 조회 */
    @PostMapping("/selectPayDetailInfoNice")
    public ResponseEntity<?> selectPayDetailInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SettlementRequest.PayDetailRequest request) {
        request.setUserKey(currentUser.getUserKey());

        return ResponseEntity.ok(service.selectPayDetailInfoNice(request));
    }

    /* NICE 정산내역 목록 - 앱 조회 */
    @PostMapping("/selectAppsBeforeNice")
    public ResponseEntity<?> selectAppsBeforeNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsAllRequest request) {
        AppsAllRsponse data = service.selectAppsBeforeNice(request);

        return ResponseEntity.ok(data);
    }

    /* NICE 정산내역 목록 - 앱별 상품 조회 */
    @PostMapping("/selectProdList")
    public ResponseEntity<?> selectProdList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PrdSettReqeust request) {
        ProductResponse.ProductListResponse data = service.selectProdList(request);

        return ResponseEntity.ok(data);
    }

    /* NICE 정산내역 목록 조회 (선불,후불 모두 포함) */
    @PostMapping("/selectUseInfoNice")
    public ResponseEntity<?> selectUseInfoNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BillingRequest.SettlementRequest request) throws Exception {
        return ResponseEntity.ok(service.selectUseInfoNice(request));
    }

    /* NICE 정산내역 상세 조회 (선불,후불 모두 포함) */
    @PostMapping("/selectUseDetailNice")
    public ResponseEntity<?> selectUseDetailNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BillingRequest.AppUseInfoRequest request) throws Exception {
        return ResponseEntity.ok(service.selectUseDetailNice(request));
    }

    /* NICE 청구정보 목록 조회 */
    @PostMapping("/selectBillingList")
    public ResponseEntity<?> selectBillingList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BillingRequest request) throws Exception {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_SALES")){
                BillingResponse.BillingListResponse data = service.selectBillingList(request);

                return ResponseEntity.ok(data);
            }
        }
        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    /* NICE 청구정보 상세 조회 */
    @PostMapping("/selectDetail")
    public ResponseEntity<?> selectBillingDetail(@CurrentUser UserPrincipal currentUser, @RequestBody BillingRequest request) {
        BillingResponse data = service.selectBillingDetail(request);

        return ResponseEntity.ok(data);
    }

    /* NICE 청구정보 등록 - 앱조회 */
    @PostMapping("/selectAppList")
    public ResponseEntity<?> selectAppList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BillingRequest request) throws Exception {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_SALES")){
                BillingResponse.BillingListResponse data = service.selectBillingAppList(request);
                return ResponseEntity.ok(data);
            }
        }
        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    /* NICE 청구정보 등록 */
    @PostMapping("/insertInfo")
    public ResponseEntity<?> insertBillingInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BillingRequest request) {
        request.setRegUser(currentUser.getUserKey());
        service.insertBillingInfo(request);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }

    /* NICE 청구정보 수정 */
    @PostMapping("/updateInfo")
    public ResponseEntity<?> updateBillingInfo(@CurrentUser UserPrincipal currentUser, @RequestBody BillingRequest request) {
        repository.updateBillingInfo(request);
        return ResponseEntity.ok("OK");
    }

    /* NICE 결제내역 엑셀 다운로드 */
    @PostMapping("/excelPayment")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelDownload(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "searchHfncd", required = false) String searchHfncd,
            @RequestParam(value = "searchStDt", required = false) String searchStDt,
            @RequestParam(value = "searchEnDt", required = false) String searchEnDt
    ) throws Exception {
        BillingRequest.PaymentRequest request = new BillingRequest.PaymentRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setSearchHfnCd(searchHfncd);
        request.setSearchStDt(searchStDt);
        request.setSearchEnDt(searchEnDt);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        excelUtil.excelPayment(request, response);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }

    /* NICE 정산내역 엑셀 다운로드 */
    @PostMapping("/excelSettlement")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelSettlement(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "searchHfncd", required = false) String searchHfncd,
            @RequestParam(value = "searchPrdId", required = false) String searchPrdId,
            @RequestParam(value = "searchAppKey", required = false) String searchAppKey,
            @RequestParam(value = "searchStDt", required = false) String searchStDt,
            @RequestParam(value = "searchEnDt", required = false) String searchEnDt
    ) throws Exception {
        BillingRequest.SettlementRequest request = new BillingRequest.SettlementRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setSearchHfnCd(searchHfncd);
        request.setSearchPrdId(searchPrdId);
        request.setSearchAppKey(searchAppKey);
        request.setSearchStDt(searchStDt);
        request.setSearchEnDt(searchEnDt);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        excelUtil.excelSettlement(request, response);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }

    /* NICE 청구관리 엑셀 다운로드 */
    @PostMapping("/excelBilling")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelBilling(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "searchHfncd", required = false) String searchHfncd,
            @RequestParam(value = "searchAppNm", required = false) String searchAppNm,
            @RequestParam(value = "searchReceiptYn", required = false) String searchReceiptYn,
            @RequestParam(value = "searchStDt", required = false) String searchStDt,
            @RequestParam(value = "searchEnDt", required = false) String searchEnDt
    ) throws Exception {
        BillingRequest request = new BillingRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setSearchHfnCd(searchHfncd);
        request.setSearchAppNm(searchAppNm);
        request.setSearchReceiptYn(searchReceiptYn);
        request.setSearchStDt(searchStDt);
        request.setSearchEnDt(searchEnDt);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        excelUtil.excelBilling(request, response);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }
}
