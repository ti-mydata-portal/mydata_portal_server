package com.hanafn.openapi.portal.views.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.UUIDUtils;
import com.hanafn.openapi.portal.views.dto.ApiTimesampRequest;
import com.hanafn.openapi.portal.views.dto.ServiceAppRequest;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.service.AppsService;
import com.hanafn.openapi.portal.views.service.LinkService;
import com.hanafn.openapi.portal.views.service.SettingNiceService;
import com.hanafn.openapi.portal.views.vo.UseorgVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/link")
@Slf4j
@RequiredArgsConstructor
public class LinkController {

    private final MessageSourceAccessor messageSource;
    private static final Logger LOGGER = LoggerFactory.getLogger(LinkController.class);
    
    @Autowired
    LinkService service;

    @Autowired
    AppsService appsService;

    @Autowired
    GWCommunicater gWCommunicater;
    
    @Autowired
    SettingRepository settingRepository;

    @Autowired
    SettingNiceService settingService;
    
    @PostMapping("/tokenReg")
    public ResponseEntity<?> tokenReg(@CurrentUser UserPrincipal currentUser, @RequestBody Map<String, Object> request) throws Exception{
    	
    	String req = (String)request.get("request");
    	String res = (String)request.get("response");
    	
    	ObjectMapper om = new ObjectMapper();
    	
    	Map<String, Object> reqMap = om.readValue(req, Map.class);
    	Map<String, Object> resMap = om.readValue(res, Map.class);
    	
    	String orgCode = (String)request.get("orgCode");
    	
    	service.tokenReg(currentUser, orgCode, reqMap, resMap);
    	
        return ResponseEntity.ok(new SignUpResponse(true, "tokenReg successfully"));
    }

    @PostMapping("/useOrgReg")
    public ResponseEntity<?> useOrgReg(@CurrentUser UserPrincipal currentUser, @RequestBody String strJson) throws Exception{
        ObjectMapper om = new ObjectMapper();

        Map<String, Object> data = om.readValue(strJson, Map.class);

        ApiTimesampRequest timeRequest = new ApiTimesampRequest();
        timeRequest.setApiUrl("/mgmts/orgs");
        timeRequest.setReqTimestamp((String)data.get("search_timestamp"));
        settingService.updateSearchTime(timeRequest);

        int orgCnt = Integer.parseInt((String)data.get("org_cnt"));

        if(orgCnt > 0) {
            List<Map<String, Object>> orgData = (List<Map<String, Object>>)data.get("org_list");

            for(Map<String, Object> info : orgData) {
                String opType = (String)info.get("op_type");
                if(opType != null && !"".equals(opType) && "I".equals(opType)) {
                    UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();

                    request.setUserKey((String)info.get("org_code"));
                    request.setUseorgId((String)info.get("org_code"));
                    request.setUseorgGb((String)info.get("org_type"));
                    if(settingService.chkUseorgDup(request) >= 1) {
                        log.error("Insert Useorg userKey dup error : [" + request.getUserKey() +"]");
                        continue;
                    }
                    if(info.containsKey("org_name")) {
                        request.setUseorgNm((String)info.get("org_name"));
                    }

                    if(info.containsKey("org_regno")) {
                        request.setBrn((String)info.get("org_regno"));
                    }

                    if(info.containsKey("corp_regno")) {
                        request.setUseorgNo((String)info.get("corp_regno"));
                    }

                    if(info.containsKey("serial_num")) {
                        request.setSerialNum((String)info.get("serial_num"));
                    }

                    if(info.containsKey("address")) {
                        request.setUseorgAddr((String)info.get("address"));
                    }

                    if(info.containsKey("domain")) {
                        request.setUseorgDomain((String)info.get("domain"));
                    }

                    if(info.containsKey("relay_org_code")) {
                        request.setRelorgCd((String)info.get("relay_org_code"));
                    }

                    if(info.containsKey("industry")) {
                        request.setEntrCd((String)info.get("industry"));
                    }

                    if(info.containsKey("auth_type")) {
                        request.setProvCertCd((String)info.get("auth_type"));
                    }

                    if(info.containsKey("cert_issuer_dn")) {
                        request.setCertIssuerDn((String)info.get("cert_issuer_dn"));
                    }

                    if(info.containsKey("cert_oid")) {
                        request.setCertOid((String)info.get("cert_oid"));
                    }
                    
                    if(info.containsKey("is_rcv_org")) {
        	    		if(info.get("cert_oid") != null && !"".equals(info.get("cert_oid")) && "true".equals(info.get("cert_oid"))) {
        	    			request.setIsRcvOrg("Y");
        	    		}else {
        	    			request.setIsRcvOrg("N");
        	    		}
        	    	}

                    request.setRegUser(currentUser.getUserKey());
                    request.setModUser(currentUser.getUserKey());

                    if(info.containsKey("domain_ip_cnt")) {
                        int domainIpCnt = Integer.parseInt((String)info.get("domain_ip_cnt"));

                        if(domainIpCnt > 0) {
                            List<Map<String, String>> domainIPs = new ArrayList<Map<String, String>>();

                            List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("domain_ip_list");

                            for(Map<String, Object> ipInfo : ipData) {
                                Map<String, String> ipMap = new HashMap<String, String>();
                                ipMap.put("domainIp", (String)ipInfo.get("domain_ip"));
                                domainIPs.add(ipMap);
                            }
                            request.setUseorgDomainIPs(domainIPs);
                        } else {
                            request.setUseorgDomainIPs(new ArrayList<Map<String, String>>());
                        }
                    }else {

                        request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
                    }

                    if(info.containsKey("ip_cnt")) {
                        int ipCnt = Integer.parseInt((String)info.get("ip_cnt"));

                        if(ipCnt > 0) {
                            List<Map<String, String>> clientIPs = new ArrayList<Map<String, String>>();

                            List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("ip_list");

                            for(Map<String, Object> ipInfo : ipData) {
                                Map<String, String> ipMap = new HashMap<String, String>();
                                ipMap.put("cnlKey", (String)ipInfo.get("ip"));
                                clientIPs.add(ipMap);
                            }
                            request.setClientIPs(clientIPs);
                        } else {
                            request.setClientIPs(new ArrayList<Map<String,String>>());
                        }
                    }else {
                        request.setClientIPs(new ArrayList<Map<String,String>>());
                    }
                    
                    if(info.containsKey("np_time_cnt")) {
                        int npTimeCnt = Integer.parseInt((String)info.get("np_time_cnt"));

                        if(npTimeCnt > 0) {
                            List<Map<String, String>> nptimes = new ArrayList<Map<String, String>>();

                            List<Map<String, Object>> nptimeData = (List<Map<String, Object>>)info.get("np_time_list");

                            for(Map<String, Object> npTimeInfo : nptimeData) {
                                Map<String, String> ipMap = new HashMap<String, String>();
                                ipMap.put("npTime", (String)npTimeInfo.get("np_time"));
                                nptimes.add(ipMap);
                            }
                            request.setNptimes(nptimes);
                        } else {
                            request.setNptimes(new ArrayList<Map<String,String>>());
                        }
                    }else {
                        request.setNptimes(new ArrayList<Map<String,String>>());
                    }

                    settingService.insertUseOrg(request, false);
                }else if("M".equals(opType)) {
                    UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
                    request.setUserKey((String)info.get("org_code"));
                    request.setUseorgId((String)info.get("org_code"));
                    request.setUseorgGb((String)info.get("org_type"));

                    if(info.containsKey("org_name")) {
                        request.setUseorgNm((String)info.get("org_name"));
                    }

                    if(info.containsKey("org_regno")) {
                        request.setBrn((String)info.get("org_regno"));
                    }

                    if(info.containsKey("corp_regno")) {
                        request.setUseorgNo((String)info.get("corp_regno"));
                    }

                    if(info.containsKey("serial_num")) {
                        request.setSerialNum((String)info.get("serial_num"));
                    }

                    if(info.containsKey("address")) {
                        request.setUseorgAddr((String)info.get("address"));
                    }

                    if(info.containsKey("domain")) {
                        request.setUseorgDomain((String)info.get("domain"));
                    }

                    if(info.containsKey("relay_org_code")) {
                        request.setRelorgCd((String)info.get("relay_org_code"));
                    }

                    if(info.containsKey("industry")) {
                        request.setEntrCd((String)info.get("industry"));
                    }

                    if(info.containsKey("auth_type")) {
                        request.setProvCertCd((String)info.get("auth_type"));
                    }

                    if(info.containsKey("cert_issuer_dn")) {
                        request.setCertIssuerDn((String)info.get("cert_issuer_dn"));
                    }

                    if(info.containsKey("cert_oid")) {
                        request.setCertOid((String)info.get("cert_oid"));
                    }
                    
                    if(info.containsKey("is_rcv_org")) {
        	    		if(info.get("cert_oid") != null && !"".equals(info.get("cert_oid")) && "true".equals(info.get("cert_oid"))) {
        	    			request.setIsRcvOrg("Y");
        	    		}else {
        	    			request.setIsRcvOrg("N");
        	    		}
        	    	}

                    request.setModUser(currentUser.getUserKey());
                    request.setRegUser(currentUser.getUserKey());
                    if(info.containsKey("domain_ip_cnt")) {
                        int domainIpCnt = Integer.parseInt((String)info.get("domain_ip_cnt"));

                        if(domainIpCnt > 0) {
                            List<Map<String, String>> domainIPs = new ArrayList<Map<String, String>>();

                            List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("domain_ip_list");

                            for(Map<String, Object> ipInfo : ipData) {
                                Map<String, String> ipMap = new HashMap<String, String>();
                                ipMap.put("domainIp", (String)ipInfo.get("domain_ip"));
                                domainIPs.add(ipMap);
                            }
                            request.setUseorgDomainIPs(domainIPs);
                        } else {
                            request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
                        }
                    }else {

                        request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
                    }

                    if(info.containsKey("ip_cnt")) {
                        int ipCnt = Integer.parseInt((String)info.get("ip_cnt"));

                        if(ipCnt > 0) {
                            List<Map<String, String>> clientIPs = new ArrayList<Map<String, String>>();

                            List<Map<String, Object>> ipData = (List<Map<String, Object>>)info.get("ip_list");

                            for(Map<String, Object> ipInfo : ipData) {
                                Map<String, String> ipMap = new HashMap<String, String>();
                                ipMap.put("cnlKey", (String)ipInfo.get("ip"));
                                clientIPs.add(ipMap);
                            }
                            request.setClientIPs(clientIPs);
                        } else {
                            request.setClientIPs(new ArrayList<Map<String,String>>());
                        }
                    }else {
                        request.setClientIPs(new ArrayList<Map<String,String>>());
                    }
                    
                    if(info.containsKey("np_time_cnt")) {
                        int npTimeCnt = Integer.parseInt((String)info.get("np_time_cnt"));

                        if(npTimeCnt > 0) {
                            List<Map<String, String>> nptimes = new ArrayList<Map<String, String>>();

                            List<Map<String, Object>> nptimeData = (List<Map<String, Object>>)info.get("np_time_list");

                            for(Map<String, Object> npTimeInfo : nptimeData) {
                                Map<String, String> ipMap = new HashMap<String, String>();
                                ipMap.put("npTime", (String)npTimeInfo.get("np_time"));
                                nptimes.add(ipMap);
                            }
                            request.setNptimes(nptimes);
                        } else {
                            request.setNptimes(new ArrayList<Map<String,String>>());
                        }
                    }else {
                        request.setNptimes(new ArrayList<Map<String,String>>());
                    }
                    
                    UseorgVO vo = settingRepository.useOrgDetail(request);
                    if(vo != null) {
                    	request.setUseorgStatCd(vo.getUseorgStatCd());
                    }else {
                    	request.setUseorgStatCd("OK");
                    }

                    settingService.useOrgUpd(request, false);
                }else {
                    UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
                    request.setUserKey((String)info.get("org_code"));
                    request.setUseorgId((String)info.get("org_code"));
                    request.setUseorgStatCd("DEL");
                    request.setModUser(currentUser.getUserId());
                    settingService.useOrgStatusUpd(request, false);
                }
            }
        }
        
        return ResponseEntity.ok(new SignUpResponse(true, "regOrg successfully"));
    }
    
    @PostMapping("/useOrgRegRcv")
    public ResponseEntity<?> useOrgRegRcv(@CurrentUser UserPrincipal currentUser, @RequestBody String strJson) throws Exception{
    	
        ObjectMapper om = new ObjectMapper();

        Map<String, Object> data = om.readValue(strJson, Map.class);

        ApiTimesampRequest timeRequest = new ApiTimesampRequest();
        timeRequest.setApiUrl("/mgmts/rcv_orgs");
        timeRequest.setReqTimestamp((String)data.get("search_timestamp"));
        settingService.updateSearchTime(timeRequest);

        int orgCnt = Integer.parseInt((String)data.get("org_cnt"));

        if(orgCnt > 0) {
            List<Map<String, Object>> orgData = (List<Map<String, Object>>)data.get("org_list");

            for(Map<String, Object> info : orgData) {
                String opType = (String)info.get("op_type");

                ServiceAppRequest.ServiceAppRegistRequest requestApp = new ServiceAppRequest.ServiceAppRegistRequest();

                requestApp.setUserKey((String)info.get("org_code"));
                requestApp.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
                requestApp.setAppStatCd("OK");
                requestApp.setAppClientId((String)info.get("client_id"));
                requestApp.setAppScr((String)info.get("client_secret"));

                if(opType != null && !"".equals(opType) && "I".equals(opType)) {
                    UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();

                    request.setUserKey((String)info.get("org_code"));
                    request.setUseorgId((String)info.get("org_code"));
                    request.setUseorgNm((String)info.get("org_code"));
                    request.setUseorgGb("99");

                    request.setRegUser(currentUser.getUserKey());
                    request.setModUser(currentUser.getUserKey());
                    request.setIsRcvOrg("Y");
                    
                    int chkUseorgDupCnt = settingService.chkUseorgDup(request);
                    int chkServiceDupCnt = appsService.chkServiceDup(requestApp);
                    
                    if(chkUseorgDupCnt >= 1 && chkServiceDupCnt >= 1) {
                    	log.error("Insert serviceApp clientId dup error : [" + requestApp.getAppClientId() + "]");
                    	log.error("Insert useorg dup error : [" + request.getUserKey() + "]");
                    	continue;
                    }
                    if(chkServiceDupCnt < 1) {
                    	requestApp.setAppKey(UUIDUtils.generateUUID());
                    	appsService.serviceAppReg(requestApp, false);
                    	gWCommunicater.oauthRegService(requestApp.getAppClientId(), requestApp.getAppScr(), requestApp.getUserKey(), "", "", "");
                    }
                    if(chkUseorgDupCnt < 1) {
                    	request.setClientIPs(new ArrayList<Map<String,String>>());
                    	request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
                    	request.setNptimes(new ArrayList<Map<String,String>>());
                    	settingService.insertUseOrg(request, false);
                    }
                }else if("M".equals(opType)) {
                    UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
                    request.setUserKey((String)info.get("org_code"));
                    request.setUseorgId((String)info.get("org_code"));
                    request.setUseorgNm((String)info.get("org_code"));
                    request.setUseorgGb("99");

                    request.setModUser(currentUser.getUserKey());
                    request.setRegUser(currentUser.getUserKey());
                    
                    request.setClientIPs(new ArrayList<Map<String,String>>());
                    request.setUseorgDomainIPs(new ArrayList<Map<String,String>>());
                    request.setNptimes(new ArrayList<Map<String,String>>());

                    settingService.useOrgUpd(request, false);

                    String appKey = appsService.selectAppKey(requestApp);

                    if(appKey != null && !"".equals(appKey)) {
                        requestApp.setAppKey(appKey);
                        
                        UseorgVO vo = settingRepository.useOrgDetail(request);
                        if(vo != null) {
                        	request.setUseorgStatCd(vo.getUseorgStatCd());
                        }else {
                        	request.setUseorgStatCd("OK");
                        }
                        
                        appsService.serviceAppUpd(requestApp, false);
                    }

                    gWCommunicater.oauthRegService(requestApp.getAppClientId(), requestApp.getAppScr(), requestApp.getUserKey(), "", "", "");
                }else {
                    UseorgRequest.UseorgUpdateRequest request = new UseorgRequest.UseorgUpdateRequest();
                    request.setUserKey((String)info.get("org_code"));
                    request.setUseorgId((String)info.get("org_code"));
                    request.setUseorgStatCd("DEL");
                    request.setModUser(currentUser.getUserId());
                    settingService.useOrgStatusUpd(request, false);

                    String appKey = appsService.selectAppKey(requestApp);

                    if(appKey != null && !"".equals(appKey)) {
                        requestApp.setAppKey(appKey);
                        ServiceAppRequest requestDel = new ServiceAppRequest();
                        requestDel.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
                        requestDel.setAppStatCd("DEL");
                        appsService.serviceAppStatusUpd(requestDel, false);
                    }
                }
            }
        }
    	
        return ResponseEntity.ok(new SignUpResponse(true, "regOrg successfully"));
    }
    
    @PostMapping("/appReg")
    public ResponseEntity<?> appReg(@CurrentUser UserPrincipal currentUser, @RequestBody String strJson) throws Exception{
    	
        ObjectMapper om = new ObjectMapper();

        Map<String, Object> data = om.readValue(strJson, Map.class);

        ApiTimesampRequest timeRequest = new ApiTimesampRequest();
        timeRequest.setApiUrl("/mgmts/service");
        timeRequest.setReqTimestamp((String)data.get("search_timestamp"));
        settingService.updateSearchTime(timeRequest);

        int orgCnt = Integer.parseInt((String)data.get("org_cnt"));

        if(orgCnt > 0) {
            List<Map<String, Object>> orgData = (List<Map<String, Object>>)data.get("org_list");

            for(Map<String, Object> orgInfo : orgData) {
                int serviceCnt = Integer.parseInt((String)orgInfo.get("service_cnt"));

                if(serviceCnt > 0) {
                    List<Map<String, Object>> serviceData = (List<Map<String, Object>>)orgInfo.get("service_list");

                    for(Map<String, Object> serviceInfo : serviceData) {
                        List<String> callbacks = new ArrayList<String>();
                        String sCallback = "";
                        List<String> schemes = new ArrayList<>();
                        String sSchemes = "";
                        
                        ServiceAppRequest.ServiceAppRegistRequest request = new ServiceAppRequest.ServiceAppRegistRequest();

                        request.setUserKey((String)orgInfo.get("org_code"));
                        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
                        request.setAppStatCd("OK");
                        request.setAppClientId((String)serviceInfo.get("client_id"));
                        request.setAppNm((String)serviceInfo.get("service_name"));
                        if(appsService.chkServiceDup(request) >= 1) {
                            log.error("Insert serviceApp clientId dup ERROR : [" + request.getAppClientId() + "]");
                            continue;
                        }
                        if(serviceInfo.containsKey("client_secret")) {
                            request.setAppScr((String)serviceInfo.get("client_secret"));
                        }

                        if(serviceInfo.containsKey("redirect_uri_cnt")) {
                            int redirectUriCnt = Integer.parseInt((String)serviceInfo.get("redirect_uri_cnt"));

                            if(redirectUriCnt > 0) {
                                List<Map<String, String>> redirectUris = new ArrayList<Map<String, String>>();

                                List<Map<String, Object>> redirectUriData = (List<Map<String, Object>>)serviceInfo.get("redirect_uri_list");

                                for(Map<String, Object> ruInfo : redirectUriData) {
                                    Map<String, String> ruMap = new HashMap<String, String>();
                                    ruMap.put("redirectUri", (String)ruInfo.get("redirect_uri"));
                                    callbacks.add((String)ruInfo.get("redirect_uri"));
                                    redirectUris.add(ruMap);
                                }
                                sCallback = String.join(",", callbacks);
                                request.setCallbackList(redirectUris);
                            } else {
                                request.setCallbackList(new ArrayList<Map<String,String>>());
                            }
                        }else {
                            request.setCallbackList(new ArrayList<Map<String,String>>());
                        }

                        if(serviceInfo.containsKey("app_scheme_cnt")) {
                            int appSchemeCnt = Integer.parseInt((String)serviceInfo.get("app_scheme_cnt"));
                            if(appSchemeCnt > 0) {
                                List<Map<String, String>> appSchemes = new ArrayList<Map<String, String>>();

                                List<Map<String, Object>> appSchemeData = (List<Map<String, Object>>)serviceInfo.get("app_scheme_list");

                                for(Map<String, Object> asInfo : appSchemeData) {
                                    Map<String, String> asMap = new HashMap<String, String>();
                                    asInfo.put("appScheme", (String)asInfo.get("app_scheme"));
                                    schemes.add((String)asMap.get("app_scheme"));
                                    appSchemes.add(asMap);
                                }
                                sSchemes = String.join(",", schemes);
                                request.setSchemeList(appSchemes);
                            } else {
                                request.setSchemeList(new ArrayList<Map<String,String>>());
                            }
                        }else {
                            request.setSchemeList(new ArrayList<Map<String,String>>());
                        }

                        String opType = (String)serviceInfo.get("op_type");
                        if(opType != null && !"".equals(opType) && "I".equals(opType)) {
                            request.setAppKey(UUIDUtils.generateUUID());
                            appsService.serviceAppReg(request, false);
                        } else if("M".equals(opType)) {
                            String appKey = appsService.selectAppKey(request);

                            if(appKey != null && !"".equals(appKey)) {
                                request.setAppKey(appKey);
                                appsService.serviceAppUpd(request, false);
                            }
                        } else {
                            String appKey = appsService.selectAppKey(request);

                            if(appKey != null && !"".equals(appKey)) {
                                request.setAppKey(appKey);
                                ServiceAppRequest requestDel = new ServiceAppRequest();
                                requestDel.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
                                requestDel.setAppStatCd("DEL");
                                appsService.serviceAppStatusUpd(requestDel, false);
                            }
                        }

                    }
                }
            }

        }
    	
        return ResponseEntity.ok(new SignUpResponse(true, "appReg successfully"));
    }
    
    @PostMapping("/statistics")
    public ResponseEntity<?> statistics(@RequestBody String strJson) throws Exception{
    	
    	Map<String, Object> vo = service.statistics(strJson);
    	
        return ResponseEntity.ok(vo);
    }
    
    @PostMapping("/apis")
    public ResponseEntity<?> apis(@RequestBody String strJson) throws Exception{
    	
    	Map<String, Object> vo = service.apis(strJson);
    	
        return ResponseEntity.ok(vo);
    }
}