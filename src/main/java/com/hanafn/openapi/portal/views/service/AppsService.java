package com.hanafn.openapi.portal.views.service;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AplvPrdReqeust;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AplvRegistRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppReadUserRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppUsageRatioRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsApiInfoRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsBlockKeyRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsCnlKeyRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsIssueRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRegRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsScrRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.AppsResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppApiInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppBlackListVO;
import com.hanafn.openapi.portal.admin.views.vo.AppCnlInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppReadInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvApprovalRequest;
import com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRsponsePaging;
import com.hanafn.openapi.portal.admin.viewsv2.service.AplvService;
import com.hanafn.openapi.portal.admin.viewsv2.vo.PrdAplvListVO;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.cmct.dto.GWResponse;
import com.hanafn.openapi.portal.enums.etcEnum;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.DateUtil;
import com.hanafn.openapi.portal.util.InnerTransUtil;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.util.RSAUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.AplvRequest;
import com.hanafn.openapi.portal.views.dto.AppCidScrDlResponse;
import com.hanafn.openapi.portal.views.dto.AppsRsponsePaging;
import com.hanafn.openapi.portal.views.dto.HfnCompanyAllResponse;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.dto.ServiceAppRequest;
import com.hanafn.openapi.portal.views.dto.ServiceAppResponsePaging;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.dto.UserRequest;
import com.hanafn.openapi.portal.views.dto.nice.ServiceCategoryEnum;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.repository.ProductRepository;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.vo.AppOwnerVO;
import com.hanafn.openapi.portal.views.vo.HfnUserRoleVO;
import com.hanafn.openapi.portal.views.vo.RequestApiVO;
import com.hanafn.openapi.portal.views.vo.ServiceAppVO;
import com.hanafn.openapi.portal.views.vo.UseorgVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class AppsService {

	@Autowired
	AppsRepository appsRepository;
	@Autowired
	SettingRepository settingRepository;
	@Autowired
	AdminRepository adminRepository;
	@Autowired
	MessageSourceAccessor messageSource;
	@Autowired
	ApisService apisService;
	@Autowired
	AplvService aplvService;
	@Autowired
	ProductRepository productRepository;
    @Autowired
    MailUtils mailUtils;
    @Autowired
    PolicyService policyService;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;
    
    @Value("${bt.tran.yn}")
	private String btTranYn;
    
    @Autowired
    public InnerTransUtil innerTransUtil;
    
    @Value("${bt.tran.svc.url}")
	private String btTranSvcUrl;
    
    @Autowired
    GWCommunicater gWCommunicater;

	private static final Logger LOGGER = LoggerFactory.getLogger(AppsService.class);

	public List<AppsVO> selectAppListAll(AppsRequest appsRequest) {
		return appsRepository.selectAppListAll(appsRequest);
	}

	public List<AppsVO> selectUserPortalAppList(AppsRequest appsRequest) {
		List<AppsVO> appList = appsRepository.selectAppAll(appsRequest);
		return appList;
	}

	public AppsRsponsePaging selectAppsListPaging(AppsRequest appsRequest) {
		if (appsRequest.getPageIdx() == 0)
			appsRequest.setPageIdx(appsRequest.getPageIdx() + 1);

		if (appsRequest.getPageSize() == 0) {
			appsRequest.setPageSize(20);
		}

		appsRequest.setPageOffset((appsRequest.getPageIdx() - 1) * appsRequest.getPageSize());
		int totCnt = appsRepository.countAppsList(appsRequest);
		List<AppsVO> list = appsRepository.selectAppsList(appsRequest);

		AppsRsponsePaging pagingData = new AppsRsponsePaging();
		pagingData.setPageIdx(appsRequest.getPageIdx());
		pagingData.setPageSize(appsRequest.getPageSize());
		pagingData.setTotCnt(totCnt);
		pagingData.setList(list);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	/**** RSA 클라이언트 id, secret 다운로드 및 재발급 ****/
	public AppCidScrDlResponse keyDownload(AppsIssueRequest request) {
		AppCidScrDlResponse appCidScrDlResponse = new AppCidScrDlResponse();
		String decryptedEncKey = tokenForNoDup(request);
		AppsRequest appsRequest = new AppsRequest();
		appsRequest.setAppKey(request.getAppKey());
		String entrCd = settingRepository.selectUseorgEntrCd(appsRequest);

		String appScr = null;
		try {
			appScr = cryptoUtil.decrypt(request.getAppScr());
		} catch (Exception e) {
			log.error("키 다운로드 앱 시크릿 복호화 에러:" + e.toString());
			throw new BusinessException("E076", messageSource.getMessage("E076"));
		}

		String original = String.format("client_id:%s\nclient_secret:%s\nencKey:%s\napp_key:%s\nentrCd:%s",
				request.getAppClientId(), appScr, decryptedEncKey, request.getAppKey(), entrCd);
		String pubkey = Optional.ofNullable(request.getPubKey()).orElse("");

		String fileContext = "";
		try {
			fileContext = RSAUtil.encrypt(original, pubkey);
		} catch (Exception e) {
			log.error("키 다운로드 암호화 에러:" + e.toString());
			throw new BusinessException("E076", messageSource.getMessage("E076"));
		}

		appCidScrDlResponse.setToken(fileContext);
		appCidScrDlResponse.setProcDttm(DateUtil.getCurrentDateTime12());

		appsRepository.updateAppDldttm(request.getAppKey());
		return appCidScrDlResponse;
	}

	public AppCidScrDlResponse reissueKeydownload(AppsIssueRequest request) {
		String decryptedEncKey = tokenForNoDup(request);
		AppsRequest appsRequest = new AppsRequest();
		appsRequest.setAppKey(request.getAppKey());
		String entrCd = settingRepository.selectUseorgEntrCd(appsRequest);

		String newAppScr = null;
		String appScr = null;

		try {
			appScr = cryptoUtil.decrypt(request.getAppScr());
		} catch (Exception e) {
			log.error("키 재발급 시크릿 복호화 에러 : " + e.toString());
			throw new BusinessException("E076", messageSource.getMessage("E076"));
		}

		try {
			newAppScr = cryptoUtil.decrypt(request.getNewAppScr());
		} catch (Exception e) {
			log.error("키 재발급 다운로드 시크릿 복호화 에러 : " + e.toString());
			throw new BusinessException("E076", messageSource.getMessage("E076"));
		}

		String original = String.format(
				"client_id:%s\nclient_secret:%s\nnew_client_id:%s\nnew_client_secret:%s\nencKey:%s\napp_key:%s\nentrCd:%s",
				request.getAppClientId(), appScr, request.getNewAppClientId(), newAppScr, decryptedEncKey,
				request.getAppKey(), entrCd);
		String pubkey = Optional.ofNullable(request.getPubKey()).orElse("");

		String fileContext = "";
		try {
			fileContext = RSAUtil.encrypt(original, pubkey);
		} catch (IllegalArgumentException e) {
			log.error("키 재발급 다운로드 암호화 에러 : " + e.toString());
			throw new BusinessException("E076", messageSource.getMessage("E076"));
		}
		AppCidScrDlResponse appCidScrDlResponse = new AppCidScrDlResponse();
		appCidScrDlResponse.setProcDttm(DateUtil.getCurrentDateTime12());
		appCidScrDlResponse.setToken(fileContext);

		appsRepository.updateAppDldttm(request.getAppKey());
		return appCidScrDlResponse;
	}

	public String tokenForNoDup(AppsIssueRequest request) {
		UseorgRequest.UseorgDetailRequest useorgDetailRequest = new UseorgRequest.UseorgDetailRequest();
		useorgDetailRequest.setUserKey(request.getUserKey());
		UseorgVO useorgVO = settingRepository.selectUseorg(useorgDetailRequest);
		String encKey = useorgVO.getEncKey();
		String token = "";
		try {
			if (encKey != null && !encKey.isEmpty()) {
				token = cryptoUtil.decrypt(encKey);
			}
		} catch (Exception e) {
			log.error("encKey DecryptAES256 Error", e);
			token = "encKey Error";
		}
		return token;
	}

	/**** 앱 상태변경 ****/
	public AdminResponse.AppsResponse updateAppStatCd(AppsRequest appsRequest) {

		String clientId = appsRequest.getAppClientId();
		String newClientId = appsRequest.getNewAppClientId();

		if (StringUtils.equals(appsRequest.getAppStatCd(), "OK")) {
			appsRequest.setAppStatCd("CLOSE");
		} else {
			appsRequest.setAppStatCd("OK");
		}

		int row = appsRepository.updateAppStatCd(appsRequest);

		// Redis Set
		try {
			if (StringUtils.equals(appsRequest.getAppStatCd(), "CLOSE")) {
				RedisCommunicater.appRedisSet(clientId, "false");
				if ("" != newClientId && null != newClientId) {
					RedisCommunicater.appRedisSet(newClientId, "false");
				}
			} else {
				RedisCommunicater.appRedisSet(clientId, "true");
				if ("" != newClientId && null != newClientId) {
					RedisCommunicater.appRedisSet(newClientId, "true");
				}
			}
		} catch (Exception e) {
			if (StringUtils.equals(appsRequest.getAppStatCd(), "CLOSE")) {
				RedisCommunicater.appRedisSet(clientId, "true");
				if ("" != newClientId && null != newClientId) {
					RedisCommunicater.appRedisSet(newClientId, "true");
				}
			} else {
				RedisCommunicater.appRedisSet(clientId, "false");
				if ("" != newClientId && null != newClientId) {
					RedisCommunicater.appRedisSet(newClientId, "false");
				}
			}
		}

		AdminResponse.AppsResponse appsRsponse = new AdminResponse.AppsResponse();
		if (row > 0) {
			AppsVO appsVO = adminRepository.selectAppDetail(appsRequest);
			appsRsponse.setAppStatCd(appsVO.getAppStatCd());
		}
		return appsRsponse;
	}

	/**** 앱 정보 등록 ****/
	public void insertAppInfo(AppsRegRequest appsRegRequest) {

	    // INSERT INTO PORTAL_APP_INFO
        // APP_STAT_CD = 'WAIT'
        // APP_APLV_STAT_CD = 10,11 'APLV' , 나머지 'REQ'
		appsRepository.insertAppInfoNice(appsRegRequest);

		// app secret his 테이블에 시크릿 추가
		AppsScrRequest appsScrRequest = new AppsScrRequest();
		appsScrRequest.setAppKey(appsRegRequest.getAppKey());
		appsScrRequest.setAppClientId(appsRegRequest.getAppClientId());
		appsScrRequest.setAppScr(appsRegRequest.getAppScr());
		appsScrRequest.setAppScrVldDttm(appsRegRequest.getAppSvcEnDt());
		appsScrRequest.setRegUser(appsRegRequest.getRegUser());
		appsScrRequest.setRegUserId(appsRegRequest.getRegUserId());
		adminRepository.insertAppNewScrHis(appsScrRequest);

		// PORTAL_APP_INFO 테이블에 앱 이동 후 상태 변경
		AppsRegRequest r = new AppsRegRequest();
		r.setAppKey(appsRegRequest.getAppKey());
		r.setAppStatCd("OK");
		r.setAppAplvStatCd("APLV");
		adminRepository.appStatChange(r);
	}

	/**** 앱등록 ****/
	// 앱등록 승인 후 처리
	public void insertAppAfterAplyAdmin(AppsRegRequest appsRegRequest) {

		adminRepository.insertAppInfoNiceAdmin(appsRegRequest);

		// app secret his 테이블에 시크릿 추가
		AppsScrRequest appsScrRequest = new AppsScrRequest();
		appsScrRequest.setAppKey(appsRegRequest.getAppKey());
		appsScrRequest.setAppClientId(appsRegRequest.getAppClientId());
		appsScrRequest.setAppScr(appsRegRequest.getAppScr());
		appsScrRequest.setAppScrVldDttm(appsRegRequest.getAppSvcEnDt());
		appsScrRequest.setRegUser(appsRegRequest.getRegUser());
		appsScrRequest.setRegUserId(appsRegRequest.getRegUserId());
		adminRepository.insertAppNewScrHis(appsScrRequest);

		// PORTAL_APP_INFO 테이블에 앱 이동 후 상태 변경
		AppsRegRequest r = new AppsRegRequest();
		r.setAppKey(appsRegRequest.getAppKey());
		r.setAppStatCd("OK");
		r.setAppAplvStatCd("APLV");
		adminRepository.appStatChange(r);
	}

	// 이용자포털 앱 등록
	public void insertAppNice(AppsRegRequest appsRegRequest) {

        boolean isBrnId = CommonUtil.brnCheck(appsRegRequest.getCopRegNo());

        if (isBrnId) {
            try {
                AplvRegistRequest aplvRequest = new AplvRegistRequest();
                aplvRequest.setRegUserId(appsRegRequest.getRegUserId());
                aplvRequest.setHfnCd(appsRegRequest.getHfnCd());

                List<String> lstGrantType = new ArrayList<String>();
                List<String> lstScope = new ArrayList<String>();

                ProductRequest productRequest;
                ProductVO productDetailVo;
                for (ProductVO vo : appsRegRequest.getPrdList()) {
                    vo.setRegUserId(appsRegRequest.getRegUserId());
                    vo.setAppKey(appsRegRequest.getAppKey());

                    adminRepository.insertAppPrdInfo(vo);

                    aplvRequest.setAplvReqCd(vo.getAppPrdSeqNo());
                    aplvRequest.setAplvDivCd("APPPRDREG");
                    aplvRequest.setAplvReqCtnt(vo.getPrdNm());
                    adminRepository.insertAplv(aplvRequest);

                    //grant_type, scope 수집
                    productRequest = new ProductRequest();
                    productRequest.setPrdId(Integer.parseInt(vo.getPrdId()));
                    productDetailVo = adminRepository.selectPrdDetail(productRequest);
                    if (productDetailVo.getGrantType() != null && !productDetailVo.getGrantType().equals("")) {
                        lstGrantType.addAll(Arrays.asList(productDetailVo.getGrantType().split(",")));
                    }
                    if (productDetailVo.getScope() != null && !productDetailVo.getScope().equals("")) {
                        lstScope.addAll(Arrays.asList(productDetailVo.getScope().split(",")));
                    }
                }

                if (appsRegRequest.getCopRegNo() != null && !"".equals(appsRegRequest.getCopRegNo()) &&
                        appsRegRequest.getCopNm() != null && !"".equals(appsRegRequest.getCopNm())) {

                    if (appsRegRequest.getOwnNm() != null && !"".equals(appsRegRequest.getOwnNm())) {
                        String encryptedOwnNm = cryptoUtil.encrypt(appsRegRequest.getOwnNm());
                        appsRegRequest.setOwnNm(encryptedOwnNm);
                        log.debug("암호화 - 대표자: {}", encryptedOwnNm);
                    }

                    if (appsRegRequest.getDirNm() != null && !"".equals(appsRegRequest.getDirNm())) {
                        String encryptedData = cryptoUtil.encrypt(appsRegRequest.getDirNm());
                        appsRegRequest.setDirNm(encryptedData);
                        log.debug("암호화 - 정산담당자 : {}", encryptedData);
                    }

                    if (appsRegRequest.getDirTelNo2() != null && !"".equals(appsRegRequest.getDirTelNo2())) {
                        String encryptedData = cryptoUtil.encrypt(appsRegRequest.getDirTelNo2());
                        appsRegRequest.setDirTelNo2(encryptedData);
                        log.debug("암호화 - 정산담당자 전화번호: {}", encryptedData);
                    }

                    if (appsRegRequest.getDirEmail() != null && !"".equals(appsRegRequest.getDirEmail())) {
                        String encryptedData = cryptoUtil.encrypt(appsRegRequest.getDirEmail());
                        appsRegRequest.setDirEmail(encryptedData);
                        log.debug("암호화 - 정산담당자 이메일: {}", encryptedData);
                    }

                    adminRepository.insertAppCopInfo(appsRegRequest);
                    adminRepository.backupAppCopInfo(appsRegRequest);
                }

                insertAppCnlKey(appsRegRequest, "Y");

                TreeSet<String> trsetGrantType = new TreeSet<String>(lstGrantType);
                TreeSet<String> trScope = new TreeSet<String>(lstScope);
                String strGrantTypes = String.join(",",trsetGrantType);
                String strScope = String.join(",",trScope);

                GWResponse oAuthResponse = GWCommunicater.createClientInfo(appsRegRequest.getPrdKind(), strGrantTypes, strScope, "20991231", appsRegRequest.getRegUserId());
                Map<String, Object> oAuthBodyResponse = oAuthResponse.getDataBody();
                String decryptId = (String) oAuthBodyResponse.get("clientId");
                String buf = (String) oAuthBodyResponse.get("clientSecret");
                String encryptScr = cryptoUtil.encrypt(buf);

                appsRegRequest.setAppClientId(decryptId);
                appsRegRequest.setAppScr(encryptScr);
                appsRegRequest.setAppSvcStDt(appsRegRequest.getAppSvcStDt());
                appsRegRequest.setAppSvcEnDt(appsRegRequest.getAppSvcEnDt());

                this.insertAppInfo(appsRegRequest);
                aplvRequest.setAplvReqCd(appsRegRequest.getAppKey());
                aplvRequest.setAplvDivCd("APP");
                aplvRequest.setAplvReqCtnt(appsRegRequest.getAppNm());
                appsRegRequest.setUserKey(appsRegRequest.getRegUserId());
                adminRepository.insertAplv2(aplvRequest);
                adminRepository.backupAppInfo(appsRegRequest);
                adminRepository.backupAppOwnerInfo(appsRegRequest);
                // INSERT INTO PORTAL_APP_OWNER_HIS

                // IP정보를 사용 중으로 상태 변경
                AppsCnlKeyRequest acr = new AppsCnlKeyRequest();
                acr.setAppKey(appsRegRequest.getAppKey());
                adminRepository.updateAppCnlInfo(acr);
                // UPDATE PORTAL_APP_CHANNL_INFO : USE_FL = 'Y'

                if (appsRegRequest.getPrdKind().equals("10") || appsRegRequest.getPrdKind().equals("11")) {
                    // 앱에 등록된 상품목록 조회
                    com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest aplvReq = new com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest();
                    aplvReq.setAppKey(appsRegRequest.getAppKey());
                    aplvReq.setSearchPrdCtgy(appsRegRequest.getHfnCd());
                    aplvReq.setProcId(appsRegRequest.getRegUserId());
                    aplvReq.setProcUser(appsRegRequest.getRegUser());
                    // 기본 구분값은 앱-상품승인 대기
                    aplvReq.setSearchAplvStatCd("WAIT");
                    aplvReq.setPageIdx(1);
                    aplvReq.setPageSize(20); // 백오피스에서 전달하는 상품은 1개밖에 없음..
                    aplvReq.setPageOffset((aplvReq.getPageIdx() - 1) * aplvReq.getPageSize());
                    aplvReq.setTrnscCall("APPREG");

                    AplvRsponsePaging aplvRes = aplvService.selectPrdAplvListPaging(aplvReq);
                    // 상품 승인완료처리
                    for (PrdAplvListVO vo : aplvRes.getList()) {
                        // 승인완료 처리
                        AplvApprovalRequest aplvApprovalRequest = new AplvApprovalRequest();
                        aplvApprovalRequest.setRegUser(appsRegRequest.getRegUserId());
                        aplvApprovalRequest.setProcUser(appsRegRequest.getRegUserId());
                        aplvApprovalRequest.setProcId(appsRegRequest.getRegUserId());
                        aplvApprovalRequest.setPrdCtgy(vo.getPrdCtgy());
                        aplvApprovalRequest.setAplvSeqNo(vo.getAplvSeqNo());
                        aplvService.aplvApproval(aplvApprovalRequest);
                    }
                }

                AdminRequest.ApiDetailRequest apiDetailRequest = new AdminRequest.ApiDetailRequest();
                apiDetailRequest.setAppKey(appsRegRequest.getAppKey());
                List<ApiVO> apiList = adminRepository.selectAppApiList(apiDetailRequest);

                // REDIS SET : CLIENTID-VALUE
                RedisCommunicater.appRedisSet(decryptId, "true");

                // REDIS SET : CLIENTID-API-VALUE
                for (ApiVO apiInfo : apiList) {
                    RedisCommunicater.appApiRedisSet(decryptId, apiInfo.getApiUrl(), "true");
                }

                // REDIS SET : CLIENTID-IP
                List<AppsCnlKeyRequest> apiChannlList = appsRegRequest.getCnlKeyList();
                for (AppsCnlKeyRequest appCnlInfo : apiChannlList) {
                    RedisCommunicater.appIpRedisSet(decryptId, appCnlInfo.getCnlKey(), "true");
                }

                this.sendMail(appsRegRequest);

            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BusinessException("E075", messageSource.getMessage("E075"));
            }
        } else {
            throw new BusinessException("E019", messageSource.getMessage("E019"));
        }
	}

    private void sendMail(AppsRegRequest request) throws Exception {

        // 발송구분 : 앱등록 완료
        MailSmsVO vo = new MailSmsVO();
        vo = appsRepository.selectSendApp(request);
        vo.setMailType("API_001");
        vo.setEmail(cryptoUtil.decrypt(vo.getEmail()));
        vo.setTagmap001("admin@mail.niceapi.co.kr");
        vo.setTagmap005(ServiceCategoryEnum.resolve(vo.getTagmap005()).getName());
        vo.setTagmap006(etcEnum.resolveByCd(vo.getTagmap006()).getName());

        List<MailSmsVO> list = new ArrayList<>();
        list.add(vo);

        mailUtils.makeMailData(list, false);

        // 발송구분 : 승인요청
        if (request.getPrdKind().equals("20") || request.getPrdKind().equals("21")) {

            HfnUserRequest hfnUserRequest = new HfnUserRequest();
            hfnUserRequest.setSearchHfnCd(request.getHfnCd());
            List<String> adminUser = appsRepository.selectSendPrdList(hfnUserRequest);

            MailSmsVO vo2 = appsRepository.selectSendPrd(request);

            List<MailSmsVO> mailList2 = new ArrayList<MailSmsVO>();
            for(String email: adminUser) {
                MailSmsVO vo3 = new MailSmsVO();
                BeanUtils.copyProperties(vo2, vo3);
                vo3.setMailType("API_006");
                vo3.setEmail(email);
                vo3.setTagmap004(ServiceCategoryEnum.resolve(vo2.getTagmap004()).getName());
                vo3.setTagmap005(etcEnum.resolveByCd(vo2.getTagmap005()).getName());
                mailList2.add(vo3);
            }
            if(mailList2 != null && mailList2.size() > 0) {
                mailUtils.makeMailData(mailList2, false);
            }
        }
    }

    // 관리자포털 앱 등록(승인전)
	public void insertAppNiceAdmin(AppsRegRequest appsRegRequest) {

	    boolean isBrnId = CommonUtil.brnCheck(appsRegRequest.getCopRegNo());

        if (isBrnId) {
            try {
                String strPrdKind = appsRegRequest.getPrdKind();

                AplvRegistRequest aplvRequest = new AplvRegistRequest();
                aplvRequest.setRegUserId(appsRegRequest.getRegUserId());
                aplvRequest.setHfnCd(appsRegRequest.getHfnCd());

                List<String> lstGrantType = new ArrayList<String>();
                List<String> lstScope = new ArrayList<String>();

                for (ProductVO vo : appsRegRequest.getPrdList()) {
                    vo.setRegUserId(appsRegRequest.getRegUserId());
                    vo.setAppKey(appsRegRequest.getAppKey());

                    adminRepository.insertAppPrdInfo(vo);

                    aplvRequest.setAplvReqCd(vo.getAppPrdSeqNo());
                    aplvRequest.setAplvDivCd("APPPRDREG");
                    aplvRequest.setAplvReqCtnt(vo.getPrdNm());
                    adminRepository.insertAplv(aplvRequest);

                    //grant_type, scope 수집
                    if (vo.getGrantType() != null && !vo.getGrantType().equals("")) {
                        lstGrantType.addAll(Arrays.asList(vo.getGrantType().split(",")));
                    }
                    if (vo.getScope() != null && !vo.getScope().equals("")) {
                        lstScope.addAll(Arrays.asList(vo.getScope().split(",")));
                    }
                }
                TreeSet<String> trsetGrantType = new TreeSet<String>(lstGrantType);
                ArrayList<String> arrGrantType = new ArrayList<String>(trsetGrantType);

                TreeSet<String> trScope = new TreeSet<String>(lstScope);
                ArrayList<String> arrScope = new ArrayList<String>(trScope);

                String copRegNo = appsRegRequest.getCopRegNo();
                String copNm = appsRegRequest.getCopNm();

                if (copRegNo != null && !"".equals(copRegNo) && copNm != null && !"".equals(copNm)) {
                    if (appsRegRequest.getOwnNm() != null && !"".equals(appsRegRequest.getOwnNm())) {
                        String encryptedOwnNm = cryptoUtil.encrypt(appsRegRequest.getOwnNm());
                        appsRegRequest.setOwnNm(encryptedOwnNm);
                        log.debug("암호화 - 대표자: {}", encryptedOwnNm);
                    }

                    if (appsRegRequest.getDirNm() != null && !"".equals(appsRegRequest.getDirNm())) {
                        String encryptedData = cryptoUtil.encrypt(appsRegRequest.getDirNm());
                        appsRegRequest.setDirNm(encryptedData);
                        log.debug("암호화 - 정산담당자 : {}", encryptedData);
                    }

                    if (appsRegRequest.getDirTelNo2() != null && !"".equals(appsRegRequest.getDirTelNo2())) {
                        String encryptedData = cryptoUtil.encrypt(appsRegRequest.getDirTelNo2());
                        appsRegRequest.setDirTelNo2(encryptedData);
                        log.debug("암호화 - 정산담당자 전화번호: {}", encryptedData);
                    }

                    if (appsRegRequest.getDirEmail() != null && !"".equals(appsRegRequest.getDirEmail())) {
                        String encryptedData = cryptoUtil.encrypt(appsRegRequest.getDirEmail());
                        appsRegRequest.setDirEmail(encryptedData);
                        log.debug("암호화 - 정산담당자 이메일: {}", encryptedData);
                    }

                    adminRepository.insertAppCopInfo(appsRegRequest);
                    adminRepository.backupAppCopInfo(appsRegRequest);
                }

                insertAppCnlKey(appsRegRequest, "Y");

                AppsRequest appsRequest = new AppsRequest();
                appsRequest.setAppKey(appsRegRequest.getAppKey());

                GWResponse oAuthResponse = GWCommunicater.createClientInfo(strPrdKind,
                        String.join(",", arrGrantType),
                        String.join(",", arrScope),
                        "20991231",
                        appsRegRequest.getRegUserId());
                Map<String, Object> oAuthBodyResponse = oAuthResponse.getDataBody();

                String encryptScr = "";
                String decryptId = (String) oAuthBodyResponse.get("clientId");
                String buf = (String) oAuthBodyResponse.get("clientSecret");

                encryptScr = cryptoUtil.encrypt(buf);

                appsRegRequest.setAppClientId(decryptId);
                appsRegRequest.setAppScr(encryptScr);
                appsRegRequest.setAppSvcStDt(appsRegRequest.getAppSvcStDt());
                appsRegRequest.setAppSvcEnDt(appsRegRequest.getAppSvcEnDt());

                this.insertAppAfterAplyAdmin(appsRegRequest);
                aplvRequest.setAplvReqCd(appsRegRequest.getAppKey());
                aplvRequest.setAplvDivCd("APP");
                aplvRequest.setAplvReqCtnt(appsRegRequest.getAppNm());
                adminRepository.insertAplv2(aplvRequest);
                adminRepository.backupAppInfo(appsRegRequest);
                adminRepository.backupAppOwnerInfo(appsRegRequest);

                // api정보, 채널정보를 사용중으로 상태변경
                AppsCnlKeyRequest acr = new AppsCnlKeyRequest();
                List<AppsBlockKeyRequest> ablr = appsRegRequest.getBlockKeyList();
                acr.setAppKey(appsRequest.getAppKey());

                for (AppsBlockKeyRequest req : ablr) {
                    req.setAppKey(appsRequest.getAppKey());
                    req.setRegUser(appsRegRequest.getRegUserId());
                    appsRepository.insertAppBlackListCnl(req);
                }

                adminRepository.updateAppCnlInfo(acr);

                if (appsRegRequest.getPrdKind().equals("10") || appsRegRequest.getPrdKind().equals("11")) {
                    // 앱에 등록된 상품목록 조회.
                    com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest aplvReq = new com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest();
                    aplvReq.setAppKey(appsRequest.getAppKey());
                    aplvReq.setSearchPrdCtgy(appsRegRequest.getHfnCd());
                    aplvReq.setProcId(appsRegRequest.getRegUserId());
                    aplvReq.setProcUser(appsRegRequest.getRegUser());
                    // 기본 구분값은 앱-상품승인 대기
                    aplvReq.setSearchAplvStatCd("WAIT");
                    aplvReq.setPageIdx(1);
                    aplvReq.setPageSize(20); // 백오피스에서 전달하는 상품은 1개밖에 없음..
                    aplvReq.setPageOffset((aplvReq.getPageIdx() - 1) * aplvReq.getPageSize());

                    AplvRsponsePaging aplvRes = aplvService.selectPrdAplvListPaging(aplvReq);
                    // 상품 승인완료처리
                    for (PrdAplvListVO vo : aplvRes.getList()) {
                        // 승인완료 처리
                        AplvApprovalRequest aplvApprovalRequest = new AplvApprovalRequest();
                        aplvApprovalRequest.setRegUser(appsRegRequest.getRegUserId());
                        aplvApprovalRequest.setProcUser(appsRegRequest.getRegUserId());
                        aplvApprovalRequest.setProcId(appsRegRequest.getRegUserId());
                        aplvApprovalRequest.setPrdCtgy(vo.getPrdCtgy());
                        aplvApprovalRequest.setAplvSeqNo(vo.getAplvSeqNo());
                        aplvService.aplvApproval(aplvApprovalRequest);

//					// redis set
//					// APP PRD Redis set
//					RedisCommunicater.appPrdRedisSet(decryptId, vo.getPrdId(), "true");
                    }
                }

                AdminRequest.ApiDetailRequest apiDetailRequest = new AdminRequest.ApiDetailRequest();
                apiDetailRequest.setAppKey(appsRegRequest.getAppKey());

                // Redis에 API SET
                List<ApiVO> apiList = adminRepository.selectAppApiList(apiDetailRequest);

                // Redis에 app 정보 세팅
                RedisCommunicater.appRedisSet(decryptId, "true");

                for (ApiVO apiInfo : apiList) {
                    RedisCommunicater.appApiRedisSet(decryptId, apiInfo.getApiUrl(), "true");
                }

                // Redis에 APP IP 등록
                List<AppsCnlKeyRequest> apiChannlList = appsRegRequest.getCnlKeyList();
                for (AppsCnlKeyRequest appCnlInfo : apiChannlList) {
                    RedisCommunicater.appIpRedisSet(decryptId, appCnlInfo.getCnlKey(), "true");
                }

                // Redis에 모든 IP 허용여부
                RedisCommunicater.appAllowAllIpRedisSet(decryptId, appsRegRequest.getAppAnyCnlYn());

                // Redis에 APP IP 등록
                String blackListIps = "";
                List<AppsBlockKeyRequest> blackChannlList = appsRegRequest.getBlockKeyList();
                for (AppsBlockKeyRequest appBlackCnlInfo : blackChannlList) {
                    blackListIps += appBlackCnlInfo.getCnlKey() + ",";
                }
                if (blackListIps != null && !blackListIps.equals("")) {
                    RedisCommunicater.appBlackIpRedisSet(decryptId, blackListIps.substring(0, blackListIps.length() - 1));
                }
            } catch (Exception e) {
                log.error(e.toString());
                throw new BusinessException("E075", messageSource.getMessage("E075"));
            }
        } else {
            throw new BusinessException("E019", messageSource.getMessage("E019"));
        }
	}

	public void deleteAppNice(AppsRegRequest appsRegRequest) {
		// 변경 테이블에서 앱 정보 가져옴
		AppsRequest appsRequest = new AppsRequest();
		appsRequest.setAppKey(appsRegRequest.getAppKey());
		appsRequest.setSearchUserKey(appsRegRequest.getUserKey());
		AppsVO appInfo = appsRepository.selectAppModifyDetail(appsRequest); // APP_INFO 테이블에서 가져옵니다.(혼동주의)
		
		appsRegRequest.setAppStatCd("DEL");
		appsRegRequest.setAppAplvStatCd("APLV");
		
		// 메인테이블 상태 CLOSE->DEL 변경
		appInfo.setAppStatCd("DEL");
		adminRepository.appStatChange(appsRegRequest);

		// 히스토리 테이블에 이력 삽입
		adminRepository.backupAppInfo(appsRegRequest);

		// 변경 테이블에 있는 내용 삭제
		appsRepository.deleteAppMod(appsRegRequest);
		
		// Redis app api del
		List<AppCnlInfoVO> appCnlInfoList = appsRepository.selectAppCnlInfo(appsRequest);
		
		AdminRequest.ApiDetailRequest apiDetailRequest = new AdminRequest.ApiDetailRequest();
		apiDetailRequest.setAppKey(appsRegRequest.getAppKey());
		
		List<ApiVO> apiList = adminRepository.selectAppApiList(apiDetailRequest);
		
		List<AppPrdInfoVO> appPrdInfoList = appsRepository.selectApPrdInfoDetailALL(appsRequest);
		// 상품 승인완료처리.
		String decryptId = appsRegRequest.getAppClientId();
		for (AppPrdInfoVO vo : appPrdInfoList) {
			// redis del
			// APP PRD Redis del
			RedisCommunicater.productCodeRedisDel(decryptId, vo.getPrdCd());
		}
		
		// Redis Delete
		try {
			for (ApiVO apiInfo : apiList) {
				RedisCommunicater.appApiRedisDel(appInfo.getAppClientId(), apiInfo.getApiUrl());
			}
		} catch (BusinessException e) {
			for(ApiVO apiInfo : apiList){
				RedisCommunicater.appApiRedisSet(appInfo.getAppClientId(), apiInfo.getApiUrl(), "true");
			}
		} catch (Exception e) {
			for(ApiVO apiInfo : apiList){
				RedisCommunicater.appApiRedisSet(appInfo.getAppClientId(), apiInfo.getApiUrl(), "true");
			}
		}
		try {
			for(AppCnlInfoVO cnlInfo : appCnlInfoList){
				RedisCommunicater.appIpRedisDel(appInfo.getAppClientId(), cnlInfo.getCnlKey());
			}
		} catch (BusinessException e) {
			for(AppCnlInfoVO cnlInfo : appCnlInfoList){
				RedisCommunicater.appIpRedisSet(appInfo.getAppClientId(), cnlInfo.getCnlKey(), "true");
			}
		} catch (Exception e) {
			for(AppCnlInfoVO cnlInfo : appCnlInfoList){
				RedisCommunicater.appIpRedisSet(appInfo.getAppClientId(), cnlInfo.getCnlKey(), "true");
			}
		}
		try {
			RedisCommunicater.appRedisDel(appInfo.getAppClientId());
		} catch (BusinessException e) {
			RedisCommunicater.appRedisSet(appInfo.getAppClientId(),"true");
		} catch (Exception e) {
			RedisCommunicater.appRedisSet(appInfo.getAppClientId(),"true");
		}

        // APP 사용량 랜덤제어 정책 삭제
        AppUsageRatioRequest appUsageRatioRequest = new AppUsageRatioRequest();
        appUsageRatioRequest.setModUser(appsRegRequest.getModUser());
        policyService.deleteAppUsageRatioPolicy(appUsageRatioRequest);
	}

	public void insertAppCnlKey(AppsRegRequest appsRegRequest, String useFL) {
		for (AppsCnlKeyRequest cnlKey : appsRegRequest.getCnlKeyList()) {
			cnlKey.setAppKey(appsRegRequest.getAppKey());
			cnlKey.setRegUser(appsRegRequest.getRegUser());
			cnlKey.setUseFl(useFL);
			cnlKey.setRegUserId(appsRegRequest.getRegUserId());
			adminRepository.insertAppCnlKey(cnlKey);
		}
	}

	public void insertAppApiInfo(AppsRegRequest appsRegRequest, String useFL) {
		for (AppsApiInfoRequest apiInfo : appsRegRequest.getApiList()) {
			apiInfo.setAppKey(appsRegRequest.getAppKey());
			apiInfo.setRegUser(appsRegRequest.getRegUser());
			ApiRequest apiRequest = new ApiRequest();
			apiRequest.setApiId(apiInfo.getApiId());
			apiInfo.setHfnCd(appsRepository.selectApi(apiRequest).getHfnCd());
			apiInfo.setUseFl(useFL);
			apiInfo.setRegUserId(appsRegRequest.getRegUserId());
			appsRepository.insertAppApiInfo(apiInfo);
		}

		AppsRequest appsReq = new AppsRequest();
		appsReq.setAppKey(appsRegRequest.getAppKey());
		// 기존에 존재한 API 구분
		List<AppApiInfoVO> oldApiList = appsRepository.selectAppApiInfo(appsReq);
		for (AppsApiInfoRequest apiInfo : appsRegRequest.getApiList()) {
			for (AppApiInfoVO appApiInfoVO : oldApiList) {
				if (StringUtils.equals(apiInfo.getApiId(), appApiInfoVO.getApiId())) {
					apiInfo.setIsExist("EXIST");
				}
			}
		}

		for (AppsApiInfoRequest apiInfo : appsRegRequest.getApiList()) {
			RequestApiVO requestApiVO = new RequestApiVO();
			requestApiVO.setAppKey(appsRegRequest.getAppKey());
			requestApiVO.setApiId(apiInfo.getApiId());
			requestApiVO.setRegUserId(appsRegRequest.getRegUserId());

			if (appsRegRequest.getAppSvcStDt() != null && appsRegRequest.getAppSvcEnDt() != null) {
				String stDt = appsRegRequest.getAppSvcStDt();
				String enDt = appsRegRequest.getAppSvcEnDt();

				requestApiVO.setStDt(this.parseDateFormat(stDt));
				requestApiVO.setEnDt(this.parseDateFormat(enDt));
			} else {
				requestApiVO.setStDt(null);
				requestApiVO.setEnDt(null);
			}

			if (apiInfo.getIsExist() == null) {
				requestApiVO.setRegUser(appsRegRequest.getRegUser());
				settingRepository.insertApiChargeDiscountRate(requestApiVO);
			}
		}

		// 삭제된 API 구분
		for (AppApiInfoVO appApiInfoVO : oldApiList) {
			for (AppsApiInfoRequest apiInfo : appsRegRequest.getApiList()) {
				if (StringUtils.equals(apiInfo.getApiId(), appApiInfoVO.getApiId())) {
					appApiInfoVO.setIsExist("EXIST");
				}
			}
		}

		for (AppApiInfoVO appApiInfoVO : oldApiList) {
			if (appApiInfoVO.getIsExist() == null) {
				RequestApiVO requestApiVO = new RequestApiVO();
				requestApiVO.setAppKey(appsRegRequest.getAppKey());
				requestApiVO.setApiId(appApiInfoVO.getApiId());
				requestApiVO.setRegUserId(appsRegRequest.getRegUserId());
				settingRepository.updateDiscountByNForAppKeyAndApiId(requestApiVO);
			}
		}
	}

	private String parseDateFormat(String date) {
		SimpleDateFormat beforeFormat = new SimpleDateFormat("yyyymmdd");
		SimpleDateFormat afterFormat = new SimpleDateFormat("yyyy-mm");

		Date tempDate = null;

		try {
			tempDate = beforeFormat.parse(date);

			return afterFormat.format(tempDate);
		} catch (ParseException e) {
			log.error("parsing erorr.", e);
		} catch (Exception e) {
			log.error("parsing erorr.", e);
		}

		return "";
	}

	/**** 앱등록-승인정보 ****/
	public void insertAplvUseorg(AppsRegRequest appsRegRequest, String aplvDivCd) {

		AplvRegistRequest aplvRegistRequest = new AplvRegistRequest();
		aplvRegistRequest.setAplvDivCd(aplvDivCd);
		aplvRegistRequest.setAplvReqCd(appsRegRequest.getAppKey());
		aplvRegistRequest.setAplvReqCtnt(appsRegRequest.getAppNm());
		aplvRegistRequest.setRegUserName(appsRegRequest.getRegUser());
		aplvRegistRequest.setRegUserId(appsRegRequest.getRegUserId());

		List<String> userparamList = new ArrayList<>();

		adminRepository.insertAplv(aplvRegistRequest);

		aplvRegistRequest.setAplvSeqNo(aplvRegistRequest.getAplvSeqNo());

		AplvRequest.AplvHisRegistRequest aplvHisRegistRequest = new AplvRequest.AplvHisRegistRequest();
		aplvHisRegistRequest.setAplvSeqNo(aplvRegistRequest.getAplvSeqNo());
		aplvHisRegistRequest.setProcUser(appsRegRequest.getRegUser());
		aplvHisRegistRequest.setProcId(appsRegRequest.getUserKey());
		aplvHisRegistRequest.setRegUserName("APLV_SYSTEM");
		aplvHisRegistRequest.setRegUserId("APLV_SYSTEM");

		settingRepository.insertAplvHis(aplvHisRegistRequest);
	}

	public List<AppsVO> selectAppAuth(AppsRequest request) {

		return appsRepository.selectAppAuth(request);
	}

	public List<AppsVO> selectEditAppAuth(AppsRequest request) {

		return appsRepository.selectEditAppAuth(request);
	}

	public AppsResponse selectAppDetail(AppsRequest appsRequest) {
		AppsVO apps;
		List<AppPrdInfoVO> appPrdInfoList;
		List<AppCnlInfoVO> appCnlInfoList;
		List<AppReadInfoVO> appReadInfoList;

		apps = appsRepository.selectAppModifyDetail(appsRequest); // APP_INFO 테이블에서 가져옵니다.(혼동주의)
		// 앱 채널 정보, 앱 API 정보
		appPrdInfoList = appsRepository.selectApPrdInfoDetail(appsRequest);
		appCnlInfoList = appsRepository.selectAppCnlInfo(appsRequest);
		appReadInfoList = appsRepository.selectAppReadInfoList(appsRequest);

		for (AppReadInfoVO vo : appReadInfoList) {
			try {
				String userNm = vo.getUserNm();
				String userEmail = vo.getUserEmail();
				String userTel = vo.getUserTel();
				// 이름
				if (userNm != null && !"".equals(userNm)) {
					String decryptedUserNm = cryptoUtil.decrypt(userNm);
					vo.setUserNm(this.name(decryptedUserNm));
				}

				if (userEmail != null && !"".equals(userEmail)) {
					String decryptedEmail = cryptoUtil.decrypt(userEmail);
					vo.setUserEmail(this.email(decryptedEmail));
				}

				if (userTel != null && !"".equals(userTel)) {
					String decryptedTel = cryptoUtil.decrypt(userTel);
					vo.setUserTel(this.phoneNum(decryptedTel));
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		if (StringUtils.equals(apps.getRegUserNmEncrypted(), "Y")) {
			try {
				// 이름
				if (apps.getRegUser() != null && !"".equals(apps.getRegUser())) {
					String decryptedUserNm = cryptoUtil.decrypt(apps.getRegUser());
					apps.setRegUser(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		if (StringUtils.equals(apps.getModUserNmEncrypted(), "Y")) {
			try {
				// 이름
				if (apps.getModUser() != null && !"".equals(apps.getModUser())) {
					String decryptedUserNm = cryptoUtil.decrypt(apps.getModUser());
					apps.setModUser(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		try {
			// 이름
			if (apps.getManagerNm() != null && !"".equals(apps.getManagerNm())) {
				String decryptedUserNm = cryptoUtil.decrypt(apps.getManagerNm());
				apps.setManagerNm(decryptedUserNm);
				log.debug("복호화 - 이름: {}", decryptedUserNm);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}

		AppsResponse response = new AppsResponse();
		String appScr = apps.getAppScr();
		String newAppScr = apps.getNewAppScr();

		try {
			if (appScr != null && !appScr.isEmpty()) {
				response.setAppScr(cryptoUtil.decrypt(appScr));
			}
			if (newAppScr != null && !newAppScr.isEmpty()) {
				response.setNewAppScr(cryptoUtil.decrypt(newAppScr));
			}
		} catch (Exception e) {
			log.error("Client Secret DecryptAES256 Error", e);
		}

		response.setAppClientId(apps.getAppClientId());
		response.setNewAppClientId(apps.getNewAppClientId());
		response.setAppScr(apps.getAppScr());
		response.setNewAppScr(apps.getNewAppScr());
		response.setAppKey(apps.getAppKey());
		response.setAppNm(apps.getAppNm());
		response.setAppStatCd(apps.getAppStatCd());
		response.setAppSvcStDt(apps.getAppSvcStDt());
		response.setAppSvcEnDt(apps.getAppSvcEnDt());
		response.setAppCtnt(apps.getAppCtnt());
		response.setTermEtdYn(apps.getTermEtdYn());
		response.setAppScrReisuYn(apps.getAppScrReisuYn());
		response.setUserKey(apps.getUserKey());
		response.setUseorgNm(apps.getUseorgNm());
		response.setAppScrVldDttm(apps.getAppScrVldDttm());
		response.setAppPrdInfo(appPrdInfoList);
		response.setDlDttm(apps.getDlDttm());
		response.setCnlKeyList(appCnlInfoList);
		response.setAppAplvStatCd(apps.getAppAplvStatCd());
		response.setRegDttm(apps.getRegDttm());
		response.setRegUser(apps.getRegUser());
		response.setModDttm(apps.getModDttm());
		response.setModUser(apps.getModUser());
		response.setAppReadInfo(appReadInfoList);

		response.setCopRegNo(apps.getCopRegNo());
		response.setCopNm(apps.getCopNm());
		response.setOwnNm(apps.getOwnNm());
		response.setCopAddr1(apps.getCopAddr1());
		response.setCopAddr2(apps.getCopAddr2());
		response.setCopAddrNo(apps.getCopAddrNo());
		response.setBizType(apps.getBizType());
		response.setBizCdtn(apps.getBizCdtn());
		response.setDirNm(apps.getDirNm());
		response.setDirTelNo1(apps.getDirTelNo1());
		response.setDirTelNo2(apps.getDirTelNo2());
		response.setDirEmail(apps.getDirEmail());
		response.setDirDomain(apps.getDirDomain());
		response.setAppSvcNm(apps.getAppSvcNm());
		response.setAppSvcUrl(apps.getAppSvcUrl());

		response.setManagerNm(apps.getManagerNm());
		response.setManagerId(apps.getManagerId());

		response.setPrdKind(apps.getPrdKind());
		response.setHfnCd(apps.getHfnCd());

		response.setAppDiv(apps.getAppDiv());

		return response;
	}

	public AppsResponse selectAppDetailModNice(AppsRequest appsRequest) {
		AppsVO apps;
		List<AppPrdInfoVO> appPrdInfoList;
		List<AppCnlInfoVO> appCnlInfoList;
		List<AppReadInfoVO> appReadInfoList;

		apps = appsRepository.selectAppModifyDetail(appsRequest); // APP_INFO 테이블에서 가져옵니다.(혼동주의)
		// 앱 채널 정보, 앱 API 정보
		appPrdInfoList = appsRepository.selectApPrdInfoDetailALL(appsRequest);
		appCnlInfoList = appsRepository.selectAppCnlInfo(appsRequest);
		appReadInfoList = appsRepository.selectAppReadInfoList(appsRequest);

		for (AppReadInfoVO vo : appReadInfoList) {
			try {
				String userNm = vo.getUserNm();
				String userEmail = vo.getUserEmail();
				String userTel = vo.getUserTel();
				// 이름
				if (userNm != null && !"".equals(userNm)) {
					String decryptedUserNm = cryptoUtil.decrypt(userNm);
					vo.setUserNm(this.name(decryptedUserNm));
				}

				if (userEmail != null && !"".equals(userEmail)) {
					String decryptedEmail = cryptoUtil.decrypt(userEmail);
					vo.setUserEmail(this.email(decryptedEmail));
				}

				if (userTel != null && !"".equals(userTel)) {
					String decryptedTel = cryptoUtil.decrypt(userTel);
					vo.setUserTel(this.phoneNum(decryptedTel));
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		if (StringUtils.equals(apps.getRegUserNmEncrypted(), "Y")) {
			try {
				// 이름
				if (apps.getRegUser() != null && !"".equals(apps.getRegUser())) {
					String decryptedUserNm = cryptoUtil.decrypt(apps.getRegUser());
					apps.setRegUser(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		if (StringUtils.equals(apps.getModUserNmEncrypted(), "Y")) {
			try {
				// 이름
				if (apps.getModUser() != null && !"".equals(apps.getModUser())) {
					String decryptedUserNm = cryptoUtil.decrypt(apps.getModUser());
					apps.setModUser(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		try {
			// 이름
			if (apps.getManagerNm() != null && !"".equals(apps.getManagerNm())) {
				String decryptedUserNm = cryptoUtil.decrypt(apps.getManagerNm());
				apps.setManagerNm(decryptedUserNm);
				log.debug("복호화 - 이름: {}", decryptedUserNm);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}

		AppsResponse response = new AppsResponse();
		String appScr = apps.getAppScr();
		String newAppScr = apps.getNewAppScr();

		try {
			if (appScr != null && !appScr.isEmpty()) {
				response.setAppScr(cryptoUtil.decrypt(appScr));
			}
			if (newAppScr != null && !newAppScr.isEmpty()) {
				response.setNewAppScr(cryptoUtil.decrypt(newAppScr));
			}
		} catch (Exception e) {
			log.error("Client Secret DecryptAES256 Error", e);
		}

		response.setAppClientId(apps.getAppClientId());
		response.setNewAppClientId(apps.getNewAppClientId());
		response.setAppScr(apps.getAppScr());
		response.setNewAppScr(apps.getNewAppScr());
		response.setAppKey(apps.getAppKey());
		response.setAppNm(apps.getAppNm());
		response.setAppStatCd(apps.getAppStatCd());
		response.setAppSvcStDt(apps.getAppSvcStDt());
		response.setAppSvcEnDt(apps.getAppSvcEnDt());
		response.setAppCtnt(apps.getAppCtnt());
		response.setTermEtdYn(apps.getTermEtdYn());
		response.setAppScrReisuYn(apps.getAppScrReisuYn());
		response.setUserKey(apps.getUserKey());
		response.setUseorgNm(apps.getUseorgNm());
		response.setAppScrVldDttm(apps.getAppScrVldDttm());
		response.setAppPrdInfo(appPrdInfoList);
		response.setDlDttm(apps.getDlDttm());
		response.setCnlKeyList(appCnlInfoList);
		response.setAppAplvStatCd(apps.getAppAplvStatCd());
		response.setRegDttm(apps.getRegDttm());
		response.setRegUser(apps.getRegUser());
		response.setModDttm(apps.getModDttm());
		response.setModUser(apps.getModUser());
		response.setAppReadInfo(appReadInfoList);

		response.setCopRegNo(apps.getCopRegNo());
		response.setCopNm(apps.getCopNm());
		response.setOwnNm(apps.getOwnNm());
		response.setCopAddr1(apps.getCopAddr1());
		response.setCopAddr2(apps.getCopAddr2());
		response.setCopAddrNo(apps.getCopAddrNo());
		response.setBizType(apps.getBizType());
		response.setBizCdtn(apps.getBizCdtn());
		response.setDirNm(apps.getDirNm());
		response.setDirTelNo1(apps.getDirTelNo1());
		response.setDirTelNo2(apps.getDirTelNo2());
		response.setDirEmail(apps.getDirEmail());
		response.setDirDomain(apps.getDirDomain());
		response.setAppSvcNm(apps.getAppSvcNm());
		response.setAppSvcUrl(apps.getAppSvcUrl());

		response.setManagerNm(apps.getManagerNm());
		response.setManagerId(apps.getManagerId());

		response.setPrdKind(apps.getPrdKind());
		response.setHfnCd(apps.getHfnCd());

		return response;
	}

	public AppsResponse selectAppDetailModNiceAdmin(AppsRequest appsRequest) {
		AppsVO apps;
		List<AppPrdInfoVO> appPrdInfoList;
		List<AppCnlInfoVO> appCnlInfoList;
		List<AppBlackListVO> appBlackList;
		List<AppReadInfoVO> appReadInfoList;

        // APP_INFO 테이블에서 가져옵니다.(혼동주의)
		// PORTAL_APP_INFO T1 , PORTAL_APP_SCR_HIS T2 , PORTAL_APP_COP_INFO T3
		apps = appsRepository.selectAppModifyDetailAdmin(appsRequest);

        // 앱 채널 정보, 앱 API 정보
        appPrdInfoList = appsRepository.selectApPrdInfoDetailALL(appsRequest);  // PORTAL_APP_PRD_INFO T1 PORTAL_PRD_INFO T2 PORTAL_COMM_CODE T3
		appCnlInfoList = appsRepository.selectAppCnlInfo(appsRequest);          // PORTAL_APP_CHANNL_INFO
		appReadInfoList = appsRepository.selectAppReadInfoList(appsRequest);    // PORTAL_APP_READ_INFO T1 PORTAL_USER_INFO T2
		appBlackList = appsRepository.selectAppBlackList(appsRequest);          // PORTAL_APP_BLACKLIST_INFO

		for (AppReadInfoVO vo : appReadInfoList) {
			try {
				String userNm = vo.getUserNm();
				String userEmail = vo.getUserEmail();
				String userTel = vo.getUserTel();
				// 이름
				if (userNm != null && !"".equals(userNm)) {
					String decryptedUserNm = cryptoUtil.decrypt(userNm);
					vo.setUserNm(this.name(decryptedUserNm));
				}

				if (userEmail != null && !"".equals(userEmail)) {
					String decryptedEmail = cryptoUtil.decrypt(userEmail);
					vo.setUserEmail(this.email(decryptedEmail));
				}

				if (userTel != null && !"".equals(userTel)) {
					String decryptedTel = cryptoUtil.decrypt(userTel);
					vo.setUserTel(this.phoneNum(decryptedTel));
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		if (StringUtils.equals(apps.getRegUserNmEncrypted(), "Y")) {
			try {
				// 이름
				if (apps.getRegUser() != null && !"".equals(apps.getRegUser())) {
					String decryptedUserNm = cryptoUtil.decrypt(apps.getRegUser());
					apps.setRegUser(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		if (StringUtils.equals(apps.getModUserNmEncrypted(), "Y")) {
			try {
				// 이름
				if (apps.getModUser() != null && !"".equals(apps.getModUser())) {
					String decryptedUserNm = cryptoUtil.decrypt(apps.getModUser());
					apps.setModUser(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		try {
			// 이름
			if (apps.getManagerNm() != null && !"".equals(apps.getManagerNm())) {
				String decryptedUserNm = cryptoUtil.decrypt(apps.getManagerNm());
				apps.setManagerNm(decryptedUserNm);
				log.debug("복호화 - 이름: {}", decryptedUserNm);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}

		AppsResponse response = new AppsResponse();
		String appScr = apps.getAppScr();
		String newAppScr = apps.getNewAppScr();

		try {
			if (appScr != null && !appScr.isEmpty()) {
				response.setAppScr(cryptoUtil.decrypt(appScr));
			}
			if (newAppScr != null && !newAppScr.isEmpty()) {
				response.setNewAppScr(cryptoUtil.decrypt(newAppScr));
			}
		} catch (Exception e) {
			log.error("Client Secret DecryptAES256 Error", e);
		}

		response.setAppClientId(apps.getAppClientId());
		response.setNewAppClientId(apps.getNewAppClientId());
		response.setAppScr(apps.getAppScr());
		response.setNewAppScr(apps.getNewAppScr());
		response.setAppKey(apps.getAppKey());
		response.setAppNm(apps.getAppNm());
		response.setAppStatCd(apps.getAppStatCd());
		response.setAppSvcStDt(apps.getAppSvcStDt());
		response.setAppSvcEnDt(apps.getAppSvcEnDt());
		response.setAppCtnt(apps.getAppCtnt());
		response.setTermEtdYn(apps.getTermEtdYn());
		response.setAppScrReisuYn(apps.getAppScrReisuYn());
		response.setUserKey(apps.getUserKey());
		response.setUseorgNm(apps.getUseorgNm());
		response.setAppScrVldDttm(apps.getAppScrVldDttm());
		response.setAppPrdInfo(appPrdInfoList);
		response.setDlDttm(apps.getDlDttm());
		response.setCnlKeyList(appCnlInfoList);
		response.setAppAplvStatCd(apps.getAppAplvStatCd());
		response.setRegDttm(apps.getRegDttm());
		response.setRegUser(apps.getRegUser());
		response.setModDttm(apps.getModDttm());
		response.setModUser(apps.getModUser());
		response.setAppReadInfo(appReadInfoList);

		response.setCopRegNo(apps.getCopRegNo());
		response.setCopNm(apps.getCopNm());
		response.setOwnNm(apps.getOwnNm());
		response.setCopAddr1(apps.getCopAddr1());
		response.setCopAddr2(apps.getCopAddr2());
		response.setCopAddrNo(apps.getCopAddrNo());
		response.setBizType(apps.getBizType());
		response.setBizCdtn(apps.getBizCdtn());
		response.setDirNm(apps.getDirNm());
		response.setDirTelNo1(apps.getDirTelNo1());
		response.setDirTelNo2(apps.getDirTelNo2());
		response.setDirEmail(apps.getDirEmail());
		response.setDirDomain(apps.getDirDomain());
		response.setAppSvcNm(apps.getAppSvcNm());
		response.setAppSvcUrl(apps.getAppSvcUrl());

		response.setManagerNm(apps.getManagerNm());
		response.setManagerId(apps.getManagerId());

		response.setPrdKind(apps.getPrdKind());
		response.setHfnCd(apps.getHfnCd());
		response.setBlockKeyList(appBlackList);
		response.setAppMemo(apps.getAppMemo());
		response.setAppAnyCnlYn(apps.getAppAnyCnlYn());

		return response;
	}

	private String phoneNum(String str) {
		String replaceString = str;

		Matcher matcher = Pattern.compile("^(\\d{3})-?(\\d{3,4})-?(\\d{4})$").matcher(str);

		if (matcher.matches()) {
			replaceString = "";

			boolean isHyphen = false;
			if (str.indexOf("-") > -1) {
				isHyphen = true;
			}

			for (int i = 1; i <= matcher.groupCount(); i++) {
				String replaceTarget = matcher.group(i);
				if (i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');

					replaceString = replaceString + String.valueOf(c);
				} else {
					replaceString = replaceString + replaceTarget;
				}

				if (isHyphen && i < matcher.groupCount()) {
					replaceString = replaceString + "-";
				}
			}
		}

		return replaceString;
	}

	private String name(String str) {
		String replaceString = str;

		String pattern = "";
		if (str.length() == 2) {
			pattern = "^(.)(.+)$";
		} else {
			pattern = "^(.)(.+)(.)$";
		}

		Matcher matcher = Pattern.compile(pattern).matcher(str);

		if (matcher.matches()) {
			replaceString = "";

			for (int i = 1; i <= matcher.groupCount(); i++) {
				String replaceTarget = matcher.group(i);
				if (i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');

					replaceString = replaceString + String.valueOf(c);
				} else {
					replaceString = replaceString + replaceTarget;
				}

			}
		}

		return replaceString;
	}

	private String email(String str) {
		String replaceString = str;

		Matcher matcher = Pattern.compile("^(..)(.*)([@]{1})(.*)$").matcher(str);

		if (matcher.matches()) {
			replaceString = "";

			for (int i = 1; i <= matcher.groupCount(); i++) {
				String replaceTarget = matcher.group(i);
				if (i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');

					replaceString = replaceString + String.valueOf(c);
				} else {
					replaceString = replaceString + replaceTarget;
				}
			}

		}

		return replaceString;
	}

	// 앱 수정시 수정테이블에 삽입
	public void appUpdateAdmin(AppsRegRequest request) throws Exception {

        AppsRequest appsRequest = new AppsRequest();
        appsRequest.setAppKey(request.getAppKey());

        // 기존 앱 정보 가져옴
        // request(수정), apps(기존)
        AppsVO apps = adminRepository.selectAppDetail(appsRequest);
        //FROM	PORTAL_APP_INFO T1
        //LEFT JOIN PORTAL_APP_SCR_HIS T2
        //ON T1.APP_CLIENT_ID = T2.APP_CLIENT_ID
        //AND T1.APP_KEY = T2.APP_KEY
        //WHERE	T1.APP_KEY = #{appKey}

        if(apps == null) {
            log.error("업데이트 하려는 앱 정보 null");
            throw new BusinessException("E073",messageSource.getMessage("E073"));
        }

        request.setRegDttm(apps.getRegDttm());

        if (request.getOwnNm() != null && !"".equals(request.getOwnNm())) {
            String encryptedOwnNm = cryptoUtil.encrypt(request.getOwnNm());
            request.setOwnNm(encryptedOwnNm);
            log.debug("암호화 - 대표자: {}", encryptedOwnNm);
        }

        if (request.getDirNm() != null && !"".equals(request.getDirNm())) {
            String encryptedData = cryptoUtil.encrypt(request.getDirNm());
            request.setDirNm(encryptedData);
            log.debug("암호화 - 정산담당자 : {}", encryptedData);
        }

        if (request.getDirTelNo2() != null && !"".equals(request.getDirTelNo2())) {
            String encryptedData = cryptoUtil.encrypt(request.getDirTelNo2());
            request.setDirTelNo2(encryptedData);
            log.debug("암호화 - 정산담당자 전화번호: {}", encryptedData);
        }

        if (request.getDirEmail() != null && !"".equals(request.getDirEmail())) {
            String encryptedData = cryptoUtil.encrypt(request.getDirEmail());
            request.setDirEmail(encryptedData);
            log.debug("암호화 - 정산담당자 이메일: {}", encryptedData);

        }

		this.appTabUpdate1(request);

		// Redis에서 api 정보, 채널 정보 등록
		AppsRegRequest regR = new AppsRegRequest();
		regR.setAppKey(request.getAppKey());
		regR.setRegUser(request.getRegUser());
		regR.setRegUserId(request.getRegUserId());
		regR.setAppClientId(request.getAppClientId());

		AppsRequest appsReq = new AppsRequest();
		appsReq.setAppKey(request.getAppKey());

		// 차단IP 조회
		List<AppBlackListVO> oldCnlList = appsRepository.selectAppBlackListInfo(appsReq);
		List<AppsBlockKeyRequest> oldCNLs = new ArrayList<AppsBlockKeyRequest>();
		for (AppBlackListVO a : oldCnlList) {
			AppsBlockKeyRequest r = new AppsBlockKeyRequest();
			r.setSeqNo(a.getSeqNo());
			r.setCnlKey(a.getCnlKey());
			r.setAppKey(a.getAppKey());
			r.setRegUser(a.getRegUser());
			r.setModUser(a.getModUser());
			r.setUseFl(a.getUseFl());

			oldCNLs.add(r);
		}

		regR.setExBlockKeyList(oldCNLs);

		// Redis에 모든 IP 허용여부
		RedisCommunicater.appAllowAllIpRedisSet(request.getAppClientId(), request.getAppAnyCnlYn());

		// 차단IP 삭제(UPDATE)
		this.delAppBlackListInfo(regR);
        // UPDATE PORTAL_APP_BLACKLIST_INFO
        // SET MOD_DTTM = NOW() , USE_FL = 'N' , MOD_USER = #{regUser}
        // WHERE APP_KEY = #{appKey}
        // AND CNL_KEY = #{cnlKey}

		// REDIS DEL 차단IP
		RedisCommunicater.appBlackIpRedisDel(regR.getAppClientId());

        // REDIS SET 차단IP
		List<AppsBlockKeyRequest> newCNLs = request.getBlockKeyList();
		String blackListIps = "";
		if (newCNLs != null) {
			for (AppsBlockKeyRequest a : newCNLs) {
				a.setRegUser(request.getRegUserId());

				blackListIps += a.getCnlKey() + ",";
			}

			regR.setBlockKeyList(newCNLs);
			this.insertAppBlackList(regR, "Y");

			if (blackListIps != null && !blackListIps.equals("")) {
				RedisCommunicater.appBlackIpRedisSet(regR.getAppClientId(),
						blackListIps.substring(0, blackListIps.length() - 1));
			}
		}

		this.appTabUpdate2(request);
		this.appTabUpdate3(request, apps);
		this.appTabUpdate4(request, apps);
	}

	public void delAppBlackListInfo(AppsRegRequest appsRegRequest) {
		for (AppsBlockKeyRequest cnlKey : appsRegRequest.getExBlockKeyList()) {
			cnlKey.setAppKey(appsRegRequest.getAppKey());
			cnlKey.setRegUser(appsRegRequest.getRegUser());
			cnlKey.setRegUser(appsRegRequest.getRegUserId());
			appsRepository.delAppBlackListInfo(cnlKey);
		}
	}

	public void insertAppBlackList(AppsRegRequest appsRegRequest, String useFL) {
		for (AppsBlockKeyRequest cnlKey : appsRegRequest.getBlockKeyList()) {
			cnlKey.setAppKey(appsRegRequest.getAppKey());
			cnlKey.setRegUser(appsRegRequest.getRegUser());
			cnlKey.setUseFl(useFL);
			cnlKey.setRegUser(appsRegRequest.getRegUserId());
			appsRepository.insertAppBlackList(cnlKey);
		}
	}

	// 앱 수정시 수정테이블에 삽입
	public void appTabUpdate1(AppsRegRequest request) {

		// tab1에 해당하는 정보 변경
		appsRepository.updateAppInfo(request);

		List<AppsCnlKeyRequest> newCNLs = request.getCnlKeyList();
		for (AppsCnlKeyRequest a : newCNLs) {
			a.setRegUser(request.getRegUser());
			a.setRegDttm(request.getRegDttm());
		}

		AppsRequest appsReq = new AppsRequest();
		appsReq.setAppKey(request.getAppKey());

		List<AppCnlInfoVO> oldCnlList = appsRepository.selectAppCnlInfo(appsReq);
		List<AppsCnlKeyRequest> oldCNLs = new ArrayList<AppsCnlKeyRequest>();
		for (AppCnlInfoVO a : oldCnlList) {
			AppsCnlKeyRequest r = new AppsCnlKeyRequest();
			r.setSeqNo(a.getSeqNo());
			r.setCnlKey(a.getCnlKey());
			r.setAppKey(a.getAppKey());
			r.setRegDttm(a.getRegDttm());
			r.setRegUser(a.getRegUser());
			r.setModDttm(a.getModDttm());
			r.setModUser(a.getModUser());
			r.setUseFl(a.getUseFl());

			oldCNLs.add(r);
		}

		// Redis에서 api 정보, 채널 정보 등록
		AppsRegRequest regR = new AppsRegRequest();
		regR.setAppKey(request.getAppKey());
		regR.setRegUser(request.getRegUser());
		regR.setRegUserId(request.getRegUserId());
		regR.setAppClientId(request.getAppClientId());
		regR.setCnlKeyList(newCNLs);
		regR.setExCnlKeyList(oldCNLs);

		// 기존 api정보, 채널정보 삭제하고, 새로운 정보로 업데이트
		this.delAppCnlInfo(regR);
		this.insertAppCnlKey(regR, "Y");
		this.updateRedisCnlInfo(regR);

		// 메인 테이블에서 앱 정보 가져와서 백업 테이블에 저장
		adminRepository.backupAppInfo(request);

		// App 상태에 따라 redis set
		if (StringUtils.equals(appsReq.getAppStatCd(), "CLOSE")
				|| StringUtils.equals(appsReq.getAppStatCd(), "UNUSED")) {
			RedisCommunicater.appRedisSet(appsReq.getAppClientId(), "false");
			if (null != appsReq.getNewAppClientId() && "" != appsReq.getNewAppClientId()) {
				RedisCommunicater.appRedisSet(appsReq.getNewAppClientId(), "false");
			}
		} else {
			RedisCommunicater.appRedisSet(appsReq.getAppClientId(), "true");
			if (null != appsReq.getNewAppClientId() && "" != appsReq.getNewAppClientId()) {
			}
		}
	}

	// 앱 수정시 수정테이블에 삽입
    public void appTabUpdate2(AppsRegRequest request) {
        String errCode = "E075";
        try {
            AppsRequest aRequest = new AppsRequest();
            aRequest.setAppKey(request.getAppKey());
            List<AppPrdInfoVO> prdList = appsRepository.selectApPrdInfoDetail(aRequest);

            String decryptId = request.getAppClientId();
            AplvRegistRequest aplvRequest = new AplvRegistRequest();
            aplvRequest.setRegUserId(request.getRegUserId());
            aplvRequest.setHfnCd(request.getHfnCd());

            AplvPrdReqeust aplvPrdReq = new AplvPrdReqeust();
            aplvPrdReq.setAppKey(request.getAppKey());

            for (AppPrdInfoVO vo2 : prdList) {
                Collection<?> c = CollectionUtils.select(request.getPrdList(), new Predicate() {

                    @Override
                    public boolean evaluate(Object object) {
                        return StringUtils.equalsIgnoreCase(((ProductVO) object).getPrdId(), vo2.getPrdId());
                    }
                });

                if(c.size() > 0) {
                    continue;
                }

                appsRepository.updateAppPrdInfo(vo2);
            }

            for (ProductVO vo : request.getPrdList()) {

                Collection<?> c = CollectionUtils.select(prdList, new Predicate() {

                    @Override
                    public boolean evaluate(Object object) {
                        String useFl = ((AppPrdInfoVO) object).getUseFl();
                        if(useFl != null && !useFl.equals("") && useFl.equals("DEL")) {
                            return false;
                        }
                        return StringUtils.equalsIgnoreCase(((AppPrdInfoVO) object).getPrdId(), vo.getPrdId());
                    }
                });

                if(c.size() > 0) {
                    continue;
                }

                vo.setRegUserId(request.getRegUserId());
                vo.setAppKey(request.getAppKey());

                adminRepository.insertAppPrdInfo(vo);

                aplvRequest.setAplvReqCd(vo.getAppPrdSeqNo());
                aplvRequest.setAplvDivCd("APPPRDREG");
                aplvRequest.setAplvReqCtnt(vo.getPrdNm());
                adminRepository.insertAplv(aplvRequest);

            }
            if (request.getPrdKind().equals("10") || request.getPrdKind().equals("11")) {
                // 앱에 등록된 상품목록 조회.
                com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest aplvReq = new com.hanafn.openapi.portal.admin.viewsv2.dto.AplvRequest();
                aplvReq.setAppKey(request.getAppKey());
                aplvReq.setSearchPrdCtgy(request.getHfnCd());
                aplvReq.setProcId(request.getRegUserId());
                aplvReq.setProcUser(request.getRegUser());
                // 기본 구분값은 앱-상품승인 대기
                aplvReq.setSearchAplvStatCd("WAIT");
                aplvReq.setPageIdx(1);
                aplvReq.setPageSize(20); // 백오피스에서 전달하는 상품은 1개밖에 없음..
                aplvReq.setPageOffset((aplvReq.getPageIdx() - 1) * aplvReq.getPageSize());

                AplvRsponsePaging aplvRes = aplvService.selectPrdAplvListPaging(aplvReq);
                // 상품 승인완료처리.
                for (PrdAplvListVO vo : aplvRes.getList()) {
                    // 승인완료 처리.
                    AplvApprovalRequest aplvApprovalRequest = new AplvApprovalRequest();
                    aplvApprovalRequest.setRegUser(request.getRegUserId());
                    aplvApprovalRequest.setProcUser(request.getRegUserId());
                    aplvApprovalRequest.setProcId(request.getRegUserId());
                    aplvApprovalRequest.setPrdCtgy(vo.getPrdCtgy());
                    aplvApprovalRequest.setAplvSeqNo(vo.getAplvSeqNo());
                    aplvService.aplvApproval(aplvApprovalRequest);
                }
            }

            AdminRequest.ApiDetailRequest apiDetailRequest = new AdminRequest.ApiDetailRequest();
            apiDetailRequest.setAppKey(request.getAppKey());

            // Redis에 API SET
            List<ApiVO> apiList = adminRepository.selectAppApiList(apiDetailRequest);

            // Redis에 app 정보 세팅
            RedisCommunicater.appRedisSet(decryptId, "true");

            for (ApiVO apiInfo : apiList) {
                RedisCommunicater.appApiRedisSet(decryptId, apiInfo.getApiUrl(), "true");
            }
        } catch (Exception e) {
            log.error(e.toString());
            throw new BusinessException(errCode, messageSource.getMessage(errCode));
        }
    }

	// 앱 수정시 수정테이블에 삽입
	public void appTabUpdate3(AppsRegRequest request, AppsVO apps) {

        String copRegNo = request.getCopRegNo();
        String copNm = request.getCopNm();

		if (copRegNo != null && !"".equals(copRegNo) && copNm != null && !"".equals(copNm)) {
			appsRepository.updateAppCopInfo(request);
			adminRepository.backupAppCopInfo(request);
		}

		String appSvcUrl = request.getAppSvcUrl();
		String appSvcNm = request.getAppSvcNm();
		boolean appSaveYn = false;

		if (appSvcUrl != null && !"".equals(appSvcUrl) && apps.getAppSvcUrl() != null
				&& !apps.getAppSvcUrl().equals(appSvcUrl)) {
			appSaveYn = true;
		}

		if (!appSaveYn && appSvcNm != null && !"".equals(appSvcNm) && apps.getAppNm() != null
				&& !apps.getAppNm().equals(appSvcNm)) {
			appSaveYn = true;
		}

		if (appSaveYn) {
			// tab1에 해당하는 정보 변경
			appsRepository.updateAppInfo(request);

			// 메인 테이블에서 앱 정보 가져와서 백업 테이블에 저장
			adminRepository.backupAppInfo(request);
		}
	}

	// 앱 수정시 수정테이블에 삽입
	public void appTabUpdate4(AppsRegRequest request, AppsVO apps) throws Exception {

        if (!apps.getUserKey().equals(request.getUserKey())) {
            appsRepository.updateAppOwnerHis(request);
        }

		List<AppReadInfoVO> appReadInfo = request.getAppReadInfo();

		if (appReadInfo != null && appReadInfo.size() > 0) {
			for (AppReadInfoVO vo : appReadInfo) {
				AppReadUserRequest readRequest = new AppReadUserRequest();

				readRequest.setAppKey(request.getAppKey());
				readRequest.setUserId(vo.getUserId());
				readRequest.setUserNm(vo.getUserNm());
				readRequest.setUserTel(vo.getUserTel());
				readRequest.setUserEmail(vo.getUserEmail());
				readRequest.setRegUser(request.getModUserId());
				readRequest.setUserKey(vo.getUserKey());

				AppsResponse users = apisService.selectAppReadUser(readRequest);

				List<AppReadInfoVO> userList = users.getAppReadInfo();

				if (userList != null && userList.size() < 1) {
					log.error("[앱 조회자] 등록할 대상자가 없음. 이미 등록되어 있거나 정보를 정상적으로 입력하지 않은경우 ");
					throw new BusinessException("E081", messageSource.getMessage("E081"));
				} else {
					if (userList != null) {
						for (AppReadInfoVO vo2 : userList) {
							String readUserKey = vo2.getUserKey();

							if (readUserKey != null && !"".equals(readUserKey)
									&& readUserKey.equals(apps.getUserKey())) {
								log.error("[앱 조회자] 소유자를 등록시도 ");
								throw new BusinessException("E080", messageSource.getMessage("E080"));
							}
						}
					}
				}

				appsRepository.insertAppReadUser(readRequest);
				appsRepository.insertBackupAppReadUser(readRequest);
			}
		}
	}

	// Redis에서 app-channel 정보 업데이트
	public void updateRedisCnlInfo(AppsRegRequest request) {

		// 앱 채널(IP) 정보
		List<AppsCnlKeyRequest> nwCnl = request.getCnlKeyList();
		List<AppsCnlKeyRequest> exCnl = request.getExCnlKeyList();
		List<AppsCnlKeyRequest> finalExKey = exCnl;
		List<AppsCnlKeyRequest> finalNwKey = nwCnl;
		exCnl = exCnl.stream().filter(i -> !finalNwKey.contains(i)).collect(Collectors.toList());
		nwCnl = nwCnl.stream().filter(i -> !finalExKey.contains(i)).collect(Collectors.toList());

		request.setExCnlKeyList(exCnl);
		request.setCnlKeyList(nwCnl);

		// Redis
		String clientId = request.getAppClientId();

		// Redis IP
		try {
			for (AppsCnlKeyRequest del : exCnl) {
				RedisCommunicater.appIpRedisDel(clientId, del.getCnlKey());
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisDel(request.getNewAppClientId(), del.getCnlKey());
				}
			}
		} catch (Exception e) {
			for (AppsCnlKeyRequest del : exCnl) {
				RedisCommunicater.appIpRedisSet(clientId, del.getCnlKey(), "true");
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisSet(request.getNewAppClientId(), del.getCnlKey(), "true");
				}
			}
		}
		try {
			for (AppsCnlKeyRequest set : nwCnl) {
				RedisCommunicater.appIpRedisSet(clientId, set.getCnlKey(), "true");
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisSet(request.getNewAppClientId(), set.getCnlKey(), "true");
				}
			}
		} catch (Exception e) {
			for (AppsCnlKeyRequest set : nwCnl) {
				RedisCommunicater.appIpRedisDel(clientId, set.getCnlKey());
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisDel(request.getNewAppClientId(), set.getCnlKey());
				}
			}
		}
	}

	// Redis에서 app-api와 app-channel 정보 업데이트
	public void updateRedisInfo(AppsRegRequest request) {

		// 앱 채널(IP) 정보
		List<AppsCnlKeyRequest> nwCnl = request.getCnlKeyList();
		List<AppsCnlKeyRequest> exCnl = request.getExCnlKeyList();
		List<AppsCnlKeyRequest> finalExKey = exCnl;
		List<AppsCnlKeyRequest> finalNwKey = nwCnl;
		exCnl = exCnl.stream().filter(i -> !finalNwKey.contains(i)).collect(Collectors.toList());
		nwCnl = nwCnl.stream().filter(i -> !finalExKey.contains(i)).collect(Collectors.toList());

		request.setExCnlKeyList(exCnl);
		request.setCnlKeyList(nwCnl);

		// 앱 API 정보
		List<AppsApiInfoRequest> nwApi = request.getApiList();
		List<AppsApiInfoRequest> exApi = request.getExApiList();
		List<AppsApiInfoRequest> finalNwApi = nwApi;
		List<AppsApiInfoRequest> finalExApi = exApi;
		exApi = exApi.stream().filter(i -> !finalNwApi.contains(i)).collect(Collectors.toList());
		nwApi = nwApi.stream().filter(i -> !finalExApi.contains(i)).collect(Collectors.toList());

		request.setApiList(nwApi);
		request.setExApiList(exApi);

		// Redis
		String clientId = request.getAppClientId();
		List<ApiVO> redisExApi = request.getRedisExApiList();
		List<ApiVO> redisNwApi = request.getRedisApiList();
		List<ApiVO> finalRedisNwApi = redisNwApi;
		List<ApiVO> finalRedisExApi = redisExApi;

		if (finalRedisNwApi != null && redisExApi != null) {
			redisExApi = redisExApi.stream().filter(i -> !finalRedisNwApi.contains(i)).collect(Collectors.toList());
		}
		if (finalRedisExApi != null && redisNwApi != null) {
			redisNwApi = redisNwApi.stream().filter(i -> !finalRedisExApi.contains(i)).collect(Collectors.toList());
		}

		// Redis API
		if (redisExApi != null) {
			try {
				for (ApiVO apiInfo : redisExApi) {
					RedisCommunicater.appApiRedisDel(clientId, apiInfo.getApiUrl());
					if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
						RedisCommunicater.appApiRedisDel(request.getNewAppClientId(), apiInfo.getApiUrl());
					}
				}
			} catch (Exception e) {
				for (ApiVO apiInfo : redisExApi) {
					RedisCommunicater.appApiRedisSet(clientId, apiInfo.getApiUrl(), "true");
					if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
						RedisCommunicater.appApiRedisSet(request.getNewAppClientId(), apiInfo.getApiUrl(), "true");
					}
				}
			}
		}
		if (redisNwApi != null) {
			try {
				for (ApiVO apiInfo : redisNwApi) {
					RedisCommunicater.appApiRedisSet(clientId, apiInfo.getApiUrl(), "true");
					if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
						RedisCommunicater.appApiRedisSet(request.getNewAppClientId(), apiInfo.getApiUrl(), "true");
					}
				}
			} catch (Exception e) {
				for (ApiVO apiInfo : redisNwApi) {
					RedisCommunicater.appApiRedisDel(clientId, apiInfo.getApiUrl());
					if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
						RedisCommunicater.appApiRedisDel(request.getNewAppClientId(), apiInfo.getApiUrl());
					}
				}
			}
		}

		// Redis IP
		try {
			for (AppsCnlKeyRequest del : exCnl) {
				RedisCommunicater.appIpRedisDel(clientId, del.getCnlKey());
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisDel(request.getNewAppClientId(), del.getCnlKey());
				}
			}
		} catch (Exception e) {
			for (AppsCnlKeyRequest del : exCnl) {
				RedisCommunicater.appIpRedisSet(clientId, del.getCnlKey(), "true");
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisSet(request.getNewAppClientId(), del.getCnlKey(), "true");
				}
			}
		}
		try {
			for (AppsCnlKeyRequest set : nwCnl) {
				RedisCommunicater.appIpRedisSet(clientId, set.getCnlKey(), "true");
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisSet(request.getNewAppClientId(), set.getCnlKey(), "true");
				}
			}
		} catch (Exception e) {
			for (AppsCnlKeyRequest set : nwCnl) {
				RedisCommunicater.appIpRedisDel(clientId, set.getCnlKey());
				if (StringUtils.equals(request.getAppScrReisuYn(), "Y")) {
					RedisCommunicater.appIpRedisDel(request.getNewAppClientId(), set.getCnlKey());
				}
			}
		}
	}

	public void delAppApiInfo(AppsRegRequest appsRegRequest) {
		for (AppsApiInfoRequest api : appsRegRequest.getExApiList()) {
			api.setAppKey(appsRegRequest.getAppKey());
			api.setRegUser(appsRegRequest.getRegUser());
			api.setRegUserId(appsRegRequest.getRegUserId());
			appsRepository.delAppApiInfo(api);
		}
	}

	public void delAppCnlInfo(AppsRegRequest appsRegRequest) {
		for (AppsCnlKeyRequest cnlKey : appsRegRequest.getExCnlKeyList()) {
			cnlKey.setAppKey(appsRegRequest.getAppKey());
			cnlKey.setRegUser(appsRegRequest.getRegUser());
			cnlKey.setRegUserId(appsRegRequest.getRegUserId());
			appsRepository.delAppCnlInfo(cnlKey);
		}
	}

	/**** 앱 수정 테이블 정보조회(승인대기) ****/
	public AppsResponse selectAppDetailMod(AppsRequest appsRequest) {

		AppsVO apps = appsRepository.selectAppModDetail(appsRequest);

		// 앱 채널 정보, 앱 API 정보
		List<AppApiInfoVO> appApiInfoList = appsRepository.selectAppApiInfoMod(appsRequest);
		List<AppCnlInfoVO> appCnlInfoList = appsRepository.selectAppCnlInfoMod(appsRequest);

		// 카테고리 정보 포함한 API정보
		List<ApiVO> list = new ArrayList<>();
		for (AppApiInfoVO appApiInfo : appApiInfoList) {
			ApiRequest apiRequest = new ApiRequest();
			apiRequest.setApiId(appApiInfo.getApiId());
			ApiVO apiInfo = appsRepository.selectApi(apiRequest);
			list.add(apiInfo);
		}

		AppsResponse response = new AppsResponse();
		String appScr = apps.getAppScr();
		String newAppScr = apps.getNewAppScr();

		try {
			if (appScr != null && !appScr.isEmpty()) {
				response.setAppScr(cryptoUtil.decrypt(appScr));
			}
			if (newAppScr != null && !newAppScr.isEmpty()) {
				response.setNewAppScr(cryptoUtil.decrypt(newAppScr));
			}
		} catch (Exception e) {
			log.error("Client Secret DecryptAES256 Error", e);
		}

		response.setAppClientId(apps.getAppClientId());
		response.setNewAppClientId(apps.getNewAppClientId());
		response.setAppScr(apps.getAppScr());
		response.setAppKey(apps.getAppKey());
		response.setAppNm(apps.getAppNm());
		response.setAppStatCd(apps.getAppStatCd());
		response.setAppSvcStDt(apps.getAppSvcStDt());
		response.setAppSvcEnDt(apps.getAppSvcEnDt());
		response.setAppCtnt(apps.getAppCtnt());
		response.setTermEtdYn(apps.getTermEtdYn());
		response.setAppScrReisuYn(apps.getAppScrReisuYn());
		response.setUserKey(apps.getUserKey());
		response.setUseorgNm(apps.getUseorgNm());
		response.setRegDttm(apps.getRegDttm());
		response.setRegUser(apps.getRegUser());
		response.setModDttm(apps.getModDttm());
		response.setModUser(apps.getModUser());
		response.setAppScrVldDttm(apps.getAppScrVldDttm());
		response.setAppApiInfo(appApiInfoList);
		response.setCnlKeyList(appCnlInfoList);
		response.setApiList(list);

		return response;
	}

	public AppsRsponsePaging selectAppsListPagingMod(AppsRequest appsRequest) {
		if (appsRequest.getPageIdx() == 0)
			appsRequest.setPageIdx(appsRequest.getPageIdx() + 1);

		if (appsRequest.getPageSize() == 0) {
			appsRequest.setPageSize(20);
		}

		appsRequest.setPageOffset((appsRequest.getPageIdx() - 1) * appsRequest.getPageSize());
		int totCnt = appsRepository.countAppsListMod(appsRequest);
		List<AppsVO> list = appsRepository.selectAppsListMod(appsRequest);

		AppsRsponsePaging pagingData = new AppsRsponsePaging();
		pagingData.setPageIdx(appsRequest.getPageIdx());
		pagingData.setPageSize(appsRequest.getPageSize());
		pagingData.setTotCnt(totCnt);
		pagingData.setList(list);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	/**
	 * Secret Key, Client ID 재발급
	 * 
	 * @param appsRegRequest
	 * @return
	 */
	public AppsResponse appSecretReisu(AppsRegRequest appsRegRequest) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 7);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String expireDay = df.format(cal.getTime());

		// 클라이언트 ID, 시크릿 재발급
		AppsRequest appsRequest = new AppsRequest();
		appsRequest.setAppKey(appsRegRequest.getAppKey());

		AppsVO appsVO = settingRepository.selectUseorgEntrCdFromInfo(appsRequest);
		String encKey = appsVO.getEncKey();
		String entrCd = appsVO.getEntrCd();

		// 재발행 진행중일 시 에러처리 추가 2019/11/13
		AppsVO appsVO2 = adminRepository.selectAppDetail(appsRequest);
		if (StringUtils.equals(appsVO2.getAppScrReisuYn(), "Y")) {
			log.error("앱키 재발행 진행중이므로 취소처리. (appkeyReisu)");
			throw new BusinessException("A001", messageSource.getMessage("A001"));
		}

		try {
			encKey = cryptoUtil.decrypt(encKey);
			log.debug("[Client/Secret 재발급] ENC KEY 복호화 : " + encKey);
		} catch (Exception e) {
			log.error("[Client/Secret 재발급] ENC KEY 복호화 에러 : " + e.toString());
			throw new BusinessException("E072", messageSource.getMessage("E072"));
		}

		GWResponse gwResponse = gWCommunicater.clientSecretNewPassword(appsVO.getHfnCd(),
				appsRegRequest.getAppClientId(), expireDay, entrCd, encKey);

		String clientId = (String) gwResponse.getDataBody().get("clientId");
		String clientScr = (String) gwResponse.getDataBody().get("clientSecret");
		String decryptId = "";
		String decryptScr = "";
		String encryptScr = "";

		try {
			decryptId = cryptoUtil.decrypt(encKey, clientId).split(":")[0];
			log.debug("[Client/Secret 재발급] CID [" + decryptId + "]");
			decryptScr = cryptoUtil.decrypt(encKey, clientScr).split(":")[0];
			log.debug("[Client/Secret 재발급] SCR [" + decryptScr + "]");
			encryptScr = cryptoUtil.encrypt(decryptScr); // DB에 넣을땐 기본 AES

		} catch (Exception e) {
			log.error("Client Secret EncryptAES256 Error", e);
			throw new BusinessException("E072", messageSource.getMessage("E072"));
		}
		// 앱 정보
		appsRegRequest.setNewAppClientId(decryptId);
		appsRegRequest.setNewAppScr(encryptScr);
		appsRepository.updateAppScr(appsRegRequest);
//        appsRepository.updateAppScrMod(appsRegRequest);

		// 앱 Secret 내역 Update (기존)
		AppsScrRequest appsScrRequest = new AppsScrRequest();
		appsScrRequest.setAppKey(appsRegRequest.getAppKey());
		appsScrRequest.setAppScrVldDttm(expireDay);
		appsScrRequest.setModUser(appsRegRequest.getModUser());
		appsScrRequest.setModUserId(appsRegRequest.getModUserId());
		appsRepository.updateAppScrHis(appsScrRequest);

		// 앱 Secret 내역 Insert (신규)
		appsScrRequest = new AppsScrRequest();
		appsScrRequest.setAppKey(appsRegRequest.getAppKey());
		appsScrRequest.setAppClientId(decryptId);
		appsScrRequest.setAppScr(encryptScr);
		appsScrRequest.setAppScrVldDttm(appsRegRequest.getAppSvcEnDt());
		appsScrRequest.setRegUser(appsRegRequest.getModUser());
		appsScrRequest.setRegUserId(appsRegRequest.getRegUserId());
		adminRepository.insertAppNewScrHis(appsScrRequest);

		// Redis
		List<ApiVO> apiList = appsRegRequest.getRedisExApiList();
		List<AppsCnlKeyRequest> cnlKeyList = appsRegRequest.getExCnlKeyList();

		try {
			RedisCommunicater.appRedisSet(decryptId, "true");
		} catch (Exception e) {
			RedisCommunicater.appRedisDel(decryptId);
		}

		try {
			for (ApiVO apiInfo : apiList) {
				RedisCommunicater.appApiRedisSet(decryptId, apiInfo.getApiUrl(), "true");
			}
		} catch (Exception e) {
			for (ApiVO apiInfo : apiList) {
				RedisCommunicater.appApiRedisDel(decryptId, apiInfo.getApiUrl());
			}
		}

		try {
			for (AppsCnlKeyRequest cnl : cnlKeyList) {
				RedisCommunicater.appIpRedisSet(decryptId, cnl.getCnlKey(), "true");
			}
		} catch (Exception e) {
			for (AppsCnlKeyRequest cnl : cnlKeyList) {
				RedisCommunicater.appIpRedisDel(decryptId, cnl.getCnlKey());
			}
		}

		AppsResponse response = new AppsResponse();
		response.setAppClientId(appsRegRequest.getAppClientId());
		response.setNewAppClientId(decryptId);
		response.setAppScr(appsRegRequest.getAppScr());
		response.setNewAppScr(encryptScr);
		response.setAppScrReisuYn("Y");
		response.setModDttm(expireDay);

		return response;
	}

	public AppsResponse keyReturn(AppsScrRequest appsScrRequest) {
		AppsVO apps = appsRepository.keyReturn(appsScrRequest);
		AppsResponse response = new AppsResponse();
		response.setAppClientId(apps.getAppClientId());
		response.setAppScr(apps.getAppScr());

		return response;
	}

	public List<AppsVO> selectAppAll(AppsRequest appsRequest) {
		return appsRepository.selectAppListAll(appsRequest);
	}

	public AppsRsponsePaging appListSelected(AppsRequest request) {

		if (request.getPageIdx() == 0) {
			request.setPageIdx(request.getPageIdx() + 1);
		}
		if (request.getPageSize() == 0) {
			request.setPageSize(20);
		}

		request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

		int totCnt = appsRepository.countAppList(request);
		List<AppsVO> list = appsRepository.selectAppList(request);

        for (AppsVO appsVo: list) {
            try {
                if (StringUtils.isNotEmpty(appsVo.getUserNm()) && !StringUtils.equals(appsVo.getUserNm(), "DEL")) {
                    String decryptedUserNm = cryptoUtil.decrypt(appsVo.getUserNm());
                    appsVo.setUserNm(decryptedUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUserNm);
                }
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            } catch ( Exception e ) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            }
        }

		AppsRsponsePaging resp = new AppsRsponsePaging();
		resp.setList(list);
		resp.setPageIdx(request.getPageIdx());
		resp.setPageSize(request.getPageSize());
		resp.setTotCnt(totCnt);
		resp.setSelCnt(list.size());

		return resp;
	}

	public HfnCompanyAllResponse hfnCompanyAll(UseorgRequest request) {
		List<HfnUserRoleVO> list = appsRepository.hfnCompanyAll(request);
		HfnCompanyAllResponse resp = new HfnCompanyAllResponse();
		resp.setList(list);
		return resp;
	}

	public AppsResponse selectAppUserList(AppsRequest request) throws Exception {
		List<AppReadInfoVO> list = appsRepository.selectAppUserList(request);

		AppsResponse data = new AppsResponse();

		for (AppReadInfoVO vo : list) {
			String userNm = vo.getUserNm();

			if (userNm != null && !"".equals(userNm)) {
				String decryptedUserNm = cryptoUtil.decrypt(userNm);
				vo.setUserNm(decryptedUserNm);
			}
		}

		data.setAppReadInfo(list);

		return data;
	}

	public AppsResponse selectOwnerUser(UserRequest.searchUserRequest request) throws Exception {
		AppsResponse data = new AppsResponse();

		request.setUserNm(cryptoUtil.encrypt(request.getUserNm()));
		AppOwnerVO info = appsRepository.selectOwnerUser(request);

		if (info != null) {
			data.setUserKey(info.getUserKey());
			data.setManagerId(info.getUserId());
			data.setManagerNm(cryptoUtil.decrypt(info.getUserNm()));
		}

		return data;
	}
	// 서비스(앱) 조회
    public ServiceAppResponsePaging selectServiceAppListPaging(ServiceAppRequest request) {
        // pageIdx 0일때 1로 세팅
        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);
        // 페이지사이즈 0일때 20으로 세팅
        if(request.getPageSize() == 0)
            request.setPageSize(20);
        // 페이지오프셋 세팅
        request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());


        int totCnt = appsRepository.countServiceAppList(request);
        ServiceAppResponsePaging pagingData = new ServiceAppResponsePaging();
        List<ServiceAppVO> list = appsRepository.selectServiceAppList(request);
        pagingData.setList(list);
        pagingData.setPageIdx(request.getPageIdx());
        pagingData.setPageSize(request.getPageSize());
        pagingData.setTotCnt(totCnt);
        pagingData.setSelCnt(list.size());
        return pagingData;
    }
    public Integer chkServiceDup(ServiceAppRequest.ServiceAppRegistRequest request) {
	    return appsRepository.chkServiceDup(request);
    }
    public void serviceAppReg(ServiceAppRequest.ServiceAppRegistRequest request) throws Exception{
        serviceAppReg(request, true);
    }
    public String selUseorgGb(ServiceAppRequest.ServiceAppRegistRequest request) throws Exception{
	    return appsRepository.selUseorgGb(request);
    }
    public void serviceAppReg(ServiceAppRequest.ServiceAppRegistRequest request, boolean sendBt) throws Exception{
	    String sCallback = "";
	    String sSchemes = "";
	    // 1.앱정보등록(PORTAL_APP_INFO)
        List<String> callbacks = new ArrayList<>();
        appsRepository.serviceAppReg(request);

        appsRepository.delAppRedirectInfo(request);
        
        appsRepository.delAppScheme(request);
        if (request.getCallbackList() != null && request.getCallbackList().size() > 0) {
            sCallback = this.insertAppRedirectInfo(request);
        }
        if (request.getSchemeList() != null && request.getSchemeList().size() > 0) {
            sSchemes = this.insertAppScheme(request);
        }

        LinkRequest reqApp = new LinkRequest();
        reqApp.setAppKey(request.getAppKey());
        reqApp.setOpType("I");
        Map<String, Object> sendData = appsRepository.serviceAppDetailMap(reqApp);
        List<Map<String, Object>> redirectData = appsRepository.serviceAppDetailRedirectMap(reqApp);
        sendData.put("redirect_url_list", redirectData);
        List<Map<String, Object>> schemeData = appsRepository.serviceAppDetailSchemeMap(reqApp);
        sendData.put("app_scheme_list", schemeData);

        if(sendBt) {
        	sendData.put("client_secret", cryptoUtil.decrypt((String)sendData.get("clinet_secret")));
            innerTransUtil.sendBt(btTranSvcUrl, sendData);
        }
        gWCommunicater.oauthRegService(request.getAppClientId(), request.getAppScr(), request.getUserKey(), sCallback, sSchemes, selUseorgGb(request));
        // 2.앱정보이력등록(PORTAL_APP_INFO_HIS)
        appsRepository.serivceAppRegHis(request);

        // 3. 레디스에 APP정보 세팅
        RedisCommunicater.appRedisSet(request.getAppClientId(), "true");
    }
    // 앱 Callback URL 테이블 적재
    public String insertAppRedirectInfo(ServiceAppRequest.ServiceAppRegistRequest request) {
	    ServiceAppRequest.AppRedirectInfoRequest appRequest = new ServiceAppRequest.AppRedirectInfoRequest();
	    List<String> callbacks = new ArrayList<>();
	    String sCallback = "";
	    for(int i = 0; i < request.getCallbackList().size(); i++) {
            appRequest.setRegUser(request.getRegUser());
            appRequest.setAppKey(request.getAppKey());
            appRequest.setRedirectUri(request.getCallbackList().get(i).get("redirectUri"));
            appsRepository.insertAppRedirectInfo(appRequest);
        }
        sCallback = String.join(",", callbacks);
	    return sCallback;
    }
    // 앱 Callback URL 상세
    public List<HashMap> selectAppRedirectInfo(ServiceAppRequest request) {
	    List<ServiceAppVO> dataVO = appsRepository.selectAppRedirectInfo(request);
	    List<HashMap> data = new ArrayList<>();
	    for (int i = 0; i < dataVO.size(); i++) {
	        HashMap<String, String> hData = new HashMap<>();
	        hData.put("redirectUri", dataVO.get(i).getRedirectUri());
	        data.add(i, hData);
        }
	    return data;
    }
    // 앱 URL 스킴 테이블 적재
    public String insertAppScheme(ServiceAppRequest.ServiceAppRegistRequest request) {
	    String sSchemes = "";
        ServiceAppRequest.AppSchemeRequest schemeRequest = new ServiceAppRequest.AppSchemeRequest();
        List<String> schemes = new ArrayList<String>();
        for (int i = 0; i < request.getSchemeList().size(); i++) {
            schemeRequest.setRegUser(request.getRegUser());
            schemeRequest.setAppKey(request.getAppKey());
            schemeRequest.setAppScheme(request.getSchemeList().get(i).get("appScheme"));
            schemes.add((String)request.getSchemeList().get(i).get("appScheme"));
            appsRepository.insertAppScheme(schemeRequest);
        }
        sSchemes = String.join(",", sSchemes);
        return sSchemes;
    }
    public List<HashMap> selectAppScheme(ServiceAppRequest request) {
        List<ServiceAppVO> dataVO = appsRepository.selectAppScheme(request);
        List<HashMap> data = new ArrayList<>();
        for (int i = 0; i < dataVO.size(); i++) {
            HashMap<String, String> hData = new HashMap<>();
            hData.put("appScheme", dataVO.get(i).getAppScheme());
            data.add(i, hData);
        }
        return data;
    }
    // 서비스앱 상세보기
    public ServiceAppVO serviceAppDetail(ServiceAppRequest request) {
	    ServiceAppVO serviceApp = appsRepository.serviceAppDetail(request);
        return serviceApp;
    }
    // 서비스앱 상태 변경
    public void serviceAppStatusUpd(ServiceAppRequest request) {
        serviceAppStatusUpd(request, true);
    }
    public void serviceAppStatusUpd(ServiceAppRequest request, boolean sendBt) {
	    if(request.getAppStatCd().equals("OK"))
	        request.setAppStatCd("CLOSE");
        else if(request.getAppStatCd().equals("CLOSE"))
            request.setAppStatCd("OK");
        else
            request.setAppStatCd("DEL");
        // 상태값 변경
        appsRepository.serviceAppStatusUpd(request);
        
        if(request.getAppStatCd().equals("DEL")) {
        	LinkRequest reqApp = new LinkRequest();
	        reqApp.setAppKey(request.getAppKey());
	        reqApp.setOpType("D");
	        Map<String, Object> sendData = appsRepository.serviceAppDetailMap(reqApp);
            if(sendBt) {
                innerTransUtil.sendBt(btTranSvcUrl, sendData);
            }
        }
        
        ServiceAppVO serviceApp = serviceAppDetail(request);
        serviceApp.setAppStatCd(request.getAppStatCd());
        serviceApp.setRegUser(request.getRegUser());
        // 서비스앱 이력관리
        appsRepository.serivceAppRegStatusHis(serviceApp);
        // RedisSet
        try {
            if(StringUtils.equals(request.getAppStatCd(), "CLOSE")) {
                RedisCommunicater.appRedisSet(request.getAppClientId(), "false");
            } else if (StringUtils.equals(request.getAppStatCd(), "OK")){
                RedisCommunicater.appRedisSet(request.getAppClientId(), "true");
            } else { // 삭제시
                RedisCommunicater.appRedisDel(request.getAppClientId());
            }
        } catch (Exception e) {
            if(StringUtils.equals(request.getAppStatCd(), "CLOSE")) {
                RedisCommunicater.appRedisSet(request.getAppClientId(), "true");
            } else if (StringUtils.equals(request.getAppStatCd(), "OK")){
                RedisCommunicater.appRedisSet(request.getAppClientId(), "false");
            } else {
                RedisCommunicater.appRedisSet(request.getAppClientId(), "true");
            }
        }
    }
    public List<HashMap> selectServiceAppIps(ServiceAppRequest request) {
        List<ServiceAppVO> dataVO = appsRepository.selectServiceAppIps(request);

        List<HashMap> data = new ArrayList<HashMap>();

        for(int i = 0; i < dataVO.size(); i++) {
            HashMap<String, String> hData = new HashMap<>();
            hData.put("cnlKey", dataVO.get(i).getCnlKey());
            data.add(i, hData);
        }
        return data;
    }
    public void serviceAppUpd(ServiceAppRequest.ServiceAppRegistRequest request) throws Exception{
        serviceAppUpd(request, true);
    }
    public void serviceAppUpd(ServiceAppRequest.ServiceAppRegistRequest request, boolean sendBt) throws Exception{
	    // clientId 비교
	    ServiceAppRequest appRequest = new ServiceAppRequest();
	    appRequest.setAppKey(request.getAppKey());
        ServiceAppVO beforeData = serviceAppDetail(appRequest);
        String sCallback = "";
        String sSchemes = "";
        ServiceAppRequest.ServiceAppChannelRequest ipData = new ServiceAppRequest.ServiceAppChannelRequest();
        // 1. PORTAL_APP_INFO 정보 UPDATE
        appsRepository.serviceAppUpd(request);
        // 2. PORTAL_APP_INFO_HIS 이력 등록
        appsRepository.serivceAppRegHis(request);

        // 3. PORTAL_APP_REDIRECT_INFO 삭제
        appsRepository.delAppRedirectInfo(request);
        // 4. PORTAL_APP_REDIRECT_INFO 적재
        if (request.getCallbackList().size() > 0) {
            sCallback = this.insertAppRedirectInfo(request);
        }


        // 5. PORTAL_APP_SCHEME_INFO 삭제
        appsRepository.delAppScheme(request);
        // 6. PORTAL_APP_SCHEME_INFO 적재
        if (request.getSchemeList().size() > 0) {
            sSchemes = this.insertAppScheme(request);
        }
        
        LinkRequest reqApp = new LinkRequest();
        reqApp.setAppKey(request.getAppKey());
        reqApp.setOpType("M");
        Map<String, Object> sendData = appsRepository.serviceAppDetailMap(reqApp);
        List<Map<String, Object>> redirectData = appsRepository.serviceAppDetailRedirectMap(reqApp);
        sendData.put("redirect_url_list", redirectData);
        List<Map<String, Object>> schemeData = appsRepository.serviceAppDetailSchemeMap(reqApp);
        sendData.put("app_scheme_list", schemeData);
        
        if (sendBt) {
        	sendData.put("client_secret", cryptoUtil.decrypt((String)sendData.get("clinet_secret")));
            innerTransUtil.sendBt(btTranSvcUrl, sendData);
        }

        // 7.Redis Set
        if(StringUtils.equals(request.getAppStatCd(), "OK")) {
            if(!(request.getAppClientId().equals(beforeData.getAppClientId()))) { // clientId 변경
                RedisCommunicater.appRedisDel(beforeData.getAppClientId());
                RedisCommunicater.appRedisSet(request.getAppClientId(), "true");
                gWCommunicater.oauthRegService(request.getAppClientId(), request.getAppScr(), request.getUserKey(), sCallback, sSchemes, selUseorgGb(request));
            }
        } else if (StringUtils.equals(request.getAppStatCd(), "CLOSE")) {
            if(!(request.getAppClientId().equals(beforeData.getAppClientId()))) { // clientId 변경
                RedisCommunicater.appRedisDel(beforeData.getAppClientId());
                RedisCommunicater.appRedisSet(request.getAppClientId(), "false");
            }
        }
    }
    public String selectAppKey(ServiceAppRequest.ServiceAppRegistRequest request) {
        return appsRepository.selectAppKey(request);
    }
    // 서비스(앱) 조회
    public List<ServiceAppVO> selectServiceAppStatsList(ServiceAppRequest.StatsAppRequest request) {
        List<ServiceAppVO> list = appsRepository.selectServiceAppStatsList(request);
        return list;
    }
}