package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.BillingVO;

import lombok.Data;

@Data
public class BillingResponse {

	private BillingVO vo;

    @Data
    public static class BillingListResponse{
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
        private List<BillingVO> list;
    }
}
