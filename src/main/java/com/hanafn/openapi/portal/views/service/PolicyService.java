package com.hanafn.openapi.portal.views.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.views.dto.*;
import com.hanafn.openapi.portal.views.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppUsageRatioRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.PolicyResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.TokenPolicyVO;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.repository.PolicyRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class PolicyService {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    AppsRepository appsRepository;
    
    @Autowired
	AdminRepository adminRepository;

    @Autowired
    MessageSourceAccessor messageSource;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;

    @Value("${redis.concurrentLimitedCountConnection.key}")
    private String redisConcurrentLimitedCountConnectionKey;

    /**
     * API정책 조회
     **/
    public ApiPolicyVO selectApiPolicy(ApiPolicyRequest request) {

        ApiPolicyVO data = policyRepository.selectApiPolicy(request);

        if (StringUtils.isNotBlank(data.getRegDttm())) {
            data.setGubun("upd");
        }
        return data;
    }

    /**
     * API정책 등록
     **/
    public void insertApiPolicy(ApiPolicyRequest request) {
        policyRepository.insertApiPolicy(request);
        apiPolicyRedisSet(request);
    }

    /**
     * API정책 수정
     **/
    public void updateApiPolicy(ApiPolicyRequest request) {
        policyRepository.updateApiPolicy(request);
        apiPolicyRedisSet(request);
    }

    /**
     * API정책 Redis Set
     **/
    public void apiPolicyRedisSet(ApiPolicyRequest request) {

        if (StringUtils.isBlank(request.getMaxUser())) {
            RedisCommunicater.apiFileCntRedisDel(request.getApiUrl());
        } else {
            RedisCommunicater.apiFileCntRedisSet(request.getApiUrl(), request.getMaxUser());
        }

        if (StringUtils.isBlank(request.getMaxSize())) {
            RedisCommunicater.apiFileSizeRedisDel(request.getApiUrl());
        } else {
            RedisCommunicater.apiFileSizeRedisSet(request.getApiUrl(), request.getMaxSize());
        }

        if (StringUtils.isBlank(request.getLimitedValue())) {
            RedisCommunicater.apiTimeLimitedRedisDel(request.getApiUrl());
        } else {
            RedisCommunicater.apiTimeLimitedRedisSet(request.getApiUrl(), request.getLimitedValue());
        }

        if (StringUtils.isBlank(request.getTxRestrValue())) {
            RedisCommunicater.apiTxRedisDel(request.getApiUrl());
        } else {
            RedisCommunicater.apiTxRedisSet(request.getApiUrl(), request.getTxRestrValue());
        }
    }

    /**
     * APP정책 전체 조회
     **/
    public PolicyResponse getTokenPolicyList(TokenPolicyRequest request) {

        if (request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

        if (request.getPageSize() == 0) {
            request.setPageSize(20);
        }

        request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

        int totCnt = policyRepository.countGetTokenPolicyList(request);
        List<TokenPolicyVO> list = policyRepository.selectGetTokenPolicyList(request);

        for (TokenPolicyVO vo : list) {
            String cnt = RedisCommunicater.getTokenCntRedisGet(vo.getClientId());
            vo.setFailCount(cnt);
            try {
                if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                    String decryptedUserNm = cryptoUtil.decrypt(vo.getUserNm());
                    vo.setUserNm(decryptedUserNm);
                }
            } catch ( UnsupportedEncodingException e ) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            } catch ( Exception e ) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            }
        }
//        List<TokenPolicyVO> useorgList = policyRepository.selectGetTokenUseorgList(request);
//        List<TokenPolicyVO> useorgList = policyRepository.selectGetTokenUserList(request);

        PolicyResponse policyResponse = new PolicyResponse();
        policyResponse.setPageIdx(request.getPageIdx());
        policyResponse.setPageSize(request.getPageSize());
        policyResponse.setTotCnt(totCnt);
        policyResponse.setList(list);
//        policyResponse.setUseorgList(useorgList);
        return policyResponse;
    }

    /**
     * APP정책 상세 조회
     **/
    public TokenPolicyVO getTokenPolicyDetail(TokenPolicyRequest request) {
        TokenPolicyVO data = policyRepository.selectGetTokenPolicy(request);
        data.setFailCount(RedisCommunicater.getTokenCntRedisGet(data.getClientId()));

        if (StringUtils.isNotBlank(data.getRegUser())) {
            data.setGubun("update");
        }

        return data;
    }

    /**
     * APP정책 등록
     **/
    public void getTokenPolicyRegistration(UserPrincipal currentUser, TokenPolicyRequest request) {

        if (StringUtils.isBlank(request.getGubun())) {
            request.setRegUser(currentUser.getUserKey());
            policyRepository.insertGetTokenPolicy(request);
        } else {
            request.setModUser(currentUser.getUserKey());
            policyRepository.updateGetTokenPolicy(request);
        }

        RedisCommunicater.getTokenSetRedisSet(request.getClientId(), request.getLimitCount());
    }

    /**
     * APP정책 초기화 Redis Set
     **/
    public void getTokenPolicyReset(TokenPolicyRequest request) {
        policyRepository.updateGetTokenPolicyReset(request);

        RedisCommunicater.getTokenCntRedisSet(request.getClientId(), "0");
    }

    /**
     * APP정책 설정OFF - Redis Del
     **/
    public void getTokenPolicyUseSetting(TokenPolicyRequest request) {

        if (StringUtils.isBlank(request.getGubun())) {
            policyRepository.insertGetTokenPolicyUseSetting(request);
        } else {
            policyRepository.updateGetTokenPolicyUseSetting(request);
        }
        RedisCommunicater.getTokenRedisDel(request.getClientId());
    }

    /**
     * 상품 정책 조회
     **/
    public PrdPolicyVO selectPrdPolicy(PrdPolicyRequest request) {
        PrdPolicyVO data = policyRepository.selectPrdPolicy(request);
        return data;
    }

    /**
     * 상품 정책 등록
     **/
    public void insertPrdPolicy(PrdPolicyRequest request) {
        policyRepository.insertPrdPolicy(request);
        prdPolicyRedisSet(request);
    }

    /**
     * 상품 정책 Redis Set
     **/
    private void prdPolicyRedisSet(PrdPolicyRequest request) {
        if (StringUtils.isBlank(request.getMaxUser())) {
            RedisCommunicater.apiFileCntRedisDel(request.getPrdCd());
        } else {
            RedisCommunicater.apiFileCntRedisSet(request.getPrdCd(), request.getMaxUser());
        }

        if (StringUtils.isBlank(request.getMaxSize())) {
            RedisCommunicater.apiFileSizeRedisDel(request.getPrdCd());
        } else {
            RedisCommunicater.apiFileSizeRedisSet(request.getPrdCd(), request.getMaxSize());
        }

        if (StringUtils.isBlank(request.getLimitedValue())) {
            RedisCommunicater.apiTimeLimitedRedisDel(request.getPrdCd());
        } else {
            RedisCommunicater.apiTimeLimitedRedisSet(request.getPrdCd(), request.getLimitedValue());
        }

        if (StringUtils.isBlank(request.getTxRestrValue())) {
            RedisCommunicater.apiTxRedisDel(request.getPrdCd());
        } else {
            RedisCommunicater.apiTxRedisSet(request.getPrdCd(), request.getTxRestrValue());
        }
    }

    /**
     * APP 사용량 랜덤제어 정책 조회
     * @param request
     * @return AppUsageRatioVO
     */
    public AppUsageRatioVO selectAppUsageRatioPolicy(AppUsageRatioRequest request) {
        return policyRepository.selectAppUsageRatio(request);
    }

    /**
     * APP 사용량 랜덤제어 정책 등록
     * @param request
     */
    public void insertAppUsageRatioPolicy(AppUsageRatioRequest request) {
        policyRepository.insertAppUsageRatio(request);
        RedisCommunicater.appUsageRatioRedisSet(getAppClientId(request.getAppKey()), Integer.toString(request.getRatio()));
    }

    /**
     * APP 사용량 랜덤제어 정책 삭제
     * @param request
     */
    public void deleteAppUsageRatioPolicy(AppUsageRatioRequest request) {
        policyRepository.deleteAppUsageRatio(request);
        RedisCommunicater.appUsageRatioRedisDel(getAppClientId(request.getAppKey()));
    }

    /** app key로 app_client_id 가져옴 **/
    private String getAppClientId(String appKey) {
        AppsRequest appsRequest = new AppsRequest();
        appsRequest.setAppKey(appKey);
        return adminRepository.selectAppDetail(appsRequest).getAppClientId();
    }

    // 신규
    // api정보조회
    public List<ApiVO> policyGetApiList() {
        List<ApiVO> list = policyRepository.policyGetApiList();
        return list;
    }
    // 클라이언트ID 조회
    public List<ServiceAppVO> policyGetClientIdList(ServiceAppRequest.policyAppRequest request) {
        List<ServiceAppVO> list = policyRepository.policyGetClientIdList(request);
        return list;
    }
    // ServiceAppIpVO
    // 서버IP 조회
    public List<ServiceAppIpVO> policyGetServerIpList(ServiceAppRequest.policyServerIpRequest request) {
        List<ServiceAppIpVO> list = policyRepository.policyGetServerIpList(request);
        return list;
    }
    // 정책 등록
    public void regPolicyService(ServicePolicyRequest request) {
        policyRepository.regPolicyService(request);
        policyRedisSet(request);
    }
    // 정책 조회
    public PolicyServiceResponsePaging getPolicyServiceList(ServicePolicyRequest request) {

        // pageIdx 0일때 1로 세팅
        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);
        // 페이지사이즈 0일때 20으로 세팅
        if(request.getPageSize() == 0)
            request.setPageSize(20);
        // 페이지오프셋 세팅
        request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());
        int totCnt = policyRepository.cntPolicyServiceList(request);

        PolicyServiceResponsePaging pagingData = new PolicyServiceResponsePaging();

        List<PolicyServiceVO> list = policyRepository.getPolicyServiceList(request);
        pagingData.setList(list);
        pagingData.setPageIdx(request.getPageIdx());
        pagingData.setPageSize(request.getPageSize());
        pagingData.setTotCnt(totCnt);
        pagingData.setSelCnt(list.size());

        return pagingData;
    }
    // 정책 상태 변경
    public void updPolicyServiceStatus(ServicePolicyRequest request) {
        //1.Table 상태 변경
        policyRepository.updPolicyServiceStatus(request);
        //2. 적용이면 redis set, 미적용이면 redis del
        if (request.getStatCd().equals("OK")) {
            policyRedisDel(request);
        } else {
            policyRedisSet(request);
        }
    }
    // 정책 삭제
    public void delPolicyService(ServicePolicyRequest request) {
        // 1.Table 상태 변경
        policyRepository.delPolicyService(request);
        // 2.Redis Del
        policyRedisDel(request);
    }
    // 정책 수정 조회
    public PolicyServiceVO getPolicyServiceDetail(ServicePolicyRequest request) {
        PolicyServiceVO data = policyRepository.getPolicyServiceDetail(request);
        return data;
    }
    // 정책 수정 updPolicyService
    public void updPolicyService(ServicePolicyRequest request) {
        PolicyServiceVO data = getPolicyServiceDetail(request);
        // 1.테이블 정보 변경
        policyRepository.updPolicyService(request);
        // 2.적용상태면 레디스까지 Set / 미적용상태면 DB만 변경
        if (request.getStatCd().equals("OK")) {
            ServicePolicyRequest beforeRequest = new ServicePolicyRequest();
            beforeRequest.setUserKey(data.getUserKey());
            beforeRequest.setCnlKey(data.getCnlKey());
            beforeRequest.setApiId(data.getApiId());
            beforeRequest.setLtdTimeFm(data.getLtdTimeFm());

            policyRedisDel(beforeRequest);
            policyRedisSet(request);
        }
    }
    // 레디스 정보 조회
    public PolicyResponse getRedisData(ServicePolicyRequest request) {
        String pattern = redisConcurrentLimitedCountConnectionKey;
        PolicyResponse data = new PolicyResponse();
        // 기관 X | 서버 IP X | API X
        if(StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP X | API X");
            pattern +=  "common";
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 O | 서버 IP X | API X
        if(!StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP X | API X");
            pattern += request.getUserKey();
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 X | 서버 IP O | API X
        if(StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP O | API X");
            pattern += request.getCnlKey();
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 X | 서버 IP X | API O
        if(StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP X | API O");
            pattern += request.getApiUrl();
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 O | 서버 IP O | API X
        if(!StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP O | API X");
            pattern += request.getCnlKey();
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 O | 서버 IP X | API O
        if(!StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP X | API O");
            pattern += request.getUserKey() + "|" + request.getApiUrl();
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 O | 서버 IP O | API O
        if(!StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP O | API O");
            pattern += request.getCnlKey() + "|" + request.getApiUrl();
            data = RedisCommunicater.getRedis(pattern);
        }
        // 기관 X | 서버 IP O | API O
        if(StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP O | API O");
            pattern += request.getCnlKey() + "|" + request.getApiUrl();
            data = RedisCommunicater.getRedis(pattern);
        }
        return data;
    }

    public void policyRedisSet(ServicePolicyRequest request) {
        // 기관 X | 서버 IP X | API X
        if(StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP X | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet("", request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet("", request.getLimitedValue());
            }
        }
        // 기관 O | 서버 IP X | API X
        if(!StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP X | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getUserKey(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getUserKey(), request.getLimitedValue());

            }
        }
        // 기관 X | 서버 IP O | API X
        if(StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP O | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getCnlKey(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getCnlKey(), request.getLimitedValue());
            }
        }
        // 기관 X | 서버 IP X | API O
        if(StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP X | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getApiUrl(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getApiUrl(), request.getLimitedValue());
            }
        }
        // 기관 O | 서버 IP O | API X
        if(!StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP O | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getCnlKey(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getCnlKey(), request.getLimitedValue());
            }
        }
        // 기관 O | 서버 IP X | API O
        if(!StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP X | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getUserKey(), request.getApiUrl(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getUserKey(), request.getApiUrl(), request.getLimitedValue());
            }
        }
        // 기관 O | 서버 IP O | API O
        if(!StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP O | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getCnlKey(), request.getApiUrl(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getCnlKey(), request.getApiUrl(), request.getLimitedValue());
            }
        }
        // 기관 X | 서버 IP O | API O
        if(StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP O | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnSet(request.getCnlKey(), request.getApiUrl(), request.getMaxUser());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnSet(request.getCnlKey(), request.getApiUrl(), request.getLimitedValue());
            }
        }
    }
    public void policyRedisDel(ServicePolicyRequest request) {
        // 기관 X | 서버 IP X | API X
        if(StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP X | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel("");
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel("");
            }
        }
        // 기관 O | 서버 IP X | API X
        if(!StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP X | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getUserKey());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getUserKey());

            }
        }
        // 기관 X | 서버 IP O | API X
        if(StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP O | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getCnlKey());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getCnlKey());
            }
        }
        // 기관 X | 서버 IP X | API O
        if(StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP X | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getApiUrl());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getApiUrl());
            }
        }
        // 기관 O | 서버 IP O | API X
        if(!StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP O | API X");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getCnlKey());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getCnlKey());
            }
        }
        // 기관 O | 서버 IP X | API O
        if(!StringUtils.isBlank(request.getUserKey()) && StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP X | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getUserKey(), request.getApiUrl());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getUserKey(), request.getApiUrl());
            }
        }
        // 기관 O | 서버 IP O | API O
        if(!StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 O | 서버 IP O | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getCnlKey(), request.getApiUrl());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getCnlKey(), request.getApiUrl());
            }
        }
        // 기관 X | 서버 IP O | API O
        if(StringUtils.isBlank(request.getUserKey()) && !StringUtils.isBlank(request.getCnlKey()) && !StringUtils.isBlank(request.getApiUrl())) {
            System.out.println("기관 X | 서버 IP O | API O");
            if (StringUtils.isBlank(request.getLtdTimeFm())) { // 동시접속자수
                RedisCommunicater.concurLimitCntConnDel(request.getCnlKey(), request.getApiUrl());
            } else { // 사용주기
                RedisCommunicater.timeLimitCntConnDel(request.getCnlKey(), request.getApiUrl());
            }
        }
    }

}
