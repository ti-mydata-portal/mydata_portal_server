package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("ipin")
public class IpinVO {

	private String certYn;
	private String userId;
}
