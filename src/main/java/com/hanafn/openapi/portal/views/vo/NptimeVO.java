package com.hanafn.openapi.portal.views.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("nptime")
public class NptimeVO {

	// -------------- DB ---------------- //
	private String userKey;
	private String seqNo;
	private String npTime;
}
