package com.hanafn.openapi.portal.views.service;

import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.dto.SettlementResponse;
import com.hanafn.openapi.portal.views.repository.SettlementRepository;
import com.hanafn.openapi.portal.views.vo.AppPrdUseVO;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class SettlementNiceService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SettlementNiceService.class);

	@Autowired
	SettlementRepository settlementRepository;

	public SettlementResponse getUseInfoNice(SettlementRequest.AppUseInfoRequest request) {
    	SettlementResponse data = new SettlementResponse();
    	
    	List<AppUseVO> appUseList = settlementRepository.selectAppUseInfo(request);
    	
    	List<AppPrdUseVO> appPrdUseList = settlementRepository.getAppPrdUseInfo(request);
    	
    	data.setAppPrdUseList(appPrdUseList);
    	data.setAppUseList(appUseList);
    	
    	
    	return data;
    }
    
    public SettlementResponse getUseInfoNicePaging(SettlementRequest.AppUseInfoRequest request) {
    	SettlementResponse data = new SettlementResponse();
    	
    	if(request.getPageIdxTot() == 0)
    		request.setPageIdxTot(request.getPageIdxTot() + 1);

        if(request.getPageSizeTot() == 0){
        	request.setPageSizeTot(10);
        }
        
        if(request.getPageIdxUse() == 0)
    		request.setPageIdxUse(request.getPageIdxUse() + 1);

        if(request.getPageSizeUse() == 0){
        	request.setPageSizeUse(10);
        }
        
        request.setPageOffsetTot((request.getPageIdxTot()-1)*request.getPageSizeTot());
        int totCntTot = settlementRepository.countAppUseInfoPaging(request);
    	List<AppUseVO> appUseList = settlementRepository.selectAppUseInfoPaging(request);
    	
    	request.setPageOffsetUse((request.getPageIdxUse()-1)*request.getPageSizeUse());
        int totCntUse = settlementRepository.countAppPrdUseInfoPaging(request);
    	List<AppPrdUseVO> appPrdUseList = settlementRepository.getAppPrdUseInfoPaging(request);
    	
    	data.setPageIdxUse(request.getPageIdxTot());
    	data.setPageSizeUse(request.getPageSizeTot());
    	data.setTotCntUse(totCntUse);
    	data.setAppPrdUseList(appPrdUseList);
    	data.setSelCntUse(appPrdUseList.size());
    	data.setPageIdxTot(request.getPageIdxTot());
    	data.setPageSizeTot(request.getPageSizeTot());
    	data.setTotCntTot(totCntTot);
    	data.setAppUseList(appUseList);
    	data.setSelCntTot(appUseList.size());

    	return data;
    }
    
    public SettlementResponse getAppUseInfoPaging(SettlementRequest.AppUseInfoRequest request) {
    	SettlementResponse data = new SettlementResponse();
    	
    	if(request.getPageIdxTot() == 0)
    		request.setPageIdxTot(request.getPageIdxTot() + 1);

        if(request.getPageSizeTot() == 0){
        	request.setPageSizeTot(10);
        }
        
        request.setPageOffsetTot((request.getPageIdxTot()-1)*request.getPageSizeTot());
        int totCnt = settlementRepository.countAppUseInfoPaging(request);
    	List<AppUseVO> appUseList = settlementRepository.selectAppUseInfoPaging(request);
    	
    	data.setPageIdxTot(request.getPageIdxTot());
    	data.setPageSizeTot(request.getPageSizeTot());
    	data.setTotCntTot(totCnt);
    	data.setAppUseList(appUseList);
    	data.setSelCntTot(appUseList.size());
    	
    	return data;
    }
    
    public SettlementResponse getUseInfoPaging(SettlementRequest.AppUseInfoRequest request) {
    	SettlementResponse data = new SettlementResponse();
    	
    	if(request.getPageIdxUse() == 0)
    		request.setPageIdxUse(request.getPageIdxUse() + 1);

        if(request.getPageSizeUse() == 0){
        	request.setPageSizeUse(10);
        }
        
        request.setPageOffsetUse((request.getPageIdxUse()-1)*request.getPageSizeUse());
        int totCnt = settlementRepository.countAppPrdUseInfoPaging(request);
    	
    	List<AppPrdUseVO> appPrdUseList = settlementRepository.getAppPrdUseInfoPaging(request);
    	
    	data.setPageIdxUse(request.getPageIdxTot());
    	data.setPageSizeUse(request.getPageSizeTot());
    	data.setTotCntUse(totCnt);
    	data.setAppPrdUseList(appPrdUseList);
    	data.setSelCntUse(appPrdUseList.size());
    	
    	return data;
    }
    
    public SettlementResponse.PayInfoResponse getPayInfoNice(SettlementRequest.AppUseInfoRequest request) {
    	SettlementResponse.PayInfoResponse data = new SettlementResponse.PayInfoResponse();
    	
    	if(request.getPageIdx() == 0)
    		request.setPageIdx(request.getPageIdx() + 1);

        if(request.getPageSize() == 0){
        	request.setPageSize(20);
        }
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
        int totCnt = settlementRepository.countPayInfoNice(request);
    	List<PayInfoVO> payInfoList = settlementRepository.getPayInfoNice(request);
    	
    	data.setPageIdx(request.getPageIdx());
    	data.setPageSize(request.getPageSize());
    	data.setTotCnt(totCnt);
    	data.setPayInfoList(payInfoList);
    	data.setSelCnt(payInfoList.size());
    	
    	return data;
    }
    
    public SettlementResponse.PayInfoResponse getPayInfoNiceALL(SettlementRequest.AppUseInfoRequest request) {
    	SettlementResponse.PayInfoResponse data = new SettlementResponse.PayInfoResponse();
    	List<PayInfoVO> payInfoList = settlementRepository.getPayInfoNiceALL(request);
    	data.setPayInfoList(payInfoList);
    	
    	return data;
    }
    
    public SettlementResponse.PayInfoResponse  getPayDetailInfoNice(SettlementRequest.PayDetailRequest request) {
    	SettlementResponse.PayInfoResponse data = new SettlementResponse.PayInfoResponse();
    	
    	List<PayInfoVO> payInfoList = settlementRepository.getPayDetailInfoNice(request);
    	
    	data.setPayInfoList(payInfoList);
    	
    	return data;
    }
}
