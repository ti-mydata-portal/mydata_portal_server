package com.hanafn.openapi.portal.views.dto;

import com.hanafn.openapi.portal.views.vo.PolicyServiceVO;
import lombok.Data;

import java.util.List;

@Data
public class PolicyServiceResponsePaging {
    private int pageIdx;
    private int pageSize;
    private int totCnt;
    private int selCnt;
    private List<PolicyServiceVO> list;
}
