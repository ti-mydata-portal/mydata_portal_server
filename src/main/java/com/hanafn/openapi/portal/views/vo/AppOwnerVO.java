package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("appOwner")
public class AppOwnerVO {
    private String userKey;
    private String userNm;
    private String userId;
}
