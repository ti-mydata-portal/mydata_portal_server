package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("payInfo")
public class PayInfoVO {
	
	private String appKey;
	private String appNm;
	private int prdCnt;
	private int amt;
	private String useCnt;
	private String prdNm;
	private String useTeam;
	
	
	private String moid;
	private String goodsName;
	private String mid;
	private String ediDate;
	private String signData;
	private String buyerName;
	private String buyerTel;
	private String buyerEmail;
	private int reqAmt;
	private String payMethod;
	private String authResultCode;
	private String authResultMsg;
	private String authToken;
	private String txTid;
	private String resultCode;
	private String resultMsg;
	private String authCode;
	private String authDate;
	private String payStatCd;
	private String cardCode;
	private String cardName;
	private String cardNo;
	private String cardQuota;
	private String cardCl;
	private String vBankCode;
	private String vBankName;
	private String vBankNum;
	private String vBankExpDate;
	private String vBanAuthCode;
}
