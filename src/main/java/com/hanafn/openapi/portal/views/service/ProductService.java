package com.hanafn.openapi.portal.views.service;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.ApiTagVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.CommCodeVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.views.dto.ApiRsponsePaging;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.dto.SvcCodeResponse;
import com.hanafn.openapi.portal.views.repository.ApiRepository;
import com.hanafn.openapi.portal.views.repository.ProductRepository;
import com.hanafn.openapi.portal.views.vo.SvcCodeVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;
    private final ApiRepository apiRepository;

    @Value("${pay.backoffice.svcCode.url}")
    private String boUrl;

    @Autowired
    MessageSourceAccessor messageSource;
    
    @Autowired
	AdminRepository adminRepository;

    public CommCodeResponse selectPrdCommList (CommCodeRequest commCodeRequest){
        List<CommCodeVO> list = repository.selectPrdCommList();

        CommCodeResponse commCodeResponse = new CommCodeResponse();
        commCodeResponse.setCodeList(list);
        commCodeResponse.setSelCnt(list.size());

        return commCodeResponse;
    }

    public ProductResponse.ProductListResponse selectPrdList(ProductRequest request){

        if(request.getPageIdx() == 0)
            request.setPageIdx(request.getPageIdx() + 1);

        if(request.getPageSize() == 0){
            request.setPageSize(20);
        }

        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        int totCnt = repository.countPrdList(request);
        List<ProductVO> list = repository.selectPrdList(request);

        ProductResponse.ProductListResponse data  = new ProductResponse.ProductListResponse();
        data.setProdList(list);
        data.setTotCnt(totCnt);
        data.setSelCnt(list.size());
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());

        return data;
    }

    public ProductResponse selectPrdDetail(ProductRequest request) {

        ProductVO prd = adminRepository.selectPrdDetail(request);
        request.setPrdCtgy(prd.getPrdCtgy());
        List<ApiVO> apiList = repository.selectPrdApiInfo(request);

        ProductResponse res = new ProductResponse();
        res.setProd(prd);
        res.setPrdApiList(apiList);

        return res;
    }

    public ApiRsponsePaging selectPrdApiList(ApiRequest request) {

        List<ApiVO> list = apiRepository.selectApiListNoPaging(request);

        for(ApiVO apiInfo : list){
            request.setApiId(apiInfo.getApiId());
            List<ApiTagVO> apiTagList = apiRepository.selectApiTagList(request);
            apiInfo.setApiTagList(apiTagList);
        }

        ApiRsponsePaging pagingData = new ApiRsponsePaging();
        pagingData.setList(list);
        pagingData.setSelCnt(list.size());

        return pagingData;
    }

    /* 상품 메인노출 순서 등록 */
    public int updatePrdOrder(ProductRequest request) {
        int cnt = repository.countPrdOrder(request);

        if (cnt > 0) {
            throw new BusinessException("E804",messageSource.getMessage("E804"));
        } else {
            repository.updatePrdOrder(request);
        }
        return cnt;
    }

    /* 백오피스 서비스코드 조회 */
    public SvcCodeResponse selectSvcCodeList(ProductRequest request, HttpServletRequest httpRequest)throws Exception{

        SvcCodeResponse data = new SvcCodeResponse();
        Map<String,Object> param = new HashMap<String, Object>();
        ObjectMapper om = new ObjectMapper();
        BufferedReader br = null;
        HttpURLConnection hcon = null;
        URL url;
        StringBuffer result = new StringBuffer();

        param.put("prdCtgy", request.getPrdCtgy());
        param.put("searchNm", request.getSearchNm());

        try {

            String jsonParam = om.writeValueAsString(param);
            url = new URL(boUrl);
            hcon = (HttpURLConnection) url.openConnection();

            hcon.setRequestMethod("POST");
            hcon.setDoOutput(true);
            hcon.setRequestProperty("Content-Type", "application/json; charset=euc-kr");
            hcon.setRequestProperty("Authorization", httpRequest.getHeader("Authorization"));

            OutputStream os;

            os = hcon.getOutputStream();
            os.write(jsonParam.getBytes());
            os.flush();

            int code = hcon.getResponseCode();
            String message = hcon.getResponseMessage();
            if(code != 200 || code < 0) {
                throw new BusinessException(code+"",message);
            }

            br = new BufferedReader(new InputStreamReader(hcon.getInputStream()));

            String temp;
            while ((temp = br.readLine()) != null) {
                result.append(temp);
            }

            log.info("****************************** 서비스코드조회 통신 시작 ************************");
            log.info("[ZOO] URL : " + boUrl);
            log.info("[ZOO] result : " + result.toString());
            log.info("****************************** 서비스코드조회 통신 끝 ************************");

            Map<String, Object> resultMap = om.readValue(result.toString(), Map.class);

            if(resultMap != null && !resultMap.get("resultCode").equals("") && resultMap.get("resultCode").equals("0000")) {
//				data.setPageIdx((int)resultMap.get("pageIdx"));
//				data.setPageSize((int)resultMap.get("pageSize"));
//				data.setTotCnt((int)resultMap.get("totCnt"));
//				data.setSelCnt((int)resultMap.get("selCnt"));
                List<Map<String, Object>> svcCodeList = (List<Map<String, Object>>) resultMap.get("list");
                List<SvcCodeVO> list = new ArrayList<SvcCodeVO>();

                for(Map<String, Object> svcCode : svcCodeList) {
                    SvcCodeVO vo = new SvcCodeVO();
                    vo.setServiceCd((String)svcCode.get("serviceCd"));
                    vo.setServiceNm((String)svcCode.get("serviceNm"));

                    list.add(vo);
                }
                data.setSvcCodeList(list);
            }else {
                throw new BusinessException((String) resultMap.get("errorCode"), (String) resultMap.get("message"));
            }
        } catch (EOFException e) {
            log.error(e.getMessage());
        } catch (BusinessException e) {
            log.error(e.getMessage());
        } finally {

            try {
                if(br != null) {
                    br.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
        return data;
    }
}
