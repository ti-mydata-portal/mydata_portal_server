package com.hanafn.openapi.portal.views.dto;

import lombok.Data;
import java.util.List;

@Data
public class MailRequest {

    private String sender;        	// 발신자
    private List<Object> receiver;  		// 수신
    private List<String> receiverPrd;
    private String receiverInput;
    private String tagmap011;
    private String tagmap900;
    private String senddate;
    private String reservate;
    
    private String trCallback;
    private String trSubject;
    private String trMsg;
    private String trSenddate;
    private List<Object> receiverSms;  		// 수신
    private List<String> receiverPrdSms;
    private String receiverInputSms;
    
    @Data
    public static class MailSender {
    	private String userKey;
    }
}
