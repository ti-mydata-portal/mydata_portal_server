package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;

@Data
public class ElasticRequest {
    private String userorgNm;
    private String useorg;
    private int pageIdx;
    private int pageSize;
    private String searchUserKey;
    private String clientId;
    private String statusCode;
    private String searchNm;
    private String searchStDt;
    private String searchEnDt;
}
