package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;

import lombok.Data;

@Data
public class NicePayRequest {
	private String appKey;
    private int price;
    private String goodsName;
    private String buyerName;
    private String buyerTel;
    private String buyerEmail;
    private String moid;
    private String ediDate;
    private String signData;
    private String mid;
    private String regUser;
    private int useCnt;
    private int reqAmt;
    private String antGbCd;
    private String payConfirmYn;
	private String payAgreeYn;
	private String creditReportYn;
    private List<AppPrdInfoVO> appPrdInfo;
    
    @Data
    public static class NicePayCancelRequest {
    	private String tid;
    	private int cancelAmt;
    	private String partialCancelCode; 
    }
    
    @Data
    public static class NicePayBoRequest {
    	private String mppCd1;
    	private String mppCd2;
    	private String mppCd3;
    	private String mppUser;
    	private int restCnt;
    	private String useEndDt;
    	private String useStDt;
    	private String serviceCd;
    	private String appKey;
    }
    
    @Data
    public static class NicePayReusltRequest {
    	private String payMethod;
    	private String goodsName;
    	private String amt;
    	private long mID;
    	private String moid;
    	private String buyerName;
    	private String buyerEmail;
    	private String buyerTel;
    	private String returnURL;
    	private String goodsCl;
    	private String transType;
    	private String charSet;
    	private String reqReserved;
    	private String ediDate;
    	private String signData;
    	private String verifySType;
    	private String encGoodsName;
    	private String encBuyerName;
    	private String npDirectYn;
    	private String npDirectLayer;
    	private String jsVer;
    	private String npSvcType;
    	private String deployedVer;
    	private String deployedDate;
    	private String deployedFileName;
    	private String authResultCode;
    	private String authResultMsg;
    	private String authToken;
    	private String txTid;
    	private String nextAppURL;
    	private String netCancelURL;
    	private String signature;
    	private String resultCode;
    	private String resultMsg;
    	private String cardCode;
    	private String cardName;
    	private String cardNo;
    	private String cardQuota;
    	private String vbankCode;
    	private String vbankName;
    	private String vbankNum;
    	private String vbankExpTime;
    	private String vbankExpDate;
    	private String modUser;
    	private String authCode;
    	private String authDate;
    	private String cardCl;
    	private List<AppPrdInfoVO> appPrdInfo;  // 앱 API 정보
    }
}
