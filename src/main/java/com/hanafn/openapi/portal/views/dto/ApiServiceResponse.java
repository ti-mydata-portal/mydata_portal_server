package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.ApiServiceVO;

import lombok.Data;

@Data
public class ApiServiceResponse {
	private String apiSvrStatCd;
	private String minVer;
	private String curVer;
    private List<ApiServiceVO> hisList;
}
