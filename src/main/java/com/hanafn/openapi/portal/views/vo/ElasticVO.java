package com.hanafn.openapi.portal.views.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("elastic")
public class ElasticVO {
	private String clientIp;
    private String message;
    private String rspMsg;
    private String appName;
    private String dealDate;
    private String apiId;
    private String clientId;
    private String httpStatus;
    private String host;
    private String apiTranId;
    private String requestUri;
    private String orgCode;
    private String topic;
    private String index;
    private String resBody;
    private String reqBody;
}
