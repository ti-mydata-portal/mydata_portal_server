package com.hanafn.openapi.portal.views.repository;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AplvPrdReqeust;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppDefresRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppReadUserRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsApiInfoRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsBlockKeyRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsCnlKeyRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRegRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppApiInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppBlackListVO;
import com.hanafn.openapi.portal.admin.views.vo.AppCnlInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppPrdInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppReadInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.dto.ServiceAppRequest;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.dto.UserRequest;
import com.hanafn.openapi.portal.views.vo.AppOwnerVO;
import com.hanafn.openapi.portal.views.vo.HfnUserRoleVO;
import com.hanafn.openapi.portal.views.vo.ServiceAppVO;

@Mapper
public interface AppsRepository {

	List<AppsVO> selectAppAll(AppsRequest appsRequest);

	int countAppsList(AppsRequest appsRequest);
	List<AppsVO> selectAppsList(AppsRequest appsRequest);
	List<AppsVO> selectAppListAll(AppsRequest appsRequest);

	int updateAppStatCd(AppsRequest appsRequest);
	int selectPrdAplvCnt(AplvPrdReqeust aplvPrdReq);

	// void insertApp(AppsRegRequest appsRegRequest);
	void insertAppInfo(AppsRegRequest appsRegRequest);
	void insertAppInfoNice(AppsRegRequest appsRegRequest);

	void updateAppPrdInfo(AppPrdInfoVO vo);
	void updateAppCopInfo(AppsRegRequest appsRegRequest);
	
	void insertAppMod(AppsRegRequest appsRegRequest);
	void insertAppModForUpdate(AppsRegRequest appsRegRequest);

	void deleteAppMod(AppsRegRequest appsRegRequest);
	void insertAppApiInfo(AppsApiInfoRequest appsApiInfoRequest);
	void updateAppApiInfo(AppsApiInfoRequest appsRequest);

	/*** 앱 삭제 ***/
	void appDel(AppsRegRequest appsRegRequest);

	/*** 앱수정 상세조회 ***/
	AppsVO selectAppModifyDetail(AppsRequest appsRequest);
	AppsVO selectAppModifyDetailAdmin(AppsRequest appsRequest);
	
	AppsVO selectAppModDetail(AppsRequest appsRequest);
	List<AppApiInfoVO> selectAppApiInfo(AppsRequest appsRequest);
	List<AppCnlInfoVO> selectAppCnlInfo(AppsRequest appsRequest);
	List<AppBlackListVO> selectAppBlackListInfo(AppsRequest appsRequest);
	
	List<AppApiInfoVO> selectAppApiInfoDetail(AppsRequest appsRequest);
	List<AppPrdInfoVO> selectApPrdInfoDetail(AppsRequest appsRequest); 
	List<AppPrdInfoVO> selectApPrdInfoDetailALL(AppsRequest appsRequest); 
	List<AppCnlInfoVO> selectAppCnlInfoDetail(AppsRequest appsRequest);
	List<AppApiInfoVO> selectAppApiInfoWait(AppsRequest appsRequest);
	List<AppCnlInfoVO> selectAppCnlInfoWait(AppsRequest appsRequest);
	List<AppReadInfoVO> selectAppReadInfoList(AppsRequest appsRequest);
	List<AppBlackListVO> selectAppBlackList(AppsRequest appsRequest);
	ApiVO selectApi(AdminRequest.ApiRequest apiRequest);

	/*** 앱수정 ***/
	void updateAppInfo(AppsRegRequest appsRegRequest);
    void updateAppOwnerHis(AppsRegRequest request);
	void updateAppInfoMod(AppsRegRequest appsRegRequest);
    void backupAppInfoReject(AppsRegRequest appsRegRequest);
	void delAppCnlInfo(AppsCnlKeyRequest appsCnlKeyRequest);
	void delAppApiInfo(AppsApiInfoRequest appsApiInfoRequest);
	void insertAppReadUser(AppReadUserRequest reqeust);
	void appReadUserDelete(String request);
	void insertBackupAppReadUser(AppReadUserRequest reqeust);
	
	void delAppBlackListInfo(AppsBlockKeyRequest appsCnlKeyRequest);
	void insertAppBlackList(AppsBlockKeyRequest appsCnlKeyRequest);

	/*** 승인대기 앱 정보 조회 관련 ***/
	List<AppApiInfoVO> selectAppApiInfoMod(AppsRequest appsRequest);
	List<AppCnlInfoVO> selectAppCnlInfoMod(AppsRequest appsRequest);
	int countAppsListMod(AppsRequest appsRequest);
	List<AppsVO> selectAppsListMod(AppsRequest appsRequest);
	
	List<AppsVO> selectAppAuth(AppsRequest appsRequest);
	List<AppsVO> selectEditAppAuth(AppsRequest appsRequest);

	/*** 클라이언트ID, 시크릿키 발급,재발급 ***/
	void updateAppScr(AppsRegRequest appsRegRequest);
	void updateAppScrMod(AppsRegRequest appsRegRequest);
	void updateAppScrHis(AdminRequest.AppsScrRequest appsScrRequest);
	void updateAppDldttm(String value);

	AppsVO keyReturn(AdminRequest.AppsScrRequest appsScrRequest);
	List<AppsVO> appListSelected(AppsRequest appsRequest);
	int countAppListSelected(AppsRequest appsRequest);

	List<HfnUserRoleVO> hfnCompanyAll(UseorgRequest request);

	/** 대응답 처리 **/
	String appUseDefresYn(String appKey);   // 앱 대응답사용여부 체크
	void appDefresChange(AppDefresRequest request); // 앱 대응답사용여부 변경

	/** NICE **/
    int countAppList(AppsRequest request);
	List<AppsVO> selectAppList(AppsRequest request);
	List<AppReadInfoVO> selectAppUserList(AppsRequest request);
	
	AppOwnerVO selectOwnerUser(UserRequest.searchUserRequest request);
	void insertAppBlackListCnl(AppsBlockKeyRequest req);

    MailSmsVO selectSendApp(AppsRegRequest request);
    MailSmsVO selectSendPrd(AppsRegRequest appsRegRequest);
    List<String> selectSendPrdList(HfnUserRequest hfnUserRequest);
    // 서비스앱
	int countServiceAppList(ServiceAppRequest request);
	int chkServiceDup(ServiceAppRequest.ServiceAppRegistRequest request);
	String selUseorgGb(ServiceAppRequest.ServiceAppRegistRequest request);
	List<ServiceAppVO> selectServiceAppList(ServiceAppRequest request);
	// 서비스앱등록
	void serviceAppReg(ServiceAppRequest.ServiceAppRegistRequest request);
	// 서비스앱 이력 등록
	void serivceAppRegHis(ServiceAppRequest.ServiceAppRegistRequest request);
	void serivceAppRegStatusHis(ServiceAppVO request);

	// 앱채널정보 등록
	void serviceAppChannelReg(ServiceAppRequest.ServiceAppChannelRequest request);
	// 서비스앱 상태변경
	void serviceAppStatusUpd(ServiceAppRequest request);
	// 서비스앱 상세보기
	ServiceAppVO serviceAppDetail(ServiceAppRequest request);
	Map<String, Object> serviceAppDetailMap(LinkRequest request);
	List<Map<String, Object>> serviceAppDetailRedirectMap(LinkRequest request);
	List<Map<String, Object>> serviceAppDetailSchemeMap(LinkRequest request);
	List<Map<String, Object>> serviceAppDetailIpMap(LinkRequest request);
	
	// 서비스앱 IP정보
	List<ServiceAppVO> selectServiceAppIps(ServiceAppRequest request);
	// 서비스앱 정보 수정
	void serviceAppUpd(ServiceAppRequest.ServiceAppRegistRequest request);
	// 서비스앱 채널 정보 삭제
	void serviceAppChDel(ServiceAppRequest.ServiceAppRegistRequest request);
	//client_id 기준 app_key 가져오기
	String selectAppKey(ServiceAppRequest.ServiceAppRegistRequest request);
	// 서비스앱 통계 조회
	List<ServiceAppVO> selectServiceAppStatsList(ServiceAppRequest.StatsAppRequest request);
	void insertAppRedirectInfo(ServiceAppRequest.AppRedirectInfoRequest request);
	void insertAppScheme(ServiceAppRequest.AppSchemeRequest request);
	List<ServiceAppVO> selectAppRedirectInfo(ServiceAppRequest request);
	List<ServiceAppVO> selectAppScheme(ServiceAppRequest request);
	void delAppRedirectInfo(ServiceAppRequest.ServiceAppRegistRequest request);
	void delAppScheme(ServiceAppRequest.ServiceAppRegistRequest request);


}