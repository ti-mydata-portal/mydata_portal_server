package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("terms")
public class TermsVO {

	private int seqNo;
	private String termsCd;
	private String subject;
	private String ctnt;
	private String instDttm;
	private String regUser;
	private String regId;
	private String regDttm;
	private String modUser;
	private String modId;
	private String modDttm;
}
