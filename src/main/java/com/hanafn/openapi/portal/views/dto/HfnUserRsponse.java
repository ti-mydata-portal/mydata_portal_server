package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.views.vo.HfnLineVO;

import lombok.Data;

@Data
public class HfnUserRsponse {
    private int selCnt;
    private HfnUserVO hfnUserVO;
    private HfnLineVO hfnLineVO;
    private List<HfnUserVO> list;

    @Data
    public static class HfnAltUsersResponse {
        private List<HfnLineVO> hfnUserAvaliableAltList;
    }
}
