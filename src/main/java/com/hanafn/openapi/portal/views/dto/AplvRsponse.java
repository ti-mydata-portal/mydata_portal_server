package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.views.vo.AplvHisVO;
import com.hanafn.openapi.portal.views.vo.RequestApiVO;
import com.hanafn.openapi.portal.views.vo.UseorgVO;

import lombok.Data;

@Data
public class AplvRsponse{

    @Data
    public static class AplvDetilResponse {
        private String aplvSeqNo;
        private String aplvReqCd;
        private String aplvStatCd;
        private String aplvDivCd;
        private String aplvReqCtnt;
        private String regDttm;
        private String regUser;
        private String regId;
        private String procDttm;
        private String procUser;
        private String rejectCtnt;
        private String aplvBtnYn;
        private String appSvcAddDt;
        private String procId;

        private UseorgVO useorgInfo;
        private AppsVO appInfo;
        private List<ApiVO> apiList;
        private List<ApiVO> ipList;
        private List<AplvHisVO> aplvHisList;
        private List<RequestApiVO> requestApiList;
    }
}
