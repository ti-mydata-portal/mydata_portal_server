package com.hanafn.openapi.portal.views.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("useorg")
public class UseorgVO {

	// -------------- DB ---------------- //
	private String userKey;
	private String useorgId;
	private String useorgNm;
	private String brn;
	private String useorgStatCd;
	private String entrCd;
	private String encKey;
	private String useorgGb;
	private String useorgNo;
	private String useorgAddr;
	private String useorgDomain;
	private String relorgCd;
	private String provCertCd;
	private String useorgCtnt;
	private String regUser;
	private String regDttm;
	private String modUser;
	private String modDttm;
	private String serialNum;
	private String useorgDomainIp;
	private String certIssuerDn;
	private String certOid;
	private String cnlKey;
	private String domainIp;
	private String isRcvOrg;
	private List<HashMap> clientIPs;
	private List<HashMap> useorgDomainIPs;
	private List<NptimeVO> nptimes;
}
