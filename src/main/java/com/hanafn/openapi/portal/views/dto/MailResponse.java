package com.hanafn.openapi.portal.views.dto;


import lombok.Data;

@Data
public class MailResponse {

	private String email;
	
}
