package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("payBo")
public class PayBoVO {
	
	private String userKey;
	private String appKey;
	private String appNm;
	private String prdCtgy;
	private String prdKind;
	private String appCtnt;
	private String appSvcNm;
	private String appSvcUrl;
	private String copRegNo;
	private String copNm;
	private String ownNm;
	private String bizCdtn;
	private String bizType;
	private String copAddr1;
	private String copAddr2;
	private String copAddrNo;
	private String dirNm;
	private String dirTelNo1;
	private String dirTelNo2;
	private String dirTelNo3;
	private String dirEmail;
	private String dirDomain;
	private String payMethod;
	private int amt;
	private String authDate;
	private String txTid;
	private String moid;
	private String goodsName;
	private String buyerName;
	private String buyerTel;
	private String buyerEmail;
	private String cardQuota;
	private String planCd;
	private String term;

	private String appClientId;
}
