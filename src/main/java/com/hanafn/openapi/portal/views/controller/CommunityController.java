package com.hanafn.openapi.portal.views.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaInsertRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeResponse;
import com.hanafn.openapi.portal.admin.views.service.CommService;
import com.hanafn.openapi.portal.admin.views.vo.CommCodeVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.views.dto.FaqRequest;
import com.hanafn.openapi.portal.views.dto.FaqResponse;
import com.hanafn.openapi.portal.views.dto.NoticeRequest;
import com.hanafn.openapi.portal.views.dto.NoticeResponse;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.service.SettingNiceService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/community")
@Slf4j
@RequiredArgsConstructor
public class CommunityController {

    private final SettingNiceService settingService;
    private final SettingRepository settingRepository;
    private final MessageSourceAccessor messageSource;
    
    @Autowired
    private CommService commService;
    
    @Autowired
	public MailUtils sendMail;

    @PostMapping("/selectNoticeList")
    public ResponseEntity<?> selectNoticeList(@Valid @RequestBody NoticeRequest.NoticeListRequest noticeListRequest, @CurrentUser UserPrincipal currentUser) {
        log.debug("###################### 개발자공간-공지사항LIST ######################");

        // 비로그인 시에는 정상 공지사항만 조회
        if(currentUser == null) {
            noticeListRequest.setStatCd("OK");
        }
        else { // 로그인 시, 관리자포탈에서 포탈관리자 일때만 정상 공지사항 및 삭제된 공지사항을 볼 수 있음
            if(StringUtils.equals(currentUser.getSiteCd(), "adminPortal") ||
                    StringUtils.equals(currentUser.getSiteCd(), "backoffice") ||
                    StringUtils.equals(currentUser.getSiteCd(), "pvBatch")) {
                Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
                for(GrantedAuthority ga : authorities){
                    if(!ga.getAuthority().contains("ROLE_SYS_ADMIN")){
                        noticeListRequest.setStatCd("OK");
                        break;
                    }
                }
            } else { // 로그인 시 이용자 포탈에서는 정상 공지사항만 볼 수 있음
                noticeListRequest.setStatCd("OK");
            }
        }

        NoticeResponse.NoticeListResponse data = settingService.selectNoticeList(noticeListRequest);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/detailNotice")
    public ResponseEntity<?> detailNotice(@Valid @RequestBody NoticeRequest noticeRequest, @CurrentUser UserPrincipal currentUser) {

        NoticeResponse data;
        if(currentUser == null) {
            data = settingService.detailNotice("", noticeRequest);
        }
        else {
            data = settingService.detailNotice(currentUser.getSiteCd(), noticeRequest);
        }


        return ResponseEntity.ok(data);
    }

    @PostMapping("/insertNotice")
    public ResponseEntity<?> insertNotice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody NoticeRequest noticeRequest) {

        noticeRequest.setRegId(currentUser.getUsername());
        noticeRequest.setRegUserId(currentUser.getUserKey());
        settingRepository.insertNotice(noticeRequest);

        return ResponseEntity.ok(new SignUpResponse(true, "Notice Regist successfully"));
    }

    @PostMapping("/deleteNotice")
    public ResponseEntity<?> deleteNotice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody NoticeRequest noticeRequest) {

        noticeRequest.setModId(currentUser.getUserId());
        settingRepository.deleteNotice(noticeRequest);
        return ResponseEntity.ok(new SignUpResponse(true, "Notice Delete successfully"));
    }

    @PostMapping("/updateNotice")
    public ResponseEntity<?> updateNotice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody NoticeRequest noticeRequest) {

        noticeRequest.setModId(currentUser.getUsername());
        noticeRequest.setModUserId(currentUser.getUserKey());
        settingRepository.updateNotice(noticeRequest);
        return ResponseEntity.ok(new SignUpResponse(true, "Notice Update successfully"));
    }

    @PostMapping("/selectFaqList")
    public ResponseEntity<?> selectFaqList(@Valid @RequestBody FaqRequest.FaqListRequest faqListRequest, @CurrentUser UserPrincipal currentUser) {
        log.debug("###################### 개발자공간-FAQ LIST ######################");

        // 비로그인 시에는 정상 FAQ만 조회
        if(currentUser == null) {
        	faqListRequest.setStatCd("OK");
        }
        else { // 로그인 시, 관리자포탈에서 포탈관리자 일때만 정상 공지사항 및 삭제된 공지사항을 볼 수 있음
            if(StringUtils.equals(currentUser.getSiteCd(), "adminPortal") ||
                    StringUtils.equals(currentUser.getSiteCd(), "pvBatch") ||
                    StringUtils.equals(currentUser.getSiteCd(), "backoffice")) {
                Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
                for(GrantedAuthority ga : authorities){
                    if(!ga.getAuthority().contains("ROLE_SYS_ADMIN")){
                        faqListRequest.setStatCd("OK");
                        break;
                    }
                }
            } else { // 로그인 시 이용자 포탈에서는 정상 공지사항만 볼 수 있음
                faqListRequest.setStatCd("OK");
            }
        } 

        FaqResponse.FaqListResponse data = settingService.selectFaqList(faqListRequest);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/detailFaq")
    public ResponseEntity<?> detailFaq(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody FaqRequest faqRequest) {

        FaqResponse data = settingService.detailFaq(faqRequest);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/insertFaq")
    public ResponseEntity<?> insertFaq(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody FaqRequest faqRequest) {

    	faqRequest.setRegId(currentUser.getUserKey());
        settingRepository.insertFaq(faqRequest);
        return ResponseEntity.ok(new SignUpResponse(true, "Faq Regist successfully"));
    }

    @PostMapping("/updateFaq")
    public ResponseEntity<?> updateFaq(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody FaqRequest faqRequest) {

    	faqRequest.setModId(currentUser.getUserKey());
        settingRepository.updateFaq(faqRequest);
        return ResponseEntity.ok(new SignUpResponse(true, "Faq Update successfully"));
    }

    @PostMapping("/deleteFaq")
    public ResponseEntity<?> deleteFaq(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody FaqRequest faqRequest) throws Exception {

        settingRepository.deleteFaq(faqRequest);
        return ResponseEntity.ok(new SignUpResponse(true, "Faq Delete successfully"));
    }

    @PostMapping("/insertQna")
    public ResponseEntity<?> insertQna(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody QnaInsertRequest request) throws Exception {

    	request.setUserKey(currentUser.getUserKey());
        settingRepository.insertQna(request);
        
        List<HashMap<String, Object>> adminList = settingRepository.selectAdminEmail(request);
        
        CommCodeRequest commCodeRequest = new CommCodeRequest();
        commCodeRequest.setCommCdId("1100");
        commCodeRequest.setCdId(request.getPrdCtgy());
        CommCodeResponse ctgyList = commService.selectCommCode(commCodeRequest);
        String prdCtgyNm = "";
        
        List<CommCodeVO> codeList = ctgyList.getCodeList();
        if(codeList != null && codeList.size() > 0) {
        	prdCtgyNm = codeList.get(0).getCdNm();
        }
        
        List<MailSmsVO> mailList = new ArrayList<MailSmsVO>();
        List<MailSmsVO> smsList = new ArrayList<MailSmsVO>();
        for(HashMap<String, Object> admin: adminList) {
        	MailSmsVO voEmail = new MailSmsVO();
        	MailSmsVO voSms = new MailSmsVO();
            
        	voEmail.setMailType("API_006");
        	voEmail.setEmail((String)admin.get("USER_EMAIL"));
            voEmail.setTagmap002(request.getPrdCtgy());
            voEmail.setTagmap003(request.getQnaType());
            voEmail.setTagmap011(request.getReqTitle());
            voEmail.setTagmap900(request.getReqCtnt());
            
            voSms.setTrType("SMS");
            voSms.setTrPhone((String)admin.get("USER_TEL"));
            Object[] msgObj = {prdCtgyNm};
            voSms.setTrMsg(messageSource.getMessage("E852",msgObj));
            
            smsList.add(voSms);
            mailList.add(voEmail);
        }
        
        if(mailList != null && mailList.size() > 0) {
        	sendMail.makeMailData(mailList, false);
        }
        
        if(smsList != null && smsList.size() > 0) {
        	sendMail.makeSmsData(smsList, false);
        }
        
        return ResponseEntity.ok(new SignUpResponse(true, "Faq Regist successfully"));
    }
}
