package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdSettReqeust;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.views.dto.AppsAllRequest;
import com.hanafn.openapi.portal.views.dto.BillingRequest;
import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.vo.AppPrdUseVO;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.BillingVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;

@Mapper
public interface BillingRepository {

    /* 선결제내역 조회 */
    int countPayInfoNice(BillingRequest.PaymentRequest request);
    List<PayInfoVO> selectPayInfoNice(BillingRequest.PaymentRequest request);

    /* 선결제내역 상세 조회 */
    List<PayInfoVO> selectPayDetailInfoNice(SettlementRequest.PayDetailRequest request);

    /* 정산내역 앱 조회 */
    List<AppsVO> selectAppsAllNice(AppsAllRequest request);

    /* 정산내역 앱별 상품 조회 */
    List<ProductVO> selectProdList(PrdSettReqeust request);

    /* 정산내역 목록 조회 */
    int countAppUseInfo(BillingRequest.SettlementRequest request);
    List<AppUseVO> selectAppUseInfo(BillingRequest.SettlementRequest request);

    List<AppPrdUseVO> getAppPrdUseInfo(BillingRequest.AppUseInfoRequest request);

    /* 청구정보 목록 조회 */
    int countBillingList(BillingRequest request);
    List<BillingVO> selectBillingList(BillingRequest request);

    /* 청구정보 상세 조회 */
    BillingVO selectBillingDetail(BillingRequest request);

    /* 청구정보 등록 - 앱조회 */
    int countBillingAppList(BillingRequest request);
    List<BillingVO> selectBillingAppList(BillingRequest request);

    /* 청구정보 등록 - 중복 체크 */
    int selectBillingInfoDup(BillingRequest request);

    /* 청구정보 등록 */
    void insertBillingInfo(BillingRequest request);

    /* 청구정보 수정 */
    void updateBillingInfo(BillingRequest request);
}
