package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.DocVO;

import lombok.Data;

@Data
public class DocResponse {

	private DocVO doc;
	
	@Data
    public static class DocListResponse{
		private List<DocVO> docList;
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }

}
