package com.hanafn.openapi.portal.views.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("token")
public class TokenVO {
	private String issueOrgCd;
	private String clientId;
	private String scr;
	private String token;
	private String grantType;
	private String statCd;
	private String scope;
	private String expiresIn;
}
