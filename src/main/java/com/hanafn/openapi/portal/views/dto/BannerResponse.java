package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.BannerVO;

import lombok.Data;

@Data
public class BannerResponse {

	private BannerVO banner;
	
	@Data
    public static class BannerListResponse{
		private List<BannerVO> bannerList;
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }

}
