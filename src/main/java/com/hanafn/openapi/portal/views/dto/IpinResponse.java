package com.hanafn.openapi.portal.views.dto;


import lombok.Data;

@Data
public class IpinResponse {

	private String sEncData;
	private String sRtnMsg; 
	private String certYn;
	private String resNo;
	private String mobileNo;
	private String name;
	private String userId;
	
	@Data
    public static class IpinAdminCertYn {
    	private String certYn;
    	private int certCnt;
    }
	
}
