package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.views.dto.DocRequest;
import com.hanafn.openapi.portal.views.vo.DocVO;

@Mapper
public interface DocRepository {
	List<DocVO> selectDocList(DocRequest req);
	DocVO selectDoc(DocRequest.DocDetailRequest req);
	
	int insertDoc(DocRequest.DocInsertRequest req);
	int deleteDoc(DocRequest.DocDetailRequest req);
	int updateDoc(DocRequest.DocUpdateRequest req);
}