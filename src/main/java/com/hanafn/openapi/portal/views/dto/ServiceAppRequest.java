package com.hanafn.openapi.portal.views.dto;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ServiceAppRequest {
    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;
    private String appNm;     // 서비스명
    private String useorgNm;  // 기관명
    private String appStatCd; // 상태
    private String appClientId;
    private String appKey;
    private String regUser;
    private String opType;

    @Data
    public static class ServiceAppRegistRequest{
        private String appKey;      // 서비스키
        private String appStatCd;   // 서비스상태
        private String userKey;     // 이용기관코드
        private String appClientId; // 클라이언트ID
        private String appScr;      // 시크릿키
        private String redirectUri; // 콜백URL
        private String appNm;       // 서비스명
        private String appMemo;     // 메모
        private String regUser;     // 등록자
        private String regDttm;     // 등록일시
        private List<Map<String, String>> clientIPs; // 허용IP
        private List<Map<String, String>> callbackList; // 허용IP
        private List<Map<String, String>> schemeList; // 허용IP
        
        private String callback;
        private String schemes;
        private String useorgGb;
    }
    @Data
    public static class ServiceAppChannelRequest{
        private String seqNo;
        private String appKey;
        private String regUser;
        private String regDttm;
        private String cnlKey; // 허용IP
    }
    @Data
    public static class policyAppRequest{
        private String appKey;
        private String userKey; // 기관코드
    }
    @Data
    public static class StatsAppRequest{
        private String searchUserKey; // 기관코드
    }
    @Data
    public static class policyServerIpRequest{
//        private List<Map<String, String>> userKey;
    	private String appKey;
        private String userKey;
    }
    @Data
    public static class AppRedirectInfoRequest {
        private String seqNo;
        private String appKey;
        private String redirectUri;
        private String regUser;
        private String regDttm;
        private String modUser;
        private String modDttm;
    }
    @Data
    public static class AppSchemeRequest {
        private String seqNo;
        private String appKey;
        private String appScheme;
        private String regUser;
        private String regDttm;
        private String modUser;
        private String modDttm;
    }


}