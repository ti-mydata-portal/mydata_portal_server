

package com.hanafn.openapi.portal.views.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.MailRequest;
import com.hanafn.openapi.portal.views.dto.MailResponse;
import com.hanafn.openapi.portal.views.service.MailService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Controller
@RequestMapping("/mail")
@Slf4j
@RequiredArgsConstructor
@SessionAttributes("cert_info")
public class MailController {
	
	@Autowired
    MailService service;
	
	@Autowired
    MailUtils mailUtils;
	
	@Autowired
	@Qualifier("Crypto")
	ISecurity cryptoUtil;
	
	/*
     * *****************************엑셀 업로드 메일수신자 가져오기******************************
    */
	@PostMapping("/selectMailSender")
    public ResponseEntity<?> selectMailSender(@CurrentUser UserPrincipal currentUser) throws Exception {
		
		MailRequest.MailSender request = new MailRequest.MailSender();
		
		request.setUserKey(currentUser.getUserKey());
		String sender = service.selectMailSender(request);
		
		MailResponse data = new MailResponse();
		
		if(sender != null && !"".equals(sender)) {
			String decryptedEmail = cryptoUtil.decrypt(sender);
			data.setEmail(decryptedEmail);
		}
		
		return ResponseEntity.ok(data);
	}
    /*
     * *****************************엑셀 업로드 메일수신자 가져오기******************************
    */
	@PostMapping("/getXlsData")
    public ResponseEntity<?> getXlsData(HttpServletRequest request, HttpSession session, @CurrentUser UserPrincipal currentUser, @RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
		
		final Workbook workbook;
		
		workbook = WorkbookFactory.create(file.getInputStream());
		
		final Sheet sheet = workbook.getSheetAt(0);
		
		int rownum = 0;
		StringBuffer sb = new StringBuffer();
		
		while(sheet.getRow(rownum+1) != null) {
			Row mailRow = sheet.getRow(rownum);
			Cell mailCell = mailRow.getCell(0);
			if(mailCell.getStringCellValue() != null && !"".equals(mailCell.getStringCellValue())) {
				sb.append(mailCell.getStringCellValue()+"; ");
			}
			
			rownum++;
		}
		
		return ResponseEntity.ok(sb.toString());
    }
	
	/*
     * *****************************엑셀 업로드 메일수신자 가져오기******************************
    */
	@PostMapping("/adminMailSend")
    public ResponseEntity<?> adminMailSend(@CurrentUser UserPrincipal currentUser, @RequestBody MailRequest request) throws Exception {
		
		List<String> prdList = request.getReceiverPrd();
		
		HashSet<String> mailSet = new HashSet<String>();
		if(prdList != null && prdList.size() > 0) {
			List<String> prdUseUsers = service.selectPrdUseUser(request);
			
			for(String email:prdUseUsers) {
	            // 이메일 
	            if (email != null && !"".equals(email)) {
	                String decryptedEmail = cryptoUtil.decrypt(email);
	                mailSet.add(decryptedEmail);
	            }
			}
		}
		
		List<Object> emailList = new ArrayList<Object>();
		emailList.addAll(request.getReceiver());
		emailList.addAll(Arrays.asList(mailSet.toArray()));
		List<MailSmsVO> result = new ArrayList<MailSmsVO>();
		
		for(Object mail: emailList) {
			MailSmsVO vo = new MailSmsVO();
			vo.setMailType("API_009");
			vo.setEmail((String)mail);
			vo.setTagmap001(request.getSender());
			vo.setTagmap011(request.getTagmap011());
			vo.setTagmap900(request.getTagmap900());
			
			String reservate = request.getReservate();
			
			if(reservate != null && !"".equals(reservate) && "true".equals(reservate)) {
				vo.setSenddate(request.getSenddate());
			}
			
			result.add(vo);
		}
		
		mailUtils.makeMailData(result, false);
		
		return ResponseEntity.ok(true);
    }
	
	/*
     * ***************************** sms발송 ******************************
    */
	@PostMapping("/adminSmsSend")
    public ResponseEntity<?> adminSmsSend(@CurrentUser UserPrincipal currentUser, @RequestBody MailRequest request) throws Exception {
		
		List<String> prdList = request.getReceiverPrdSms();
		request.setReceiverPrd(prdList);
		
		StringBuffer sb = new StringBuffer();
		String receiverInput = request.getReceiverInputSms();
		
		if(receiverInput != null && !"".equals(receiverInput)) {
			receiverInput = receiverInput.replaceAll("-", "").replaceAll(" ", "");
		}
		sb.append(receiverInput);
		
		if(prdList != null && prdList.size() > 0) {
			List<String> prdUseUsers = service.selectPrdUseUserSms(request);
			
			for(String num:prdUseUsers) {
	            // 이메일 
	            if (num != null && !"".equals(num)) {
	                String decryptedNum = cryptoUtil.decrypt(num)+";";
	                if(sb.indexOf(decryptedNum) > -1) {
	                	continue;
	                }
	                sb.append(decryptedNum);
	            }
			}
		}
		
		List<MailSmsVO> result = new ArrayList<MailSmsVO>();
		
		MailSmsVO vo = new MailSmsVO();
		vo.setTrType("LMS");
		vo.setTrCallback(request.getTrCallback());
		vo.setTrSubject(request.getTrSubject());
		vo.setTrMsg(request.getTrMsg());
		vo.setTrPhone(sb.toString());
		
		String reservate = request.getReservate();
		
		if(reservate != null && !"".equals(reservate) && "true".equals(reservate)) {
			vo.setTrSenddate(request.getTrSenddate());
		}
		
		result.add(vo);
		
		mailUtils.makeSmsData(result, false);
		
		return ResponseEntity.ok(true);
    }
}