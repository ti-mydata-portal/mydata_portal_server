package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.dto.TermsRequest;
import com.hanafn.openapi.portal.views.vo.TermsInstVO;
import com.hanafn.openapi.portal.views.vo.TermsVO;

@Mapper
public interface TermsRepository {
	List<TermsVO> selectTermsList(TermsRequest termsRequest);
	List<TermsInstVO> selectTermsInstList(TermsRequest.TermsUserRequest termsRequest);
	TermsVO selectTerms(TermsRequest.TermsDetailRequest termsRequest);
	TermsVO selectTermsUser(TermsRequest.TermsUserRequest termsRequest);
	
	int insertTerms(TermsRequest.TermsInsertRequest termsRequest);
	int deleteTerms(TermsRequest.TermsDetailRequest termsRequest);
	int updateTerms(TermsRequest.TermsUpdateRequest termsRequest);
}