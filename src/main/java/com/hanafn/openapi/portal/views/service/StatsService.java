package com.hanafn.openapi.portal.views.service;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import com.hanafn.openapi.portal.views.dto.*;
import com.hanafn.openapi.portal.views.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppDetailStatsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.StatsRequest;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.DateUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.repository.StatsRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor
public class StatsService {

	private final StatsRepository statsRepository;
	private final SettingRepository settingRepository;
	private final AppsService appsService;

	@Autowired
    MessageSourceAccessor messageSource;
	
	@Autowired
	@Qualifier("Crypto")
	ISecurity cryptoUtil;

	public DashBoardRsponse dashBoard(DashBoardRequest dashBoardRequest){

		DashBoardVO dashBoardInfo = statsRepository.dashBoardCount(dashBoardRequest);

		DashBoardRsponse dashBoardRsponse = new DashBoardRsponse();
		dashBoardRsponse.setApiUseCnt(dashBoardInfo.getApiUseCnt());
		dashBoardRsponse.setApiCloseCnt(dashBoardInfo.getApiCloseCnt());
		dashBoardRsponse.setUseorgOkCnt(dashBoardInfo.getUseorgOkCnt());
		dashBoardRsponse.setUseorgCloseCnt(dashBoardInfo.getUseorgCloseCnt());
		dashBoardRsponse.setAppOkCnt(dashBoardInfo.getAppOkCnt());
		dashBoardRsponse.setAppCloseCnt(dashBoardInfo.getAppCloseCnt());
		dashBoardRsponse.setAppExpireCnt(dashBoardInfo.getAppExpireCnt());
		dashBoardRsponse.setAplvWaitCnt(dashBoardInfo.getAplvWaitCnt());
		dashBoardRsponse.setQnaWaitCnt(dashBoardInfo.getQnaWaitCnt());

		return dashBoardRsponse;
	}
	
	public DashBoardRsponse dashBoardThreeDayTotal(DashBoardRequest dashBoardRequest){
		
		DashBoardVO dashBoardThreeDayTotalInfo = statsRepository.dashBoardThreeDayTotal(dashBoardRequest);
		DashBoardRsponse dashBoardRsponse = new DashBoardRsponse();
		dashBoardRsponse.setApiTrxCnt(dashBoardThreeDayTotalInfo.getApiTrxCnt());
		dashBoardRsponse.setAvgProcTerm(dashBoardThreeDayTotalInfo.getAvgProcTerm());
		dashBoardRsponse.setGwError(dashBoardThreeDayTotalInfo.getGwError());
		dashBoardRsponse.setApiError(dashBoardThreeDayTotalInfo.getApiError());
		dashBoardRsponse.setMaxProcTerm(dashBoardThreeDayTotalInfo.getMaxProcTerm());
		
		return dashBoardRsponse;
	}
	
	public DashBoardRsponse dashBoardApiUseList(DashBoardRequest dashBoardRequest){

		List<DashBoardVO> topApiUseList = statsRepository.dashBoardThreeDayTopApiUse(dashBoardRequest);

		DashBoardRsponse dashBoardRsponse = new DashBoardRsponse();
		dashBoardRsponse.setTopApiUseList(topApiUseList);

		return dashBoardRsponse;
	}
	
	public DashBoardRsponse dashBoardResTmList(DashBoardRequest dashBoardRequest){

		List<DashBoardVO> topResTmList = statsRepository.dashBoardThreeDayTopProcTerm(dashBoardRequest);

		DashBoardRsponse dashBoardRsponse = new DashBoardRsponse();
		dashBoardRsponse.setTopProcTermList(topResTmList);

		return dashBoardRsponse;
	}
	
	public DashBoardRsponse dashBoardApiErrorList(DashBoardRequest dashBoardRequest){

		List<DashBoardVO> topApiErrorList = statsRepository.dashBoardThreeDayTopApiError(dashBoardRequest);

		DashBoardRsponse dashBoardRsponse = new DashBoardRsponse();
		dashBoardRsponse.setTopApiErrorList(topApiErrorList);

		return dashBoardRsponse;
	}
	
	public DashBoardRsponse dashBoardThreeDayList(DashBoardRequest dashBoardRequest){

		List<DashBoardVO> dashBoardThreeDayList = statsRepository.dashBoardThreeDayList(dashBoardRequest);

		DashBoardRsponse dashBoardRsponse = new DashBoardRsponse();
		dashBoardRsponse.setDashBoardDayList(dashBoardThreeDayList);

		return dashBoardRsponse;
	}

	// TODO NICE XXX
//	public DashBoardRsponse.UseorgDashBoardRsponse useorgDashBoard(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest){
//
//		AppsRequest request = new AppsRequest();
//
//		request.setSearchUserKey(useorgDashBoardRequest.getUserKey());
//
//		List<AppsVO> appModList = appsService.selectUserPortalAppList(request);
//	    // 상태별 앱 ,기간만료
//		UseorgDashBoardVO useorgDashBoardInfo = statsRepository.useorgDashBoardCount(useorgDashBoardRequest);
//        // 당월 앱 사용정보
//		List<UseorgDashBoardVO> appApiTrxList = statsRepository.useorgDashBoardAppApiTrxList(useorgDashBoardRequest);
//		// 당월 API 사용정보
//		List<UseorgDashBoardVO> apiTrxList = statsRepository.useorgDashBoardApiTrxList(useorgDashBoardRequest);
//        // 당일 API 사용정보
//		List<UseorgDashBoardVO> useorgDashBoardThreeDayList = statsRepository.useorgDashBoardThreeDayList(useorgDashBoardRequest);
//
//		DashBoardRsponse.UseorgDashBoardRsponse useorgDashBoardRsponse = new DashBoardRsponse.UseorgDashBoardRsponse();
//
//		useorgDashBoardRsponse.setUseorgNm(useorgDashBoardInfo.getUseorgNm());
//		useorgDashBoardRsponse.setAppTotalCnt(useorgDashBoardInfo.getAppTotalCnt());
//		useorgDashBoardRsponse.setAppWaitCnt(useorgDashBoardInfo.getAppWaitCnt());
//		useorgDashBoardRsponse.setAppOkCnt(useorgDashBoardInfo.getAppOkCnt());
//		useorgDashBoardRsponse.setAppCloseCnt(useorgDashBoardInfo.getAppCloseCnt());
//		useorgDashBoardRsponse.setAppExpireCnt(useorgDashBoardInfo.getAppExpireCnt());
//		useorgDashBoardRsponse.setAppExpireAplvWaitCnt(useorgDashBoardInfo.getAppExpireAplvWaitCnt());
//		useorgDashBoardRsponse.setAppExpireExpectCnt(useorgDashBoardInfo.getAppExpireExpectCnt());
//
//
//		// My apps
//		List<UseorgDashBoardVO> useorgApiTrxMonthInfo = statsRepository.useorgApiTrxMonthInfo(useorgDashBoardRequest);
//
//		List<UseorgDashBoardVO> useorgApiTrxMonthMod = statsRepository.useorgApiTrxMonthMod(useorgDashBoardRequest);
//
//		boolean dupCheck = false;
//		for(UseorgDashBoardVO appVO : useorgApiTrxMonthInfo){
//			for(UseorgDashBoardVO appModVO : useorgApiTrxMonthMod){
//				if(StringUtils.equals(appVO.getAppKey(),appModVO.getAppKey())) {
//					dupCheck = true;
//					break;
//				}
//				dupCheck = false;
//			}
//			if(dupCheck != true) {
//				useorgApiTrxMonthMod.add(appVO);
//			}
//		}
//
//		if (useorgApiTrxMonthMod.size() > 0) {
//			useorgApiTrxMonthMod.get(0).setRnum(String.valueOf(useorgApiTrxMonthMod.size()));
//		}
//
//		useorgDashBoardRsponse.setMyAppList(useorgApiTrxMonthMod);
//
//		useorgDashBoardRsponse.setAppApiTrxList(appApiTrxList);
//		useorgDashBoardRsponse.setApiTrxList(apiTrxList);
//		useorgDashBoardRsponse.setDashBoardDayList(useorgDashBoardThreeDayList);
//		useorgDashBoardRsponse.setAppModList(appModList);
//
//		return useorgDashBoardRsponse;
//	}
	
	public DashBoardRsponse.UseorgDashBoardRsponse userDashBoardNice(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest){

		AppsRequest request = new AppsRequest();

		request.setSearchUserKey(useorgDashBoardRequest.getUserKey());

		List<AppsVO> appModList = appsService.selectUserPortalAppList(request);
	    // 상태별 앱 ,기간만료
		UseorgDashBoardVO useorgDashBoardInfo = statsRepository.useorgDashBoardCountNice(useorgDashBoardRequest);
		UseorgDashBoardVO userDashBoardPrdInfo = statsRepository.userDashBoardCountPrdNice(useorgDashBoardRequest);
        // 당월 앱 사용정보
		List<UseorgDashBoardVO> appApiTrxList = statsRepository.useorgDashBoardAppApiTrxListNice(useorgDashBoardRequest);
		List<UseorgDashBoardVO> prdApiTrxList = statsRepository.useorgDashBoardPrdApiTrxList(useorgDashBoardRequest);
        // 당일 API 사용정보
		List<UseorgDashBoardVO> apiTrxTodayInfo = statsRepository.userDashBoardTodayNice(useorgDashBoardRequest);

		DashBoardRsponse.UseorgDashBoardRsponse useorgDashBoardRsponse = new DashBoardRsponse.UseorgDashBoardRsponse();

		useorgDashBoardRsponse.setUseorgNm(useorgDashBoardInfo.getUseorgNm());
		useorgDashBoardRsponse.setAppTotalCnt(useorgDashBoardInfo.getAppTotalCnt());
		useorgDashBoardRsponse.setAppWaitCnt(useorgDashBoardInfo.getAppWaitCnt());
		useorgDashBoardRsponse.setAppOkCnt(useorgDashBoardInfo.getAppOkCnt());
		useorgDashBoardRsponse.setAppCloseCnt(useorgDashBoardInfo.getAppCloseCnt());
		useorgDashBoardRsponse.setAppExpireCnt(useorgDashBoardInfo.getAppExpireCnt());
		useorgDashBoardRsponse.setAppExpireAplvWaitCnt(useorgDashBoardInfo.getAppExpireAplvWaitCnt());
		useorgDashBoardRsponse.setAppExpireExpectCnt(useorgDashBoardInfo.getAppExpireExpectCnt());
		
		useorgDashBoardRsponse.setPrdAplvWaitCnt(userDashBoardPrdInfo.getPrdAplvWaitCnt());
		useorgDashBoardRsponse.setPrdExpireCnt(userDashBoardPrdInfo.getPrdExpireCnt());
		useorgDashBoardRsponse.setPrdExpireExpectCnt(userDashBoardPrdInfo.getPrdExpireExpectCnt());

		useorgDashBoardRsponse.setAppApiTrxList(appApiTrxList);
		useorgDashBoardRsponse.setPrdApiTrxList(prdApiTrxList);
		useorgDashBoardRsponse.setDashBoardDayList(apiTrxTodayInfo);
		useorgDashBoardRsponse.setAppModList(appModList);

		return useorgDashBoardRsponse;
	}

	// TODO NICE XXX
//	public List<UseorgDashBoardVO> defaultUseorgDashBoardDay(){
//		List<UseorgDashBoardVO> threeDayList = new ArrayList<>();
//
//		for(int day=-2; day<=0; day++){
//			String date = DateUtil.getDate(day);
//			for(int tm=0; tm<24; tm++) {
//				// 시간별 정보 초기화
//				UseorgDashBoardVO useorgDashBoardInfo = new UseorgDashBoardVO();
//				useorgDashBoardInfo.setDay(date);
//				useorgDashBoardInfo.setTm(String.valueOf(tm + 1));
//				useorgDashBoardInfo.setApiTrxCnt("0");
//				useorgDashBoardInfo.setProcTerm("0.0");
//				useorgDashBoardInfo.setApiError("0");
//
//				threeDayList.add(useorgDashBoardInfo);
//			}
//		}
//
//		return threeDayList;
//	}

	public StatsRsponse apiStatsAsis(StatsRequest apiStatsRequest){

		StatsVO dayTotalInfo = statsRepository.apiDayTotal(apiStatsRequest);
		List<StatsVO> apiTrxList = statsRepository.apiTrxDayList(apiStatsRequest);

		List<StatsVO> apiTrxDayList = defaultApiTrxDay(apiStatsRequest);

		for(StatsVO apiTrxInfo : apiTrxList){
			for(StatsVO apiTrxDayInfo :apiTrxDayList){
				if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
					apiTrxDayInfo.setApiTrxCnt(apiTrxInfo.getApiTrxCnt());
					apiTrxDayInfo.setProcTerm(apiTrxInfo.getProcTerm());
					apiTrxDayInfo.setApiError(apiTrxInfo.getApiError());
				}
			}
		}

		List<StatsVO> topApiUseList = statsRepository.apiStatsTopApiUse(apiStatsRequest);
		List<StatsVO> topProcTermList = statsRepository.apiStatsTopProcTerm(apiStatsRequest);
		List<StatsVO> topApiErrorList = statsRepository.apiStatsTopApiError(apiStatsRequest);

		StatsRsponse apiStatsRsponse = new StatsRsponse();
		apiStatsRsponse.setApiTrxCnt(dayTotalInfo.getApiTrxCnt());
		apiStatsRsponse.setAvgProcTerm(dayTotalInfo.getAvgProcTerm());
		apiStatsRsponse.setGwError(dayTotalInfo.getGwError());
		apiStatsRsponse.setApiError(dayTotalInfo.getApiError());
		apiStatsRsponse.setMaxProcTerm(dayTotalInfo.getMaxProcTerm());

		apiStatsRsponse.setDayList(apiTrxDayList);

		apiStatsRsponse.setTopApiUseList(topApiUseList);
		apiStatsRsponse.setTopProcTermList(topProcTermList);
		apiStatsRsponse.setTopApiError(topApiErrorList);

		return apiStatsRsponse;
	}

	public StatsRsponse.UseorgStatsRsponse useorgStats(StatsRequest apiStatsRequest) throws Exception{

        if (StringUtils.isNotBlank(apiStatsRequest.getSearchUserKey())) {
            try {
                String userName = cryptoUtil.encrypt(apiStatsRequest.getSearchUserKey());
                apiStatsRequest.setSearchUserKey(userName);
                log.debug("암호화 - 이용자 : {}", cryptoUtil.encrypt(apiStatsRequest.getSearchUserKey()));
            } catch (GeneralSecurityException e) {
                log.error("암호화 err", e);
            } catch (UnsupportedEncodingException e) {
				log.error("암호화 err", e);
            }
        }

		String lastUpdateDate = statsRepository.getLastUpdateDate();
		StatsVO dayTotalInfo = statsRepository.apiDayTotal(apiStatsRequest);
		List<StatsVO> apiTrxList = statsRepository.apiTrxDayList(apiStatsRequest);

		List<StatsVO> apiTrxDayList = defaultApiTrxDay(apiStatsRequest);

		for(StatsVO apiTrxInfo : apiTrxList){
			for(StatsVO apiTrxDayInfo :apiTrxDayList){
				if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
					apiTrxDayInfo.setApiTrxCnt(apiTrxInfo.getApiTrxCnt());
					apiTrxDayInfo.setProcTerm(apiTrxInfo.getProcTerm());
					apiTrxDayInfo.setApiError(apiTrxInfo.getApiError());
				}
			}
		}

		List<StatsVO> appTrxList = statsRepository.appTrxList(apiStatsRequest);

        for (StatsVO vo : appTrxList) {
            try {
                // 이름
                if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                    String decryptedUserNm = cryptoUtil.decrypt(vo.getUserNm());
                    vo.setUserNm(decryptedUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUserNm);
                }
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			} catch (Exception e) {
				log.error(e.getMessage());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
        }

		StatsRsponse.UseorgStatsRsponse useorgStatsRsponse = new StatsRsponse.UseorgStatsRsponse();
		useorgStatsRsponse.setApiTrxCnt(dayTotalInfo.getApiTrxCnt());
		useorgStatsRsponse.setAvgProcTerm(dayTotalInfo.getAvgProcTerm());
		useorgStatsRsponse.setGwError(dayTotalInfo.getGwError());
		useorgStatsRsponse.setApiError(dayTotalInfo.getApiError());
		useorgStatsRsponse.setMaxProcTerm(dayTotalInfo.getMaxProcTerm());
		useorgStatsRsponse.setDayList(apiTrxDayList);
		useorgStatsRsponse.setAppTrxList(appTrxList);
		useorgStatsRsponse.setLastUpdateDate(lastUpdateDate);

		return useorgStatsRsponse;
	}
	
	public StatsRsponse.UseorgStatsRsponse useorgStatsNice(StatsRequest apiStatsRequest){

		String lastUpdateDate = statsRepository.getLastUpdateDate();
		StatsVO dayTotalInfo = statsRepository.apiDayTotal(apiStatsRequest);
		List<StatsVO> apiTrxList = statsRepository.apiTrxDayList(apiStatsRequest);

		List<StatsVO> apiTrxDayList = defaultApiTrxDay(apiStatsRequest);

		for(StatsVO apiTrxInfo : apiTrxList){
			for(StatsVO apiTrxDayInfo :apiTrxDayList){
				if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
					apiTrxDayInfo.setApiTrxCnt(apiTrxInfo.getApiTrxCnt());
					apiTrxDayInfo.setProcTerm(apiTrxInfo.getProcTerm());
					apiTrxDayInfo.setApiError(apiTrxInfo.getApiError());
				}
			}
		}

		List<StatsVO> appTrxList = statsRepository.appTrxListNice(apiStatsRequest);

		StatsRsponse.UseorgStatsRsponse useorgStatsRsponse = new StatsRsponse.UseorgStatsRsponse();
		useorgStatsRsponse.setApiTrxCnt(dayTotalInfo.getApiTrxCnt());
		useorgStatsRsponse.setAvgProcTerm(dayTotalInfo.getAvgProcTerm());
		useorgStatsRsponse.setGwError(dayTotalInfo.getGwError());
		useorgStatsRsponse.setApiError(dayTotalInfo.getApiError());
		useorgStatsRsponse.setMaxProcTerm(dayTotalInfo.getMaxProcTerm());
		useorgStatsRsponse.setDayList(apiTrxDayList);
		useorgStatsRsponse.setAppTrxList(appTrxList);
		useorgStatsRsponse.setLastUpdateDate(lastUpdateDate);

		return useorgStatsRsponse;
	}

	public List<StatsVO> defaultApiTrxDay(StatsRequest apiStatsRequest){
		List<StatsVO> dayList = new ArrayList<>();

		int dayDiff = DateUtil.getDayDiff(apiStatsRequest.getSearchStDt(), apiStatsRequest.getSearchEnDt());

		for(int i=0; i<=dayDiff; i++){
			String date = DateUtil.addDay(apiStatsRequest.getSearchStDt(), i);

			StatsVO dayInfo = new StatsVO();
			dayInfo.setDay(DateUtil.formatDateTime(date, "yyyyMMdd"));
			dayInfo.setApiTrxCnt("0");
			dayInfo.setProcTerm("0.0");
			dayInfo.setApiError("0");

			dayList.add(dayInfo);
		}

		return dayList;
	}

	public StatsRsponse.AppApiDetailStatsRsponse useorgAppDetailStats(AppDetailStatsRequest appDetailStatsRequest){

		AppsRequest appsRequest = new AppsRequest();
		appsRequest.setAppKey(appDetailStatsRequest.getAppKey());

		AppsVO appInfo = settingRepository.selectAppDetailInfo(appsRequest);
		List<StatsVO> appApiDetailStatsList = statsRepository.useorgAppApiDetailStatsList(appDetailStatsRequest);

        try {
            if (appInfo.getUserNm() != null && !"".equals(appInfo.getUserNm())) {
                String decryptedUserNm = cryptoUtil.decrypt(appInfo.getUserNm());
                appInfo.setUserNm(decryptedUserNm);
                log.debug("복호화 - 이름: {}", decryptedUserNm);
            }
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch ( Exception e ) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }

		StatsRsponse.AppApiDetailStatsRsponse appApiDetailStatsRsponse = new StatsRsponse.AppApiDetailStatsRsponse();
		appApiDetailStatsRsponse.setHfnCd(appInfo.getHfnCd());
		appApiDetailStatsRsponse.setUserNm(appInfo.getUserNm());
		appApiDetailStatsRsponse.setAppNm(appInfo.getAppNm());
		appApiDetailStatsRsponse.setAppApiCnt(appApiDetailStatsList.size());
		appApiDetailStatsRsponse.setApiStatsList(appApiDetailStatsList);

		return appApiDetailStatsRsponse;
	}

    /** TOBE 관리자포털 일자별 통계 **/
    public StatsRsponse statsByDate(StatsRequest request){

        StatsVO dayTotalInfo = statsRepository.tobeApiDayTotal(request);
        List<StatsVO> apiTrxList = statsRepository.tobeApiTrxDayList(request);
        List<StatsVO> apiTrxDayList = defaultApiTrxDay(request);

        for(StatsVO apiTrxInfo : apiTrxList){
            for(StatsVO apiTrxDayInfo :apiTrxDayList){
                if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
                    apiTrxDayInfo.setApiTrxCnt(apiTrxInfo.getApiTrxCnt());
                    apiTrxDayInfo.setProcTerm(apiTrxInfo.getProcTerm());
                    apiTrxDayInfo.setApiError(apiTrxInfo.getApiError());
                }
            }
        }

        int totCnt = statsRepository.countTrxList(request);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
        List<StatsVO> appTrxList = statsRepository.trxList(request);

        for (StatsVO vo : appTrxList) {
            try {
                if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                    String decryptedUserNm = cryptoUtil.decrypt(vo.getUserNm());
                    vo.setUserNm(decryptedUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUserNm);

                    if(StringUtils.isNotEmpty(request.getSearchType())) {
                        String tmpNm = CommonUtil.maskingName(vo.getUserNm());
                        String tmpId = CommonUtil.maskingId(vo.getUserId());
                        vo.setUserId(tmpId+"("+tmpNm+")");
                    }
                }
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new BusinessException("E026", messageSource.getMessage("E026"));
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BusinessException("E026", messageSource.getMessage("E026"));
            }
        }

        StatsRsponse response = new StatsRsponse();
        response.setApiTrxCnt(dayTotalInfo.getApiTrxCnt());
        response.setAvgProcTerm(dayTotalInfo.getAvgProcTerm());
        response.setGwError(dayTotalInfo.getGwError());
        response.setApiError(dayTotalInfo.getApiError());
        response.setMaxProcTerm(dayTotalInfo.getMaxProcTerm());
        response.setDayList(apiTrxDayList);
        response.setAppTrxList(appTrxList);
        response.setSelCnt(appTrxList.size());
        response.setPageIdx(request.getPageIdx());
        response.setPageSize(request.getPageSize());
        response.setTotCnt(totCnt);

        return response;
    }

    public StatsRsponse bizStatsByDate(StatsRequest request) {
        StatsVO dayTotalInfo = statsRepository.tobeApiDayTotal(request);
        List<StatsVO> apiTrxList = statsRepository.tobeApiTrxDayList(request);
        List<StatsVO> apiTrxDayList = defaultApiTrxDay(request);

        for(StatsVO apiTrxInfo : apiTrxList){
            for(StatsVO apiTrxDayInfo :apiTrxDayList){
                if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
                    apiTrxDayInfo.setApiTrxCnt(apiTrxInfo.getApiTrxCnt());
                    apiTrxDayInfo.setProcTerm(apiTrxInfo.getProcTerm());
                    apiTrxDayInfo.setApiError(apiTrxInfo.getApiError());
                }
            }
        }

        int totCnt = statsRepository.countBizTrxList(request);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
        List<StatsVO> appTrxList = statsRepository.bizTrxList(request);

        for (StatsVO vo : appTrxList) {
            try {
                if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                    String decryptedUserNm = cryptoUtil.decrypt(vo.getUserNm());
                    vo.setUserNm(decryptedUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUserNm);

                    if(StringUtils.isNotEmpty(request.getSearchType())) {
                        String tmpNm = CommonUtil.maskingName(vo.getUserNm());
                        String tmpId = CommonUtil.maskingId(vo.getUserId());
                        vo.setUserId(tmpId+"("+tmpNm+")");
                    }
                }
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new BusinessException("E026", messageSource.getMessage("E026"));
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new BusinessException("E026", messageSource.getMessage("E026"));
            }
        }

        StatsRsponse response = new StatsRsponse();
        response.setApiTrxCnt(dayTotalInfo.getApiTrxCnt());
        response.setAvgProcTerm(dayTotalInfo.getAvgProcTerm());
        response.setGwError(dayTotalInfo.getGwError());
        response.setApiError(dayTotalInfo.getApiError());
        response.setMaxProcTerm(dayTotalInfo.getMaxProcTerm());
        response.setDayList(apiTrxDayList);
        response.setAppTrxList(appTrxList);
        response.setSelCnt(appTrxList.size());
        response.setPageIdx(request.getPageIdx());
        response.setPageSize(request.getPageSize());
        response.setTotCnt(totCnt);

        return response;
    }

    public StatsRsponse statsByDateDetail(StatsRequest request) {
        List<StatsVO> list = statsRepository.statsByDateDetail(request);

        StatsRsponse data = new StatsRsponse();
        data.setDayList(list);

        return data;
    }

    public StatsRsponse bizStatsByDateDetail(StatsRequest request) {
        List<StatsVO> list = statsRepository.bizStatsByDateDetail(request);

        StatsRsponse data = new StatsRsponse();
        data.setDayList(list);

        return data;
    }
    // 신규
    public StatsApiResponse statsByDateApi(StatisticsRequest request){
		StatsApiVO dayTotalInfo = statsRepository.statsApiDayTotal(request); // 왼쪽 거래관련된 애들.

		List<StatsApiVO> apiTrxList = statsRepository.statsApiDayList(request); // 일자별 SUM
		List<StatsApiVO> apiTrxDayList = defaultStatsApiTrxDay(request); // line chart

		for(StatsApiVO apiTrxInfo : apiTrxList){
			for(StatsApiVO apiTrxDayInfo :apiTrxDayList){
				if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
					apiTrxDayInfo.setMaxApiCnt(apiTrxInfo.getMaxApiCnt()); // 총이용건수
					apiTrxDayInfo.setApiError(apiTrxInfo.getApiError()); // 에러율
					apiTrxDayInfo.setFailApiCnt(apiTrxInfo.getFailApiCnt()); //실패건수
					apiTrxDayInfo.setSuccessApiCnt(apiTrxInfo.getSuccessApiCnt()); // 정상건수
				}
			}
		}
		// pageIdx 0일때 1로 세팅
		if(request.getPageIdx() == 0)
			request.setPageIdx(request.getPageIdx() + 1);
		// 페이지사이즈 0일때 20으로 세팅
		if(request.getPageSize() == 0)
			request.setPageSize(20);
		// 페이지오프셋 세팅
		request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());

		int totCnt = statsRepository.countStatsTrxList(request);

		List<StatsApiVO> appTrxList = statsRepository.statsTrxList(request);

		StatsApiResponse response = new StatsApiResponse();
		response.setMaxApiCnt(dayTotalInfo.getMaxApiCnt());// 왼쪽 : 총 이용건수
		response.setApiError(dayTotalInfo.getApiError());  // 왼쪽 : API 에러율
		response.setMaxApiCnt(dayTotalInfo.getMaxApiCnt());
		response.setFailApiCnt(dayTotalInfo.getFailApiCnt());
		response.setSuccessApiCnt(dayTotalInfo.getSuccessApiCnt());
		response.setDayList(apiTrxDayList);
		response.setAppTrxList(appTrxList);
		response.setSelCnt(appTrxList.size());
		response.setPageIdx(request.getPageIdx());
		response.setPageSize(request.getPageSize());
		response.setTotCnt(totCnt);
		return response;
	}
	public List<StatsApiVO> defaultStatsApiTrxDay(StatisticsRequest apiStatsRequest){
		List<StatsApiVO> dayList = new ArrayList<>();

		int dayDiff = DateUtil.getDayDiff(apiStatsRequest.getSearchStDt(), apiStatsRequest.getSearchEnDt());

		for(int i=0; i<=dayDiff; i++){
			String date = DateUtil.addDay(apiStatsRequest.getSearchStDt(), i);
			StatsApiVO dayInfo = new StatsApiVO();
			dayInfo.setDay(DateUtil.formatDateTime(date, "yyyyMMdd"));
			dayInfo.setMaxApiCnt("0"); // api건수
			dayInfo.setApiError("0"); // api 에러율
			dayInfo.setFailApiCnt("0"); // 오류건수
			dayInfo.setSuccessApiCnt("0"); // 정상건수


			dayList.add(dayInfo);
		}
		return dayList;
	}
	// 일별API 상세조회
	public StatsApiResponse statsByDateDetailApi(StatisticsRequest.statisticsDetailRequest request) {
		List<StatsApiVO> list = statsRepository.statsByDateDetailApi(request);
		StatsApiResponse data = new StatsApiResponse();
		data.setDayList(list);

		return data;
	}
	// 일자별 사업자 조회
	public StatsApiResponse statsByDateUseorg(StatisticsRequest request){
		StatsApiVO dayTotalInfo = statsRepository.statsUseorgDayTotal(request); // 왼쪽 거래관련된 애들.
		List<StatsApiVO> apiTrxList = statsRepository.statsUseorgDayList(request); // 그리드 리스트

		List<StatsApiVO> apiTrxDayList = defaultStatsApiTrxDay(request); // line chart

		for(StatsApiVO apiTrxInfo : apiTrxList){
			for(StatsApiVO apiTrxDayInfo :apiTrxDayList){
				if(StringUtils.equals(apiTrxInfo.getDay(), apiTrxDayInfo.getDay())){
					apiTrxDayInfo.setMaxApiCnt(apiTrxInfo.getMaxApiCnt()); // 총이용건수
					apiTrxDayInfo.setApiError(apiTrxInfo.getApiError()); // 에러율
					apiTrxDayInfo.setFailApiCnt(apiTrxInfo.getFailApiCnt()); //실패건수
					apiTrxDayInfo.setSuccessApiCnt(apiTrxInfo.getSuccessApiCnt()); // 정상건수
				}
			}
		}
		// pageIdx 0일때 1로 세팅
		if(request.getPageIdx() == 0)
			request.setPageIdx(request.getPageIdx() + 1);
		// 페이지사이즈 0일때 20으로 세팅
		if(request.getPageSize() == 0)
			request.setPageSize(20);
		// 페이지오프셋 세팅
		request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());

		int totCnt = statsRepository.countStatsUseorgTrxList(request);

		List<StatsApiVO> appTrxList = statsRepository.statsUseorgTrxList(request);

		StatsApiResponse response = new StatsApiResponse();
		response.setMaxApiCnt(dayTotalInfo.getMaxApiCnt());// 왼쪽 : 총 이용건수
		response.setApiError(dayTotalInfo.getApiError());  // 왼쪽 : API 에러율
		response.setFailApiCnt(dayTotalInfo.getFailApiCnt());
		response.setSuccessApiCnt(dayTotalInfo.getSuccessApiCnt());
		
		
		response.setDayList(apiTrxDayList);
		response.setAppTrxList(appTrxList);
		response.setSelCnt(appTrxList.size());
		response.setPageIdx(request.getPageIdx());
		response.setPageSize(request.getPageSize());
		response.setTotCnt(totCnt);

		return response;
	}
	public StatsApiResponse statsByDateDetailUseorg(StatisticsRequest.statisticsUseorgDetailRequest request) {
		List<StatsApiVO> list = statsRepository.statsByDateDetailUseorg(request);
		StatsApiResponse data = new StatsApiResponse();
		data.setDayList(list);
		return data;
	}
	//
	public StatsApiResponse.mydataStatsRsponse statsByDateDetailMydata(StatisticsRequest.statisticsMydataRequest request) {
		List<StatsMydataVO> list = statsRepository.statsByDateDetailMydata(request);
		StatsApiResponse.mydataStatsRsponse data = new StatsApiResponse.mydataStatsRsponse();
		data.setMydataList(list);
		return data;
	}
	// 마이데이터 통계 조회
	public StatsApiResponse.mydataStatsRsponse statsByDateMydata(StatisticsRequest.statisticsMydataRequest request){
		StatsApiResponse.mydataStatsRsponse response = new StatsApiResponse.mydataStatsRsponse();
		// 1. 토큰 수 계산
//		List<StatsMydataVO> tokenData = statsRepository.statsGetTokenData(request);
//		response.setTokenData(tokenData);
		if(request.getPageIdx() == 0)
			request.setPageIdx(request.getPageIdx() + 1);
		// 페이지사이즈 0일때 20으로 세팅
		if(request.getPageSize() == 0)
			request.setPageSize(20);
		// 페이지오프셋 세팅
		request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());

		int totCnt = statsRepository.countStatsGetMydata(request);
		// 2.그리드 데이터
		List<StatsMydataVO> myDataList = statsRepository.statsGetMydata(request);

		response.setSelCnt(myDataList.size());
		response.setPageIdx(request.getPageIdx());
		response.setPageSize(request.getPageSize());
		response.setTotCnt(totCnt);
		response.setMydataList(myDataList);

		return response;
	}
}
