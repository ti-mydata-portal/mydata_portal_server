package com.hanafn.openapi.portal.views.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserLoginLockCheckRequest;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.views.dto.IpinRequest;
import com.hanafn.openapi.portal.views.dto.IpinResponse;
import com.hanafn.openapi.portal.views.repository.IpinRepository;
import com.hanafn.openapi.portal.views.vo.IpinVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class IpinService {
	private static final Logger LOGGER = LoggerFactory.getLogger(IpinService.class);

	@Autowired
	IpinRepository repository;
	
	@Autowired
	AdminRepository adminRepository;
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public IpinResponse selectCI(IpinRequest request){
		IpinVO ipin = repository.selectCI(request);
		
		IpinResponse res = new IpinResponse();
		if(ipin != null) {
			res.setCertYn(ipin.getCertYn());
		} else {
			res.setCertYn("N");
		}
		
		return res;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public IpinResponse selectCertUserId(IpinRequest request){
		IpinVO ipin = repository.selectCertUserId(request);
		IpinResponse res = new IpinResponse();
		if(ipin != null && ipin.getUserId() != null) {
			res.setUserId(ipin.getUserId());
			res.setCertYn("Y");
		}else {
			res.setCertYn("N");
		}
		
		return res;
	}
	
	/*
	 * ******************************Code CATEGORY******************************
	 * */
	public void setPwd(IpinRequest.IpinUpdatePw request){
		repository.updateEmptyResNo(request);
        repository.updateUserPwd(request);
        repository.updateUserPwdHis(request);
        
        // 비밀번호 변경시 로그인잠금 관련 세팅 초기화
 		UserLoginLockCheckRequest userLoginLockCheckRequest = new UserLoginLockCheckRequest();
 		userLoginLockCheckRequest.setUserId(request.getUserId());
 		adminRepository.userLoginLockRelease(userLoginLockCheckRequest);
	}

}