package com.hanafn.openapi.portal.views.repository;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiColumnListRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiTagVO;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.views.dto.ApiColumnRequest;
import com.hanafn.openapi.portal.views.dto.ApiRegistRequest;
import com.hanafn.openapi.portal.views.dto.ApiServiceRequest;
import com.hanafn.openapi.portal.views.dto.ApiStatModRequest;
import com.hanafn.openapi.portal.views.dto.ApiTagRequest;
import com.hanafn.openapi.portal.views.dto.EchoRequest;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.vo.ApiColumnListVO;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;
import com.hanafn.openapi.portal.views.vo.ApiServiceVO;
import com.hanafn.openapi.portal.views.vo.ApiStatModHisVO;
import com.hanafn.openapi.portal.views.vo.EchoVO;
import com.hanafn.openapi.portal.views.vo.StatsRealTimeVO;

@Mapper
public interface ApiRepository {

	/*******************************API*******************************/
	int countApiList(AdminRequest.ApiRequest apiRequest);
	List<ApiVO> selectApiList(AdminRequest.ApiRequest apiRequest);
	List<ApiVO> selectApiListNoPaging(AdminRequest.ApiRequest apiRequest);
	
	List<ApiVO> selectAllApi(AdminRequest.ApiRequest apiRequest);
	ApiVO selectApi(AdminRequest.ApiRequest apiRequest);
	Map<String, Object> selectApiMap(LinkRequest request);
	int selectApiBtSendYn(LinkRequest request);

	List<ApiTagVO> selectApiTagList(AdminRequest.ApiRequest apiRequest);
	List<ApiColumnRequest> selectApiColumnRequestList(ApiColumnRequest apiColumnRequest);
	List<ApiColumnVO> selectApiColumnList(ApiColumnRequest apiColumnRequest);
	List<ApiColumnListRequest> selectApiColumnDetailRequestList(AdminRequest.ApiColumnListRequest apiColumnListRequest);
	List<ApiColumnListVO> selectApiColumnDetailList(AdminRequest.ApiColumnListRequest apiColumnListRequest);
	List<ApiStatModHisVO> selectApiStatModHisList(AdminRequest.ApiRequest apiRequest);

	int apiDupCheck(ApiRegistRequest apiRequest);
	void insertApi(ApiRegistRequest apiRequest);
	void insertApiHis(ApiRegistRequest apiRequest);
	void insertApiTag(ApiTagRequest apiTagRequest);
	void insertApiColumn(ApiColumnRequest apiColumnRequest);
	void insertApiColumnList(AdminRequest.ApiColumnListRequest apiColumnListRequest);
    void insertMapper(ApiRegistRequest apiRegistRequest);
    
    void updateHttpUrl(ApiRegistRequest apiRequest);
	void updateApi(ApiRegistRequest apiRequest);
	void deleteApi(AdminRequest.ApiRequest apiRequest);
	void deleteApiTag(ApiRegistRequest apiRequest);
	void deleteApiColumn(ApiRegistRequest apiRequest);
	void deleteApiColumnList(ApiRegistRequest apiRequest);

	void apiStatCdChange(AdminRequest.ApiRequest apiRequest);
	void insertApiStatModHis(ApiStatModRequest apiStatModRequest);
	void updateApiDlyTerm(ApiStatModRequest apiStatModRequest);
	ApiVO selectApiDetalInfo(AdminRequest.ApiRequest apiRequest);
	List<ApiVO> selectApiAllList(AdminRequest.ApiAllListRequest apiAllListRequest);
	List<ApiVO> searchCtgrApiAll(AdminRequest.CtgrApiAllListRequest apiAllListRequest);

	/********************************NOTICE 관련*******************************/

	/** 대응답 정보 처리 **/
	List<EchoVO> selectApiEcho(EchoRequest request);
	int selectApiEchoCount(EchoRequest request);
	List<ApiVO> selectApisForEcho(EchoRequest.ApisRequest request);
	void regApiEcho(EchoRequest.RegEchoRequest request);
	void updateApiEcho(EchoRequest.RegEchoRequest request);
	ApiVO getApiInfo(EchoRequest.RegEchoRequest request);
	int checkApiEchoDup(EchoRequest.RegEchoRequest request);
	EchoVO detailApiEcho(EchoRequest.RegEchoRequest request);
	List<EchoVO> apiEchos(EchoRequest.RegEchoRequest request);
	void deleteApiEcho(EchoRequest.RegEchoRequest request);
	void deleteApiEchoAll(EchoRequest.RegSearchKeyRequest request);
	void regApiSearchKey(EchoRequest.RegSearchKeyRequest request);
	void updateApiSearchKey(EchoRequest.RegSearchKeyRequest request);
	EchoVO getApiSearchKey(EchoRequest.RegSearchKeyRequest request);
	
	List<ApiVO> selectApiCodeList(AdminRequest.apiCodeListRequest request);
	List<StatsRealTimeVO> selectRealTimeList(AdminRequest.apiRealTimeRequest request);
	int countRealTimeList(AdminRequest.apiRealTimeRequest request);
	List<StatsRealTimeVO> selectRealTimeDetailList(AdminRequest.apiRealTimeRequest request);
	
	List<StatsRealTimeVO> selectUserRealTimeList(AdminRequest.apiRealTimeRequest request);
	int countUserRealTimeList(AdminRequest.apiRealTimeRequest request);
	List<StatsRealTimeVO> selectUserRealTimeDetailList(AdminRequest.apiRealTimeRequest request);
	
	List<StatsRealTimeVO> selectPrdRealTimeList(AdminRequest.apiRealTimeRequest request);
	int countPrdRealTimeList(AdminRequest.apiRealTimeRequest request);
	List<StatsRealTimeVO> selectPrdRealTimeDetailList(AdminRequest.apiRealTimeRequest request);
	
	List<StatsRealTimeVO> selectBizRealTimeList(AdminRequest.apiRealTimeRequest request);
	int countBizRealTimeList(AdminRequest.apiRealTimeRequest request);
	List<StatsRealTimeVO> selectBizRealTimeDetailList(AdminRequest.apiRealTimeRequest request);
	
	ApiServiceVO selectApiSvcInfo();
	List<ApiServiceVO> selectApiSvcInfoHis();
	void updateApiSvcInfo(ApiServiceRequest request);
	void insertApiSvcInfoHis(ApiServiceRequest request);
	
}