package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.FaqVO;

import lombok.Data;

@Data
public class FaqResponse {
	private FaqVO faq;

    @Data
    public static class FaqListResponse{
        private List<FaqVO> faqList;
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }
}
