package com.hanafn.openapi.portal.views.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties.Redis;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnLoginLockCheckRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnUserPwdUpdateRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnUserRegistRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnUserUpdateRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaDeleteRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.SndCertMgntRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserDetailRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserLoginLockCheckRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserPwdUpdateRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest.CommCodeInsert;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.CertMailVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.CommonResponse;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpRequest;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.security.service.SignupService;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.CryptoUtil;
import com.hanafn.openapi.portal.util.ExcelUtil;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.util.PasswordValidator;
import com.hanafn.openapi.portal.views.dto.AplvRequest;
import com.hanafn.openapi.portal.views.dto.AplvRsponsePaging;
import com.hanafn.openapi.portal.views.dto.BatchLogListRequest;
import com.hanafn.openapi.portal.views.dto.BatchLogListResponsePaging;
import com.hanafn.openapi.portal.views.dto.HfnInfoRequest;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.HfnUserRsponse;
import com.hanafn.openapi.portal.views.dto.HfnUserRsponsePaging;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.dto.PrivacyReleaseLogRequest;
import com.hanafn.openapi.portal.views.dto.QnaResponse;
import com.hanafn.openapi.portal.views.dto.SndCertMgntResponse;
import com.hanafn.openapi.portal.views.dto.TokenResponse;
import com.hanafn.openapi.portal.views.dto.TrxRequest;
import com.hanafn.openapi.portal.views.dto.TrxResponse;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.dto.UseorgRsponsePaging;
import com.hanafn.openapi.portal.views.dto.UserLoginRequest;
import com.hanafn.openapi.portal.views.dto.UserRequest;
import com.hanafn.openapi.portal.views.dto.UserRsponse;
import com.hanafn.openapi.portal.views.dto.UserRsponsePaging;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.service.SettingNiceService;
import com.hanafn.openapi.portal.views.vo.AplvHisVO;
import com.hanafn.openapi.portal.views.vo.BatchLogVO;
import com.hanafn.openapi.portal.views.vo.NptimeVO;
import com.hanafn.openapi.portal.views.vo.TrxVO;
import com.hanafn.openapi.portal.views.vo.UseorgVO;
import com.hanafn.openapi.portal.views.vo.UserWithdrawVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
public class SettingController {

    @Autowired
    SettingNiceService settingService;
    @Autowired
    SignupService signUpService;
    @Autowired
    SettingRepository settingRepository;
    @Autowired
    AppsRepository appsRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    CommonUtil commonUtil;
    @Autowired
    FileService fileService;
    @Autowired
    ExcelUtil excelUtil;
    @Autowired
    PasswordValidator passwordValidator;
    @Autowired
    public MailUtils mailUtils;
    
    @Autowired
    CryptoUtil cryptoUtil;
    
    @PostMapping("/findHfnUser")
    public ResponseEntity<?> findHfnUser(@Valid @RequestBody HfnInfoRequest request) {

        HfnUserRsponse data = settingService.selectMyHfnMember(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/user")
    public ResponseEntity<?> user(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserDetailRequest request) {

        if(StringUtils.equals(currentUser.getSiteCd(), "userPortal")) {
            request.setUserKey(currentUser.getUserKey());
        }
        UserVO data = settingService.selectUser(request);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/users")
    public ResponseEntity<?> userList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest request) throws Exception {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(!ga.getAuthority().contains("ROLE_SYS_ADMIN"))
            {
                request.setSearchHfnCd(currentUser.getHfnCd());
            }
            request.setUserKey(currentUser.getUserKey());
            UserRsponsePaging data = settingService.selectUserListPaging(request);

            return ResponseEntity.ok(data);
        }

        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    @PostMapping("/userUpdate")
    public ResponseEntity<?> userUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest.UserUpdateRequest request) throws Exception {

        if(request.getNewUserPwd() != null && !StringUtils.equals(request.getNewUserPwd(), "")){
            // 비밀번호 검증
            List<UserPwHisVO> userList = settingRepository.getUserIdPw(currentUser.getUserKey());

            for (UserPwHisVO userData : userList) {
                if(!commonUtil.compareWithShaStringsPw(request.getNewUserPwd(), userData.getUserPwd())) {
                    log.error(messageSource.getMessage("E112"));
                    throw new BusinessException("E112",messageSource.getMessage("E112"));
                }

                // 비밀번호 검증
                passwordValidator.isValidPassword(request.getNewUserPwd(),userData.getUserId());
            }

            request.setNewUserPwd(passwordEncoder.encode(request.getNewUserPwd()));

            // 패스워드 NotNull 일때만 변경
            UserLoginRequest.UpdatePwdRequest userLoginRequest = new UserLoginRequest.UpdatePwdRequest();
            userLoginRequest.setUserKey(request.getUserKey());
            userLoginRequest.setNewPwd(request.getNewUserPwd());
            userLoginRequest.setUserId(request.getUserId());
            settingService.updateUserLoginPwd(userLoginRequest);
        }

        request.setModUserName(currentUser.getUsername());
        request.setModUserId(currentUser.getUserKey());

        if (null != request.getUserEmail()) {
            SignUpRequest.UserSingnUpRequest sign = new SignUpRequest.UserSingnUpRequest();
            sign.setUserId(request.getUserId());
            sign.setUserEmail(request.getUserEmail());
            signUpService.signupUserDupCheckUpdate(sign);
        }

        settingService.updateUser(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User Update successfully"));
    }

    @PostMapping("/userStatCdChange")
    public ResponseEntity<?> userStatCdChange(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest.UserStatCdChangeRequest request) {
        request.setRegUserId(currentUser.getUserKey());
        request.setRegUserName(currentUser.getUsername());
        settingService.updateUserStatCdChange(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User_Stat_Cd Change successfully"));
    }

    @PostMapping("/userPwdUpdate")
    public ResponseEntity<?> userPwdUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserPwdUpdateRequest request) {

        request.setRegUserName(currentUser.getUsername());
        request.setUserKey(currentUser.getUserKey());
        settingService.userPwdUpdate(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_Update successfully"));
    }

    @PostMapping("/userPwdAndTosUpdate")
    public ResponseEntity<?> userPwdAndTosUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest.UserPwdAndTosUpdateRequest request) {

        request.setRegUserName(currentUser.getUsername());
        request.setUserKey(currentUser.getUserKey());
        settingService.userPwdAndTosUpdate(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_And_Tos_Update successfully"));
    }

    @PostMapping("/getUserWithdraw")
    public ResponseEntity<?> secedeUser(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest request) {
        UserWithdrawVO userWithdrawVO = settingService.selectUserWithdraw(request);
        return ResponseEntity.ok(userWithdrawVO);
    }

    @PostMapping("/secedeUser")
    public ResponseEntity<?> secedeUser(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest.UserSecedeRequest request) {
        request.setUserKey(currentUser.getUserKey());
        settingService.secedeUserNice(request);
        return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_Update successfully"));
    }

//    TODO NICE XX
//    @PostMapping("/secedeUseorg")
//    public ResponseEntity<?> secedeUseorg(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest.UseorgSecedeRequest request) {
//        request.setUserKey(currentUser.getUserKey());
//        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
//            request.setUserKey(currentUser.getUseorgKey()); // 개발자관련
//        }
//
//        request.setUserId(currentUser.getUserId());
//        request.setUserNm(currentUser.getUsername());
//        settingService.secedeUseorg(request);
//        return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_Update successfully"));
//    }

    @PostMapping("/tmpPwdIssue")
    public ResponseEntity<?> tmpPwdIssue(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest.UserTmpPwdUpdateRequest request) {

         /*
        request.setRegUserName(currentUser.getUsername());
        UserRsponse.UserTmpPwdIssueResponse data = settingService.tmpPwdIssue(request);

        return ResponseEntity.ok(data);
         */
         request.setRegUserId(currentUser.getUserKey());

         Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
         for(GrantedAuthority ga : authorities){
             if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_ADMIN")){
                 request.setRegUserName(currentUser.getUsername());
                 UserRsponse.UserTmpPwdIssueResponse data = settingService.tmpPwdIssue(request);

                 return ResponseEntity.ok(data);
             }
         }

        log.error(messageSource.getMessage("E021"));
         throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    /*
     * ******************************승인******************************
     * */

    @PostMapping("/getAplvHisList")
    public ResponseEntity<?> aplvHisList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvRequest.AplvDetailRequest request) {
        List<AplvHisVO> data = settingRepository.selectAplvHis(request);
        return ResponseEntity.ok(data);
    }

    // 승인 리스트 로드
    @PostMapping("/aplvs")
    public ResponseEntity<?> aplvList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AplvRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setSearchHfnCd(currentUser.getHfnCd());
                break;
            }
        }

        request.setProcId(currentUser.getUseorgKey());
        request.setProcUser(currentUser.getUsername());
        AplvRsponsePaging data = settingService.selectAplvListPaging(request);

        return ResponseEntity.ok(data);
    }

    /*******************************관계사사용자********************************/

    @PostMapping("/hfnUserById")
    public ResponseEntity<?> hfnUserById(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request) {

        HfnUserRsponse data = settingService.selectHfnUserById(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/hfnUserByIdMasking")
    public ResponseEntity<?> hfnUserByIdMasking(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request) throws Exception{

        HfnUserRsponse data = settingService.selectHfnUserById(request);
        
        HfnUserVO vo = data.getHfnUserVO();

        String userNm = vo.getUserNm();
        String userTel = vo.getUserTel();
        if(userNm != null && !"".equals(userNm)) {
        	vo.setUserNm(cryptoUtil.decrypt(userNm));
        	userNm = vo.getUserNm();
        	vo.setUserNm(commonUtil.maskingName(userNm));
        }
        
        if(userTel != null && !"".equals(userTel)) {
        	vo.setUserTel(cryptoUtil.decrypt(userTel));
        	userTel = vo.getUserTel();
        	vo.setUserTel(commonUtil.maskingPhone(userTel));
        }
        
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/hfnUserByIdUnMasking")
    public ResponseEntity<?> hfnUserByIdUnMasking(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request) throws Exception{
    	
    	boolean result = commonUtil.compareWithShaStrings(request.getUserPwd(), currentUser.getPassword());
    	
    	if(!result) {
    		return ResponseEntity.ok(result);
    	}
        HfnUserRsponse data = settingService.selectHfnUserById(request);
        
        HfnUserVO vo = data.getHfnUserVO();
        
        String userNm = vo.getUserNm();
        String userTel = vo.getUserTel();
        if(userNm != null && !"".equals(userNm)) {
        	vo.setUserNm(cryptoUtil.decrypt(userNm));
        }
        
        if(userTel != null && !"".equals(userTel)) {
        	vo.setUserTel(cryptoUtil.decrypt(userTel));
        }
        
        return ResponseEntity.ok(data);
    }

    @PostMapping("/hfnUser")
    public ResponseEntity<?> hfnUser(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request, HttpServletResponse httpServletResponse)  throws IOException, Exception {

        if (!StringUtils.equals(currentUser.getUserKey(), request.getUserKey())) {
            httpServletResponse.setHeader("ErrorCode", "expired");
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "세션이 만료되었습니다. 다시 로그인하십시오.");
        }
        request.setUserKey(currentUser.getUserKey());
        HfnUserRsponse data = settingService.selectHfnUser(request);

        String userNm = data.getHfnUserVO().getUserNm();
        String userTel = data.getHfnUserVO().getUserTel();
        if (userNm != null && !"".equals(userNm)) {
            data.getHfnUserVO().setUserNm(cryptoUtil.decrypt(data.getHfnUserVO().getUserNm()));
            userNm = data.getHfnUserVO().getUserNm();
            data.getHfnUserVO().setUserNm(commonUtil.maskingName(userNm));
        }
        if (userTel != null && !"".equals(userTel)) {
            data.getHfnUserVO().setUserTel(cryptoUtil.decrypt(data.getHfnUserVO().getUserTel()));
            userTel = data.getHfnUserVO().getUserTel();
            data.getHfnUserVO().setUserTel(commonUtil.maskingPhone(userTel));
        }

        return ResponseEntity.ok(data);
    }

    @PostMapping("/hfnUsers")
    public ResponseEntity<?> userList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest hfnUserRequest) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_ADMIN")){
                if(!ga.getAuthority().contains("ROLE_SYS_ADMIN"))
                {
                    hfnUserRequest.setSearchHfnCd(currentUser.getHfnCd());
                }
                hfnUserRequest.setUserKey(currentUser.getUserKey());
                HfnUserRsponsePaging data = settingService.selectHfnUserListPaging(hfnUserRequest);

                return ResponseEntity.ok(data);
            }
        }

        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }

    @PostMapping("/hfnUserRegist")
    public ResponseEntity<?> hfnUserRegist(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRegistRequest request) {

        //패드워드 유효성 검사(영문자 대문자 + 숫자 8~16자리)
        settingService.passwordCheck(request.getUserPwd());

        request.setTmpPwd(passwordEncoder.encode(request.getUserPwd()));
        request.setUserPwd(passwordEncoder.encode(request.getUserPwd()));

        request.setRegUserName(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        settingService.insertHfnUser(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User registered successfully"));
    }

    @PostMapping("/hfnIdDupCheck")
    public ResponseEntity<?> userHfnIdDupCheck(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDupCheckRequest request) {
        UserRsponse.UserDupCheckResponse data = settingService.hfnIdDupCheck(request);

        if (StringUtils.equals(data.getUserIdDupYn(), "Y")) {
            log.error(messageSource.getMessage("E028"));
            throw new BusinessException("E028",messageSource.getMessage("E028"));
        }
        return ResponseEntity.ok(data);
    }

    @PostMapping("/hfnUserUpdate")
    public ResponseEntity<?> hfnUserUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserUpdateRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(!ga.getAuthority().equals(request.getRoleCd())) {
                log.error(messageSource.getMessage("C001"));
                throw new BusinessException("C001",messageSource.getMessage("C001"));
            }
        }

        if (request.getNewUserPwd() != null && !StringUtils.equals(request.getNewUserPwd(), "")) {
            settingService.passwordCheck(request.getUserPwd());
            settingService.passwordCheck(request.getNewUserPwd());

            request.setNewUserPwd(passwordEncoder.encode(request.getNewUserPwd()));
        }

        request.setRegUserName(currentUser.getUsername());
        request.setRegUser(currentUser.getUserKey());
        settingService.updateHfnUser(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User Update successfully"));
    }
    
    @PostMapping("/hfnUserUpdateNice")
    public ResponseEntity<?> hfnUserUpdateNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserUpdateRequest request) {
    	
    	boolean result = commonUtil.compareWithShaStrings(request.getUserPwd(), currentUser.getPassword());
    	
    	if(!result) {
    		return ResponseEntity.ok(result);
    	}
    	
    	Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(!ga.getAuthority().equals(request.getRoleCd())) {
                log.error(messageSource.getMessage("C001"));
                throw new BusinessException("C001",messageSource.getMessage("C001"));
            }
        }

        request.setRegUserName(currentUser.getUsername());
        request.setRegUser(currentUser.getUserKey());
        settingService.updateHfnUseNice(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User Update successfully"));
    }

    @PostMapping("/hfnMyInfoUpdate")
    public ResponseEntity<?> hfnMyInfoUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserUpdateRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(!ga.getAuthority().equals(request.getRoleCd())) {
                log.error(messageSource.getMessage("C001"));
                throw new BusinessException("C001",messageSource.getMessage("C001"));
            }
        }

        UserDetailRequest userDetailRequest = new UserDetailRequest();
        userDetailRequest.setUserKey(currentUser.getUserKey());
        userDetailRequest.setHfnId(currentUser.getUserId());
        UserVO user = adminRepository.selectHfnUserPwd(userDetailRequest);

        if(passwordEncoder.matches(request.getUserPwd(), user.getUserPwd())) {
            request.setRegUserName(currentUser.getUsername());
            request.setRegUser(currentUser.getUserKey());
            request.setUserKey(currentUser.getUserKey());
            request.setHfnId(currentUser.getUserId());
            settingService.updateHfnUser(request);
            return ResponseEntity.ok(new SignUpResponse(true, "Hfn User Update successfully"));
        } else {
            log.error(messageSource.getMessage("L002"));
            throw new BusinessException("L002",messageSource.getMessage("L002"));
        }
    }

    @PostMapping("/HfnUserStatCdChange")
    public ResponseEntity<?> hfnUserStatCdChange(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserStatCdChangeRequest request) {

        request.setRegUserName(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        settingService.updateHfnUserStatCdChange(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User_Stat_Cd Change successfully"));
    }

    @PostMapping("/hfnUserPwdUpdate")
    public ResponseEntity<?> hfnUserPwdUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserPwdUpdateRequest request) {

        request.setRegUserId(currentUser.getUserKey());
        request.setRegUserName(currentUser.getUsername());
        request.setUserKey(currentUser.getUserKey());

        // 비밀번호 검증
        List<UserPwHisVO> userList = settingRepository.getUserIdPw(request.getUserKey());

        for (UserPwHisVO userData : userList) {
            if(!commonUtil.compareWithShaStringsPw(request.getUserPwd(), userData.getUserPwd())) {
                log.error(messageSource.getMessage("E112"));
                throw new BusinessException("E112",messageSource.getMessage("E112"));
            }
        }

        settingService.hfnUserPwdUpdate(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_Update successfully"));
    }

    @PostMapping("/hfnUserPwdAndTosUpdate")
    public ResponseEntity<?> hfnUserPwdAndTosUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserPwdAndTosUpdateRequest request) {

        request.setRegUserName(currentUser.getUsername());
        request.setUserKey(currentUser.getUserKey());
        settingService.hfnUserPwdAndTosUpdate(request);

        return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_And_Tos_Update successfully"));
    }

    /*** 보안 체크리스트 파일 다운로드 ***/
//    @PostMapping("/downloadChecklist")
//    public ResponseEntity<?> downloadChecklist(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody FileDownloadRequest fileDownloadRequest) {
//        FileDownloadResponse fileDownloadResponse = new FileDownloadResponse();
//        File path = new File(".");
//
//        if (StringUtils.equals(fileDownloadRequest.getName(), "A")) {
//            //fileDownloadResponse.setUrl("");
//            log.debug("project path = " + path.getAbsolutePath());
//            String classPath = SettingController.class.getResource("").getPath();
//            log.debug("class path = " + classPath);
//            String filePath = classPath + "../../util/SecurityCheckList.txt";
//            File test = new File(filePath);
//            log.debug("file path = " + test.getAbsolutePath());
//        }
//
//        fileDownloadResponse.setName(path.getName());
//
//        return ResponseEntity.ok(fileDownloadResponse);
//
//    }

    //------------------------------------------------------------------------------------------------------
    // 개발자공간-API제휴신청
    //------------------------------------------------------------------------------------------------------
//    TODO NICE XX
//    @PostMapping("/apiJehuReg")
//    public ResponseEntity<?> apiJehuReg(@CurrentUser UserPrincipal currentUser,
//            @RequestParam(value = "userId",       required = false) String userId,
//            @RequestParam(value = "compNm",       required = false) String compNm,
//            @RequestParam(value = "qnaType",      required = false) String qnaType,
//            @RequestParam(value = "userNm",       required = false) String userNm,
//            @RequestParam(value = "userTel",      required = false) String userTel,
//            @RequestParam(value = "apiNm",        required = false) String apiNm,
//            @RequestParam(value = "hfnCd",        required = false) String hfnCd,
//            @RequestParam(value = "reqTitle",     required = false) String reqTitle,
//            @RequestParam(value = "reqCtnt",      required = false) String reqCtnt,
//            @RequestParam(value = "fileData1",    required = false) MultipartFile upLoadFileData1,
//            @RequestParam(value = "fileData2",    required = false) MultipartFile upLoadFileData2
//    ) throws IOException {
//
//        try {
//            ApiJehuRequest request = new ApiJehuRequest();
//            String fileData1 = "";
//            String fileData2 = "";
//
//            if ( upLoadFileData1 != null ) {
//                fileData1 = fileService.fileSave(upLoadFileData1);
//            }
//            if ( upLoadFileData2 != null ) {
//                fileData2 = fileService.fileSave(upLoadFileData2);
//            }
//
//            request.setUserKey(currentUser.getUserKey());
//            request.setUserId(currentUser.getUserId());
//            request.setCompNm(compNm);
//            request.setQnaType(qnaType);
//            request.setUserNm(userNm);
//            request.setUserTel(userTel);
//            request.setApiNm(apiNm);
//            request.setHfnCd(hfnCd);
//            request.setReqTitle(reqTitle);
//            request.setReqCtnt(reqCtnt);
//            request.setFileData1(fileData1);
//            request.setFileData2(fileData2);
//
//            settingService.insertApiJehu(request);
//
//            return ResponseEntity.ok(new SignUpResponse(true, "ApiJehuReg successfully"));
//        }catch(IOException e){
//            return ResponseEntity.ok(new SignUpResponse(false, "ApiJehuReg Fail"));
//        }catch(Exception e){
//            return ResponseEntity.ok(new SignUpResponse(false, "ApiJehuReg Fail"));
//        }
//    }

    @PostMapping("/selectQnaList")
    public ResponseEntity<?> selectQnaList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody QnaRequest qnaRequest) {

        if(!currentUser.getSiteCd().equals("userPortal")) {
            try {
            	Object[] auths = currentUser.getAuthorities().toArray();
            	QnaResponse data = null;
            	GrantedAuthority auth = (GrantedAuthority)auths[0];
            	String strRole = auth.getAuthority();
            	if(auth != null && !strRole.equals("") && strRole.equals("ROLE_SYS_ADMIN")) {
                    data = settingService.selectQnaList(qnaRequest);
            	}else {
            		qnaRequest.setUserKey(currentUser.getUserKey());
                    data = settingService.selectQnaListAuth(qnaRequest);
            	}
            	
                return ResponseEntity.ok(data);
            } catch (BusinessException e) {
                return ResponseEntity.ok(new SignUpResponse(false, "QnA search Fail"));
            }catch(Exception e){
                return ResponseEntity.ok(new SignUpResponse(false, "QnA search Fail"));
            }
        } else if (currentUser.getSiteCd().equals("userPortal")) {
            try {

                if (!currentUser.getUserKey().equals(qnaRequest.getUserKey())) {
                    log.error("올바른 사용자정보가 아닙니다.");
                    throw new BusinessException("E026","올바른 사용자정보가 아닙니다.");
                }

                QnaResponse data = settingService.selectQnaList(qnaRequest);
                return ResponseEntity.ok(data);
            } catch (BusinessException e) {
                return ResponseEntity.ok(new SignUpResponse(false, "QnA search Fail"));
            }catch(Exception e){
                return ResponseEntity.ok(new SignUpResponse(false, "QnA search Fail"));
            }
        }
        return ResponseEntity.ok(new SignUpResponse(false, "올바르지 않은 접근입니다."));
    }

    @PostMapping("/detailQna")
    public ResponseEntity<?> detailQna(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody QnaRequest qnaRequest) {

        try {
            QnaResponse data = settingService.detailQna(qnaRequest);
            return ResponseEntity.ok(data);
        } catch(BusinessException e) {
            return ResponseEntity.ok(new SignUpResponse(false, "Qna search Fail"));
        } catch(Exception e) {
            return ResponseEntity.ok(new SignUpResponse(false, "Qna search Fail"));
        }
    }

    @PostMapping("/qnaDownload")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<Resource> qnaDownload(@RequestParam String url) {

        String fileName = url;
        Resource resource = null;
        String realFileName = "";

        try {
            if (!fileName.startsWith("/openapi/upload/")) {
                log.error("파일경로 에러 : " + fileName);
                throw new BusinessException("E026", messageSource.getMessage("E026"));
            }
            resource = fileService.loadFileAsResource(fileName);
            realFileName = URLEncoder.encode(resource.getFilename(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            log.error(e.toString());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch (Exception e) {
            log.error(e.toString());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + realFileName + "\"")
                .body(resource);
    }

    @PostMapping("/deleteQna")
    public ResponseEntity<?> deleteQna(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody QnaDeleteRequest qnaDeleteRequest) {

        try {
            QnaRequest qnaRequest = new QnaRequest();
            qnaRequest.setModId(currentUser.getUserKey());

            for(String seqNo : qnaDeleteRequest.getSeqList())
            {
                qnaRequest.setSeqNo(seqNo);
                settingRepository.deleteQna(qnaRequest);
            }

            return ResponseEntity.ok(new SignUpResponse(true, "Qna Delete successfully"));
        }catch(BusinessException e){
            return ResponseEntity.ok(new SignUpResponse(false, "Qna Delete Fail"));
        }catch(Exception e){
            return ResponseEntity.ok(new SignUpResponse(false, "Qna Delete Fail"));
        }
    }

    @PostMapping("/updateAnswer")
    public ResponseEntity<?> updateAnswer(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody QnaRequest request) {

        try {
            request.setModId(currentUser.getUserKey());
            settingRepository.updateAnswer(request);

            MailSmsVO vo = settingRepository.selectSendQna(request);
            vo.setMailType("API_005");
            vo.setEmail(cryptoUtil.decrypt(vo.getEmail()));

            List<MailSmsVO> list = new ArrayList<>();
            list.add(vo);

            mailUtils.makeMailData(list);

            return ResponseEntity.ok(new SignUpResponse(true, "Answer Update successfully"));
        }catch(BusinessException e){
            return ResponseEntity.ok(new SignUpResponse(false, "Answer Update Fail"));
        }catch(Exception e){
            return ResponseEntity.ok(new SignUpResponse(false, "Answer Update Fail"));
        }
    }

    @PostMapping("/downloadAttachment")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<Resource> downloadAttachment(@CurrentUser UserPrincipal currentUser, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam String seqNo, @RequestParam String att) {
        QnaRequest req = new QnaRequest();
        req.setSeqNo(seqNo);

        String fileName = null;
        if(StringUtils.equals(att, "att1")) {
            fileName = settingRepository.selectQnaAttachment01(req);
        } else if(StringUtils.equals(att, "att2")) {
            fileName = settingRepository.selectQnaAttachment02(req);
        }
        log.debug("@@FilePath = " + fileName);

        Resource resource = null;
        try {
            resource = fileService.loadFileAsResource(fileName);
            log.debug("File is " + fileName);
        } catch (NullPointerException e) {
            log.error("File exist error :" + fileName );
            throw new BusinessException("E026","파일이 존재하지 않습니다.");
        } catch(Exception e){
            log.error("File exist error :" + fileName );
            throw new BusinessException("E026","파일이 존재하지 않습니다.");
        }

        String contentType = "application/octet-stream";

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @PostMapping("/downloadManual")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition "})
    public ResponseEntity<Resource> downloadManual(@CurrentUser UserPrincipal currentUser, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        if(StringUtils.equals(currentUser.getSiteCd(), "adminPortal") ||
                StringUtils.equals(currentUser.getSiteCd(), "backoffice") ||
                StringUtils.equals(currentUser.getSiteCd(), "pvBatch")) {
            Resource resource = null;

            try {
                String filePath = "/openapi/upload/manual/HFN_OpenAPI_AdminPortal_Manual_v3.0.doc";
                resource = fileService.loadFileAsResource(filePath);
            } catch(NullPointerException e) {
                log.error("@@ Manual exist error" + e.toString());
                throw new BusinessException("E026","관리자포탈 이용 매뉴얼 다운로드 에러");
            } catch(Exception e) {
                log.error("@@ Manual exist error" + e.toString());
                throw new BusinessException("E026","관리자포탈 이용 매뉴얼 다운로드 에러");
            }

            String contentType = "application/octet-stream";

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        } else {
            log.error("허용되지 않는 접근입니다.");
            throw new BusinessException("E026","허용되지 않는 접근입니다.");
        }
    }

    @PostMapping("/hfnLoginLockReleasse")
    public ResponseEntity<?> hfnLoginLockRelease(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnLoginLockCheckRequest hfnLoginLockCheckRequest) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                	adminRepository.hfnLoginLockRelease(hfnLoginLockCheckRequest);
                    return ResponseEntity.ok(new SignUpResponse(true, "Login Lockk Release Success"));
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Login Lockk Release fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Login Lockk Release fail"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    @PostMapping("/userLoginLockRelease")
    public ResponseEntity<?> userLoginLockRelease(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserLoginLockCheckRequest userLoginLockCheckRequest) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                	adminRepository.userLoginLockRelease(userLoginLockCheckRequest);
                    return ResponseEntity.ok(new SignUpResponse(true, "Login Lockk Release Success"));
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Login Lockk Release fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Login Lockk Release fail"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    /**
     * 개인 정보 해제 로그 등록
     * @param currentUser
     * @param request
     * @return
     */
    @PostMapping("/registerPrivacyReleaseLog")
    public ResponseEntity<?> registerPrivacyReleaseLog(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PrivacyReleaseLogRequest request) {
        request.setRegId(currentUser.getUserKey());
        settingRepository.insertPrivacyReleaseLog(request);

        return ResponseEntity.ok("");
    }

    /** 인증 관련 메일 리스트 조회 **/
    @PostMapping("/selectCertMailList")
    public ResponseEntity<?> selectCertMailList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SndCertMgntRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    SndCertMgntResponse sndCertMgntResponse = settingService.selectCertMailList(request);
                    return ResponseEntity.ok(sndCertMgntResponse);
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "인증메일 리스트 조회 에러"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "인증메일 리스트 조회 에러"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));

    }

    @PostMapping("/detailCertMail")
    public ResponseEntity<?> detailCertMail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SndCertMgntRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    SndCertMgntResponse sndCertMgntResponse = new SndCertMgntResponse();
                    CertMailVO certMail = settingRepository.detailCertMail(request);
                    sndCertMgntResponse.setCertMail(certMail);
                    return ResponseEntity.ok(sndCertMgntResponse);
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "인증메일 상세 조회 에러"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "인증메일 상세 조회 에러"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    @PostMapping("/fetchBatchIdList")
    public ResponseEntity<?> fetchBatchIdList() {
        List<BatchLogVO> batchLogVOList = settingRepository.selectBatchIdList();

        return ResponseEntity.ok(batchLogVOList);
    }

    @PostMapping("/batchLogList")
    public ResponseEntity<?> fetchBatchLogList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody BatchLogListRequest request) {
        BatchLogListResponsePaging data = settingService.selectBatchLogListPaging(request);

        return ResponseEntity.ok(data);
    }

    /** 관리자 - 트랜잭션 관리 **/
    @PostMapping("/selectTrx")
    public ResponseEntity<?> selectTrx(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TrxRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    TrxResponse response = settingService.selectTrx(request);
                    return ResponseEntity.ok(response);
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "트랜잭션 리스트 조회 에러"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "트랜잭션 리스트 조회 에러"));
                }
            }
        }
        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    @PostMapping("/regTrx")
    public ResponseEntity<?> regTrx(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TrxRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                request.setRegUser(currentUser.getUsername());
                try {
                    settingRepository.regTrx(request);
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Transaction Register Fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Transaction Register Fail"));
                }
                return ResponseEntity.ok(new SignUpResponse(true, "Transaction Register Success"));
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    @PostMapping("/detailTrx")
    public ResponseEntity<?> detailTrx(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TrxRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    TrxVO trx = settingRepository.detailTrx(request);
                    return ResponseEntity.ok(trx);
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "트랜잭션 상세 조회 에러"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "트랜잭션 상세 조회 에러"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    @PostMapping("/updateTrx")
    public ResponseEntity<?> updateTrx(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TrxRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                request.setModUser(currentUser.getUsername());
                try {
                    settingRepository.updateTrx(request);
                    return ResponseEntity.ok(new SignUpResponse(true, "Transaction Update Success"));
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Transaction Update Fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Transaction Update Fail"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }
    @PostMapping("/deleteTrx")
    public ResponseEntity<?> deleteTrx(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TrxRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    settingRepository.deleteTrx(request);
                    return ResponseEntity.ok(new SignUpResponse(true, "Transaction Delete Success"));
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Transaction Delete Fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Transaction Delete Fail"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }

    /* NICE 이용목록 엑셀 다운로드 */
    @PostMapping("/excelUser")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelDownload(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "searchNm", required = false) String searchNm
    ) throws Exception {
        UserRequest request = new UserRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setSearchNm(searchNm);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        excelUtil.excelUser(request, response);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }
    @PostMapping("/useOrgList")
    public ResponseEntity<?> useOrgList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest request) {
        UseorgRsponsePaging data = settingService.selectUseOrgListPaging(request);
        return ResponseEntity.ok(data);
    }
    @PostMapping("/useOrgReg")
    public ResponseEntity<?> useOrgReg(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest.UseorgUpdateRequest request) {
        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
        settingService.insertUseOrg(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "regOrg Insert successfully"));
    }

    @PostMapping("/useOrgStatusUpd")
    public ResponseEntity<?> useOrgStatusUpd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest.UseorgUpdateRequest request) {
        request.setModUser(currentUser.getUserKey());
        settingService.useOrgStatusUpd(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "regOrg status update successfully"));
    }
    // useOrgDetail
    @PostMapping("/useOrgDetail")
    public ResponseEntity<?> useOrgDetail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest.UseorgUpdateRequest request) {
        UseorgVO data = settingService.useOrgDetail(request);
        List<HashMap> hData = settingService.useorgIps(request);
        List<HashMap> uData = settingService.useorgDomainIps(request);
        List<NptimeVO> nData = settingRepository.useorgNptimes(request);
        
        data.setClientIPs(hData);
        data.setUseorgDomainIPs(uData);
        data.setNptimes(nData);

        return ResponseEntity.ok(data);
    }
    // useOrgDetail
    @PostMapping("/useOrgUnDetail")
    public ResponseEntity<?> useOrgUnDetail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest.UseorgUnMasking request) {
        // PW check
        boolean result = commonUtil.compareWithShaStrings(request.getUserPwdChk(), currentUser.getPassword());
        if(!result) {
            return ResponseEntity.ok(result);
        }
        UseorgRequest.UseorgUpdateRequest dataRequst = new UseorgRequest.UseorgUpdateRequest();
        dataRequst.setUserKey(request.getUserKey());
        UseorgVO data = settingService.useOrgDetail(dataRequst);

        return ResponseEntity.ok(data);
    }
    @PostMapping("/useOrgUpd")
    public ResponseEntity<?> useOrgUpd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest.UseorgUpdateRequest request) {
        request.setModUser(currentUser.getUserKey());
        settingService.useOrgUpd(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "regOrg update successfully"));
    }
    
    // 계정관리
    @PostMapping("/addAccount")
    public ResponseEntity<?> addAccount(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRegistRequest request) throws Exception{
        String userTel = request.getUserTel().trim();
        String userNm = request.getUserNm().trim();
        if(userTel != null && !"".equals(userTel)) {
        	request.setUserTel(cryptoUtil.encrypt(userTel));
        }
        
        if(userNm != null && !"".equals(userNm)) {
        	request.setUserNm(cryptoUtil.encrypt(userNm));
        }

        //패드워드 유효성 검사(영문자 대문자 + 숫자 8~16자리)
        settingService.passwordCheck(request.getUserPwd());

        // 임ㅅ패스워드 세팅
        request.setTmpPwd(passwordEncoder.encode(request.getUserPwd()));
        request.setUserPwd(passwordEncoder.encode(request.getUserPwd()));

        request.setRegUserName(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        settingService.addAccount(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User registered successfully"));
    }
    // 계정관리 상태값 변경
    //
//    @PostMapping("/accountStatusUpd")
//    public ResponseEntity<?> accountStatusUpd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserUpdateRequest request) {
//        System.out.println("accountStatusUpd enter");
//        request.setRegUser(currentUser.getUserId());
//        settingService.accountStatusUpd(request);
//
//        return ResponseEntity.ok(new SignUpResponse(true, "regOrg status update successfully"));
//    }
    @PostMapping("/accountStatusUpd")
    public ResponseEntity<?> accountStatusUpd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserStatCdChangeRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        request.setRegUserId(currentUser.getUserKey());
        
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    settingService.accountStatusUpd(request);
                    return ResponseEntity.ok(new SignUpResponse(true, "account status update successfully"));
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "account status update fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "account status update fail"));
                }
            }
        }
        
        return ResponseEntity.ok(new SignUpResponse(false, "account status update fail"));
    }
    // 계정관리 수정
    @PostMapping("/detailAccount")
    public ResponseEntity<?> detailAccount(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request) {

        HfnUserRsponse data = settingService.selectHfnUserById(request);

        HfnUserVO vo = data.getHfnUserVO();

        String userNm = vo.getUserNm();
        String userTel = vo.getUserTel();
        String userEmail = vo.getUserEmail();
        if(userNm != null && !"".equals(userNm)) {
            vo.setUserNm(commonUtil.maskingName(userNm));
        }

        if(userTel != null && !"".equals(userTel)) {
            vo.setUserTel(commonUtil.maskingPhone(userTel));
        }

        return ResponseEntity.ok(data);
    }
    // 계정관리 마스킹해제
    @PostMapping("/accountUnMasking")
    public ResponseEntity<?> accountUnMasking(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request) {

        boolean result = commonUtil.compareWithShaStrings(request.getUserPwd(), currentUser.getPassword());

        if(!result) {
            return ResponseEntity.ok(result);
        }
        HfnUserRsponse data = settingService.accountUnMasking(request);

        return ResponseEntity.ok(data);
    }
    // 계정관리 수정
    @PostMapping("/accountUpd")
    public ResponseEntity<?> accountUpd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserUpdateRequest request) {

        boolean result = commonUtil.compareWithShaStrings(request.getUserPwd(), currentUser.getPassword());

        if(!result) {
            return ResponseEntity.ok(result);
        }
        // 같은 권한의 유형만 수정하는 로직 (제외)
//        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
//        for(GrantedAuthority ga : authorities) {
//            if(!ga.getAuthority().equals(request.getRoleCd())) {
//                log.error(messageSource.getMessage("C001"));
//                throw new BusinessException("C001",messageSource.getMessage("C001"));
//            }
//        }

        request.setRegUserName(currentUser.getUsername());
        request.setRegUser(currentUser.getUserKey());
        if(request.getUserTel().indexOf("-") > -1) {
            settingService.updateHfnUseNice(request);
        } else {
            settingService.updateHfnUserTel(request);
        }
        settingService.updateHfnUseNice(request);


        return ResponseEntity.ok(new SignUpResponse(true, "Hfn User Update successfully"));
    }
    @PostMapping("/accountLockRelease")
    public ResponseEntity<?> accountLockRelease(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody HfnUserRequest.HfnUserDetailRequest request) {
        HfnUserRsponse accountData = settingService.selectHfnUserById(request);
        HfnLoginLockCheckRequest data = new HfnLoginLockCheckRequest();

        data.setHfnId(accountData.getHfnUserVO().getHfnId());
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() == "ROLE_SYS_ADMIN") {
                try {
                    adminRepository.hfnLoginLockRelease(data);
                    return ResponseEntity.ok(new SignUpResponse(true, "Login Lockk Release Success"));
                } catch (BusinessException e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Login Lockk Release fail"));
                } catch (Exception e) {
                    return ResponseEntity.ok(new SignUpResponse(false, "Login Lockk Release fail"));
                }
            }
        }

        return ResponseEntity.ok(new SignUpResponse(false, "권한이 없습니다."));
    }
    
    @PostMapping("/selectTokenInfo")
    public ResponseEntity<?> selectTokenInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody LinkRequest.TokenRequest request) {
    	TokenResponse data = settingService.selectTokenInfo(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/saveTokenInfo")
    public ResponseEntity<?> saveTokenInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody LinkRequest.TokenRequestInsert request) {

    	List<LinkRequest.TokenRequest> saveList = request.getTokenInfoList();
		
		for(LinkRequest.TokenRequest data : saveList) {
			String status = data.getStatus();
			
			if(status != null && !"".equals(status) && "N".equals(status)) {
				settingRepository.insertTokenInfo(data);
			}else if(status != null && !"".equals(status) && "U".equals(status)) {
				settingRepository.updateTokenInfo(data);
			}
			
			RedisCommunicater.redisAccTokenIssKeySet(data.getGrantType() + "|" + data.getScope(), data.getClientId()+"|"+data.getScr());
		}
		
		return ResponseEntity.ok(new SignUpResponse(true, "OK"));
    }
    
    @PostMapping("/deleteTokenInfo")
    public ResponseEntity<?> deleteTokenInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody LinkRequest.TokenRequest request) {
    	settingRepository.deleteTokenInfo(request);
		RedisCommunicater.redisAccTokenIssKeyDel(request.getGrantType() + "|" + request.getScope());
		
		return ResponseEntity.ok(new SignUpResponse(true, "OK"));
    }
}
