package com.hanafn.openapi.portal.views.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppReadUserRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppScrRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.apiTestRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.AppsResponse;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeResponse;
import com.hanafn.openapi.portal.admin.views.service.CommService;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.cmct.dto.GWResponse;
import com.hanafn.openapi.portal.cmct.dto.OAuthRequest;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.ApiCtgrRsponse;
import com.hanafn.openapi.portal.views.dto.ApisRequest;
import com.hanafn.openapi.portal.views.dto.DocRequest;
import com.hanafn.openapi.portal.views.dto.DocResponse;
import com.hanafn.openapi.portal.views.dto.GuideRequest;
import com.hanafn.openapi.portal.views.dto.PlanResponse;
import com.hanafn.openapi.portal.views.dto.PrdApiResponse;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.dto.nice.ServiceCategoryEnum;
import com.hanafn.openapi.portal.views.repository.ApisRepository;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.service.ApiService;
import com.hanafn.openapi.portal.views.service.ApisService;
import com.hanafn.openapi.portal.views.service.AppsService;
import com.hanafn.openapi.portal.views.service.GuideService;
import com.hanafn.openapi.portal.views.service.SettingNiceService;
import com.hanafn.openapi.portal.views.vo.ApiTestVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/apis")
@Slf4j
public class ApisController {

    @Value("${sandboxEntrCd}")
    private String sandboxEntrCd;

    @Value("${sandboxClientId}")
    private String sandboxClientId;

    @Value("${sandboxClientSecret}")
    private String sandboxClientSecret;
    @Value("${gw.server.url}")
    private String gwServerUrl;
    @Value("${gw.noauth.url}")
    private String gwNoauthUrl;

    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    ApisService apisService;
    @Autowired
	SettingNiceService settingService;
    @Autowired
    GuideService guideService;
    @Autowired
    CommService commService;
    @Autowired
    CommonUtil commonUtil;
    @Autowired
    AppsRepository appsRepository;
    @Autowired
    ApisRepository apisRepository;
    
    @Autowired
    ApiService apiService;
    
    @Autowired
    AppsService appsService;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;
    
    @GetMapping("/drApiUpd")
    public ResponseEntity<?> drApiUpd(@RequestParam("drIp") String drIp) throws Exception {
        apiService.drApiUpd(drIp);
        
        return ResponseEntity.ok(new SignUpResponse(true, "Api Update successfully"));
    }
    
    @GetMapping("/encrypto")
    public ResponseEntity<?> encrypto(@CurrentUser UserPrincipal currentUser, @RequestParam("str") String str) throws Exception {
    	return ResponseEntity.ok(cryptoUtil.encrypt(str));
    }
    
    @PostMapping("/apis")
    public ResponseEntity<?> apis(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApisRequest request) {
        log.debug("★request:"+request);
        ApiCtgrRsponse.ApisResponse data = apisService.getApis(request);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/sandBoxCall")
    public ResponseEntity<?> sandBoxCall(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody GuideRequest request) {

        OAuthRequest.ClientInfo clientInfo = new OAuthRequest.ClientInfo();
        clientInfo.setClientId(sandboxClientId);
        clientInfo.setEntrCd(sandboxEntrCd);
        clientInfo.setHfnCd(request.getHfnCd());
        String encKey = guideService.selectEnckeyByEntrCd(sandboxEntrCd);
        log.debug("★encKey : "+ encKey);

        String decryptedEnckey = null;
        try {
			decryptedEnckey = cryptoUtil.decrypt(encKey);
			log.debug("★decryptedEnckey:" + decryptedEnckey);
			clientInfo.setDecryptedEnckey(decryptedEnckey);
			clientInfo.setClientSecret(cryptoUtil.decrypt(sandboxClientSecret));
			log.debug("★decrypted Secret:" + cryptoUtil.decrypt(sandboxClientSecret));
		} catch (UnsupportedEncodingException e) {
			log.error("ClientSecret Decrypt Error", e);
        } catch (Exception e) {
            log.error("ClientSecret Decrypt Error", e);
        }

        GWResponse res = GWCommunicater.getAceessToken(clientInfo);
        String accessToken = (String)res.getDataBody().get("access_token");

        Map<String, Object> params = new HashMap<>();

        Map<String, Object> dataHeader = new HashMap<>();
        dataHeader.put("CNTY_CD","kr");
        dataHeader.put("CLNT_IP_ADDR","");
        dataHeader.put("ENTR_CD",sandboxEntrCd);

        params.put("dataHeader", dataHeader);
        params.put("dataBody", request.getParams());
        params.put("decryptedEnckey",clientInfo.getDecryptedEnckey());
        params.put("ENTR_CD",sandboxEntrCd);

        // client id set
        params.put("client_id", sandboxClientId);

        // 관계사코드 + URL
        String hfnCd = ServiceCategoryEnum.resolve(request.getHfnCd()).getCode();
        String url = hfnCd + request.getUri();

        // 복호화된 accesstoken 넘겨준다.
        //res = GWCommunicater.communicateGateway("", url, HttpMethod.valueOf(request.getMethod()), accessToken, params);
        return ResponseEntity.ok(res);
    }
    
    @PostMapping("/selectProdList")
    public ResponseEntity<?> selectProdList(@CurrentUser UserPrincipal currentUser, @RequestBody ProductRequest request) {

        request.setSiteCd((null == currentUser) ? "userPortal" : currentUser.getSiteCd());
    	ProductResponse.ProductListResponse data = apisService.selectProdList(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectCommCodeList")
    public ResponseEntity<?> selectCommCodeList(@Valid @RequestBody CommCodeRequest commCodeRequest, @CurrentUser UserPrincipal currentUser) {
        CommCodeResponse data = commService.selectCommCode(commCodeRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectMainPrdList")
    public ResponseEntity<?> selectMainProdList(@CurrentUser UserPrincipal currentUser) {
    	
    	ProductResponse.ProductListResponse data = apisService.selectMainProdList();

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectAppScrKey")
    public ResponseEntity<?> selectAppScrKey(@CurrentUser UserPrincipal currentUser, @RequestBody AppScrRequest request) throws Exception {
    	
    	if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
      		 
	  		 AppsRequest appsRequest = new AppsRequest();
	  		 appsRequest.setAppKey(request.getAppKey());
	  		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
	           
	           List<AppsVO>  autoList = appsService.selectEditAppAuth(appsRequest);
	       	
	       	if(autoList == null || autoList.size() <= 0) {
	       		log.error(messageSource.getMessage("E021"));
	               throw new BusinessException("E021",messageSource.getMessage("E021"));
	       	}
    	}
    	
    	boolean result = commonUtil.compareWithShaStrings(request.getPassword(), currentUser.getPassword());
    	AppsResponse data = null;
    	if(result) {
    		data = apisService.selectAppScrKey(request);
    	}
    	
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectAppReadUsers")
    public ResponseEntity<?> selectAppReadUsers(@CurrentUser UserPrincipal currentUser, @RequestBody AppScrRequest request) throws Exception {
    	
    	if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
   		 
   		 AppsRequest appsRequest = new AppsRequest();
   		 appsRequest.setAppKey(request.getAppKey());
   		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
            
            List<AppsVO>  autoList = appsService.selectEditAppAuth(appsRequest);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021",messageSource.getMessage("E021"));
        	}
        }
    	
    	boolean result = commonUtil.compareWithShaStrings(request.getPassword(), currentUser.getPassword());
    	AppsResponse data = null;
    	if(result) {
    		data = apisService.selectAppReadUsers(request);
    	}
    	
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/deleteAppReadUsers")
    public ResponseEntity<?> deleteAppReadUsers(@CurrentUser UserPrincipal currentUser, @RequestBody AppScrRequest request) throws Exception {
    	
    	if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
   		 
   		 AppsRequest appsRequest = new AppsRequest();
   		 appsRequest.setAppKey(request.getAppKey());
   		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
            
            List<AppsVO>  autoList = appsService.selectEditAppAuth(appsRequest);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021",messageSource.getMessage("E021"));
        	}
        }
    	
    	apisService.deleteAppReadUsers(request);
    	
        return ResponseEntity.ok("Read User Delete successfully");
    }
    
    @PostMapping("/selectAppReadUser")
    public ResponseEntity<?> selectAppReadUser(@CurrentUser UserPrincipal currentUser, @RequestBody AppReadUserRequest request) throws Exception {

        request.setReqId(currentUser.getUserId());
    	AppsResponse data = apisService.selectAppReadUser(request);
    	
        return ResponseEntity.ok(data);
    }
    
    /*** 조회자삭제 ***/
    @PostMapping("/appReadUserDelete")
    public ResponseEntity<?> appReadUserDelete(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody List<String> request) throws Exception {
    	for(String seq : request) {
    		appsRepository.appReadUserDelete(seq);
    	}
        
        return ResponseEntity.ok(new SignUpResponse(true, "OPENAPI_PORTAL_APP_READ_INFO delete successfully"));
    }
    
    @PostMapping("/selectSubmitData")
    public ResponseEntity<?> selectSubmitData(@CurrentUser UserPrincipal currentUser, @RequestBody DocRequest.DocUserSubmitData request) throws Exception {
    	
    	DocResponse.DocListResponse data = apisService.selectSubmitData(request);
    	
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectAppPrdList")
    public ResponseEntity<?> selectAppPrdList(@CurrentUser UserPrincipal currentUser, @RequestBody AppsRequest request) throws Exception {
    	
    	AppsResponse data = apisService.selectAppPrdList(request);
    	
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectApi")
    public ResponseEntity<?> selectApi(@CurrentUser UserPrincipal currentUser, @RequestBody PrdApiRequest request) throws Exception {
    	
    	PrdApiResponse data = apisService.selectApiList(request);
    	
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectApiTest")
    public ResponseEntity<?> selectApiTest(HttpServletRequest httpRequest, @CurrentUser UserPrincipal currentUser, @RequestBody apiTestRequest request) throws Exception {

		
		ApiVO apiInfo = apisRepository.selectApiId(request);
		
		BufferedReader br = null;
		HttpURLConnection hcon = null;
		URL url;
		StringBuffer result = new StringBuffer();
		
		try {
			String conUrl = "";
			if(apiInfo != null) {
				conUrl=apiInfo.getApiUrl();
			}
			url = new URL(gwNoauthUrl+"/demo"+conUrl);
			try {
				hcon = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				log.error(e.getMessage());
			}

			if(hcon != null) {
				try {
					hcon.setRequestMethod("POST");
				} catch (ProtocolException e) {
					log.error(e.getMessage());
				}
				hcon.setDoOutput(true);
				hcon.setDoInput(true);
				hcon.setUseCaches(false);
				hcon.setDefaultUseCaches(false);

				hcon.setRequestProperty("Content-Type", "application/json");
				hcon.setRequestProperty("Accept", "application/json");
				
				String body = request.getBodyJson();
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = mapper.readValue(body, Map.class);

				Map header = (Map)map.get("dataHeader");

				header.put("CNTY_CD", "kr");

				body = mapper.writeValueAsString(map);

				OutputStream os;
				log.info("request url : " + gwNoauthUrl+"/demo"+conUrl);
				log.info("reqeust body : " + body);

				os = hcon.getOutputStream();
				os.write(body.getBytes("UTF-8"));
				os.flush();

				int code = hcon.getResponseCode();
				String message = hcon.getResponseMessage();
				
				log.info("response code : " + code);
				log.info("response message : " + message);
				if(code != 200) {
					throw new BusinessException(code+"",message);
				}

				br = new BufferedReader(new InputStreamReader(hcon.getInputStream(), "utf-8"));

				String temp;
				while ((temp = br.readLine()) != null) {
					result.append(temp);
				}
			} else {
				throw new BusinessException("HttpURLConnection Error");
			}
		} catch (MalformedURLException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (BusinessException e) {
			log.error(e.getMessage());
		}finally {
			if (hcon != null)
				hcon.disconnect();

			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
    	
		PrdApiResponse.apiTestResponse data = new PrdApiResponse.apiTestResponse();
		data.setResultData(result.toString());
		log.info("response result : " + result.toString());

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectApiTestUser")
    public ResponseEntity<?> selectApiTestUser(HttpServletRequest httpRequest, @CurrentUser UserPrincipal currentUser, @RequestBody apiTestRequest request) throws Exception {
    	
    	ApiTestVO apiTestInfo = apisRepository.selectApiTestAppInfo(request);
    	String clientId = apiTestInfo.getClientId();
		String token = apiTestInfo.getScr();
		
		OAuthRequest.ClientInfo clientInfo = new OAuthRequest.ClientInfo();
		clientInfo.setClientId(clientId);
		clientInfo.setClientSecret(cryptoUtil.decrypt(token));
		
		GWResponse res = GWCommunicater.getAceessToken(clientInfo);
		
		String accessToken = (String)res.getDataBody().get("access_token");
		
		ApiVO apiInfo = apisRepository.selectApiId(request);
		
		Date currentDate = new Date();
		long unixTime = currentDate.getTime() / 1000;

		String stringToken = accessToken + ":" + unixTime + ":" + clientId;
		//token 틀리게 해서 토큰 실패 건수가 증가하는지 확인한다
		//String StringToken = token +  "333:" + unixTime + ":" + client_id;
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("StringToken", stringToken);
		String newToken = "bearer " + new String(Base64.getEncoder().encode(stringToken.getBytes("UTF-8")));
		
		BufferedReader br = null;
		HttpURLConnection hcon = null;
		URL url;
		StringBuffer result = new StringBuffer();
		
		try {
			String conUrl = "";
			if(apiInfo != null) {
				conUrl=apiInfo.getApiUrl();
			}
			url = new URL(gwServerUrl+conUrl);
			try {
				hcon = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				log.error(e.getMessage());
			}

			if(hcon != null) {
				try {
					hcon.setRequestMethod("POST");
				} catch (ProtocolException e) {
					log.error(e.getMessage());
				}
				hcon.setDoOutput(true);
				hcon.setDoInput(true);
				hcon.setUseCaches(false);
				hcon.setDefaultUseCaches(false);

				hcon.setRequestProperty("Content-Type", "application/json");
				hcon.setRequestProperty("Accept", "application/json");
				hcon.setRequestProperty("Authorization", newToken);
                hcon.setRequestProperty("ProductID", apiInfo.getPrdCd());

				String body = request.getBodyJson();
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = mapper.readValue(body, Map.class);

				Map header = (Map)map.get("dataHeader");

				header.put("CNTY_CD", "kr");

				body = mapper.writeValueAsString(map);

				OutputStream os;
				log.info("request url : " + gwServerUrl+conUrl);
				log.info("reqeust body : " + body);
				log.info("reqeust stringToken : " + stringToken);

				os = hcon.getOutputStream();
				os.write(body.getBytes("UTF-8"));
				os.flush();

				int code = hcon.getResponseCode();
				String message = hcon.getResponseMessage();
				
				log.info("response code : " + code);
				log.info("response message : " + message);
				if(code != 200) {
					throw new BusinessException(code+"",message);
				}

				br = new BufferedReader(new InputStreamReader(hcon.getInputStream(), "utf-8"));

				String temp;
				while ((temp = br.readLine()) != null) {
					result.append(temp);
				}
			} else {
				throw new BusinessException("HttpURLConnection Error");
			}
		} catch (MalformedURLException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (BusinessException e) {
			log.error(e.getMessage());
		}finally {
			if (hcon != null)
				hcon.disconnect();

			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		}
    	
		PrdApiResponse.apiTestResponse data = new PrdApiResponse.apiTestResponse();
		data.setResultData(result.toString());
		log.info("response result : " + result.toString());

        return ResponseEntity.ok(data);
    }
    
    private String getClientIP(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");

        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }
    
    @PostMapping("/selectPayPlanList")
    public ResponseEntity<?> selectPayPlanList(@CurrentUser UserPrincipal currentUser, @RequestBody ProductRequest request, HttpServletRequest httpRequest) throws Exception {
    	
    	PlanResponse data = apisService.selectPayPlanList(request, httpRequest);
    	
        return ResponseEntity.ok(data);
    }
    
    //백오피스 임의 인터페이스 연계 테스트용차후삭제
    @PostMapping("/selectBoTest")
    public String selectBoTest(HttpServletRequest httpRequest) {
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	ObjectMapper mapper = new ObjectMapper();

    	String parameter = null;
    	StringBuilder stringBuilder = new StringBuilder();
    	BufferedReader bufferedReader = null;
    	try {
    	    InputStream inputStream = httpRequest.getInputStream();
    	    if (inputStream != null) {
    	        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    	        char[] charBuffer = new char[128];
    	        int bytesRead = -1;
    	        while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
    	            stringBuilder.append(charBuffer, 0, bytesRead);
    	        }
    	    }
    	    
    	    parameter = stringBuilder.toString();
    	    
    		Map<String, Object> map = mapper.readValue(parameter, Map.class);
    		
    		
    		result.put("resultCode", "0000");
    		result.put("message", "정상처리");
    		result.put("appKey", map.get("appKey"));
    		List<Map<String,Object>> resultPrdList = new ArrayList<Map<String,Object>>();
    		List<Map<String,Object>> prdList = (List<Map<String,Object>>)map.get("list");
    		
    		for(Map<String,Object> prd : prdList) {
    			Map<String, Object> prdInfo = new HashMap<String, Object>();
    			
    			prdInfo.put("serviceCd", prd.get("serviceCd"));
    			prdInfo.put("endDate", "20200930");
    			int useCnt = (int) prd.get("useCnt");
    			prdInfo.put("remainCount", useCnt-10);
    			prdInfo.put("useCnt", useCnt);
    			prdInfo.put("prdAmt", prd.get("prdAmt"));
    			prdInfo.put("mappingCode1", "Cd1221");
    			prdInfo.put("mappingCode2", "39dDas");
    			prdInfo.put("mappingCode3", "0dFs13");
    			prdInfo.put("mappingUser", "System");
    			prdInfo.put("prdAmt", prd.get("planCd"));
    			prdInfo.put("term", prd.get("term"));
    			
    			resultPrdList.add(prdInfo);
    		}
    		
    		result.put("list", resultPrdList);
    		
    	} catch (IOException e) {
    		log.error(e.getMessage());
    	} finally {
    	    if (bufferedReader != null) {
    	        try {
    	            bufferedReader.close();
    	        } catch (IOException e) {
    	        	log.error(e.getMessage());
    	        }
    	    }
    	}
    	
    	String resultString = "";
    	
    	try {
			resultString = mapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
		}
    	
    	return resultString;
    }
}
