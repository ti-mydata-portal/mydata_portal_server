package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class UseorgRequest {
    private String searchNm;
    private String searchUseorgStatCd;
    private String searchHfnCd;
    private String useorgNm;     // 기관명
    private String brn;          // 사업자등록번호
    private String useorgStatCd; // 이용기관상태
    private boolean ignoreAplvStat = false;

    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;

    @Data
    public static class UseorgDetailRequest{
        @NotBlank
        private String userKey;
        private String opType;
    }
    @Data
    public static class UseorgRegistRequest{
        private String userKey;
        private String useorgId;
        private String useorgNm;
        private String brn;
        private String useorgStatCd;
        private String useorgGb;
        private String entrCd;
        private String useorgNo;
        private String useorgAddr;
        private String useorgDomain;
        private String relorgCd;
        private String provCertCd;
        private String useorgCtnt;
        private String useorgUserNm;
        private String position;
        private String useorgUserEmail;
        private String useorgUserTel;
        private String regUser;
    }

    @Data
    public static class UseorgUpdateRequest{
        private String userKey;
        private String useorgId;
        private String useorgNm;
        private String brn;
        private String useorgStatCd;
        private String useorgGb;
        private String entrCd;
        private String encKey;
        private String useorgNo;
        private String useorgAddr;
        private String useorgDomain;
        private String relorgCd;
        private String provCertCd;
        private String useorgCtnt;
        private String useorgUserNm;
        private String position;
        private String useorgUserEmail;
        private String useorgUserTel;
        private String regUser;
        private String regDttm;
        private String modUser;
        private String modDttm;
        private String serialNum;
        private String useorgDomainIp;
        private String certIssuerDn;
        private String certOid;
        private String isRcvOrg;
        private List<Map<String, String>> clientIPs; // 허용IP
        private List<Map<String, String>> useorgDomainIPs; // 허용IP
        private List<Map<String, String>> nptimes; // 허용IP
    }
    // 마스킹해제용 request
    @Data
    public static class UseorgUnMasking{
        private String userPwdChk; // 본인인증 PWD 확인용
        private String userKey;
    }
    @Data
    public static class UseorgChannelRequest{
        private String seqNo;
        private String userKey;
        private String regUser;
        private String regDttm;
        private String cnlKey; // 허용IP
    }
    @Data
    public static class UseorgDomainRequest{
        private String seqNo;
        private String userKey;
        private String regUser;
        private String regDttm;
        private String domainIp; // 허용IP
    }
    @Data
    public static class UseorgNptimeRequest{
    	private String seqNo;
    	private String userKey;
    	private String regUser;
    	private String regDttm;
    	private String npTime;
    }

    @Data
    public static class UseorgDupCheckRequest{
        @NotBlank
        private String brn;
    }

    @Data
    public static class UseorgIdDupCheckRequest{
        @NotBlank
        private String useorgId;
    }

    @Data
    public static class UseorgStatCdChangeRequest {
        @NotBlank
        private String userKey;

        private String useorgStatCd;

        private String errorMsg;

        private String regUserName;

        private String regUserId;

        private String hbnUseYn;
        private String hnwUseYn;
        private String hlfUseYn;
        private String hcpUseYn;
        private String hcdUseYn;
        private String hsvUseYn;
        private String hmbUseYn;

        private String hfnCd;
    }

    @Data
    public static class UseorgSecedeRequest {
        @NotBlank
        private String userKey;
        private String userId;
        private String userNm;
        private String reasonGb;
        private String reasonDetail;
        private String aplvSeqNo;
        private String hbnUseYn;
        private String hnwUseYn;
        private String hlfUseYn;
        private String hcpUseYn;
        private String hcdUseYn;
        private String hsvUseYn;
        private String hmbUseYn;
    }

    @Data
    public static class HfnAplvRequest {
        private String userKey;
        private String userId;
        private String useorgNm;
        private String useorgUserNm;
        private String useorgCtnt;
        private String useorgId;
        private String userNm;
        private String hfnCd;
        private String useorgUpload;
        private String statCd;
    }

    @Data
    public static class HfnAplvRejectRequest {
        private String userKey;
        private String userId;
        private String useorgNm;
        private String useorgUserNm;
        private String useorgCtnt;
        private String useorgId;
        private String userNm;
        private String hfnCd;
        private String useorgUpload;
    }

    @Data
    public static class UseorgUploadRequest {
        @NotBlank
        private String userKey;
        private String useorgUpload;
    }
}
