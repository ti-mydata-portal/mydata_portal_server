

package com.hanafn.openapi.portal.views.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.SndCertMgntRequest;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.security.repository.SignupRepository;
import com.hanafn.openapi.portal.security.service.SignupService;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.PasswordValidator;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.IpinRequest;
import com.hanafn.openapi.portal.views.dto.IpinResponse;
import com.hanafn.openapi.portal.views.repository.IpinRepository;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.service.IpinService;
import com.hanafn.openapi.portal.views.vo.IpinVO;

import Kisinfo.Check.IPIN2Client;
import NiceID.Check.CPClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Controller
@RequestMapping("/ipin")
@Slf4j
@RequiredArgsConstructor
@SessionAttributes("cert_info")
public class IpinController {
	
	private final IpinService service;

	@Value("${cert.ipin.code}")
	private String ipinSiteCode;

	@Value("${cert.ipin.pw}")
	private String ipinSitePw;
	
	@Value("${cert.mobile.code}")
	private String mobileSiteCode;
	
	@Value("${cert.mobile.pw}")
	private String mobileSitePw;
	
	@Value("${cert.ipin.code.admin}")
	private String ipinSiteCodeAdmin;

	@Value("${cert.ipin.pw.admin}")
	private String ipinSitePwAdmin;
	
	@Value("${cert.mobile.code.admin}")
	private String mobileSiteCodeAdmin;
	
	@Value("${cert.mobile.pw.admin}")
	private String mobileSitePwAdmin;
	
	@Value("${cert.host}")
	private String host;
	
	@Value("${cert.host.admin}")
	private String hostAdmin;
	
	@Autowired
    MessageSourceAccessor messageSource;
	
	@Autowired
    SignupService signUpService;
	
	@Autowired
	SignupRepository signupRepository;
	
	@Autowired
	IpinRepository repository;
	
	@Autowired
    PasswordEncoder passwordEncoder;
	
	@Autowired
    SettingRepository settingRepository;
	
	@Autowired
    CommonUtil commonUtil;

    @Autowired
    PasswordValidator passwordValidator;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;

    /*
     * ******************************IPIN CATEGORY******************************
    */
	@PostMapping("/result")
    public ResponseEntity<?> result(@Valid @RequestBody IpinRequest ipinRequest, HttpServletRequest request, HttpSession session, @CurrentUser UserPrincipal currentUser) throws Exception {
		
		/********************************************************************************************************************************************
			NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
			
			서비스명 : IPIN 가상주민번호서비스 서비스
			페이지명 : IPIN 가상주민번호서비스 결과 페이지
		*********************************************************************************************************************************************/
		
		String sSiteCode	= ipinSiteCode;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트코드
		String sSitePw		= ipinSitePw;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트패스워드
				
		// ipin_process에서 전달받은 인증결과 암호화 데이터 취득
		String sResponseData = requestReplace(ipinRequest.getResultEncData(), "encodeData");    
	    
		// ipin_main에서 세션에 저장한 CP요청번호 취득
		String sCPRequest = (String)session.getAttribute("CPREQUEST");   
			
		// 모듈 객체 생성
	    IPIN2Client pClient = new IPIN2Client();
		
		// 인증결과 데이터 복호화
		// : CP요청번호 파라미터 추가 시 세션 추출값과 전송된 데이터 비교해 데이터 위변조 검사 가능
		// 예) int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData, sCPRequest);	
		int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData);
		
		// 인증결과 추출
		String sName					= pClient.getName();			// 성명 (EUC-KR)
		String sBirthDate				= pClient.getBirthDate();		// 생년월일 (YYYYMMDD)
		String sCPRequestNum			= pClient.getCPRequestNO();		// CP 요청번호
		String sDupInfo					= pClient.getDupInfo();			// 중복가입확인값 (64byte, 개인식별값, DI:Duplicate Info) 
		String sCIUpdate			    = pClient.getCIUpdate();		// CI 갱신정보 (1~: 가이드 참조)
		String sAuthInfo			    = pClient.getAuthInfo();		// 본인확인수단 (0~4: 가이드 참조)
		String errCode = "N001";
		IpinResponse data = new IpinResponse();
		// 복호화 처리결과코드에 따른 처리Ò
	    if (iRtn == 1) {
	        /*
	            추출된 사용자 정보는 '성명'만 이용자에게 노출 가능합니다.
	        
	            사용자 정보를 다른 페이지에서 이용하는 경우,
	            암호화 데이터(sResponseData)를 전송 후 해당 페이지에서 복호화하는 형태로 이용해주십시오
	            
	            복호화된 사용자 정보를 전달할 경우, 
	                데이터가 유출되지 않도록 세션처리를 권장드립니다. (예: CP요청번호를 세션에 저장/취득하는 방식)
	            form의 hidden 태그를 이용한 전달방식은 데이터 유출 위험이 높으므로 지양해주시기 바랍니다.
	        */
	                
	    	ipinRequest.setDi(sDupInfo);
	    	
	    	String certYn = "N";
	    	
	    	if(ipinRequest.getCertType().equals("1")) {
	    		SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
	        	sndCertMgntRequest.setSendCd("I");
				sndCertMgntRequest.setSendNo(sDupInfo);
				sndCertMgntRequest.setResultCd(sCPRequestNum);

                String overYn = repository.selectOverDI(sndCertMgntRequest);

                if (overYn != null && !"".equals(overYn) && "Y".equals(overYn)) {
                    errCode = "E032";
                    throw new BusinessException("E032", messageSource.getMessage("E032"));
                }

	            sndCertMgntRequest.setSendCtnt(cryptoUtil.encrypt(sName + ":" +sBirthDate));
				signupRepository.insertSndCertMgntForSelfAuth(sndCertMgntRequest);
				
				data.setResNo(sCPRequestNum);
				data.setMobileNo("");
				data.setName(sName);
				
				certYn = "Y";
	    	}else if(ipinRequest.getCertType().equals("2")) {
	    		ipinRequest.setUserKey(currentUser.getUserKey());
	    		data = service.selectCI(ipinRequest);
	    		
	    		certYn = data.getCertYn();
	    	}else if(ipinRequest.getCertType().equals("3")) {
	    		
	    		data = service.selectCertUserId(ipinRequest);
	    		
	    		certYn = data.getCertYn();
	    	}else if(ipinRequest.getCertType().equals("4")) {
	    		
	    		data = service.selectCertUserId(ipinRequest);
	    		
	    		certYn = data.getCertYn();
	    		
	    		if(certYn != null && !"".equals(certYn) && "Y".equals(certYn)) {
	    			IpinRequest.IpinUpdateReqNo updateReq = new IpinRequest.IpinUpdateReqNo();
		    		
		    		updateReq.setResNo(sCPRequestNum);
		    		updateReq.setDi(sDupInfo);
		    		data.setResNo(sCPRequestNum);
		    		
		    		repository.updateCertUserIdResNo(updateReq);
	    		}
	    	}
			
			if(!ipinRequest.getCertType().equals("2")) {
				if(certYn != null && !"".equals(certYn) && "Y".equals(certYn)){
					data.setSRtnMsg("정상 처리되었습니다.");
				}else{
					errCode = "L011";
					throw new BusinessException("L011", messageSource.getMessage("L011"));
				}
			}else {
				data.setSRtnMsg("정상 처리되었습니다.");
			}
	    } else {
	        throw new BusinessException(iRtn+"", messageSource.getMessage(errCode));
	    }
		
		return ResponseEntity.ok(data);
    }
	
	private String requestReplace (String paramValue, String gubun) {
        String result = "";
        
        if (paramValue != null) {
        	result = paramValue;

			result = result.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

			result = result.replaceAll("\\*", "");
			result = result.replaceAll("\\?", "");
			result = result.replaceAll("\\[", "");
			result = result.replaceAll("\\{", "");
			result = result.replaceAll("\\(", "");
			result = result.replaceAll("\\)", "");
			result = result.replaceAll("\\^", "");
			result = result.replaceAll("\\$", "");
			result = result.replaceAll("'", "");
			result = result.replaceAll("@", "");
			result = result.replaceAll("%", "");
			result = result.replaceAll(";", "");
			result = result.replaceAll(":", "");
			result = result.replaceAll("-", "");
			result = result.replaceAll("#", "");
			result = result.replaceAll("--", "");
			result = result.replaceAll("-", "");
			result = result.replaceAll(",", "");
        	
        	if(gubun != "encodeData"){
				result = result.replaceAll("\\+", "");
				result = result.replaceAll("/", "");
				result = result.replaceAll("=", "");
        	}
            
        }
        return result;
  }
    
    
    /*
     * ******************************IPIN CATEGORY******************************
    */
    @PostMapping("/main")
    public ResponseEntity<?>  main(HttpServletRequest request, HttpSession session) {
    	/********************************************************************************************************************************************
			NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
			
			서비스명 : IPIN 가상주민번호 서비스 
			페이지명 : IPIN 가상주민번호 서비스 호출 페이지
		*********************************************************************************************************************************************/
		
    	String sSiteCode	= ipinSiteCode;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트코드
		String sSitePw		= ipinSitePw;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트패스워드
		
		
		/*
		┌ sReturnURL 변수에 대한 설명  ─────────────────────────────────────────────────────
			암호화된 인증결과 데이터를 리턴받을 URL을 프로토콜부터 풀주소로 정의해주세요.
			
			* URL은 반드시 프로토콜부터 입력해야 하며 외부에서 접속이 가능한 주소여야 합니다.
			* 당사 샘플페이지 중 ipin_process 페이지가 인증결과 데이터를 리턴받는 페이지입니다.
			
			예 - http://www.test.co.kr/ipin_process.jsp
				 https://www.test.kr:4343/ipin_process.jsp
		└────────────────────────────────────────────────────────────────────
		*/
		String sReturnURL   = host+"/ipin/proc";
		
		/*
		┌ sCPRequest 변수에 대한 설명  ─────────────────────────────────────────────────────
			CP요청번호는 추가적인 보안처리를 위한 변수입니다. 인증 후 인증결과 데이터와 함께 전달됩니다.
			세션에 저장된 값과 비교해 데이터 위변조를 검사하거나, 사용자 특정용으로 이용할 수 있습니다.	
			위변조 검사는 인증에 필수적인 처리는 아니며 보안을 위한 권고 사항입니다.		
			
			+ CP요청번호 설정 방법
				1. 당사에서 배포된 모듈로 생성
				2. 귀사에서 임의로 정의(최대 30byte) 
		└────────────────────────────────────────────────────────────────────
		*/
		String sCPRequest = "";	
		
		
		// 모듈객체 생성
		IPIN2Client pClient = new IPIN2Client();	
		
		// CP요청번호 생성
		sCPRequest = pClient.getRequestNO(sSiteCode);
		
		// CP요청번호 세션에 저장 
		// : 저장된 값은 ipin_result 페이지에서 데이터 위변조 검사에 이용됩니다.
		session.setAttribute("CPREQUEST" , sCPRequest);
		
		
		// 인증요청 암호화 데이터 생성
		int iRtn = pClient.fnRequest(sSiteCode, sSitePw, sCPRequest, sReturnURL);
		
		String sEncData		= "";	// 인증요청 암호화 데이터
		String sRtnMsg		= "";	// 처리결과 메세지
		
		// 암호화 처리결과코드에 따른 처리
		if (iRtn == 0)
		{
	        // 인증요청 암호화 데이터 추출
			sEncData = pClient.getCipherData();		
			sRtnMsg = "정상 처리되었습니다.";
		}
		else if (iRtn == -1)
		{
			sRtnMsg = "암호화 시스템 오류 : 귀사 서버 환경에 맞는 모듈을 이용해주십시오.<br>오류가 지속되는 경우 iRtn 값, 서버 환경정보, 사이트코드를 기재해 문의주시기 바랍니다.";
		}
		else if (iRtn == -2)
		{
			sRtnMsg = "암호화 처리 오류 : 최신 모듈을 이용해주십시오. 오류가 지속되는 경우 iRtn 값, 서버 환경정보, 사이트코드를 기재해 문의주시기 바랍니다.";
		}
		else if (iRtn == -9)
		{
			sRtnMsg = "입력 정보 오류 : 암호화 함수에 입력된 파라미터 값을 확인해주십시오.<br>오류가 지속되는 경우, 함수 실행 직전 각 파라미터 값을 로그로 출력해 발송해주시기 바랍니다.";
		}
		else
		{
			sRtnMsg = "기타 오류: iRtn 값과 적용한 샘플소스를 발송해주시기 바랍니다.";
		}
	
		IpinResponse data = new IpinResponse();
		
		data.setSEncData(sEncData);
		data.setSRtnMsg(sRtnMsg);
		data.setResNo(sCPRequest);
    	
        return ResponseEntity.ok(data);
    }
    
    /*
     * ******************************IPIN CATEGORY******************************
    */
    @PostMapping("/mainAdmin")
    public ResponseEntity<?>  mainAdmin(HttpServletRequest request, HttpSession session) {
    	/********************************************************************************************************************************************
			NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
			
			서비스명 : IPIN 가상주민번호 서비스 
			페이지명 : IPIN 가상주민번호 서비스 호출 페이지
		*********************************************************************************************************************************************/
		
    	String sSiteCode	= ipinSiteCode;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트코드
		String sSitePw		= ipinSitePw;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트패스워드
		
		
		/*
		┌ sReturnURL 변수에 대한 설명  ─────────────────────────────────────────────────────
			암호화된 인증결과 데이터를 리턴받을 URL을 프로토콜부터 풀주소로 정의해주세요.
			
			* URL은 반드시 프로토콜부터 입력해야 하며 외부에서 접속이 가능한 주소여야 합니다.
			* 당사 샘플페이지 중 ipin_process 페이지가 인증결과 데이터를 리턴받는 페이지입니다.
			
			예 - http://www.test.co.kr/ipin_process.jsp
				 https://www.test.kr:4343/ipin_process.jsp
		└────────────────────────────────────────────────────────────────────
		*/
		String sReturnURL   = hostAdmin+"/ipin/proc";
		
		/*
		┌ sCPRequest 변수에 대한 설명  ─────────────────────────────────────────────────────
			CP요청번호는 추가적인 보안처리를 위한 변수입니다. 인증 후 인증결과 데이터와 함께 전달됩니다.
			세션에 저장된 값과 비교해 데이터 위변조를 검사하거나, 사용자 특정용으로 이용할 수 있습니다.	
			위변조 검사는 인증에 필수적인 처리는 아니며 보안을 위한 권고 사항입니다.		
			
			+ CP요청번호 설정 방법
				1. 당사에서 배포된 모듈로 생성
				2. 귀사에서 임의로 정의(최대 30byte) 
		└────────────────────────────────────────────────────────────────────
		*/
		String sCPRequest = "";	
		
		
		// 모듈객체 생성
		IPIN2Client pClient = new IPIN2Client();	
		
		// CP요청번호 생성
		sCPRequest = pClient.getRequestNO(sSiteCode);
		
		// CP요청번호 세션에 저장 
		// : 저장된 값은 ipin_result 페이지에서 데이터 위변조 검사에 이용됩니다.
		session.setAttribute("CPREQUEST" , sCPRequest);
		
		
		// 인증요청 암호화 데이터 생성
		int iRtn = pClient.fnRequest(sSiteCode, sSitePw, sCPRequest, sReturnURL);
		
		String sEncData		= "";	// 인증요청 암호화 데이터
		String sRtnMsg		= "";	// 처리결과 메세지
		
		// 암호화 처리결과코드에 따른 처리
		if (iRtn == 0)
		{
	        // 인증요청 암호화 데이터 추출
			sEncData = pClient.getCipherData();		
			sRtnMsg = "정상 처리되었습니다.";
		}
		else if (iRtn == -1)
		{
			sRtnMsg = "암호화 시스템 오류 : 귀사 서버 환경에 맞는 모듈을 이용해주십시오.<br>오류가 지속되는 경우 iRtn 값, 서버 환경정보, 사이트코드를 기재해 문의주시기 바랍니다.";
		}
		else if (iRtn == -2)
		{
			sRtnMsg = "암호화 처리 오류 : 최신 모듈을 이용해주십시오. 오류가 지속되는 경우 iRtn 값, 서버 환경정보, 사이트코드를 기재해 문의주시기 바랍니다.";
		}
		else if (iRtn == -9)
		{
			sRtnMsg = "입력 정보 오류 : 암호화 함수에 입력된 파라미터 값을 확인해주십시오.<br>오류가 지속되는 경우, 함수 실행 직전 각 파라미터 값을 로그로 출력해 발송해주시기 바랍니다.";
		}
		else
		{
			sRtnMsg = "기타 오류: iRtn 값과 적용한 샘플소스를 발송해주시기 바랍니다.";
		}
	
		IpinResponse data = new IpinResponse();
		
		data.setSEncData(sEncData);
		data.setSRtnMsg(sRtnMsg);
		data.setResNo(sCPRequest);
    	
        return ResponseEntity.ok(data);
    }
    
    /*
     * ******************************IPIN CATEGORY******************************
    */
    @RequestMapping(value = {"/proc"},produces = "text/html; charset=utf8")
    @ResponseBody
    public String proc(@CurrentUser UserPrincipal currentUser, HttpServletResponse response, @RequestParam(value = "enc_data")String encData) {
    	String result ="";
    	result += "<html>\r\n";
    	result += "<head>\r\n";
    	result += "<meta charset=\"EUC-KR\">\r\n";
    	result += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
    	result += "<title>NICE평가정보 가상주민번호 서비스</title>\r\n";
    	result += "<script>\r\n";
    	result += "function fnLoad()\r\n"; 
    	result += "{\r\n";
    	
    	String sResponseData = requestReplace(encData, "encodeData");
    	
    	result += "if(!!opener.$) {\r\n";
    	result += "opener.$(\"#wrap>section\").prop(\"__vue__\").$children[0].ipin.resultEncData = \""+sResponseData+"\";\r\n"; 
    	result += "opener.$(\"#wrap>section\").prop(\"__vue__\").$children[0].result_ipin();\r\n";
    	result += "} else {\r\n";
		result += "opener.document.getElementById('app').__vue__.$children[0].ipin.resultEncData = \""+sResponseData+"\";\r\n"; 
    	result += "opener.document.getElementById('app').__vue__.$children[0].result_ipin();\r\n";
    	result += "}\r\n";
    	
    	result += "\r\n";
    	result += "self.close();\r\n"; 
    	result += "}\r\n";
    	result += "</script>\r\n"; 
    	result += "</head>\r\n";
    	
    	if (!sResponseData.equals("") && sResponseData != null) {
    		result += "<body onLoad=\"fnLoad()\">\r\n";
    	}else {
    		result += "<body onLoad=\"self.close()\">\r\n";
    	}
    	result += "</body>\r\n"; 
    	result += "</html>";
    	
    	response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
    	
        return result;
    }
    
    /*
     * ******************************MOBILE CATEGORY******************************
    */
    @PostMapping("/mainMobile")
    public ResponseEntity<?>  mainMobile(HttpServletRequest request, HttpSession session) {
    	NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
        
        String sSiteCode = mobileSiteCode;			// NICE로부터 부여받은 사이트 코드
        String sSitePassword = mobileSitePw;		// NICE로부터 부여받은 사이트 패스워드
        
        String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 
                                                        	// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
        sRequestNumber = niceCheck.getRequestNO(sSiteCode);
      	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
      	
       	String sAuthType = "";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
       	
       	String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
    	String customize 	= "";		//없으면 기본 웹페이지 / Mobile : 모바일페이지
    	
    	String sGender = ""; 			//없으면 기본 선택 값, 0 : 여자, 1 : 남자 
    	
        // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    	//리턴url은 인증 전 인증페이지를 호출하기 전 url과 동일해야 합니다. ex) 인증 전 url : http://www.~ 리턴 url : http://www.~
        String sReturnUrl = host+"/ipin/success";      // 성공시 이동될 URL
        String sErrorUrl = host+"/ipin/fail";          // 실패시 이동될 URL

        // 입력될 plain 데이타를 만든다.
        String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                            "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
                            "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                            "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                            "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                            "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                            "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize + 
    						"6:GENDER" + sGender.getBytes().length + ":" + sGender;
        
        String sMessage = "";
        String sEncData = "";
        
        int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);
        if( iReturn == 0 )
        {
            sEncData = niceCheck.getCipherData();
        }
        else if( iReturn == -1)
        {
            sMessage = "암호화 시스템 에러입니다.";
        }    
        else if( iReturn == -2)
        {
            sMessage = "암호화 처리오류입니다.";
        }    
        else if( iReturn == -3)
        {
            sMessage = "암호화 데이터 오류입니다.";
        }    
        else if( iReturn == -9)
        {
            sMessage = "입력 데이터 오류입니다.";
        }    
        else
        {
            sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
        }
        
        IpinResponse data = new IpinResponse();
		
		data.setSEncData(sEncData);
		data.setSRtnMsg(sMessage);
		data.setResNo(sRequestNumber);
    	
        return ResponseEntity.ok(data);
    }
    
    /*
     * ******************************MOBILE CATEGORY******************************
    */
    @PostMapping("/mainMobileAdmin")
    public ResponseEntity<?>  mainMobileAdmin(HttpServletRequest request, HttpSession session) {
    	NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();
        
        String sSiteCode = mobileSiteCode;			// NICE로부터 부여받은 사이트 코드
        String sSitePassword = mobileSitePw;		// NICE로부터 부여받은 사이트 패스워드
        
        String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로 
                                                        	// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
        sRequestNumber = niceCheck.getRequestNO(sSiteCode);
      	session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.
      	
       	String sAuthType = "";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서
       	
       	String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
    	String customize 	= "";		//없으면 기본 웹페이지 / Mobile : 모바일페이지
    	
    	String sGender = ""; 			//없으면 기본 선택 값, 0 : 여자, 1 : 남자 
    	
        // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
    	//리턴url은 인증 전 인증페이지를 호출하기 전 url과 동일해야 합니다. ex) 인증 전 url : http://www.~ 리턴 url : http://www.~
        String sReturnUrl = hostAdmin+"/ipin/success";      // 성공시 이동될 URL
        String sErrorUrl = hostAdmin+"/ipin/fail";          // 실패시 이동될 URL

        // 입력될 plain 데이타를 만든다.
        String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                            "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
                            "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                            "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                            "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                            "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                            "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize + 
    						"6:GENDER" + sGender.getBytes().length + ":" + sGender;
        
        String sMessage = "";
        String sEncData = "";
        
        int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);
        if( iReturn == 0 )
        {
            sEncData = niceCheck.getCipherData();
        }
        else if( iReturn == -1)
        {
            sMessage = "암호화 시스템 에러입니다.";
        }    
        else if( iReturn == -2)
        {
            sMessage = "암호화 처리오류입니다.";
        }    
        else if( iReturn == -3)
        {
            sMessage = "암호화 데이터 오류입니다.";
        }    
        else if( iReturn == -9)
        {
            sMessage = "입력 데이터 오류입니다.";
        }    
        else
        {
            sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
        }
        
        IpinResponse data = new IpinResponse();
		
		data.setSEncData(sEncData);
		data.setSRtnMsg(sMessage);
		data.setResNo(sRequestNumber);
    	
        return ResponseEntity.ok(data);
    }
    
    @PostMapping(value = {"/success","/fail"},produces = "text/html; charset=utf8")
    @ResponseBody
    public String success(HttpServletRequest httpServletRequest, HttpServletResponse response, @RequestParam(value = "EncodeData")String request, HttpSession session, @CurrentUser UserPrincipal currentUser) {    	
    	String result ="";
    	result += "<html>\r\n";
    	result += "<head>\r\n";
    	result += "<meta charset=\"utf-8\">\r\n";
    	result += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
    	result += "<title>NICE평가정보 - CheckPlus 안심본인인증</title>\r\n";
    	result += "<script>\r\n";
    	result += "function fnLoad()\r\n"; 
    	result += "{\r\n";
    	
    	String sResponseData = requestReplace(request, "encodeData");
    	
    	result += "if(!!opener.$) {\r\n";
    	result += "opener.$(\"#wrap>section\").prop(\"__vue__\").$children[0].ipinMobile.resultEncData = \""+sResponseData+"\";\r\n"; 
    	result += "opener.$(\"#wrap>section\").prop(\"__vue__\").$children[0].result_mobile();\r\n";
    	result += "} else {\r\n";
		result += "opener.document.getElementById('app').__vue__.$children[0].ipinMobile.resultEncData = \""+sResponseData+"\";\r\n"; 
    	result += "opener.document.getElementById('app').__vue__.$children[0].result_mobile();\r\n";
    	result += "}\r\n";
		
    	result += "\r\n";
    	result += "self.close();\r\n"; 
    	result += "}\r\n";
    	result += "</script>\r\n"; 
    	result += "</head>\r\n";
    	
    	if (!sResponseData.equals("") && sResponseData != null) {
    		result += "<body onLoad=\"fnLoad()\">\r\n";
    	}else {
    		result += "<body onLoad=\"self.close()\">\r\n";
    	}
    	result += "</body>\r\n"; 
    	result += "</html>";
    	
    	response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
    	
        return result;
    }
    
    @GetMapping(value = {"/success","/fail"},produces = "text/html; charset=utf8")
    @ResponseBody
    public String success2(HttpServletRequest httpServletRequest, HttpServletResponse response, @RequestParam(value = "EncodeData")String request, HttpSession session, @CurrentUser UserPrincipal currentUser) {    	
    	String result ="";
    	result += "<html>\r\n";
    	result += "<head>\r\n";
    	result += "<meta charset=\"utf-8\">\r\n";
    	result += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n";
    	result += "<title>NICE평가정보 - CheckPlus 안심본인인증</title>\r\n";
    	result += "<script>\r\n";
    	result += "function fnLoad()\r\n"; 
    	result += "{\r\n";
    	
    	String sResponseData = requestReplace(request, "encodeData");
    	
    	result += "if(!!opener.$) {\r\n";
    	result += "opener.$(\"#wrap>section\").prop(\"__vue__\").$children[0].ipinMobile.resultEncData = \""+sResponseData+"\";\r\n"; 
    	result += "opener.$(\"#wrap>section\").prop(\"__vue__\").$children[0].result_mobile();\r\n";
    	result += "} else {\r\n";
		result += "opener.document.getElementById('app').__vue__.$children[0].ipinMobile.resultEncData = \""+sResponseData+"\";\r\n"; 
    	result += "opener.document.getElementById('app').__vue__.$children[0].result_mobile();\r\n";
    	result += "}\r\n";
		
    	result += "\r\n";
    	result += "self.close();\r\n"; 
    	result += "}\r\n";
    	result += "</script>\r\n"; 
    	result += "</head>\r\n";
    	
    	if (!sResponseData.equals("") && sResponseData != null) {
    		result += "<body onLoad=\"fnLoad()\">\r\n";
    	}else {
    		result += "<body onLoad=\"self.close()\">\r\n";
    	}
    	result += "</body>\r\n"; 
    	result += "</html>";
    	
    	response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
    	
        return result;
    }
    
    /*
     * ******************************IPIN CATEGORY******************************
    */
	@PostMapping("/mobile")
    public ResponseEntity<?> mobile(@Valid @RequestBody IpinRequest ipinRequest, HttpServletRequest request, HttpSession session, @CurrentUser UserPrincipal currentUser) {
    	CPClient niceCheck = new CPClient();
    	
    	String sEncodeData = requestReplace(ipinRequest.getResultEncData(), "encodeData");
    	
    	String sSiteCode = mobileSiteCode;			// NICE로부터 부여받은 사이트 코드
        String sSitePassword = mobileSitePw;		// NICE로부터 부여받은 사이트 패스워드

        String sConnInfo = "";				// 연계정보 확인값 (CI_88 byte)
        String sPlainData = "";
        String mobileNo = "";
        String name = "";   
        String brithDt = "";

        int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);
        String errCode = "N001";
        
        HashMap mapresult = null;
        IpinResponse data = new IpinResponse();

        try {
			if (iReturn == 0) {
				sPlainData = niceCheck.getPlainData();
				mapresult = niceCheck.fnParse(sPlainData);
				sConnInfo = (String) mapresult.get("CI");
				mobileNo = (String) mapresult.get("MOBILE_NO");
				name = (String) mapresult.get("NAME");
				brithDt = (String) mapresult.get("BIRTHDATE");

				String sResponseNumber = (String) mapresult.get("RES_SEQ");
				String sDi = (String) mapresult.get("DI");

				ipinRequest.setDi((String) mapresult.get("DI"));

				String certYn = "N";

				if (ipinRequest.getCertType().equals("1")) {
					SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
					sndCertMgntRequest.setSendCd("M");
					sndCertMgntRequest.setSendNo(sDi);
					sndCertMgntRequest.setResultCd(sResponseNumber);
					sndCertMgntRequest.setSendCtnt(cryptoUtil.encrypt(name + mobileNo + ":" + brithDt));

					String overYn = repository.selectOverDI(sndCertMgntRequest);

					if (overYn != null && !"".equals(overYn) && "Y".equals(overYn)) {
						errCode = "E032";
						throw new BusinessException("E032", messageSource.getMessage("E032"));
					}

					signupRepository.insertSndCertMgntForSelfAuth(sndCertMgntRequest);

					data.setResNo(sResponseNumber);
					data.setMobileNo(mobileNo);
					data.setName(name);

					certYn = "Y";
				} else if (ipinRequest.getCertType().equals("2")) {
					ipinRequest.setUserKey(currentUser.getUserKey());

					data = service.selectCI(ipinRequest);

					certYn = data.getCertYn();
				} else if (ipinRequest.getCertType().equals("3")) {

					data = service.selectCertUserId(ipinRequest);

					certYn = data.getCertYn();
				} else if (ipinRequest.getCertType().equals("4")) {

					data = service.selectCertUserId(ipinRequest);

					certYn = data.getCertYn();

					if (certYn != null && !"".equals(certYn) && "Y".equals(certYn)) {
						IpinRequest.IpinUpdateReqNo updateReq = new IpinRequest.IpinUpdateReqNo();

						updateReq.setResNo(sResponseNumber);
						updateReq.setDi(sDi);
						data.setResNo(sResponseNumber);

						repository.updateCertUserIdResNo(updateReq);
					}
				}
				
				if(!ipinRequest.getCertType().equals("2")) {
					if (certYn != null && !"".equals(certYn) && "Y".equals(certYn)) {
						data.setSRtnMsg("정상 처리되었습니다.");
					} else {
						errCode = "L011";
						throw new BusinessException("L011", messageSource.getMessage("L011"));
					}
				}else {
					data.setSRtnMsg("정상 처리되었습니다.");
				}

			} else {
				throw new BusinessException(iReturn + "", messageSource.getMessage("N001"));
			}
		} catch ( BusinessException e ) {
			throw e;
		} catch ( Exception e ) {
			log.error("본인인증 성공 이력 DB 확 중 에러 : " + e.toString());
			throw new BusinessException(iReturn+"", messageSource.getMessage(errCode));
		}

        return ResponseEntity.ok(data);
    }
	
	/***********  개인회원 비밀번호변경 *************/
    @PostMapping("/newUserPwd")
    public ResponseEntity<?> setUserPwd(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody IpinRequest.IpinUpdatePw request) {

    	IpinVO data = repository.selectCertResNo(request);

        if (data != null) {
            List<UserPwHisVO> userList = settingRepository.getUserIdPw(request.getUserId());

            if (userList != null) {
                for (UserPwHisVO userData : userList) {
                    if(!commonUtil.compareWithShaStringsPw(request.getUserPwd(), userData.getUserPwd())) {
                        log.error("비밀번호 검증에러 [불일치]");
                        throw new BusinessException("E112", messageSource.getMessage("E112"));
                    }
                    // 비밀번호 검증
                    passwordValidator.isValidPassword(request.getUserPwd(),userData.getUserId());
                }
                request.setUserId(data.getUserId());
            	request.setUserPwd(passwordEncoder.encode(request.getUserPwd()));
            	
            	service.setPwd(request);
            }
        	
            return ResponseEntity.ok(new SignUpResponse(true, "User_Pwd_Update successfully"));
        } else {
            log.error(messageSource.getMessage("L005"));
            throw new BusinessException("L005", messageSource.getMessage("L005"));
        }
    }
    
    /***********  관리자 인증 현황 체크 *************/
    @PostMapping("/selectCertCnt")
    public ResponseEntity<?> selectCertCnt(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody IpinRequest request) {
        // 비밀번호 검증
    	IpinResponse.IpinAdminCertYn data = new IpinResponse.IpinAdminCertYn();
    	request.setUserKey(currentUser.getUserKey());
    	int certCnt = repository.selectCertCnt(request);
    	
    	data.setCertCnt(certCnt);
    	if(certCnt <= 0) {
    		data.setCertYn("Y");
    	}else {
    		data.setCertYn("N");
    	}
        
        return ResponseEntity.ok(data);
    }
    
    /***********  관리자 인증 등록 *************/
    @PostMapping("/updateAdminCert")
    public ResponseEntity<?> updateAdminCert(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody IpinRequest request) {
    	if(request.getSendCd().equals("M")) {
    		CPClient niceCheck = new CPClient();
        	
        	String sEncodeData = requestReplace(request.getResultEncData(), "encodeData");
        	
        	String sSiteCode = mobileSiteCode;			// NICE로부터 부여받은 사이트 코드
            String sSitePassword = mobileSitePw;		// NICE로부터 부여받은 사이트 패스워드

            int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);
            String errCode = "N001";
            
            HashMap mapresult = null;
            IpinResponse data = new IpinResponse();
            String sPlainData = "";
            
    		if (iReturn == 0) {
    			sPlainData = niceCheck.getPlainData();
    			mapresult = niceCheck.fnParse(sPlainData);
    			String sResponseNumber = (String) mapresult.get("RES_SEQ");
    			String sDi = (String) mapresult.get("DI");
    			request.setDi((String) mapresult.get("DI"));
    			
    			SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
    			sndCertMgntRequest.setSendCd(request.getSendCd());
    			sndCertMgntRequest.setSendNo(sDi);
    			sndCertMgntRequest.setUserKey(currentUser.getUserKey());

    			repository.insertAdminCert(sndCertMgntRequest);
    		}
    	}else {
    		String sSiteCode	= ipinSiteCode;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트코드
    		String sSitePw		= ipinSitePw;		//  NICE평가정보에서 발급한 IPIN 서비스 사이트패스워드
    				
    		String sResponseData = requestReplace(request.getResultEncData(), "encodeData");    
    	    
    	    IPIN2Client pClient = new IPIN2Client();
    		
    		int iRtn = pClient.fnResponse(sSiteCode, sSitePw, sResponseData);
    		
    		String sName					= pClient.getName();			// 성명 (EUC-KR)
    		String sBirthDate				= pClient.getBirthDate();		// 생년월일 (YYYYMMDD)
    		String sCPRequestNum			= pClient.getCPRequestNO();		// CP 요청번호
    		String sDupInfo					= pClient.getDupInfo();			// 중복가입확인값 (64byte, 개인식별값, DI:Duplicate Info) 
    		String sCIUpdate			    = pClient.getCIUpdate();		// CI 갱신정보 (1~: 가이드 참조)
    		String sAuthInfo			    = pClient.getAuthInfo();		// 본인확인수단 (0~4: 가이드 참조)
    		String errCode = "N001";
    		IpinResponse data = new IpinResponse();
    	    if (iRtn == 1) {
    	    	request.setDi(sDupInfo);
    	    	
    	    	String certYn = "N";
    	    	
    	    	SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
	        	sndCertMgntRequest.setSendCd(request.getSendCd());
				sndCertMgntRequest.setSendNo(sDupInfo);
				sndCertMgntRequest.setUserKey(currentUser.getUserKey());
				
				repository.insertAdminCert(sndCertMgntRequest);
    	    }
    	}
    	
    	return ResponseEntity.ok(new SignUpResponse(true, "admin Cert update successfully"));
    }
}