package com.hanafn.openapi.portal.views.repository;

import java.security.Policy;
import java.util.List;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.views.dto.*;
import com.hanafn.openapi.portal.views.vo.*;
import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppUsageRatioRequest;
import com.hanafn.openapi.portal.admin.views.vo.TokenPolicyVO;

@Mapper
public interface PolicyRepository {

    /***************************API 정책***************************/
    ApiPolicyVO selectApiPolicy(ApiPolicyRequest apiPolicyRequest);
    void insertApiPolicy(ApiPolicyRequest apiPolicyRequest);
    void updateApiPolicy(ApiPolicyRequest apiPolicyRequest);

    /***************************APP 정책***************************/
    int countGetTokenPolicyList(TokenPolicyRequest tokenPolicyRequest);
    List<TokenPolicyVO> selectGetTokenPolicyList(TokenPolicyRequest tokenPolicyRequest);
    List<TokenPolicyVO> selectGetTokenUseorgList(TokenPolicyRequest tokenPolicyRequest);
    TokenPolicyVO selectGetTokenPolicy(TokenPolicyRequest tokenPolicyRequest);
    void insertGetTokenPolicy(TokenPolicyRequest tokenPolicyRequest);
    void updateGetTokenPolicy(TokenPolicyRequest tokenPolicyRequest);
    void updateGetTokenPolicyReset(TokenPolicyRequest tokenPolicyRequest);
    void insertGetTokenPolicyUseSetting(TokenPolicyRequest tokenPolicyRequest);
    void updateGetTokenPolicyUseSetting(TokenPolicyRequest tokenPolicyRequest);

    /** NICE **/
    List<TokenPolicyVO> selectGetTokenUserList(TokenPolicyRequest request);

    /***************************상품 정책***************************/
    PrdPolicyVO selectPrdPolicy(PrdPolicyRequest request);
    void insertPrdPolicy(PrdPolicyRequest request);

    /***************************** app 사용량 랜덤제어 *****************************/
    AppUsageRatioVO selectAppUsageRatio(AppUsageRatioRequest request);
    void insertAppUsageRatio(AppUsageRatioRequest request);
    void deleteAppUsageRatio(AppUsageRatioRequest request);

    // 신규
    // api 정보 조회
    List<ApiVO> policyGetApiList();
    // 클라이언트ID 정보 조회
    List<ServiceAppVO> policyGetClientIdList(ServiceAppRequest.policyAppRequest request);
    // 서버IP 정보 조회
    List<ServiceAppIpVO> policyGetServerIpList(ServiceAppRequest.policyServerIpRequest request);
    // 정책 등록
    public void regPolicyService(ServicePolicyRequest request);
    // 정책 조회
    List<PolicyServiceVO> getPolicyServiceList(ServicePolicyRequest request);
    // 정책조회 카운트
    int cntPolicyServiceList(ServicePolicyRequest request);
    // 정책조회 상태 변경
    void updPolicyServiceStatus(ServicePolicyRequest request);
    // 정책 삭제
    void delPolicyService(ServicePolicyRequest request);
    // 정책조회 수정 조회
    PolicyServiceVO getPolicyServiceDetail(ServicePolicyRequest request);
    // 정책조회 수정
    void updPolicyService(ServicePolicyRequest request);
}