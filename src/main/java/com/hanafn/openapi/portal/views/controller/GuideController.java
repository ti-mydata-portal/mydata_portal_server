package com.hanafn.openapi.portal.views.controller;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdSettReqeust;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.cmct.dto.GWResponse;
import com.hanafn.openapi.portal.cmct.dto.OAuthRequest;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.ApiDevGuideRequest;
import com.hanafn.openapi.portal.views.dto.ApiRsponsePaging;
import com.hanafn.openapi.portal.views.dto.AppsAllRequest;
import com.hanafn.openapi.portal.views.dto.AppsAllRsponse;
import com.hanafn.openapi.portal.views.dto.GuideRequest;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.dto.nice.ServiceCategoryEnum;
import com.hanafn.openapi.portal.views.service.ApiService;
import com.hanafn.openapi.portal.views.service.GuideService;
import com.hanafn.openapi.portal.views.vo.ApiCtgrVO;
import com.hanafn.openapi.portal.views.vo.ApiDevGuideVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class GuideController {

    private final GuideService guideService;
    private final ApiService apiService;
    private final MessageSourceAccessor messageSource;
    private static final Logger LOGGER = LoggerFactory.getLogger(GuideController.class);

    @Value("${spring.profiles.active}")
    private String thisServer;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;
    
    @Autowired
    GWCommunicater gWCommunicater;
    
    @Autowired
    CommonUtil commonUtil;

    @PostMapping("/guideGw")
    public ResponseEntity<?> guideOAuth(@CurrentUser UserPrincipal currentUser, HttpServletRequest servletRequest, @Valid @RequestBody GuideRequest request) {

        OAuthRequest.ClientInfo clientInfo = new OAuthRequest.ClientInfo();

        clientInfo.setClientId(request.getClientId());
        clientInfo.setEntrCd(request.getEntrCd());
        String encKey = guideService.selectEnckeyByEntrCd(request.getEntrCd());
        log.debug("★encKey : "+ encKey);

        String decryptedEnckey = null;

        try {
            decryptedEnckey = cryptoUtil.decrypt(encKey);
            log.debug("★decryptedEnckey:" + decryptedEnckey);
            clientInfo.setDecryptedEnckey(decryptedEnckey);
            clientInfo.setClientSecret(cryptoUtil.decrypt(request.getClientSecret()));
            log.debug("★decrypted Secret:" + cryptoUtil.decrypt(request.getClientSecret()));
            clientInfo.setHfnCd(request.getHfnCd());
        } catch (UnsupportedEncodingException e) {
            log.error("ClientSecret Decrypt Error", e);
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch (Exception e) {
            log.error("ClientSecret Decrypt Error", e);
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }

//         ENCKEY로 암호화 된 ACCESS TOKEN이 반환된다.
        GWResponse res = GWCommunicater.getAceessToken(clientInfo);
        String accessToken = (String)res.getDataBody().get("access_token");

        String clientIpAddr = servletRequest.getHeader("X-FORWARDED-FOR");
        if (clientIpAddr == null ) clientIpAddr = servletRequest.getRemoteAddr();

        Map<String, Object> dataHeader = new HashMap<>();
        dataHeader.put("CNTY_CD","kr");
        dataHeader.put("CLNT_IP_ADDR",clientIpAddr);
        dataHeader.put("ENTR_CD",request.getEntrCd());

       if (thisServer.equals("development")) {
           dataHeader.put("DEV_CD", "T");
        }

        Map<String, Object> params = new HashMap<>();
        params.put("dataHeader", dataHeader);
        params.put("dataBody", request.getParams());
        params.put("decryptedEnckey",clientInfo.getDecryptedEnckey());
        params.put("ENTR_CD",request.getEntrCd());
        params.put("APP_KEY",request.getAppKey());

        // client id set
        params.put("client_id", request.getClientId());

        // ENCKEY 암호화된 ACCESSTOKEN 그대로 넘겨준다.
        // GW통신 요청 : [Authorization Aes256Uitil.encrypt(decryptedEncKey, (복호화된 ACCESSTOKEN:UNIXTIME:CLIENT_ID))]
        // GW통신 응답 : GW 응답 전문(암호화X)

        // 관계사코드 + URL
        String hfnCd = ServiceCategoryEnum.resolve(request.getHfnCd()).getCode();
        if (thisServer.equals("development")) hfnCd +="-dev";
        if ("onefgw".equals(request.getGwType())) hfnCd = "file";
        String url = hfnCd + request.getUri();

        res = gWCommunicater.communicateGateway(request.getGwType(), url, HttpMethod.valueOf(request.getMethod()), accessToken, params);

        return ResponseEntity.ok(res);
    }

    /********************************개발 가이드********************************/

    @PostMapping("/devGuides")
    public ResponseEntity<?> devGuideList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiDevGuideRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(!ga.getAuthority().contains("ROLE_SYS_ADMIN")){
                request.setSearchHfnCd(currentUser.getHfnCd());
            }
        }

        List<ApiCtgrVO> searchApiCtgrList = request.getSearchApiCtgrList();
        String qApiCtgrs = guideService.settingSearchApiCtgrs(searchApiCtgrList);
        request.setSearchApiCtgrs(qApiCtgrs);

        List<ApiDevGuideVO> data = null;

        try {
            if (StringUtils.isBlank(request.getSearchUserKey())) {
                data = guideService.selectApiDevGuideAllList(request);
            } else {
                data = guideService.selectApiDevGuideList(request);
            }

            return ResponseEntity.ok(data);
        } catch (BusinessException e) {
            log.error("execute Error", e);
            throw new BusinessException("E020",messageSource.getMessage("E020"));
        } catch (Exception e) {
            log.error("execute Error", e);
            throw new BusinessException("E020",messageSource.getMessage("E020"));
        }
    }

    @PostMapping("/appsAll")
    public ResponseEntity<?> appsAll(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsAllRequest request) {
        AppsAllRsponse data = guideService.selectAppsAll(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/appsAllNice")
    public ResponseEntity<?> appsAllNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsAllRequest request) {
    	request.setSearchUserKey(currentUser.getUserKey());
        AppsAllRsponse data = guideService.selectAppsAllNice(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectAppsAfterNice")
    public ResponseEntity<?> selectAppsAfterNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsAllRequest request) {
    	request.setSearchUserKey(currentUser.getUserKey());
        AppsAllRsponse data = guideService.selectAppsAfterNice(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectAppsBeforeNice")
    public ResponseEntity<?> selectAppsBeforeNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsAllRequest request) {
    	request.setSearchUserKey(currentUser.getUserKey());
        AppsAllRsponse data = guideService.selectAppsBeforeNice(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectProdList")
    public ResponseEntity<?> selectProdList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PrdSettReqeust request) {
    	request.setUserKey(currentUser.getUserKey());
    	ProductResponse.ProductListResponse data = guideService.selectProdList(request);

        return ResponseEntity.ok(data);
    }

    /*** NICE ***/
    @PostMapping("/devApiList")
    public ResponseEntity<?> devApiList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiRequest request) {

        ApiRsponsePaging data = apiService.selectApiListPaging(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/devGuideGw")
    public ResponseEntity<?> devGuideGw(@CurrentUser UserPrincipal currentUser, HttpServletRequest servletRequest, @Valid @RequestBody GuideRequest request) throws Exception {

        String clientIpAddr = servletRequest.getHeader("X-FORWARDED-FOR");
        if (clientIpAddr == null ) clientIpAddr = servletRequest.getRemoteAddr();

        Map<String, Object> dataHeader = new HashMap<>();
        dataHeader.put("iss", request.getIss());
        dataHeader.put("client_id", request.getClientId());
        dataHeader.put("guid", commonUtil.getGuid());
        dataHeader.put("aud", request.getAud());
        dataHeader.put("exp", request.getExp());
        dataHeader.put("ci", request.getCi());
        dataHeader.put("scope", request.getScope());

        Map<String, Object> params = new HashMap<>();
        params.put("dataHeader", dataHeader);
        params.put("dataBody", request.getParams());
        
        String url = request.getUrl();
        url=url.substring(1);

        GWResponse res = gWCommunicater.devCommunicateGateway(request.getHfnCd(), request.getApiAuthType(), url, HttpMethod.valueOf(request.getMethod()), params);

        return ResponseEntity.ok(res);
    }
}