package com.hanafn.openapi.portal.views.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaInsertRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserDetailRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserPwdUpdateRequest;
import com.hanafn.openapi.portal.admin.views.vo.AppCnlInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.CertMailVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.MailSmsVO;
import com.hanafn.openapi.portal.admin.views.vo.QnaVO;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.views.dto.ApiTimesampRequest;
import com.hanafn.openapi.portal.views.dto.AplvRequest;
import com.hanafn.openapi.portal.views.dto.BatchLogListRequest;
import com.hanafn.openapi.portal.views.dto.ElasticRequest;
import com.hanafn.openapi.portal.views.dto.FaqRequest;
import com.hanafn.openapi.portal.views.dto.HfnInfoRequest;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;
import com.hanafn.openapi.portal.views.dto.LinkRequest;
import com.hanafn.openapi.portal.views.dto.MailRequest;
import com.hanafn.openapi.portal.views.dto.NoticeRequest;
import com.hanafn.openapi.portal.views.dto.PrivacyReleaseLogRequest;
import com.hanafn.openapi.portal.views.dto.TrxRequest;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.dto.UserLoginRequest;
import com.hanafn.openapi.portal.views.dto.UserRequest;
import com.hanafn.openapi.portal.views.vo.AplvHisVO;
import com.hanafn.openapi.portal.views.vo.AplvVO;
import com.hanafn.openapi.portal.views.vo.BatchLogVO;
import com.hanafn.openapi.portal.views.vo.FaqVO;
import com.hanafn.openapi.portal.views.vo.HfnInfoVO;
import com.hanafn.openapi.portal.views.vo.NoticeVO;
import com.hanafn.openapi.portal.views.vo.NptimeVO;
import com.hanafn.openapi.portal.views.vo.RequestApiVO;
import com.hanafn.openapi.portal.views.vo.TokenVO;
import com.hanafn.openapi.portal.views.vo.TrxVO;
import com.hanafn.openapi.portal.views.vo.UseorgVO;
import com.hanafn.openapi.portal.views.vo.UserWithdrawVO;

@Mapper
public interface SettingRepository {

	/********************************이용기관*******************************/
	UseorgVO selectUseorg(UseorgRequest.UseorgDetailRequest useorgDetailRequest);

    UserWithdrawVO selectUserWithDraw(UserRequest request);

	UserVO getAdminAuth(AdminRequest.HfnLoginLockCheckRequest request);

	/********************************사용자*******************************/
	UserVO selectUser(UserDetailRequest userDetailRequest);

	int countUserList(UserRequest userRequest);
	List<UserVO> selectUserList(UserRequest userRequest);
	void insertUser(UserRequest.UserRegistRequest userRegistRequest);

	UserVO selectUserForUserId(UserRequest.UserRegistRequest userRegistRequest);
	void insertUserRole(UserRequest.UserRegistRequest userRegistRequest);
	UserVO selectUserPwd(UserDetailRequest userDetailRequest);
	void updateUser(UserRequest.UserUpdateRequest userUpdateRequest);
	void updateUserStatCdChange(UserRequest.UserStatCdChangeRequest userRequest);
	void userPwdUpdate(UserPwdUpdateRequest userPwdUpdateRequest);
	void userPwdAndTosUpdate(UserRequest.UserPwdAndTosUpdateRequest userPwdAndTosUpdateRequest);
	void userTepPwdUpdate(UserRequest.UserTmpPwdUpdateRequest userTmpPwdUpdateRequest);
	void updateUserLoginType(UserLoginRequest.UpdateTypeRequest request);

	void secedeUser(UserRequest.UserSecedeRequest request);
	void secedeUserLogin(UserRequest.UserSecedeRequest request);
	void secedeUserWithDraw(UserRequest.UserSecedeRequest request);
	UserRequest.UserSecedeRequest selectUserWithDrawReason(String userKey);

	int secedeUseAppCheck(UserRequest.UserSecedeRequest request);
	
	void insertSecedeUserWithDraw(UserRequest.UserSecedeRequest request);
	void updateSecedeUser(UserRequest.UserSecedeRequest request);
	void deleteSecedeUserLogin(UserRequest.UserSecedeRequest request);
	void deleteSecedeCert(UserRequest.UserSecedeRequest request);
	
	/********************************승인*******************************/

	String selectAplvBtnYn(AplvRequest.AplvDetailRequest aplvDetailRequest);
	AplvVO selectAplvDetail(String aplvSeqNo);

	List<AplvHisVO> selectAplvHis(AplvRequest.AplvDetailRequest aplvDetailRequest);
	AppsVO selectAppDetailInfo(AppsRequest appsRequest);

	List<AppCnlInfoVO> selectAppChannlList(AdminRequest.ApiDetailRequest apiDetailRequest);

	int countAplvList(AplvRequest aplvRequest);
	List<AplvVO> selectAplvList(AplvRequest aplvRequest);
	void insertAplvHis(AplvRequest.AplvHisRegistRequest aplvRequest);
	void updateAplvInfo(AplvRequest.AplvApprovalRequest aplvApprovalRequest);

	/**************************관계사 사용자*******************************/
	int hfnIdDupCheck(HfnUserRequest.HfnUserDupCheckRequest hfnUserDupCheckRequest);
	void insertHfnUser(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
	HfnUserVO selectHfnUserForId(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
	void insertHfnUserRole(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
	int countHfnUserList(HfnUserRequest hfnUserRequest);
	List<HfnUserVO> selectHfnUserList(HfnUserRequest hfnUserRequest);
	void updateHfnUserStatCdChange(HfnUserRequest.HfnUserStatCdChangeRequest hfnUserStatCdChangeRequest);
	void hfnUserPwdUpdate(AdminRequest.HfnUserPwdUpdateRequest hfnUserPwdUpdateRequest);
	void hfnUserPwdHisUpdate(AdminRequest.HfnUserPwdUpdateRequest hfnUserPwdUpdateRequest);
	void hfnUserPwdAndTosUpdate(HfnUserRequest.HfnUserPwdAndTosUpdateRequest hfnUserPwdAndTosUpdateRequest);

	/********************************관계사*******************************/
	List<HfnInfoVO> selectHfnCdList();
	List<HfnUserVO> selectMyHfnMember(HfnInfoRequest hfnInfoRequest);

	HfnUserVO selectHfnUser(HfnUserRequest.HfnUserDetailRequest hfnUserDetailRequest);


	void updateHfnUserNice(AdminRequest.HfnUserUpdateRequest hfnUserUpdateRequest);
	void updateHfnUserTel(AdminRequest.HfnUserUpdateRequest hfnUserUpdateRequest);

	// 이용기관의 기관코드 리턴
	String selectUseorgEntrCd(AppsRequest request);
	AppsVO selectUseorgEntrCdFromInfo(AppsRequest request);

	// 이용기관 포탈 - 개발자 등록, 삭제(기관코드, user_type, role 변경)
	List<UserPwHisVO> getUserIdPw(String userId);

    /** 로그인 테이블관련 **/
	void updateUserLoginPwd(UserLoginRequest.UpdatePwdRequest request);
	void insertPwHisUser(UserLoginRequest.UpdatePwdRequest request);

	String hfnLoginLockYn(AdminRequest.HfnLoginLockCheckRequest hfnLoginLockCheckRequest);			// 관리자 포탈 로그인 잠금 여부 리턴

	/** 개인회원 아이디 찾기 **/
	UserVO searchUserId(UserRequest.searchUserRequest request);

	/** 가입완료 **/
	void authCompleteCertMgnt(AdminRequest.SndCertMgntRequest request);

	/** 인증완료 **/
	void updateCertMgnt(AdminRequest.SndCertMgntRequest request);

    /** 인증정보조회**/
    UserVO selectCertMgntUser(UserRequest.searchUserRequest request);

    /** 인증정보조회**/
    UseorgVO selectCertMgntUseorg(UserRequest.searchUserRequest request);

	//------------------------------------------------------------------------------------------------------
	// 개발자공간-공지사항목록
	//------------------------------------------------------------------------------------------------------
	List<NoticeVO> selectNoticeList(NoticeRequest.NoticeListRequest noticeListRequest);
	int countNoticeList(NoticeRequest.NoticeListRequest noticeListRequest);
	NoticeVO detailNotice(NoticeRequest noticeRequest);
	void increaseNoticeViewCnt(NoticeRequest noticeRequest);
	void insertNotice(NoticeRequest noticeRequest);
	void deleteNotice(NoticeRequest noticeRequest);
	void updateNotice(NoticeRequest noticeRequest);

	List<FaqVO> selectFaqList(FaqRequest.FaqListRequest faqListRequest);
	int countFaqList(FaqRequest.FaqListRequest faqListRequest);
	FaqVO detailFaq(FaqRequest faqRequest);
	void insertFaq(FaqRequest faqRequest);
	void updateFaq(FaqRequest faqRequest);
	void deleteFaq(FaqRequest faqRequest);

	void insertQna(QnaInsertRequest request);
	List<HashMap<String, Object>> selectAdminEmail(QnaInsertRequest request);
	
	List<QnaVO> selectQnaListAuth(QnaRequest qnaRequest);
	int countQnaAuth(QnaRequest qnaRequest);
	QnaVO detailQna(QnaRequest qnaRequest);
	void deleteQna(QnaRequest qnaRequest);
	void updateAnswer(QnaRequest qnaRequest);

	String selectQnaAttachment01(QnaRequest qnaRequest);
	String selectQnaAttachment02(QnaRequest qnaRequest);

	/** api 할인율등록 **/
	void insertApiChargeDiscountRate(RequestApiVO request);
	/** api 할인율수정 **/
	void updateApi(RequestApiVO request);
	/** api 할인율 승인 **/
	void updateDiscountByNForAppKeyAndApiId(RequestApiVO request);

	// 개인 정보 해제 로그 등록
    void insertPrivacyReleaseLog(PrivacyReleaseLogRequest request);

    /** 메일 재발송 관련 **/
    String selectUserkeyByUserid(AdminRequest.SndCertMgntRequest sndCertMgntRequest);
    List<CertMailVO> selectCertMailList(AdminRequest.SndCertMgntRequest sndCertMgntRequest);
    int countCertMailList(AdminRequest.SndCertMgntRequest sndCertMgntRequest);
    CertMailVO detailCertMail(AdminRequest.SndCertMgntRequest sndCertMgntRequest);

    // 배치 ID 목록
    List<BatchLogVO> selectBatchIdList();

	int countBatchLogList(BatchLogListRequest request);

	List<BatchLogVO> selectBatchLogList(BatchLogListRequest request);

	/** 관리자 - 트랜잭션 관리 **/
	List<TrxVO> selectTrx(TrxRequest request);
	int cntTrxList(TrxRequest request);
	void regTrx(TrxRequest request);
	TrxVO detailTrx(TrxRequest request);
	void updateTrx(TrxRequest request);
	void deleteTrx(TrxRequest request);
    /* NICE 관리자포털 */
    UserVO adminSelectUser(UserDetailRequest userDetailRequest);
    List<UserVO> adminSelectUserList(UserRequest userRequest);
    void adminUpdateUser(UserRequest.UserUpdateRequest userUpdateRequest);

    /* 메일 발송 */
    String selectMailSender(MailRequest.MailSender request);

    MailSmsVO selectSendQna(QnaRequest request);
    /* 수신자관리(기관) */
	int cntUseOrgList(UseorgRequest request);
	// 수신자관리(기관) 카운트
	List<UseorgVO> selectUseOrgListPaging(UseorgRequest request);
	// 수신자관리(기관) 등록
	void insertUseOrg(UseorgRequest.UseorgUpdateRequest request);
	// 수신자관리(기관) 상세보기
	UseorgVO useOrgDetail(UseorgRequest.UseorgUpdateRequest request);
	Map<String, Object> useOrgDetailMap(LinkRequest request);
	List<Map<String, Object>> useOrgDetailIpMap(LinkRequest request);
	List<Map<String, Object>> useOrgDetailDomainIpMap(LinkRequest request);
	List<Map<String, Object>> useOrgDetailNptimeMap(LinkRequest request);
	// 수신자관리(기관) 상태값 변경
	void useOrgStatusUpd(UseorgRequest.UseorgUpdateRequest request);
	// 수신자관리(기관) 정보값 변경
	void useOrgUpd(UseorgRequest.UseorgUpdateRequest request);
	// 수신자관리(기관) 이력관리
	void insertUseOrgHis(UseorgRequest.UseorgUpdateRequest request);
	void insertUseOrgStatusHis(UseorgVO request);
	int chkUseorgDup(UseorgRequest.UseorgUpdateRequest request);
	void addAccount(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
	void addAccountRole(AdminRequest.HfnUserRegistRequest hfnUserRegistRequest);
	void accountStatusUpd(HfnUserRequest.HfnUserStatCdChangeRequest hfnUserRegistRequest);
	void accountRoleUpd(AdminRequest.HfnUserUpdateRequest request);
	
	int selectSearchTimeCount(ApiTimesampRequest request);
	void insertSearchTime(ApiTimesampRequest request);
	void updateSearchTime(ApiTimesampRequest request);
	
	int selectTokenCnt(LinkRequest.TokenRequest reqeust);
	void insertToken(LinkRequest.TokenRequest reqeust);
	void updateToken(LinkRequest.TokenRequest reqeust);
	void useorgChannelReg(UseorgRequest.UseorgChannelRequest request);
	void useorgDomainReg(UseorgRequest.UseorgDomainRequest request);
	void useorgNptimeReg(UseorgRequest.UseorgNptimeRequest request);
	List<UseorgVO> useorgIps(UseorgRequest.UseorgUpdateRequest request);
	List<UseorgVO> useorgDomainIps(UseorgRequest.UseorgUpdateRequest request);
	List<NptimeVO> useorgNptimes(UseorgRequest.UseorgUpdateRequest request);
	void useorgIpsDel(UseorgRequest.UseorgUpdateRequest request);
	void useorgDomainIpsDel(UseorgRequest.UseorgUpdateRequest request);
	void useorgNptimesDel(UseorgRequest.UseorgUpdateRequest request);
	
	Map<String, Object> selectVerInfo();
	List<Map<String, Object>> selectApiList();
	List<String> getDateList(ElasticRequest request);
	List<TokenVO> selectTokenInfo(LinkRequest.TokenRequest request);
	void insertTokenInfo(LinkRequest.TokenRequest request);
	void updateTokenInfo(LinkRequest.TokenRequest request);
	void deleteTokenInfo(LinkRequest.TokenRequest request);
}
