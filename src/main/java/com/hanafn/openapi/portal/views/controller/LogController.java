package com.hanafn.openapi.portal.views.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.MultiSearchResponse.Item;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.ApiLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.ApiTrxCommInfoDTO;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.dto.PortalLogRsponsePaging;
import com.hanafn.openapi.portal.admin.views.service.LogService;
import com.hanafn.openapi.portal.admin.views.vo.ApiLogVO;
import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.views.dto.ElasticRequest;
import com.hanafn.openapi.portal.views.dto.ElasticResponse;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.vo.ElasticVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class LogController {

    private final LogService logService;
    private final MessageSourceAccessor messageSource;
    
    @Autowired
	RestHighLevelClient highLevelClient;
    
    @Autowired
    SettingRepository settingRepository;
    
    @Value("${spring.mydata.log.index}")
	String esIndex;
	
	@Value("${spring.mydata.log.topic}")
	String esTopic;

    @PostMapping("/portalLog")
    public ResponseEntity<?> portalLog(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PortalLogRequest.PortalLogDetailRequest request) {
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities){
            if(ga.getAuthority().contains("ROLE_SYS_ADMIN") || ga.getAuthority().contains("ROLE_HFN_ADMIN")){

                PortalLogVO data = logService.selectPortalLog(request);
                return ResponseEntity.ok(data);
            }
        }
        log.error(messageSource.getMessage("E021"));
        throw new BusinessException("E021",messageSource.getMessage("E021"));
    }
    
    @PostMapping("/portalLogs")
    public ResponseEntity<?> portalLogList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody PortalLogRequest request) {
    	PortalLogRsponsePaging data = logService.selectPortalLogListPaging(request);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/apiLog")
    public ResponseEntity<?> apiLog(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiLogRequest.ApiLogDetailRequest request) {

        ApiLogVO data = logService.selectApiLog(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/apiLogs")
    public ResponseEntity<?> apiLogList(@CurrentUser UserPrincipal currentUser, @RequestBody ElasticRequest request) throws Exception {
    	
    	String startDate = request.getSearchStDt();
    	String gte = CommonUtil.chgToGMTTimeFromSeoulTime(startDate+"000000");
    	String lte = CommonUtil.chgToGMTTimeFromSeoulTime(startDate+"235959");
    	
    	String esDatastream = "";
    	
    	SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    	searchSourceBuilder.size(request.getPageSize());
    	searchSourceBuilder.from((request.getPageIdx()-1)*request.getPageSize());
    	
    	List<String> dates = settingRepository.getDateList(request);
    	MultiSearchRequest mSearchRequest = new MultiSearchRequest();
    	for(String regDateTime : dates) {
    		BoolQueryBuilder bq = QueryBuilders.boolQuery();
        	
        	bq = bq.must(QueryBuilders.matchQuery("topic", esTopic));
        	if(request.getStatusCode() != null && !"".equals(request.getStatusCode()) && "200".equals(request.getStatusCode())) {
        		bq = bq.must(QueryBuilders.termQuery("httpStatus", "200"));
        	}else{
        		bq = bq.filter(QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery("httpStatus", "200")));
        	}
        	
        	if(request.getClientId() != null && !"".equals(request.getClientId())) {
        		bq = bq.must(QueryBuilders.matchQuery("clientId", request.getClientId()));
        	}
        	
        	if(request.getSearchNm() != null && !"".equals(request.getSearchNm())) {
        		bq = bq.must(QueryBuilders.wildcardQuery("requestUri", "*"+request.getSearchNm()+"*"));
        	}
        	
        	if(request.getUseorg() != null && !"".equals(request.getUseorg())) {
        		bq = bq.must(QueryBuilders.matchQuery("orgCode", request.getUseorg()));
        	}
        	
    		esDatastream = esIndex + StringUtils.substring(regDateTime, 0, 4) + "." 
                    + StringUtils.substring(regDateTime, 4, 6) + "."
                    + StringUtils.substring(regDateTime, 6, 8); 
        	
    		bq = bq.must(QueryBuilders.rangeQuery("dealTimestamp").gte(gte).lt(lte));
        	
        	searchSourceBuilder.query(bq);
        	log.info("ELKQY1 => [ ELKQY1[{}] ]", searchSourceBuilder.toString());
        	
        	SearchRequest searchRequest = new SearchRequest();
        	GetIndexRequest req = new GetIndexRequest();
        	req.indices(esDatastream);
        	
        	boolean res = highLevelClient.indices().exists(req, RequestOptions.DEFAULT);
        	
        	if(res) {
        		searchRequest.indices(esDatastream) ;

            	searchRequest.source(searchSourceBuilder);
            	log.info("ELKQY2 => [ ELKQY2[{}] ]", searchRequest.toString());
            	
            	mSearchRequest.add(searchRequest);
        	}
    	}
    	
    	ElasticResponse response = new ElasticResponse();
    	response.setTotCnt(0);
    	List<ElasticVO> list = new ArrayList<ElasticVO>();
    	if(mSearchRequest.requests().size() > 0) {
    		MultiSearchResponse searchResponse = highLevelClient.msearch(mSearchRequest, RequestOptions.DEFAULT);
        	
        	for(Item item : searchResponse.getResponses()) {
        		SearchResponse res = item.getResponse();
        		ObjectMapper om = new ObjectMapper();
        		Long totalHits = res.getHits().getTotalHits().value;
        		response.setTotCnt(response.getTotCnt() + totalHits.intValue());
        		
        		Iterator<SearchHit> iter = res.getHits().iterator();

        		while(iter.hasNext()){
        			SearchHit sh = iter.next();
        			Map<String, Object> row = sh.getSourceAsMap();
        			
        			ElasticVO vo = new ElasticVO();
        			
        			vo.setClientId((String)row.get("clientId"));
        			vo.setMessage((String)row.get("message"));
        			vo.setRspMsg((String)row.get("message"));
        			vo.setAppName((String)row.get("appName"));
        			vo.setDealDate((String)row.get("dealDate"));
        			vo.setApiId((String)row.get("apiId"));
        			vo.setHttpStatus((String)row.get("httpStatus"));
        			vo.setHost((String)row.get("clientIp"));
        			vo.setRequestUri((String)row.get("requestUri"));
        			vo.setOrgCode((String)row.get("orgCode"));
        			vo.setApiTranId((String)row.get("xApiTranId"));
        			vo.setTopic((String)row.get("topic"));
        			vo.setIndex(sh.getIndex());
        			vo.setResBody((String)row.get("resBody"));
        			vo.setReqBody((String)row.get("reqBody"));
        			list.add(vo);
        		}
        	}
    	}
    	response.setList(list);
    	
    	return ResponseEntity.ok(response);
    }

    @PostMapping("/getApiTrxCommInfoListWithStat")
    public ResponseEntity<?> getApiTrxCommInfoListWithStat(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiTrxCommInfoDTO.getListWithStatDTO request) {
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());  // 페이징
        return ResponseEntity.ok(logService.getApiTrxCommInfoListWithStat(request));
    }

    @PostMapping("/getApiTrxCommInfo")
    public ResponseEntity<?> getApiTrxCommInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiTrxCommInfoDTO.getDTO request) {
        return ResponseEntity.ok(logService.getApiTrxCommInfo(request));
    }
}
