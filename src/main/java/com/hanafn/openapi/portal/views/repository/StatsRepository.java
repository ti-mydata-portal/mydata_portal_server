package com.hanafn.openapi.portal.views.repository;

import java.util.List;
import java.util.Map;

import com.hanafn.openapi.portal.views.vo.*;
import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.views.dto.DashBoardRequest;
import com.hanafn.openapi.portal.views.dto.StatisticsRequest;

@Mapper
public interface StatsRepository {
	/* 대시 보드 */
	DashBoardVO dashBoardCount(DashBoardRequest dashBoardRequest);
	DashBoardVO dashBoardThreeDayTotal(DashBoardRequest dashBoardRequest);

	List<DashBoardVO> dashBoardThreeDayTopApiUse(DashBoardRequest dashBoardRequest);
	List<DashBoardVO> dashBoardThreeDayTopProcTerm(DashBoardRequest dashBoardRequest);
	List<DashBoardVO> dashBoardThreeDayTopApiError(DashBoardRequest dashBoardRequest);

	List<DashBoardVO> dashBoardThreeDayList(DashBoardRequest dashBoardRequest);

	/* 이용기관 대시 보드 */
	UseorgDashBoardVO useorgDashBoardCountNice(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest);
	UseorgDashBoardVO userDashBoardCountPrdNice(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest);
	List<UseorgDashBoardVO> useorgApiTrxMonthInfo(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest);
	List<UseorgDashBoardVO> useorgApiTrxMonthMod(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest);
	List<UseorgDashBoardVO> usrApiTrxMonthInfoNice(DashBoardRequest.UseorgDashBoardRequest useorgDashBoardRequest);
	List<UseorgDashBoardVO> useorgDashBoardAppApiTrxList(DashBoardRequest.UseorgDashBoardRequest dashBoardRequest);
	List<UseorgDashBoardVO> useorgDashBoardAppApiTrxListNice(DashBoardRequest.UseorgDashBoardRequest dashBoardRequest);
	List<UseorgDashBoardVO> useorgDashBoardPrdApiTrxList(DashBoardRequest.UseorgDashBoardRequest dashBoardRequest);
	List<UseorgDashBoardVO> useorgDashBoardApiTrxList(DashBoardRequest.UseorgDashBoardRequest dashBoardRequest);
	List<UseorgDashBoardVO> useorgDashBoardThreeDayList(DashBoardRequest.UseorgDashBoardRequest dashBoardRequest);
	List<UseorgDashBoardVO> userDashBoardTodayNice(DashBoardRequest.UseorgDashBoardRequest dashBoardRequest);

	/* API 통계 */
	StatsVO apiDayTotal(AdminRequest.StatsRequest apiStatsRequest);
	List<StatsVO> apiTrxDayList(AdminRequest.StatsRequest apiStatsRequest);

	List<StatsVO> apiStatsTopApiUse(AdminRequest.StatsRequest apiStatsRequest);
	List<StatsVO> apiStatsTopProcTerm(AdminRequest.StatsRequest apiStatsRequest);
	List<StatsVO> apiStatsTopApiError(AdminRequest.StatsRequest apiStatsRequest);

	/* 이용관리 통계 */
	List<StatsVO> appTrxList(AdminRequest.StatsRequest apiStatsRequest);
	List<StatsVO> appTrxListNice(AdminRequest.StatsRequest apiStatsRequest);
	List<StatsVO> useorgAppApiDetailStatsList(AdminRequest.AppDetailStatsRequest appDetailStatsRequest);

	// 최종 업데이트 일자
	String getLastUpdateDate();

	// NICE 관리자포털 통계 추가 개발
    StatsVO tobeApiDayTotal(AdminRequest.StatsRequest statsRequest);
    List<StatsVO> tobeApiTrxDayList(AdminRequest.StatsRequest statsRequest);
    List<StatsVO> trxList(AdminRequest.StatsRequest statsRequest);
    List<StatsVO> bizTrxList(AdminRequest.StatsRequest request);
    List<StatsVO> statsByDateDetail(AdminRequest.StatsRequest request);
    List<StatsVO> bizStatsByDateDetail(AdminRequest.StatsRequest request);

    int countTrxList(AdminRequest.StatsRequest request);
    int countBizTrxList(AdminRequest.StatsRequest request);

    // 신규
	// 일자별 API 통계 총 건수
	StatsApiVO statsApiDayTotal(StatisticsRequest request);
	// 일자별 API 그리드 총 건수
	int countStatsTrxList(StatisticsRequest request);
	// 일자별 API 그리드 출력
	List<StatsApiVO> statsTrxList(StatisticsRequest statsRequest);
	// 일자별 API 일별 총 건수
	List<StatsApiVO> statsApiDayList(StatisticsRequest statsRequest);
	// 일자별 API 상세 조회 (수정해야함)
	List<StatsApiVO> statsByDateDetailApi(StatisticsRequest.statisticsDetailRequest request);


	// 일자별 API 통계 총 건수
	StatsApiVO statsUseorgDayTotal(StatisticsRequest request);
	// 일자별 API 그리드 총 건수
	int countStatsUseorgTrxList(StatisticsRequest request);
	// 일자별 API 그리드 출력
	List<StatsApiVO> statsUseorgTrxList(StatisticsRequest statsRequest);
	// 일자별 API 일별 총 건수
	List<StatsApiVO> statsUseorgDayList(StatisticsRequest statsRequest);
	// 일자별 API 상세 조회
	List<StatsApiVO> statsByDateDetailUseorg(StatisticsRequest.statisticsUseorgDetailRequest request);
	// 마이데이터 이용통계 토큰 정보
	List<StatsMydataVO> statsGetTokenData(StatisticsRequest.statisticsMydataRequest request);
	// 마이데이터 이용통계 리스트
//	List<StatsMydataVO> statsGetMydata(StatisticsRequest.statisticsMydataRequest request);
	List<StatsMydataVO> statsGetMydata(StatisticsRequest.statisticsMydataRequest request);
	List<StatsMydataVO> statsByDateDetailMydata(StatisticsRequest.statisticsMydataRequest request);

	int countStatsGetMydata(StatisticsRequest.statisticsMydataRequest request);
	
	Map<String, Object> selectMainStatInfo(StatisticsRequest.statisticsDetailRequest request);
	List<Map<String, Object>> selectSub2StatInfo(StatisticsRequest.statisticsDetailRequest request);
	List<Map<String, Object>> selectSub3StatInfo(StatisticsRequest.statisticsDetailRequest request);
	List<Map<String, Object>> selectSub4StatInfo(StatisticsRequest.statisticsDetailRequest request);
}
