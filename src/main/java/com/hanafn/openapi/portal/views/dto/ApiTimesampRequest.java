package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class ApiTimesampRequest {
	private String apiUrl;
    private String reqTimestamp;
}
