package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.views.vo.ApiConsentInfoVO;

import lombok.Data;

@Data
public class ApiResponse {
    private List<ApiVO> list;
    private List<ApiConsentInfoVO> apiConsentInfoVOS;
}
