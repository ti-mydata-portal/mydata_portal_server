package com.hanafn.openapi.portal.views.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

import org.apache.ibatis.type.Alias;

@Data
@Alias("statMd3")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatMd3VO {

	private String apiType;
	private String tmSlotCnt;
	private List<StatMd4VO> tmSlotList;
}
