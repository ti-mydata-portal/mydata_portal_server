package com.hanafn.openapi.portal.views.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppDetailStatsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.StatsRequest;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.BatchServiceProxy;
import com.hanafn.openapi.portal.util.CommExcelUtil;
import com.hanafn.openapi.portal.views.dto.DashBoardRequest;
import com.hanafn.openapi.portal.views.dto.DashBoardRsponse;
import com.hanafn.openapi.portal.views.dto.StatisticsRequest;
import com.hanafn.openapi.portal.views.dto.StatsApiResponse;
import com.hanafn.openapi.portal.views.dto.StatsRsponse;
import com.hanafn.openapi.portal.views.service.StatsService;
import com.hanafn.openapi.portal.views.vo.StatsApiVO;
import com.hanafn.openapi.portal.views.vo.StatsMydataVO;
import com.hanafn.openapi.portal.views.vo.StatsVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
@RequiredArgsConstructor
public class StatsController {

    private final StatsService statsService;

    @Autowired
    CommExcelUtil excelExtUtil;

    @Autowired
    BatchServiceProxy batchServiceProxy;
    
    @Value("${grafana.url")
    private String grafanaUrl;

    @PostMapping("/dashBoard")
    public ResponseEntity<?> dashBoard(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {
    	
        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
        	request.setRoleNm(ga.getAuthority());
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setHfnCd(currentUser.getHfnCd());
                request.setUserKey(currentUser.getUserKey());
                break;
            }
        }
        DashBoardRsponse data = statsService.dashBoard(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectGrafana")
    public String selectGrafana(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {
    	return grafanaUrl;
    }
    
    @PostMapping("/dashBoardThreeDayTotal")
    public ResponseEntity<?> dashBoardThreeDayTotal(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setHfnCd(currentUser.getHfnCd());
                request.setUserKey(currentUser.getUserKey());
                break;
            }
        }
        DashBoardRsponse data = statsService.dashBoardThreeDayTotal(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/dashBoardApiUseList")
    public ResponseEntity<?> dashBoardApiUseList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setHfnCd(currentUser.getHfnCd());
                request.setUserKey(currentUser.getUserKey());
                break;
            }
        }
        DashBoardRsponse data = statsService.dashBoardApiUseList(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/dashBoardResTmList")
    public ResponseEntity<?> dashBoardResTmList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setHfnCd(currentUser.getHfnCd());
                request.setUserKey(currentUser.getUserKey());
                break;
            }
        }
        DashBoardRsponse data = statsService.dashBoardResTmList(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/dashBoardApiErrorList")
    public ResponseEntity<?> dashBoardApiErrorList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setHfnCd(currentUser.getHfnCd());
                request.setUserKey(currentUser.getUserKey());
                break;
            }
        }
        DashBoardRsponse data = statsService.dashBoardApiErrorList(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/dashBoardThreeDayList")
    public ResponseEntity<?> dashBoardThreeDayList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setHfnCd(currentUser.getHfnCd());
                request.setUserKey(currentUser.getUserKey());
                break;
            }
        }
        DashBoardRsponse data = statsService.dashBoardThreeDayList(request);

        return ResponseEntity.ok(data);
    }

//    TODO NICE XX
//    @PostMapping("/userorgDashBoard")
//    public ResponseEntity<?> userorgDashBoard(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest.UseorgDashBoardRequest request) {
//        request.setEntrCd(currentUser.getEntrCd());
//
//        if ("ORGD".equals(currentUser.getUserType())) {
//            request.setUserKey(request.getEntrCd());
//        } else {
//            request.setUserKey(currentUser.getUserKey());
//        }
//
//        DashBoardRsponse.UseorgDashBoardRsponse data = statsService.useorgDashBoard(request);
//        return ResponseEntity.ok(data);
//    }
    
    @PostMapping("/userDashBoardNice")
    public ResponseEntity<?> userDashBoardNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DashBoardRequest.UseorgDashBoardRequest request) {
        request.setUserKey(currentUser.getUserKey());

        DashBoardRsponse.UseorgDashBoardRsponse data = statsService.userDashBoardNice(request);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/apiStats")
    public ResponseEntity<?> apiStatsAsis(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) {

        StatsRsponse data = statsService.apiStatsAsis(request);

        return ResponseEntity.ok(data);
    }

    /** 관리자포털 통계 - 이용자 사용 통계 >> 현재 사용 중(운영)이나 일별 이용자 통게 반영시 삭제 **/
    @PostMapping("/useorgStats")
    public ResponseEntity<?> useorgStats(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) throws Exception{

        StatsRsponse.UseorgStatsRsponse data = statsService.useorgStats(request);

        return ResponseEntity.ok(data);
    }

    /** 이용자포털 통계 **/
    @PostMapping("/useorgStatsNice")
    public ResponseEntity<?> useorgStatsNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) {

        StatsRsponse.UseorgStatsRsponse data = statsService.useorgStatsNice(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/useorgAppDetailStats")
    public ResponseEntity<?> useorgAppDetailStats(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppDetailStatsRequest request) {

        StatsRsponse.AppApiDetailStatsRsponse data = statsService.useorgAppDetailStats(request);

        return ResponseEntity.ok(data);
    }


    /** 관리자포털 일자별 통계 : API, 이용자, 상품
     * ASIS useorgStat */
    @PostMapping("/statsByDate")
    public ResponseEntity<?> statsByDate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) {

        StatsRsponse data = statsService.statsByDate(request);

        return ResponseEntity.ok(data);
    }

    /** 관리자포털 일자별 통계 상세 */
    @PostMapping("/statsByDateDetail")
    public ResponseEntity<?> statsByDateDetail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) {

        StatsRsponse data = statsService.statsByDateDetail(request);

        return ResponseEntity.ok(data);
    }

    /** 관리자포털 일자별 사업자 통계 */
    @PostMapping("/bizStatsByDate")
    public ResponseEntity<?> bizStatsByDate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) {

        StatsRsponse data = statsService.bizStatsByDate(request);

        return ResponseEntity.ok(data);
    }

    /** 관리자포털 일자별 사업자 통계 상세 */
    @PostMapping("/bizStatsByDateDetail")
    public ResponseEntity<?> bizStatsByDateDetail(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatsRequest request) {

        StatsRsponse data = statsService.bizStatsByDateDetail(request);

        return ResponseEntity.ok(data);
    }

    // 일자별사용통계 엑셀다운로드
    @PostMapping("/statsByDateExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> statsByDateExcelDown(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam String searchType,
            @RequestParam String searchStDt,
            @RequestParam(value = "searchEnDt", required = false) String searchEnDt,
            @RequestParam(value = "searchHfnCd", required = false) String searchHfnCd,
            @RequestParam(value = "searchUserId", required = false) String searchUserId,
            @RequestParam(value = "searchPrdCd", required = false) String searchPrdCd,
            @RequestParam(value = "searchAppKey", required = false) String searchAppKey,
            @RequestParam(value = "searchApiId", required = false) String searchApiId,
            @RequestParam(value = "searchCopRegNo", required = false) String searchCopRegNo
    ) throws Exception {
        StatsRequest request = new StatsRequest();
        request.setSearchType(searchType);
        request.setSearchStDt(searchStDt);
        request.setSearchEnDt(searchEnDt);
        request.setSearchHfnCd(searchHfnCd);
        request.setSearchUserId(searchUserId);
        request.setSearchPrdCd(searchPrdCd);
        request.setSearchAppKey(searchAppKey);
        request.setSearchApiId(searchApiId);
        request.setSearchCopRegNo(searchCopRegNo);

        String[] sheetNm;
        String[] header;
        String[] exportColumn;
        List<StatsVO> vo = null;
        String fileNm = "";

        if("apiByDate".equals(searchType)) {
            sheetNm = new String[]{"일자별 API 사용 통계"};
            header = new String[]{"상품", "API", "URL", "일자", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"prdNm", "ApiNm", "apiUrl", "trxDate", "apiTrxCnt", "procTerm", "apiError"};
            fileNm = searchStDt+"-"+searchEnDt+"_일자별API사용통계";

            StatsRsponse res = statsService.statsByDate(request);
            vo = res.getAppTrxList();
        } else if ("apiTimeTable".equals(searchType)) {
            sheetNm = new String[]{"일자별 API 사용 통계 상세"};
            header = new String[]{"시간", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"tm", "apiTrxCnt", "procTerm", "apiError"};
            fileNm = searchStDt+"_일자별API사용통계상세";

            StatsRsponse res = statsService.statsByDateDetail(request);
            vo = res.getDayList();
        } else if ("prdByDate".equals(searchType)) {
            sheetNm = new String[]{"일자별 상품 사용 통계"};
            header = new String[]{"상품ID", "상품", "일자", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"prdCd", "prdNm", "trxDate", "apiTrxCnt", "procTerm", "apiError"};
            fileNm =  searchStDt+"-"+searchEnDt+"+일자별상품사용통계";

            StatsRsponse res = statsService.statsByDate(request);
            vo = res.getAppTrxList();
        } else if ("prdTimeTable".equals(searchType)) {
            sheetNm = new String[]{"일자별 상품 사용 통계 상세"};
            header = new String[]{"시간", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"tm", "apiTrxCnt", "procTerm", "apiError"};
            fileNm = searchStDt+"_일자별상품사용통계상세";

            StatsRsponse res = statsService.statsByDateDetail(request);
            vo = res.getDayList();
        } else if ("userByDate".equals(searchType)) {
            sheetNm = new String[]{"일자별 이용자 사용 통계"};
            header = new String[]{"이용자", "앱", "상품", "일자", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"userId", "appNm", "prdNm", "trxDate", "apiTrxCnt", "procTerm", "apiError"};
            fileNm =  searchStDt+"-"+searchEnDt+"_일자별이용자사용통계";

            StatsRsponse res = statsService.statsByDate(request);
            vo = res.getAppTrxList();
        } else if ("userTimeTable".equals(searchType)) {
            sheetNm = new String[]{"일자별 이용자 사용 통계 상세"};
            header = new String[]{"시간", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"tm", "apiTrxCnt", "procTerm", "apiError"};
            fileNm = searchStDt+"_일자별이용자사용통계상세";

            StatsRsponse res = statsService.statsByDateDetail(request);
            vo = res.getDayList();
        } else if ("bizByDate".equals(searchType)) {
            sheetNm = new String[]{"일자별 사업자 사용 통계"};
            header = new String[]{"사업자등록번호", "이용자", "앱", "상품", "일자", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"copRegNo", "userId", "appNm", "prdNm", "trxDate", "apiTrxCnt", "procTerm", "apiError"};
            fileNm =  searchStDt+"-"+searchEnDt+"_일자별사업자사용통계";

            StatsRsponse res = statsService.bizStatsByDate(request);
            vo = res.getAppTrxList();

        } else {
            sheetNm = new String[]{"일자별 사업자 사용 통계 상세"};
            header = new String[]{"시간", "건수", "평균응답시간(ms)", "에러율(%)"};
            exportColumn = new String[]{"tm", "apiTrxCnt", "procTerm", "apiError"};
            fileNm = searchStDt+"_일자별사업자사용통계상세";

            StatsRsponse res = statsService.bizStatsByDateDetail(request);
            vo = res.getDayList();
        }

        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);

        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(vo);

        List<String[]> exportColumns = new ArrayList<String[]>();
        exportColumns.add(exportColumn);
        excelExtUtil.excelDown(1, sheetNm, headers, bodys, exportColumns, response, fileNm);

        return ResponseEntity.ok(new SignUpResponse(true, "OK"));
    }
    // 신규
    // 일자별 API 사용통계(총)
    @PostMapping("/statsByDateApi")
    public ResponseEntity<?> statsByDateApi(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest request) throws Exception {
        StatsApiResponse data = statsService.statsByDateApi(request);
        return ResponseEntity.ok(data);
    }
    // 일자별 API 사용통계 상세
    @PostMapping("/statsByDateDetailApi")
    public ResponseEntity<?> statsByDateDetailApi(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest.statisticsDetailRequest request) throws Exception {
        StatsApiResponse data = statsService.statsByDateDetailApi(request);
        return ResponseEntity.ok(data);
    }
    // 일자별 사업자 사용통계(총)
    @PostMapping("/statsByDateUseorg")
    public ResponseEntity<?> statsByDateUseorg(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest request) throws Exception {
        StatsApiResponse data = statsService.statsByDateUseorg(request);
        return ResponseEntity.ok(data);
    }
    // 일자별 사업자 사용통계 상세
    @PostMapping("/statsByDateDetailUseorg")
    public ResponseEntity<?> statsByDateDetailUseorg(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest.statisticsUseorgDetailRequest request) throws Exception {
        StatsApiResponse data = statsService.statsByDateDetailUseorg(request);
        return ResponseEntity.ok(data);
    }
    // 마이데이터 통계 조회/
    @PostMapping("/statsByDateMydata")
    public ResponseEntity<?> statsByDateMydata(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest.statisticsMydataRequest request) throws Exception {
        StatsApiResponse.mydataStatsRsponse data = statsService.statsByDateMydata(request);
        return ResponseEntity.ok(data);
    }
    @PostMapping("/statsByDateDetailMydata")
    public ResponseEntity<?> statsByDateDetailMydata(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest.statisticsMydataRequest request) throws Exception {
        StatsApiResponse.mydataStatsRsponse data = statsService.statsByDateDetailMydata(request);
        return ResponseEntity.ok(data);
    }
    // 일자별사용통계 엑셀다운로드
    @PostMapping("/statsByDateMydataExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> statsByDateMydataExcelDown(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam String searchType,
            @RequestParam String searchStDt,
            @RequestParam(value = "searchEnDt", required = false) String searchEnDt,
            @RequestParam(value = "searchUserKey", required = false) String searchUserKey,
            @RequestParam(value = "searchApiType", required = false) String searchApiType,
            @RequestParam(value = "clientId", required = false) String clientId,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "apiType", required = false) String apiType,
            @RequestParam(value = "searchUseorgNm", required = false) String searchUseorgNm,
            @RequestParam(value = "statDate", required = false) String statDate,
            @RequestParam(value = "userKey", required = false) String userKey
    ) throws Exception {
        String[] sheetNm = null;
        String[] header = null;
        String[] exportColumn = null;
        String fileNm = "";
        List<StatsMydataVO> vo = null;
        List<StatsApiVO> vo2 = null;
        if (searchType.equals("myDataDate")) { // 마이데이터 이용통계
            StatisticsRequest.statisticsMydataRequest request = new StatisticsRequest.statisticsMydataRequest();
            request.setSearchUserKey(searchUserKey);
            request.setSearchStDt(searchStDt);
            request.setSearchEnDt(searchEnDt);
            request.setClientId(clientId);
            request.setSearchApiType(searchApiType);
            sheetNm = new String[]{"마이데이터 이용통계"};
            header = new String[]{"일자", "기관명", "서비스(APP)명", "분류 ", "시간대", "이용건수", "완료건수", "오류건수"};
            exportColumn = new String[]{"statDate", "userNm", "appNm", "apiTypeNm", "tmSlot", "maxApiCnt", "successApiCnt", "failApiCnt"};
            fileNm = searchStDt+"_마이데이터_이용통계";

            StatsApiResponse.mydataStatsRsponse res = statsService.statsByDateMydata(request);
            vo = res.getMydataList();
        } else if (searchType.equals("myDataDateDetail")) {
        	StatisticsRequest.statisticsMydataRequest request = new StatisticsRequest.statisticsMydataRequest();
        	request.setSearchUserKey(searchUserKey);
        	request.setSearchStDt(searchStDt);
        	request.setClientId(clientId);
        	request.setSearchApiType(searchApiType);
        	sheetNm = new String[] {"마이데이터 이용통계 상세"};
        	header = new String[] {"시간", "총건수", "완료건수", "오류건수"};
        	exportColumn = new String[] {"tm", "maxApiCnt", "successApiCnt", "failApiCnt"};
        	fileNm = searchStDt+"_마이데이터_이용통계 상세";
        	
        	StatsApiResponse.mydataStatsRsponse res = statsService.statsByDateDetailMydata(request);
        	vo = res.getMydataList();
        } else if (searchType.equals("apiByDate")) { // 일자별API 사용통계
            StatisticsRequest request = new StatisticsRequest();
            request.setSearchStDt(searchStDt);
            request.setSearchEnDt(searchEnDt);
            request.setSearchApiType(searchApiType);
            sheetNm = new String[]{"일자별 API 사용통계"};
            header = new String[]{"일자", "분류", "이용건수", "오류건수", "완료건수"};
            exportColumn = new String[]{"statDate", "apiTypeNm", "maxApiCnt", "failApiCnt", "successApiCnt"};
            fileNm = searchStDt + "-" + searchEnDt + "_일자별API사용통계";

            StatsApiResponse res = statsService.statsByDateApi(request);
            vo2 = res.getAppTrxList();

        } else if (searchType.equals("apiByDateDetail")) { // 일자별API 사용통계
            StatisticsRequest.statisticsDetailRequest request = new StatisticsRequest.statisticsDetailRequest();
            request.setApiType(apiType);
            request.setStatDate(statDate);
            sheetNm = new String[]{"일자별 API 사용통계 상세조회"};
            header = new String[]{"시간", "총건수", "완료건수", "오류건수"};
            exportColumn = new String[]{"tm", "maxApiCnt", "successApiCnt", "failApiCnt"};
            fileNm = searchStDt + "-" + searchEnDt + "_일자별API사용통계 상세조회";

            StatsApiResponse res = statsService.statsByDateDetailApi(request);
            vo2 = res.getDayList();

        } else if (searchType.equals("useorgByDate")) {
            StatisticsRequest request = new StatisticsRequest();
            request.setSearchStDt(searchStDt);
            request.setSearchEnDt(searchEnDt);
            request.setSearchUserKey(searchUserKey);
            sheetNm = new String[]{"일자별 사업자 사용통계"};
            header = new String[]{"일자", "기관", "시간대", "이용건수", "완료건수", "오류건수"};
            exportColumn = new String[]{"statDate", "userNm", "tmSlot", "maxApiCnt", "successApiCnt", "failApiCnt"};
            fileNm = searchStDt +"-" + searchEnDt +"_일자별 사업자 사용통계";
            StatsApiResponse res = statsService.statsByDateUseorg(request);
            vo2 = res.getAppTrxList();
        }  else if (searchType.equals("useorgByDateDetail")) {
            StatisticsRequest.statisticsUseorgDetailRequest request = new StatisticsRequest.statisticsUseorgDetailRequest();
            request.setUserKey(userKey);
            request.setStatDate(statDate);
            sheetNm = new String[]{"일자별 사업자 사용통계 상세조"};
            header = new String[]{"시간","총건수", "완료건수", "오류건수"};
            exportColumn = new String[]{"tm", "maxApiCnt", "successApiCnt", "failApiCnt"};
            fileNm = searchStDt +"-" + searchEnDt +"_일자별 사업자 사용통계 상세조회";
            StatsApiResponse res = statsService.statsByDateDetailUseorg(request);
            vo2 = res.getDayList();
        }
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);

        List<List<?>> bodys = new ArrayList<List<?>>();
        if (searchType.equals("myDataDate") || searchType.equals("myDataDateDetail")) {
            bodys.add(vo);
        } else {
            bodys.add(vo2);
        }

        List<String[]> exportColumns = new ArrayList<String[]>();
        exportColumns.add(exportColumn);
        excelExtUtil.excelDown(1, sheetNm, headers, bodys, exportColumns, response, fileNm);

        return ResponseEntity.ok(new SignUpResponse(true, "OK"));
    }
    @PostMapping("/batchManual")
    public ResponseEntity<?> batchManual(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody StatisticsRequest.statisticsBatchRequest request) {
    	
        batchServiceProxy.batchManual(request.getRegDate(), request.getTmSlot());
        
        return ResponseEntity.ok(new SignUpResponse(true, "OK"));
    }

}
