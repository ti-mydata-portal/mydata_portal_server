package com.hanafn.openapi.portal.views.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppDefresRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsIssueRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRegRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.AppsRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminResponse.AppsResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.CommonResponse;
import com.hanafn.openapi.portal.cmct.GWCommunicater;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.ExcelUtil;
import com.hanafn.openapi.portal.util.UUIDUtils;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.AppCidScrDlResponse;
import com.hanafn.openapi.portal.views.dto.AppsRsponsePaging;
import com.hanafn.openapi.portal.views.dto.HfnCompanyAllResponse;
import com.hanafn.openapi.portal.views.dto.ServiceAppRequest;
import com.hanafn.openapi.portal.views.dto.ServiceAppResponsePaging;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.dto.UseorgRsponsePaging;
import com.hanafn.openapi.portal.views.dto.UserRequest;
import com.hanafn.openapi.portal.views.repository.AppsRepository;
import com.hanafn.openapi.portal.views.service.AppsService;
import com.hanafn.openapi.portal.views.service.SettingNiceService;
import com.hanafn.openapi.portal.views.vo.ServiceAppVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class AppsController {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    CommonUtil commonUtil;
    @Autowired
    AppsService appsService;
    @Autowired
    AppsRepository appsRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    FileService fileService;
    @Autowired
    ExcelUtil excelUtil;
    @Autowired
    SettingNiceService settingService;
    
    @Autowired
    GWCommunicater gWCommunicater;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;
    
    /******************** 앱 목록 조회 ********************/
    @PostMapping("/appList")
    public ResponseEntity<?> appList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) {
        AppsRsponsePaging data = appsService.selectAppsListPaging(request);
        return ResponseEntity.ok(data);
    }

    /*** 사용중 - 앱 상태 변경 ***/
    @PostMapping("/appUpdateStatCd")
    public ResponseEntity<?> appUpdateStatCd (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) {

        request.setModUserId(currentUser.getUserKey());
        request.setModUser(currentUser.getUsername());
        AppsResponse data = appsService.updateAppStatCd(request);

        return ResponseEntity.ok(data);
    }

    /*** 이용자포털 앱 등록 ***/
    @PostMapping("/appReg")
    public ResponseEntity<?> appReg (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {

        request.setAppKey(UUIDUtils.generateUUID());
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());

        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
            request.setUserKey(currentUser.getUseorgKey());
        }

        appsService.insertAppNice(request);

        return ResponseEntity.ok(new SignUpResponse(true, "App registered successfully"));
    }
    
    /******************** 앱 등록 ********************/
    @PostMapping("/appRegAdmin")
    public ResponseEntity<?> appRegAdmin(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {

        request.setAppKey(UUIDUtils.generateUUID());
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());

        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
            request.setUserKey(currentUser.getUseorgKey());
        }

        appsService.insertAppNiceAdmin(request);

        return ResponseEntity.ok(new SignUpResponse(true, "App registered successfully"));
    }

    /******************** 앱 상세조회 ********************/
    @PostMapping("/appDetail")
    public ResponseEntity<?> appDetail (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) throws Exception{
    	request.setSearchUserKey(currentUser.getUserKey());
        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){

            List<AppsVO>  autoList = appsService.selectAppAuth(request);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021","[E021]"+messageSource.getMessage("E021"));
        	}
        }

        AppsResponse data = appsService.selectAppDetail(request);
        
		if (data.getOwnNm() != null && !"".equals(data.getOwnNm())) {
			String decryptedData = cryptoUtil.decrypt(data.getOwnNm());
			data.setOwnNm(this.name(decryptedData));
			log.debug("복호화 - 대표자명: {}", decryptedData);
		}
		
		if (data.getDirNm() != null && !"".equals(data.getDirNm())) {
			String decryptedData = cryptoUtil.decrypt(data.getDirNm());
			data.setDirNm(this.name(decryptedData));
			log.debug("복호화 - 정산담당자 이름: {}", decryptedData);
		}
		
		if (data.getDirEmail() != null && !"".equals(data.getDirEmail())) {
			String decryptedData = cryptoUtil.decrypt(data.getDirEmail());
			data.setDirEmail(this.email(decryptedData));
			log.debug("복호화 - 정산담당자 이메일: {}", decryptedData);
		}
		
		if (data.getDirTelNo2() != null && !"".equals(data.getDirTelNo2())) {
			String decryptedData = cryptoUtil.decrypt(data.getDirTelNo2());
			data.setDirTelNo2(this.phoneNum(decryptedData));
			log.debug("복호화 - 정산담당자 전화번호: {}", decryptedData);
		}
		
		if (data.getManagerNm() != null && !"".equals(data.getManagerNm())) {
			data.setManagerNm(this.name(data.getManagerNm()));
		}

        return ResponseEntity.ok(data);
    }
    
    private String phoneNum(String str) {
		String replaceString = str;
		
		Matcher matcher = Pattern.compile("^(\\d{3})?(\\d{3,4})?(\\d{4})$").matcher(str);
	
		if(matcher.matches()) {
			replaceString = "";
			
			boolean isHyphen = false;
			if(str.indexOf("-") > -1) {
				isHyphen = true;
			}
			
			for(int i=1;i<=matcher.groupCount();i++) {
				String replaceTarget = matcher.group(i);
				if(i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');
					
					replaceString = replaceString + String.valueOf(c);	
				} else {
					if(replaceTarget != null && !"".equals(replaceTarget)) {
						replaceString = replaceString + replaceTarget;
					}
					
				}
				
				if(isHyphen && i < matcher.groupCount()) {
					replaceString = replaceString + "-";
				}
			}
		}
		
		return replaceString;
	}
	
    private String name(String str) {
		String replaceString = str;
		
		String pattern = "";
		if(str.length() == 2) {
			pattern = "^(.)(.+)$";
		} else {
			pattern = "^(.)(.+)(.)$";
		}
		
		Matcher matcher = Pattern.compile(pattern).matcher(str);
	
		if(matcher.matches()) {
			replaceString = "";
			
			for(int i=1;i<=matcher.groupCount();i++) {
				String replaceTarget = matcher.group(i);
				if(i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');
					
					replaceString = replaceString + String.valueOf(c);
				} else {
					replaceString = replaceString + replaceTarget;
				}
				
			}
		}
		
		return replaceString;
	}
	
	private String email(String str) {
		String replaceString = str;
		
		Matcher matcher = Pattern.compile("^(..)(.*)$").matcher(str);
		
		if(matcher.matches()) {
			replaceString = "";
			
			for(int i=1;i<=matcher.groupCount();i++) {
				String replaceTarget = matcher.group(i);
				if(i == 2) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');
					
					replaceString = replaceString + String.valueOf(c);
				} else {
					replaceString = replaceString + replaceTarget;
				}
			}
			
		}
		
		return replaceString;
	}
    
    /******************** 앱 상세조회 ********************/
    @PostMapping("/appDetailModNice")
    public ResponseEntity<?> appDetailModNice(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) throws Exception {

        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
            request.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
            
            List<AppsVO>  autoList = appsService.selectEditAppAuth(request);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021","[E021]"+messageSource.getMessage("E021"));
        	}
        }

        AppsResponse data = appsService.selectAppDetailModNice(request);
        
 		if (data.getOwnNm() != null && !"".equals(data.getOwnNm())) {
 			String decryptedData = cryptoUtil.decrypt(data.getOwnNm());
 			data.setOwnNm(decryptedData);
 			log.debug("복호화 - 대표자명: {}", decryptedData);
 		}
 		
 		if (data.getDirNm() != null && !"".equals(data.getDirNm())) {
 			String decryptedData = cryptoUtil.decrypt(data.getDirNm());
 			data.setDirNm(decryptedData);
 			log.debug("복호화 - 정산담당자 이름: {}", decryptedData);
 		}
 		
 		if (data.getDirEmail() != null && !"".equals(data.getDirEmail())) {
 			String decryptedData = cryptoUtil.decrypt(data.getDirEmail());
 			data.setDirEmail(decryptedData);
 			log.debug("복호화 - 정산담당자 이메: {}", decryptedData);
 		}
 		
 		if (data.getDirTelNo2() != null && !"".equals(data.getDirTelNo2())) {
 			String decryptedData = cryptoUtil.decrypt(data.getDirTelNo2());
 			data.setDirTelNo2(decryptedData);
 			log.debug("복호화 - 정산담당자 전화번호: {}", decryptedData);
 		}
 		
 		if (data.getManagerNm() != null && !"".equals(data.getManagerNm())) {
 			data.setManagerNm(data.getManagerNm());
 		}

        return ResponseEntity.ok(data);
    }
    
    /******************** 앱 상세조회 ********************/
    @PostMapping("/appDetailModNiceAdmin")
    public ResponseEntity<?> appDetailModNiceAdmin(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) throws Exception {

        AppsResponse data = appsService.selectAppDetailModNiceAdmin(request);
        
 		if (data.getOwnNm() != null && !"".equals(data.getOwnNm())) {
 			String decryptedData = cryptoUtil.decrypt(data.getOwnNm());
 			data.setOwnNm(decryptedData);
 			log.debug("복호화 - 대표자명: {}", decryptedData);
 		}
 		
 		if (data.getDirNm() != null && !"".equals(data.getDirNm())) {
 			String decryptedData = cryptoUtil.decrypt(data.getDirNm());
 			data.setDirNm(decryptedData);
 			log.debug("복호화 - 정산담당자 이름: {}", decryptedData);
 		}
 		
 		if (data.getDirEmail() != null && !"".equals(data.getDirEmail())) {
 			String decryptedData = cryptoUtil.decrypt(data.getDirEmail());
 			data.setDirEmail(decryptedData);
 			log.debug("복호화 - 정산담당자 이메: {}", decryptedData);
 		}
 		
 		if (data.getDirTelNo2() != null && !"".equals(data.getDirTelNo2())) {
 			String decryptedData = cryptoUtil.decrypt(data.getDirTelNo2());
 			data.setDirTelNo2(decryptedData);
 			log.debug("복호화 - 정산담당자 전화번호: {}", decryptedData);
 		}
 		
 		if (data.getManagerNm() != null && !"".equals(data.getManagerNm())) {
 			data.setManagerNm(data.getManagerNm());
 		}

        return ResponseEntity.ok(data);
    }

    // TODO NICE XXX
//    @PostMapping("/appUpdate")
//    public ResponseEntity<?> appUpdate (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {
//
//        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
//            request.setUserKey(currentUser.getUseorgKey()); // 개발자관련
//        }
//
//        AppsRequest appsRequest = new AppsRequest();
//        appsRequest.setAppKey(request.getAppKey());
//
//        AppsVO apps = appsRepository.selectAppDetail(appsRequest);
//
//        if(apps == null) {
//            log.error("업데이트 하려는 앱 정보 null");
//            throw new BusinessException("E073",messageSource.getMessage("E073"));
//        }
//
//        request.setRegDttm(apps.getRegDttm());
//        request.setModUser(currentUser.getUsername());
//        request.setRegUser(apps.getRegUser());
//        request.setRegUserId(apps.getRegUserKey());
//        request.setModUserId(currentUser.getUserKey());
//        appsService.updateAppInfo(request);
//        return ResponseEntity.ok(new SignUpResponse(true, "OPENAPI_PORTAL_APP_INFO Update successfully"));
//    }
    
    @PostMapping("/appUpdateAdmin")
    public ResponseEntity<?> appUpdateAdmin(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) throws Exception{
        request.setModUser(currentUser.getUsername());
        request.setRegUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserKey());
        request.setModUserId(currentUser.getUserKey());
        appsService.appUpdateAdmin(request);

        return ResponseEntity.ok(new SignUpResponse(true, "OK"));
    }
    
    /*** 앱 업데이트 ***/
    @PostMapping("/appTabUpdate1")
    public ResponseEntity<?> appTabUpdate1 (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {
    	
    	 if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
    		 
    		 AppsRequest appsRequest = new AppsRequest();
    		 appsRequest.setAppKey(request.getAppKey());
    		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
             
             List<AppsVO>  autoList = appsService.selectEditAppAuth(appsRequest);
         	
         	if(autoList == null || autoList.size() <= 0) {
         		log.error(messageSource.getMessage("E021"));
                 throw new BusinessException("E021",messageSource.getMessage("E021"));
         	}
         }
    	 
        AppsRequest appsRequest = new AppsRequest();
        appsRequest.setAppKey(request.getAppKey());

        AppsVO apps = adminRepository.selectAppDetail(appsRequest);

        if(apps == null) {
            log.error("업데이트 하려는 앱 정보 null");
            throw new BusinessException("E073",messageSource.getMessage("E073"));
        }

        request.setRegDttm(apps.getRegDttm());
        request.setModUser(currentUser.getUsername());
        request.setRegUser(apps.getRegUser());
        request.setRegUserId(apps.getRegUserKey());
        request.setModUserId(currentUser.getUserKey());
        appsService.appTabUpdate1(request);
        return ResponseEntity.ok(new SignUpResponse(true, "OPENAPI_PORTAL_APP_INFO Update successfully"));
    }
    
    /*** 앱 업데이트 ***/
    @PostMapping("/appTabUpdate2")
    public ResponseEntity<?> appTabUpdate2 (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {
    	
    	if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
   		 
   		 AppsRequest appsRequest = new AppsRequest();
   		 appsRequest.setAppKey(request.getAppKey());
   		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
            
            List<AppsVO> autoList = appsService.selectEditAppAuth(appsRequest);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021",messageSource.getMessage("E021"));
        	}
        }
    	
        AppsRequest appsRequest = new AppsRequest();
        appsRequest.setAppKey(request.getAppKey());

        AppsVO apps = adminRepository.selectAppDetail(appsRequest);

        if(apps == null) {
            log.error("업데이트 하려는 앱 정보 null");
            throw new BusinessException("E073",messageSource.getMessage("E073"));
        }

        request.setRegDttm(apps.getRegDttm());
        request.setModUser(currentUser.getUsername());
        request.setRegUser(apps.getRegUser());
        request.setRegUserId(apps.getRegUserKey());
        request.setModUserId(currentUser.getUserKey());
        appsService.appTabUpdate2(request);
        return ResponseEntity.ok(new SignUpResponse(true, "OPENAPI_PORTAL_APP_INFO Update successfully"));
    }
    
    /*** 앱 업데이트 ***/
    @PostMapping("/appTabUpdate3")
    public ResponseEntity<?> appTabUpdate3 (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) throws Exception{
    	
    	if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
   		 
   		 AppsRequest appsRequest = new AppsRequest();
   		 appsRequest.setAppKey(request.getAppKey());
   		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
            
            List<AppsVO>  autoList = appsService.selectEditAppAuth(appsRequest);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021",messageSource.getMessage("E021"));
        	}
        }
    	
        AppsRequest appsRequest = new AppsRequest();
        appsRequest.setAppKey(request.getAppKey());

        AppsVO apps = adminRepository.selectAppDetail(appsRequest);

        if(apps == null) {
            log.error("업데이트 하려는 앱 정보 null");
            throw new BusinessException("E073",messageSource.getMessage("E073"));
        }

        request.setRegDttm(apps.getRegDttm());
        request.setModUser(currentUser.getUsername());
        request.setRegUser(apps.getRegUser());
        request.setRegUserId(apps.getRegUserKey());
        request.setModUserId(currentUser.getUserKey());
        
		if (request.getOwnNm() != null && !"".equals(request.getOwnNm())) {
			String encryptedOwnNm = cryptoUtil.encrypt(request.getOwnNm());
			request.setOwnNm(encryptedOwnNm);
			log.debug("암호화 - 대표자: {}", encryptedOwnNm);
		}
		
		if (request.getDirNm() != null && !"".equals(request.getDirNm())) {
			String encryptedData = cryptoUtil.encrypt(request.getDirNm());
			request.setDirNm(encryptedData);
			log.debug("암호화 - 정산담당자 : {}", encryptedData);
		}
		
		if (request.getDirTelNo2() != null && !"".equals(request.getDirTelNo2())) {
			String encryptedData = cryptoUtil.encrypt(request.getDirTelNo2());
			request.setDirTelNo2(encryptedData);
			log.debug("암호화 - 정산담당자 전화번호: {}", encryptedData);
		}
		
		if (request.getDirEmail() != null && !"".equals(request.getDirEmail())) {
			String encryptedData = cryptoUtil.encrypt(request.getDirEmail());
			request.setDirEmail(encryptedData);
			log.debug("암호화 - 정산담당자 이메일: {}", encryptedData);
		
		}
        appsService.appTabUpdate3(request, apps);
        return ResponseEntity.ok(new SignUpResponse(true, "OPENAPI_PORTAL_APP_INFO Update successfully"));
    }
    
    /*** 앱 업데이트 ***/
    @PostMapping("/appTabUpdate4")
    public ResponseEntity<?> appTabUpdate4 (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) throws Exception {
    	
    	if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
   		 
   		 AppsRequest appsRequest = new AppsRequest();
   		 appsRequest.setAppKey(request.getAppKey());
   		 appsRequest.setSearchUserKey(currentUser.getUserKey()); // 개발자관련
            
            List<AppsVO>  autoList = appsService.selectEditAppAuth(appsRequest);
        	
        	if(autoList == null || autoList.size() <= 0) {
        		log.error(messageSource.getMessage("E021"));
                throw new BusinessException("E021",messageSource.getMessage("E021"));
        	}
        }
    	
        AppsRequest appsRequest = new AppsRequest();
        appsRequest.setAppKey(request.getAppKey());

        AppsVO apps = adminRepository.selectAppDetail(appsRequest);

        if(apps == null) {
            log.error("업데이트 하려는 앱 정보 null");
            throw new BusinessException("E073",messageSource.getMessage("E073"));
        }

        request.setRegDttm(apps.getRegDttm());
        request.setModUser(currentUser.getUsername());
        request.setRegUser(apps.getRegUser());
        request.setRegUserId(apps.getRegUserKey());
        request.setModUserId(currentUser.getUserKey());
        appsService.appTabUpdate4(request, apps);
        return ResponseEntity.ok(new SignUpResponse(true, "OPENAPI_PORTAL_APP_READ_INFO Update successfully"));
    }
    
    @PostMapping("/appSecretReisu")
    public ResponseEntity<?> appSecretReisu (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {

        if(StringUtils.equals(currentUser.getSiteCd(),"userPortal")){
            request.setUserKey(currentUser.getUseorgKey()); // 개발자관련
        }

        request.setModUser(currentUser.getUsername());
        request.setModUserId(currentUser.getUserKey());
        request.setRegUserId(currentUser.getUserKey());
        AppsResponse data = appsService.appSecretReisu(request);

        return ResponseEntity.ok(data);
    }

    /* 관리자포털 APP 목록 조회 */
    @PostMapping("/appListSelected")
    public ResponseEntity<?> appListSelected (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setSearchHfnCd(currentUser.getHfnCd());
                break;
            }
        }

        AppsRsponsePaging resp = new AppsRsponsePaging();
        resp = appsService.appListSelected(request);

        return ResponseEntity.ok(resp);
    }

    /* NICE APP목록 엑셀 다운로드 */
    @PostMapping("/excelApp")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelDownload(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "searchHfncd", required = false) String searchHfncd,
            @RequestParam(value = "searchUserKey", required = false) String searchUserKey,
            @RequestParam(value = "searchNm", required = false) String searchNm,
            @RequestParam(value = "searchStat", required = false) String searchStat,
            @RequestParam(value = "searchAplvStat", required = false) String searchAplvStat
    ) throws Exception {
        AppsRequest request = new AppsRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setSearchHfnCd(searchHfncd);
        request.setSearchUserKey(searchUserKey);
        request.setSearchNm(searchNm);
        request.setSearchStat(searchStat);
        request.setSearchAplvStat(searchAplvStat);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        excelUtil.excelApp(request, response);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }

    @PostMapping("/hfnCompanyAll")
    public ResponseEntity<?> hfnCompanyAll (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest request) {
        HfnCompanyAllResponse resp = new HfnCompanyAllResponse();
        resp = appsService.hfnCompanyAll(request);

        return ResponseEntity.ok(resp);
    }

    /* 키 다운로드 */
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    @PostMapping("/appCidSecretDownload")
    public ResponseEntity<?> appCidSecretIssue(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) {

        AppsResponse data = appsService.selectAppDetail(request);
        AppsIssueRequest appsIssueRequest = objectMapper.convertValue(data, AppsIssueRequest.class);
        appsIssueRequest.setPubKey(request.getPubKey());

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String clientIp = commonUtil.getIp(httpServletRequest);

        AppCidScrDlResponse appCidScrDlResponse = new AppCidScrDlResponse();
        if(StringUtils.equals(data.getAppScrReisuYn(),"Y")) {
            appCidScrDlResponse = appsService.reissueKeydownload(appsIssueRequest);
        }
        else
            appCidScrDlResponse = appsService.keyDownload(appsIssueRequest);

        appCidScrDlResponse.setReqServerIp(clientIp);
        appCidScrDlResponse.setUserId(currentUser.getUserId());
        appCidScrDlResponse.setUserKey(currentUser.getUserKey());

        return ResponseEntity.ok(appCidScrDlResponse);
    }

    @PostMapping("/appDel")
    public ResponseEntity<?> appDel (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRegRequest request) {

        request.setModUser(currentUser.getUsername());
        request.setRegUserId(currentUser.getUserId());
        request.setRegUser(currentUser.getUsername());
        appsService.deleteAppNice(request);

        return ResponseEntity.ok(new SignUpResponse(true, "APP Delete successfully"));
    }

    /* 승인 대기중 앱 관련 (AppInfoMod) */
    @PostMapping("/appDetailMod")
    public ResponseEntity<?> appDetailMod (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) {
        AppsResponse data = appsService.selectAppDetailMod(request);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/appGuideDownload")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> appGuideDownload(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request, HttpServletResponse response){
        String appGuideFileName = "공개키생성_모듈+가이드문서";

        String fileName = "/openapi/support/aa.zip";
        Resource resource = null;
        String realFileName = "";

        try {
            resource = fileService.loadFileAsResource(fileName);
            realFileName = URLEncoder.encode(resource.getFilename(), "utf-8");
        } catch (IOException e) {
            log.error(messageSource.getMessage("E095"));
            throw new BusinessException("E095",messageSource.getMessage("E095"));
        } catch (Exception e) {
            log.error(messageSource.getMessage("E095"));
            throw new BusinessException("E095",messageSource.getMessage("E095"));
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + realFileName + "\"")
                .body(resource);
    }

    /** 앱 대응답처리 설정 : 대응답 사용여부 확인 **/
    @PostMapping("/appDefresUseYn")
    public ResponseEntity<?> appDefresUseYn(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppDefresRequest request) {
        String appKey = request.getAppKey();
        return ResponseEntity.ok(appsRepository.appUseDefresYn(appKey));
    }

    /** 앱 대응답처리 설정 : 대응답 사용여부 변경 (사용/해제) **/
    @PostMapping("/appDefresChange")
    public ResponseEntity<?> appDefresChange(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppDefresRequest request) {
        appsRepository.appDefresChange(request);
        if(StringUtils.equals(request.getUseDefresYn(), "Y")) { // redis에 true(사용) 설정
            RedisCommunicater.setAppEchoResponse(request.getAppKey(), "true");
        } else { // 대응답 사용 중지
//            RedisCommunicater.setAppEchoResponse(request.getAppKey(), "false");   // redis에 false 설정
            RedisCommunicater.delAppEchoResponse(request.getAppKey());  // redis에서 삭제
        }
        return ResponseEntity.ok(new SignUpResponse(true, "Defined Response Setting Changes successfully"));
    }

    @PostMapping("/selectAppUserList")
    public ResponseEntity<?> selectAppUserList (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AppsRequest request) throws Exception {

        Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
        for(GrantedAuthority ga : authorities) {
            if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
                request.setSearchHfnCd(currentUser.getHfnCd());
                break;
            }
        }

        AppsResponse data = appsService.selectAppUserList(request);
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectOwnerUser")
    public ResponseEntity<?> selectOwnerUser (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UserRequest.searchUserRequest request) throws Exception {
        AppsResponse data = appsService.selectOwnerUser(request);
        return ResponseEntity.ok(data);
    }
    // 서비스(앱) 조회
    @PostMapping("/selectServiceAppList")
    public ResponseEntity<?> selectServiceAppList (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest request) throws Exception {
        ServiceAppResponsePaging data = appsService.selectServiceAppListPaging(request);
        return ResponseEntity.ok(data);
    }
    // 서비스(앱) 기관검색
    @PostMapping("/searchUseorg")
    public ResponseEntity<?> searchUseorg (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody UseorgRequest request) throws Exception {
        UseorgRsponsePaging data = settingService.selectUseOrgListPaging(request);
        return ResponseEntity.ok(data);
    }
    // 서비스(앱) 등록
    @PostMapping("/serviceAppReg")
    public ResponseEntity<?> serviceAppReg(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest.ServiceAppRegistRequest request) throws Exception{
        request.setAppKey(UUIDUtils.generateUUID());
        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
        request.setAppStatCd("OK");
        
        String appScr = request.getAppScr();
        if(appScr != null && !"".equals(appScr)) {
            request.setAppScr(cryptoUtil.encrypt(request.getAppScr()));
        }

        appsService.serviceAppReg(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "App registered successfully"));
    }
    // 서비스(앱) 상태변경
    @PostMapping("/serviceAppStatusUpd")
    public ResponseEntity<?> serviceAppStatusUpd (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest request) throws Exception {
        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
        appsService.serviceAppStatusUpd(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "App registered successfully"));
    }
    // 서비스(앱) 상세조회
    @PostMapping("/serviceAppDetail")
    public ResponseEntity<?> serviceAppDetail (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest request) throws Exception {
        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
        ServiceAppVO data = appsService.serviceAppDetail(request);
        
        String appScr = data.getAppScr();
        if(appScr != null && !"".equals(appScr)) {
        	data.setAppScr(cryptoUtil.encrypt(data.getAppScr()));
        }

        List<HashMap> rData = appsService.selectAppRedirectInfo(request);
        List<HashMap> sData =  appsService.selectAppScheme(request);

        data.setCallbackList(rData);
        data.setSchemeList(sData);
        return ResponseEntity.ok(data);
    }
    // 서비스(앱) 정보변경
    @PostMapping("/serviceAppUpd")
    public ResponseEntity<?> serviceAppUpd (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest.ServiceAppRegistRequest request) throws Exception {
        request.setRegUser(currentUser.getUserKey()); // 등록자는 유저ID
        String appScr = request.getAppScr();
        if(appScr != null && !"".equals(appScr)) {
            request.setAppScr(cryptoUtil.encrypt(request.getAppScr()));
        }
        appsService.serviceAppUpd(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "App updated successfully"));
    }
    // 통계 서비스(앱) 조회
    @PostMapping("/selectServiceAppStatsList")
    public ResponseEntity<?> selectServiceAppStatsList (@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ServiceAppRequest.StatsAppRequest request) throws Exception {
        List<ServiceAppVO> list = appsService.selectServiceAppStatsList(request);
        return ResponseEntity.ok(list);
    }

    @PostMapping("/insertDirectOauthClientDetail")
    public ResponseEntity<?>  insertDirectOauthClientDetail (@CurrentUser UserPrincipal currentUser, @RequestBody ServiceAppRequest.ServiceAppRegistRequest request) throws Exception {
    	gWCommunicater.oauthRegService(request.getAppClientId(), request.getAppScr(), request.getUserKey(), request.getCallback(), request.getSchemes(), request.getUseorgGb());
    	return ResponseEntity.ok("OK");
    }
    
}