package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;

import lombok.Data;

@Data
public class PrdApiResponse {
    private List<ApiVO> apiList;
    private List<ApiColumnVO> apiReqColumnList;
    private List<ApiColumnVO> apiResColumnList;
    
    @Data
    public static class apiTestResponse{
    	private String resultData;
    }
}
