package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class HfnUserRequest {

    private String userKey;
    private String searchHfnCd;
    private String searchRoleCd;
    private String searchNm;
    private String searchStatus;

    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;

    @Data
    public static class HfnLineRequest {
        @NotNull
        private String userKey;
    }

    @Data
    public static class HfnUserDetailRequest{
        @NotNull
        private String userKey;
        private String userId;
        private String userPwd;
    }

    @Data
    public static class HfnAltUserRequest{
        private String hfnCd;
        private String hfnId;
        private String searchText;
    }

    @Data
    public static class HfnUserDupCheckRequest{
        @NotBlank
        private String hfnId;
        private String hfnCd;
    }

    @Data
    public static class HfnUserStatCdChangeRequest{
        @NotNull
        private String userKey;
        private String userStatCd;
        private String regUserName;
        private String regUserId;
    }

    @Data
    public static class HfnUserPwdAndTosUpdateRequest{
        private String userKey;

        @NotBlank
        private String userPwd;
        @NotBlank
        private String portalTosYn;
        @NotBlank
        private String privacyTosYn;

        private String regUserName;
    }

    @Data
    public static class HfnUserTmpPwdUpdateRequest{
        @NotNull
        private String userKey;
        private String userPwd;
        private String tmpPwd;
        private String regUserName;
    }

    @Data
    public static class HfnUserAplvLevelRequest {
        private String userKey;
        private String hfnCd;
    }
}
