package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class TokenPolicyRequest {
    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;

    private String searchHfnCd;
    private String searchUserKey;
    private String searchNm;
    private String searchStat;
    private String gubun;
    private String userKey;
    private String appKey;
    private String clientId;
    private String entrCd;
    private Boolean useSetting;
    private String limitCount;
    private String failCount;
    private String regUser;
    private String regDttm;
    private String modUser;
    private String modDttm;
}
