package com.hanafn.openapi.portal.views.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

import org.apache.ibatis.type.Alias;

@Data
@Alias("statMd2")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatMd2VO {

	private String orgCode;
	private String clientId;
	private String tokenNew;
	private String tokenRenew;
	private String tokenRevoke;
	private String tokenOwn;
	private String apiTypeCnt;
	private List<StatMd3VO> apiTypeList;
}
