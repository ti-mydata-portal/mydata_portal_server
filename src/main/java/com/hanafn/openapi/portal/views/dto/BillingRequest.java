package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class BillingRequest {
    private String searchHfnCd;
    private String searchPrdCtgy;
    private String searchNm;
    private String searchAppNm;
    private String searchUserNm;
    private String searchReceiptYn;
    private String searchStDt;
    private String searchEnDt;

    private String prdCtgy;
    private String appKey;
    private String appNm;
    private String billingStDt;
    private String billingEnDt;
    private String billingDate;
    private String billingAmount;
    private String receiptDate;
    private String receiptAmount;
    private String receiptYn;
    private String unpaidAmount;
    private String memo;
    private String regUser;
    private String regDttm;
    private String modUser;
    private String modDttm;

    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;

    @Data
    public static class PaymentRequest {
        private String searchHfnCd;
        private String searchStDt;
        private String searchEnDt;
        private int pageIdx = 0;
        private int pageSize = 20;
        private int pageOffset = 0;
    }

    @Data
    public static class SettlementRequest {
        private String searchHfnCd;
        private String searchPrdId;
        private String searchAppKey;
        private String searchStDt;
        private String searchEnDt;
        private int pageIdx = 0;
        private int pageSize = 20;
        private int pageOffset = 0;
    }

    @Data
    public static class AppUseInfoRequest {
        private String searchAppKey;
        private String searchUserKey;
        private String searchPrdId;
        private String searchStDt;
        private String searchEnDt;
        private String[] searchPrdKind;
        private int pageIdx = 0;
        private int pageSize = 20;
        private int pageOffset = 0;
        private int pageIdxTot = 0;
        private int pageSizeTot = 10;
        private int pageOffsetTot = 0;
        private int pageIdxUse = 0;
        private int pageSizeUse = 10;
        private int pageOffsetUse = 0;
    }
}
