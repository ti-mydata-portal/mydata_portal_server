package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.UserVO;

import lombok.Data;

@Data
public class UserRsponse {

    @Data
    public static class UserDupCheckResponse{
        private String userIdDupYn;
        private String hfnIdDupYn;
        private String userEmailDupYn;
    }

    @Data
    public static class UserTmpPwdIssueResponse{
        private String tepPwd;
    }

    @Data
    public static class DeveloperListResponse{
        private List<UserVO> devList;
    }

}
