package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiTagVO;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;
import com.hanafn.openapi.portal.views.vo.ApiStatModHisVO;

import lombok.Data;

@Data
public class ApiDetailRsponse {
    private String apiId;
    private String apiNm;
    private String apiStatCd;
    private String ctgrCd;
    private String ctgrNm;
    private String apiSvc;
    private String apiVer;
    private String apiUri;
    private String apiUrl;
    private String apiMthd;
    private String apiCtnt;
    private String apiHtml;
    private String apiPubYn;
    private String userKey;
    private String dlyTermDiv;
    private String dlyTermDt;
    private String dlyTermTm;
    private String gwType;
    private String msgType;
    private String encodingType;
    private String apiProcType;
    private String apiAuthType;
    private String apiContentProvider;
    private String cnsntIngrpNm;
    private String cnsntIngrpCd;
    private String cnsntIntypCd;
    private String hfnCd;
    private String hfnSvcCd;
    private String apiProcUrl;
    private String apiTosUrl;
    private String subCtgrCd;
    private String baseRateYn;
    private String apiRscdYn;
    private String apiRsKey;
    private String apiRsValue;
    private String parentId;
    private String regDttm;
    private String regUser;
    private String procDttm;
    private String procUser;
    private String regUserName;
    private String httpPrtcl;
    private String httpMethod;
    private String httpUrl;
    private String httpCtOut;
    private String httpRtOut;
    private String tcpIp;
    private String tcpType;
    private String tcpPort;
    private String tcpSvc;
    private String tcpCtOut;
    private String tcpRtOut;
    private String tcpEnc;
    private String scope;
    private String apiType;
    private String apiIndustry;
    private String apiBtId;
    private String apiTransCycle;
    private String contentType;
    private String apiVerYn;
    private String checkSchema;

    private List<ApiTagVO> apiTagList;
    private List<ApiColumnVO> apiRequestList;
    private List<ApiColumnVO> apiResponseList;
    private List<ApiStatModHisVO> apiStatModHisList;
}
