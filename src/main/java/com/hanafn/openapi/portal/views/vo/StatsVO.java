package com.hanafn.openapi.portal.views.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("stats")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatsVO {

	private String day;
	private String tm;
	private String hfnCd;
	private String apiId;
	private String apiNm;
	private String apiUrl;
	private String appKey;
	private String appNm;
	private String ctgrNm;
	private String userId;
	private String userNm;

	private String prdCd;
	private String prdNm;
	private String copRegNo;

	private String avgProcTerm;		// 응답시간 평균


	private String gwError;			// G/W 에러율


	// 신규
    // 분류
	private String apiType;
	// 분류 한글명
	private String kApiType;
	// 기관코드
	private String userKey;
	// 기관명
	private String useorgNm;
	// 일자
	private String trxDate;
	// API 총 이용건수
	private String apiTrxCnt;
	// 오류건수
	private String errApiTrxCnt;
	// 완료건수
	private String okApiTrxCnt;
	// 최소응답시간
	private String minProcTerm;
	// 최대응답시간
	private String maxProcTerm;
	// 평균응답시간
	private String procTerm;
	// api 에러율
	private String apiError;
	// 통계코드
	private String tmSlot;
}
