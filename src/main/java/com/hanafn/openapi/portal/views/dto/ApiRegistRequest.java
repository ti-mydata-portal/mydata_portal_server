package com.hanafn.openapi.portal.views.dto;


import lombok.Data;

import java.util.List;

@Data
public class ApiRegistRequest {
    private String apiId;
    private String apiNm;
    private String apiStatCd;
    private String ctgrCd;
    private String apiVer;
    private String apiVerYn;
    private String apiUri;
    private String apiUrl;
    private String apiMthd;
    private String apiCtnt;
    private String msgType;
    private String encodingType;
    private String apiProcType;
    private String dlyTermDiv;
    private String dlyTermDt;
    private String dlyTermTm;
    private String regUserId;
    private String regUserName;
    private String httpPrtcl;
    private String httpMethod;
    private String httpUrl;
    private String httpCtOut;
    private String httpRtOut;
    private String tcpIp;
    private String tcpType;
    private String tcpPort;
    private String tcpSvc;
    private String tcpCtOut;
    private String tcpRtOut;
    private String tcpEnc;
    private String mapper;
    private String apiIndustry;
    private String scope;
    private String apiType;
    private String seqNo;
    private String apiBtId;
    private String gwType;
    private String apiTransCycle;
    private String apiAuthType;
    private String contentType;
    private String checkSchema;
    
    //미사용
    private String apiSvc;
    private String apiHtml;
    private String apiPubYn;
    private String userKey;
    private String apiContentProvider;
    private String cnsntIngrpNm;
    private String cnsntIngrpCd;
    private String cnsntIntypCd;
    private String apiTosUrl;
    private String subCtgrCd;
    private String hfnCd;
    private String hfnSvcCd;
    private String baseRateYn;
    private String apiRscdYn;
    private String apiRsKey;
    private String apiRsValue;
    private String apiProcUrl;
    private String parentId;

    private List<ApiTagRequest> apiTagList;
    private List<ApiColumnRequest> apiRequestList;
    private List<ApiColumnRequest> apiResponseList;
}
