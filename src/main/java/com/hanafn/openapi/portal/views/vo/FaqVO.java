package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("faq")
public class FaqVO {
    private int seqNo;
    private int idx;
    private String faqGb;
    private String subject;
    private String ctnt;
    private String statcd;
    private String modUser;
    private String regUser;
    private String regId;
    private String regDttm;
    private String modId;
    private String modDttm;
    private String faqGbNm;
}
