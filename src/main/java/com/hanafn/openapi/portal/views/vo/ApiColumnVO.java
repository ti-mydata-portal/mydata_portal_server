package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;

@Data
@Alias("apiColumn")
public class ApiColumnVO {
    private String apiId;
    private String clmCd;
	private String clmNm;
	private String clmReqDiv;
	private Long clmOrd;
	private String clmType;
    private String clmCtnt;
    private String clmDefRes;
    private String clmValue;
    private String clmLength;
    private String required;
    private String masking;
    private String logVisible;
    private String sqlInjection;
    private String nonidYn;
    private String hideYn;
	private String testValue;
	private String depth;
	private String upClmCd;
	private List<ApiColumnListVO> apiColumnList;
}
