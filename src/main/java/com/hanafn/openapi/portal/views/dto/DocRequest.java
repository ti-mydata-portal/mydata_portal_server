package com.hanafn.openapi.portal.views.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class DocRequest {
    private String searchDoc;
    private String docCd;
    private String seqNo;
    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;
    
    @Data
    public static class DocDetailRequest{
        @NotBlank
        private String seqNo;
        private String modId;
        private String modDttm;
    }
    
    @Data
    public static class DocInsertRequest{
        private int seqNo;
        private String docNm;
        private String docCd;
        private String docPath;
        private String regId;
        private String regDttm;
    }
    
    @Data
    public static class DocUpdateRequest{
        private int seqNo;
        private String docNm;
        private String docCd;
        private String docPath;
        private String modId;
        private String modDttm;
    }
    
    @Data
    public static class DocUserSubmitData{
    	private String ctgyCd;
    	private String appKey;
    }

}
