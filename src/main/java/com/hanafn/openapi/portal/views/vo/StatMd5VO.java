package com.hanafn.openapi.portal.views.vo;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@Alias("statMd5")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatMd5VO {

	private String statusCode;
	private String apiCallCnt;
}
