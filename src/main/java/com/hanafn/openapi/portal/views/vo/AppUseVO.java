package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("appUse")
public class AppUseVO {

    private String appNm;
    private String prdNm;
    private int reqCnt;
    private int resCnt;
    private int payAmt;
    
    private String prdId;
    private String appKey;
}
