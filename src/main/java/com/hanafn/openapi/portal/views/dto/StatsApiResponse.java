package com.hanafn.openapi.portal.views.dto;

import com.hanafn.openapi.portal.views.vo.StatsApiVO;
import com.hanafn.openapi.portal.views.vo.StatsMydataVO;
import com.hanafn.openapi.portal.views.vo.StatsRealTimeVO;
import com.hanafn.openapi.portal.views.vo.StatsVO;
import lombok.Data;

import java.util.List;

@Data
public class StatsApiResponse {
    private int totCnt;
    private int selCnt;
    private int pageIdx;
    private int pageSize;
    private String maxApiCnt; // API 총건수
    private String failApiCnt; // 오류건수
    private String successApiCnt; // 완료건수
    private String apiError; //API 에러율

    List<StatsApiVO> dayList;
    List<StatsApiVO> appTrxList;
    List<StatsApiVO> topApiUseList;
    List<StatsApiVO> topProcTermList;
    List<StatsApiVO> topApiError;
    @Data
    public static class mydataStatsRsponse {
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
        private String tokenNew;
        private String tokenRenew;
        private String tokenRevoke;
        private String tokenOwn;
        List<StatsMydataVO> mydataList;
        List<StatsMydataVO> tokenData;
    }
    @Data
    public static class UseorgStatsRsponse {

        private String apiTrxCnt;			//API 총 건수
        private String avgProcTerm;			//응답시간 평균
        private String maxProcTerm;			//최고 응답시간
        private String gwError;				//G/W 에러율
        private String apiError;			//API 에러율
        private String lastUpdateDate;

        List<StatsVO> dayList;
        List<StatsVO> appTrxList;
    }

    @Data
    public static class UserStatsRsponse {

        private String apiTrxCnt;			//API 총 건수
        private String avgProcTerm;			//응답시간 평균
        private String maxProcTerm;			//최고 응답시간
        private String gwError;				//G/W 에러율
        private String apiError;			//API 에러율
        private String lastUpdateDate;

        List<StatsVO> dayList;
        List<StatsVO> appTrxList;
    }

    @Data
    public static class RealTimeStatsResponse {
        List<StatsRealTimeVO> dayList;
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }

    @Data
    public static class AppApiDetailStatsRsponse {

        private String hfnCd;
        private String userNm;
        private String useorgNm;
        private String appKey;
        private String appNm;
        private int appApiCnt;
        List<StatsVO> apiStatsList;
    }
}
