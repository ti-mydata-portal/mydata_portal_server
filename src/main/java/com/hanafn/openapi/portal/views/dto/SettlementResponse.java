package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.AppPrdUseVO;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;

import lombok.Data;

@Data
public class SettlementResponse {
    private int totCnt;
    private int selCnt;
    private int pageIdx;
    private int pageSize;

	List<AppUseVO> appUseList;
	List<AppPrdUseVO> appPrdUseList;
	
	private int totCntTot;
    private int selCntTot;
    private int pageIdxTot;
    private int pageSizeTot;
    private int totCntUse;
    private int selCntUse;
    private int pageIdxUse;
    private int pageSizeUse;
	
	@Data
	public static class PayInfoResponse {
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
		private List<PayInfoVO> payInfoList;
	}
}
