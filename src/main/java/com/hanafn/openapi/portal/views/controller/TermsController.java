package com.hanafn.openapi.portal.views.controller;

import javax.validation.Valid;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.views.dto.TermsRequest;
import com.hanafn.openapi.portal.views.dto.TermsResponse;
import com.hanafn.openapi.portal.views.repository.TermsRepository;
import com.hanafn.openapi.portal.views.service.TermsService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/terms")
@Slf4j
@RequiredArgsConstructor
public class TermsController {

    private final TermsService termsService;
    private final TermsRepository termsRepository;
    private final MessageSourceAccessor messageSource;

    @PostMapping("/selectTermsList")
    public ResponseEntity<?> selectTermsList(@Valid @RequestBody TermsRequest termsRequest, @CurrentUser UserPrincipal currentUser) {
        TermsResponse.TermsListResponse data = termsService.selectTermsList(termsRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectTermsDetail")
    public ResponseEntity<?> selectTermsDetail(@Valid @RequestBody TermsRequest.TermsDetailRequest termsRequest, @CurrentUser UserPrincipal currentUser) {
        TermsResponse data = termsService.selectTerms(termsRequest);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectTermsUser")
    public ResponseEntity<?> selectTermsUser(@Valid @RequestBody TermsRequest.TermsUserRequest termsRequest, @CurrentUser UserPrincipal currentUser) {
        TermsResponse data = termsService.selectTermsUser(termsRequest);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/insertTerms")
    public ResponseEntity<?> insertTerms(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TermsRequest.TermsInsertRequest termsRequest) {

        try {
            termsRequest.setRegId(currentUser.getUserKey());
            termsRepository.insertTerms(termsRequest);

            return ResponseEntity.ok(new SignUpResponse(true, "Terms Regist successfully"));
        } catch (BusinessException e) {
            return ResponseEntity.ok(new SignUpResponse(false, "Terms Regist Fail"));
        }catch(Exception e){
            return ResponseEntity.ok(new SignUpResponse(false, "Terms Regist Fail"));
        }
    }

    @PostMapping("/deleteTerms")
    public ResponseEntity<?> deleteTerms(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TermsRequest.TermsDetailRequest termsRequest) {

        try {
            termsRepository.deleteTerms(termsRequest);
            return ResponseEntity.ok(new SignUpResponse(true, "Terms Delete successfully"));
        } catch(BusinessException e){
            return ResponseEntity.ok(new SignUpResponse(false, "Terms Delete Fail"));
        }catch(Exception e){
            return ResponseEntity.ok(new SignUpResponse(false, "Terms Delete Fail"));
        }
    }

    @PostMapping("/updateTerms")
    public ResponseEntity<?> updateTerms(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody TermsRequest.TermsUpdateRequest termsRequest) {

        try {
        	termsRequest.setModId(currentUser.getUserKey());
        	termsRepository.updateTerms(termsRequest);
            return ResponseEntity.ok(new SignUpResponse(true, "Terms Update successfully"));
        }catch(BusinessException e){
            return ResponseEntity.ok(new SignUpResponse(false, "Terms Update Fail"));
        }catch(Exception e){
            return ResponseEntity.ok(new SignUpResponse(false, "Terms Update Fail"));
        }
    }
}
