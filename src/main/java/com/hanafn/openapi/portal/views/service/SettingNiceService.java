package com.hanafn.openapi.portal.views.service;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.views.dto.*;
import com.hanafn.openapi.portal.views.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnUserPwdUpdateRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnUserRegistRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.HfnUserUpdateRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.QnaRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.SndCertMgntRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserDetailRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserDupCheckRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserLoginLockCheckRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserPwdUpdateRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.RoleEnum;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.CertMailVO;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.QnaVO;
import com.hanafn.openapi.portal.admin.views.vo.UserPwHisVO;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.repository.SignupRepository;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.InnerTransUtil;
import com.hanafn.openapi.portal.util.MailUtils;
import com.hanafn.openapi.portal.util.UUIDUtils;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.repository.SettingRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class SettingNiceService {

	@Autowired
	CommonUtil commonUtil;
	@Autowired
    SettingRepository settingRepository;
	@Autowired
	AdminRepository adminRepository;
	
	@Autowired
	MessageSourceAccessor messageSource;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	SignupRepository signupRepository;
	@Autowired
	public MailUtils sendMail;

	private static int aplvSvcAddYear;

	@Value("${authUrl}")
	private String authUrl;

	@Value("${spring.profiles.active}")
	private String thisServer;

	@Value("${aplv.svc.add.year}")
	public void setAplvSvcAddYear (int aplvSvcAddYear) {
		this.aplvSvcAddYear = aplvSvcAddYear;
	}
	
	@Autowired
	@Qualifier("Crypto")
	ISecurity cryptoUtil;
	
	@Autowired
    public InnerTransUtil innerTransUtil;
    
    @Value("${bt.tran.org.url}")
	private String btTranOrgUrl;

	public boolean isBrnId (String str) {

		int hap = 0;
		int temp = 0;

		int check[] = {1,3,7,1,3,7,1,3,5};

		if(str.length() != 10) return false;

		for(int i=0; i < 9; i++){
			if(str.charAt(i) < '0' || str.charAt(i) > '9') 	return false;
			hap = hap + (Character.getNumericValue(str.charAt(i)) * check[temp]);
			temp++;
		}

		hap += (Character.getNumericValue(str.charAt(8))*5)/10;

		if ((10 - (hap%10))%10 == Character.getNumericValue(str.charAt(9))) {
			return true;
		} else {
			return false;
		}
	}

//	TODO NICE XX
//	public UseorgRsponse selectUseorgAllListByHfn(UseorgRequest useorgRequest) {
//
//		List<UseorgVO> list = settingRepository.selectUseorgAllListByHfn(useorgRequest);
//
//		UseorgRsponse data = new UseorgRsponse();
//		data.setList(list);
//
//		return data;
//	}

	/*
	 * ******************************사용자******************************
	 * */

	public UserVO selectUser(UserDetailRequest userDetailRequest){
		//UserVO userVO = settingRepository.selectUser(userDetailRequest);
        UserVO userVO = settingRepository.adminSelectUser(userDetailRequest);

		try {
			if (userVO.getUserNm() != null && !"".equals(userVO.getUserNm())) {
				String decryptedUserNm = cryptoUtil.decrypt(userVO.getUserNm());
				userVO.setUserNm(decryptedUserNm);
				log.debug("복호화 - 이름: {}", decryptedUserNm);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		try {
			if (userVO.getUserTel() != null && !"".equals(userVO.getUserTel())) {
				String decryptedUserTel = cryptoUtil.decrypt(userVO.getUserTel());
				userVO.setUserTel(decryptedUserTel);
				log.debug("복호화 - 전화번호: {}", decryptedUserTel);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		try {
			if (userVO.getUserEmail() != null && !"".equals(userVO.getUserEmail())) {
				String decryptedUserEmail = cryptoUtil.decrypt(userVO.getUserEmail());
				userVO.setUserEmail(decryptedUserEmail);
				log.debug("복호화 - 이메일: {}", decryptedUserEmail);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

        try {
            if (userVO.getUserBirthDt() != null && !"".equals(userVO.getUserBirthDt())) {
                String decryptedUserBday = cryptoUtil.decrypt(userVO.getUserBirthDt());
                userVO.setUserBirthDt(decryptedUserBday);
                log.debug("복호화 - 생년월일: {}", decryptedUserBday);
            }
        } catch ( UnsupportedEncodingException e) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch ( Exception e ) {
            log.error(e.getMessage());
            throw new BusinessException("E026",messageSource.getMessage("E026"));
        }

		if(userVO != null) {
			if(StringUtils.equals(userVO.getRoleCd(),"5")) {
				userVO.setUserCompany(userVO.getUseorgNm());
			}
		}
		return userVO;
	}

	public UserRsponsePaging selectUserListPaging(UserRequest userRequest) throws Exception {
		if(userRequest.getPageIdx() == 0)
			userRequest.setPageIdx(userRequest.getPageIdx() + 1);

		if(userRequest.getPageSize() == 0){
			userRequest.setPageSize(20);
		}

		userRequest.setPageOffset((userRequest.getPageIdx()-1)*userRequest.getPageSize());

		if (StringUtils.isNotEmpty(userRequest.getEncSearchNm())) {
			try {
				userRequest.setEncSearchNm(cryptoUtil.encrypt(userRequest.getEncSearchNm()));
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}
		}

		if(StringUtils.isNotEmpty(userRequest.getEncSearchTel())) {
            try {
                userRequest.setEncSearchTel(cryptoUtil.encrypt(userRequest.getEncSearchTel()));
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            }
        }

        if(StringUtils.isNotEmpty(userRequest.getEncSearchEmail())) {
            try {
                userRequest.setEncSearchEmail(cryptoUtil.encrypt(userRequest.getEncSearchEmail()));
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            }
        }

		int totCnt = settingRepository.countUserList(userRequest);
		List<UserVO> list = settingRepository.adminSelectUserList(userRequest);

		for (UserVO userVO : list) {
			try {
				if (userVO.getUserNm() != null && !"".equals(userVO.getUserNm())) {
					String decryptedUserNm = cryptoUtil.decrypt(userVO.getUserNm());
					userVO.setUserNm(decryptedUserNm);
					log.debug("복호화 - 이름: {}", decryptedUserNm);
				}
			} catch ( UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}

			try {
				if (userVO.getUserTel() != null && !"".equals(userVO.getUserTel())) {
					String decryptedUserTel = cryptoUtil.decrypt(userVO.getUserTel());
					userVO.setUserTel(decryptedUserTel);
					log.debug("복호화 - 전화번호: {}", decryptedUserTel);
				}
			} catch ( UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}

			try {
				if (userVO.getUserEmail() != null && !"".equals(userVO.getUserEmail())) {
					String decryptedUserEmail = cryptoUtil.decrypt(userVO.getUserEmail());
					userVO.setUserEmail(decryptedUserEmail);
					log.debug("복호화 - 이메일: {}", decryptedUserEmail);
				}
			} catch ( UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}
		}

		UserRsponsePaging pagingData = new UserRsponsePaging();
		pagingData.setPageIdx(userRequest.getPageIdx());
		pagingData.setPageSize(userRequest.getPageSize());
		pagingData.setList(list);
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	public void updateUser(UserRequest.UserUpdateRequest userUpdateRequest){
		try {
			if (userUpdateRequest.getUserNm() != null && !"".equals(userUpdateRequest.getUserNm())) {
				String encryptedUserNm = cryptoUtil.encrypt(userUpdateRequest.getUserNm());
				userUpdateRequest.setUserNm(encryptedUserNm);
				log.debug("암호화 - 이름: {}", encryptedUserNm);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		try {
			if (userUpdateRequest.getUserTel() != null && !"".equals(userUpdateRequest.getUserTel())) {
				String encryptedUserTel = cryptoUtil.encrypt(userUpdateRequest.getUserTel());
				userUpdateRequest.setUserTel(encryptedUserTel);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		try {
			if (userUpdateRequest.getUserEmail() != null && !"".equals(userUpdateRequest.getUserEmail())) {
				String encryptedUserEmail = cryptoUtil.encrypt(userUpdateRequest.getUserEmail());
				userUpdateRequest.setUserEmail(encryptedUserEmail);
				log.debug("암호화 - 이메일: {}", encryptedUserEmail);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		settingRepository.updateUser(userUpdateRequest);

		// USER_ROLE_INFO 테이블 UPDATE
		HfnUserUpdateRequest hfnUserUpdateRequest = new HfnUserUpdateRequest();
		hfnUserUpdateRequest.setRoleCd(userUpdateRequest.getRoleCd());
		hfnUserUpdateRequest.setUserKey(userUpdateRequest.getUserKey());
		//settingRepository.updateHfnUserRole(hfnUserUpdateRequest);

		// USER_LOGIN 테이블도 UPDATE
		UserLoginRequest.UpdateTypeRequest updateTypeRequest = new UserLoginRequest.UpdateTypeRequest();
		updateTypeRequest.setUserKey(userUpdateRequest.getUserKey());
		String requestRoleCd = userUpdateRequest.getRoleCd();
		String userType = Optional.ofNullable(commonUtil.getUserTypeByRoleCd(requestRoleCd)).orElse("");
		updateTypeRequest.setUserType(userType);
		settingRepository.updateUserLoginType(updateTypeRequest);

		// 본인인증도 업데이트 했을 시 (운영서버 반영)
		if(StringUtils.equals(userUpdateRequest.getUserGb(), "K") && StringUtils.isNotBlank(userUpdateRequest.getUserDi())){
			try {
				SndCertMgntRequest sndCertMgntRequest = new SndCertMgntRequest();
				sndCertMgntRequest.setSendCd("S1");        // 본인인증 코드
				sndCertMgntRequest.setSendNo(userUpdateRequest.getUserDi());
				sndCertMgntRequest.setResultCd(userUpdateRequest.getUserResSeq());
				sndCertMgntRequest.setUserKey(userUpdateRequest.getUserKey());
				signupRepository.insertSndCertMgntForSelfAuth(sndCertMgntRequest);
			} catch (BusinessException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch (Exception e){
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}
		}
	}

	public UserRsponse.UserDupCheckResponse userEmailDupCheck(UserDupCheckRequest userDupCheckRequest){
		try {
			if (userDupCheckRequest.getUserEmail() != null && !"".equals(userDupCheckRequest.getUserEmail())) {
				String encryptedUseorgUserEmail = cryptoUtil.encrypt(userDupCheckRequest.getUserEmail());
				userDupCheckRequest.setUserEmail(encryptedUseorgUserEmail);
				log.debug("암호화 - 이메일: {}", encryptedUseorgUserEmail);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		int userEmailDupCheck = adminRepository.userEmailDupCheck(userDupCheckRequest);

		String userEmailDupYn = "N";

		if(userEmailDupCheck > 0){
			userEmailDupYn = "Y";
		}

		UserRsponse.UserDupCheckResponse data = new UserRsponse.UserDupCheckResponse();
		data.setUserEmailDupYn(userEmailDupYn);

		return data;
	}

	public UserRsponse.UserDupCheckResponse userEmailDupCheckUpdate(UserDupCheckRequest userDupCheckRequest){
        try {
			if (userDupCheckRequest.getUserEmail() != null && !"".equals(userDupCheckRequest.getUserEmail())) {
				String encryptedUserEmail = cryptoUtil.encrypt(userDupCheckRequest.getUserEmail());
				userDupCheckRequest.setUserEmail(encryptedUserEmail);
				log.debug("암호화 - 이메일: {}", encryptedUserEmail);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
        } catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
        }

		int userEmailDupCheck = adminRepository.userEmailDupCheckUpdate(userDupCheckRequest);

		String userEmailDupYn = "N";

		if(userEmailDupCheck > 0){
			userEmailDupYn = "Y";
		}

		UserRsponse.UserDupCheckResponse data = new UserRsponse.UserDupCheckResponse();
		data.setUserEmailDupYn(userEmailDupYn);

		return data;
	}

	public void updateUserStatCdChange(UserRequest.UserStatCdChangeRequest userStatCdChangeRequest){
		UserDetailRequest userDetailRequest = new UserDetailRequest();
		userDetailRequest.setUserKey(userStatCdChangeRequest.getUserKey());

		UserVO userInfo = settingRepository.selectUser(userDetailRequest);

		try {
			if (userInfo.getUserNm() != null && !"".equals(userInfo.getUserNm())) {
				String decryptedUseorgUserNm = cryptoUtil.decrypt(userInfo.getUserNm());
				userInfo.setUserNm(decryptedUseorgUserNm);
				log.debug("복호화 - 이름: {}", decryptedUseorgUserNm);
			}
		} catch ( UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		}

		if(StringUtils.equals(userInfo.getUserStatCd(), "WAIT")){
			throw new BusinessException("E014",messageSource.getMessage("E014"));
		}

		if(StringUtils.equals(userInfo.getUserStatCd(), "OK")){
			userStatCdChangeRequest.setUserStatCd("CLOSE");
		}else if(StringUtils.equals(userInfo.getUserStatCd(), "CLOSE")){
			userStatCdChangeRequest.setUserStatCd("OK");
		}

		settingRepository.updateUserStatCdChange(userStatCdChangeRequest);
	}
	public void userPwdUpdate(UserPwdUpdateRequest userPwdUpdateRequest){
		passwordCheck(userPwdUpdateRequest.getUserPwd());

		userPwdUpdateRequest.setUserPwd(passwordEncoder.encode(userPwdUpdateRequest.getUserPwd()));
		settingRepository.userPwdUpdate(userPwdUpdateRequest);
	}

	public void userPwdAndTosUpdate(UserRequest.UserPwdAndTosUpdateRequest userPwdAndTosUpdateRequest){
		passwordCheck(userPwdAndTosUpdateRequest.getUserPwd());

		if(StringUtils.equals(userPwdAndTosUpdateRequest.getPortalTosYn(), "N")){
			throw new BusinessException("E015",messageSource.getMessage("E015"));
		}

		if(StringUtils.equals(userPwdAndTosUpdateRequest.getPrivacyTosYn(), "N")){
			throw new BusinessException("E015",messageSource.getMessage("E015"));
		}

		userPwdAndTosUpdateRequest.setUserPwd(passwordEncoder.encode(userPwdAndTosUpdateRequest.getUserPwd()));
		settingRepository.userPwdAndTosUpdate(userPwdAndTosUpdateRequest);
	}

	public UserRsponse.UserTmpPwdIssueResponse tmpPwdIssue(UserRequest.UserTmpPwdUpdateRequest userTmpPwdUpdateRequest){
		String tmpPwd = UUIDUtils.generateLoginUUID();
		tmpPwd = StringUtils.substring(StringUtils.upperCase(tmpPwd),0,10);

		userTmpPwdUpdateRequest.setUserPwd(passwordEncoder.encode(tmpPwd));
		userTmpPwdUpdateRequest.setTmpPwd(passwordEncoder.encode(tmpPwd));
		settingRepository.userTepPwdUpdate(userTmpPwdUpdateRequest);

		UserRsponse.UserTmpPwdIssueResponse userTmpPwdIssueResponse = new UserRsponse.UserTmpPwdIssueResponse();
		userTmpPwdIssueResponse.setTepPwd(tmpPwd);

		return userTmpPwdIssueResponse;
	}

	public UserWithdrawVO selectUserWithdraw(UserRequest request) {
		UserWithdrawVO userWithdrawVO = settingRepository.selectUserWithDraw(request);
		return userWithdrawVO;
	}

	public void secedeUser(UserRequest.UserSecedeRequest request){
		settingRepository.secedeUser(request);
		settingRepository.secedeUserLogin(request);
		settingRepository.secedeUserWithDraw(request);
	}
	
	public void secedeUserNice(UserRequest.UserSecedeRequest request){
		
		int useAppCnt = settingRepository.secedeUseAppCheck(request);
		
		if(useAppCnt > 0) {
			log.info("사용중인 앱 존재");
			throw new BusinessException("E083",messageSource.getMessage("E083"));
		}
		settingRepository.deleteSecedeCert(request);
		settingRepository.insertSecedeUserWithDraw(request);
		settingRepository.updateSecedeUser(request);
		settingRepository.deleteSecedeUserLogin(request);
		
	}

	/*
	 * ******************************관계사******************************
	 * */

	public void insertHfnUser(HfnUserRegistRequest request){

		HfnUserRequest.HfnUserDupCheckRequest userDupCheckRequest = new HfnUserRequest.HfnUserDupCheckRequest();
		userDupCheckRequest.setHfnId(request.getHfnId());
		userDupCheckRequest.setHfnCd(request.getHfnCd());

		UserRsponse.UserDupCheckResponse userDupCheckResponse = hfnIdDupCheck(userDupCheckRequest);
		if(StringUtils.equals(userDupCheckResponse.getHfnIdDupYn(), "Y")){
			throw new BusinessException("E028",messageSource.getMessage("E028"));
		}

		request.setRoleCd(RoleEnum.resolve(request.getRoleCd()).value());

		settingRepository.insertHfnUser(request);
		settingRepository.insertHfnUserRole(request);
	}

	public UserRsponse.UserDupCheckResponse hfnIdDupCheck(HfnUserRequest.HfnUserDupCheckRequest userDupCheckRequest){
		int hfnIdDupCheck = settingRepository.hfnIdDupCheck(userDupCheckRequest);

		String hfnIdDupYn = "N";

		if(hfnIdDupCheck > 0){
			hfnIdDupYn = "Y";
		}

		UserRsponse.UserDupCheckResponse data = new UserRsponse.UserDupCheckResponse();
		data.setHfnIdDupYn(hfnIdDupYn);

		return data;
	}

	public void updateHfnUser(HfnUserUpdateRequest request){

		HfnUserRegistRequest hfnUserRegistRequest = new HfnUserRegistRequest();
        hfnUserRegistRequest.setHfnId(request.getHfnId());
        hfnUserRegistRequest.setHfnCd(request.getHfnCd());
        hfnUserRegistRequest.setRegUserId(request.getRegUser());

        HfnUserVO hfnUserInfo = settingRepository.selectHfnUserForId(hfnUserRegistRequest);
        request.setUserKey(hfnUserInfo.getUserKey());

        if (request.getNewUserPwd() != null && !StringUtils.equals(request.getNewUserPwd(), "")) {
            passwordCheck(request.getUserPwd());
            passwordCheck(request.getNewUserPwd());

            // 비밀번호 검증
            List<UserPwHisVO> userList = settingRepository.getUserIdPw(request.getUserKey());

            if (userList != null) {
                for (UserPwHisVO userData : userList) {
                    if(!commonUtil.compareWithShaStringsPw(request.getNewUserPwd(), userData.getUserPwd())) {
                        log.error("이전에 사용했던 비밀번호는 다시 사용할 수 없습니다.");
                        throw new BusinessException("E112", messageSource.getMessage("E112"));
                    }
                }
            } else {
                log.error(messageSource.getMessage("L005"));
                throw new BusinessException("L005", messageSource.getMessage("L005"));
            }

            hfnUserRegistRequest.setUserKey(request.getUserKey());
            hfnUserRegistRequest.setNewUserPwd(passwordEncoder.encode(request.getNewUserPwd()));
            request.setNewUserPwd(passwordEncoder.encode(request.getNewUserPwd()));
            adminRepository.insertHfnUserPwHis(hfnUserRegistRequest);
        }
        adminRepository.updateHfnUser(request);
        adminRepository.updateHfnUserRole(request);
    }
	
	public void updateHfnUseNice(HfnUserUpdateRequest request){
        settingRepository.updateHfnUserNice(request);
    }
	public void updateHfnUserTel(HfnUserUpdateRequest request){
		settingRepository.updateHfnUserTel(request);
	}

	public void updateHfnUserPwd(HfnUserPwdUpdateRequest request) {
		adminRepository.updateHfnUserPwd(request);
		adminRepository.setHfnUserPwdHis(request);
	}

	public HfnUserRsponsePaging selectHfnUserListPaging(HfnUserRequest hfnUserRequest){
		if(hfnUserRequest.getPageIdx() == 0)
			hfnUserRequest.setPageIdx(hfnUserRequest.getPageIdx() + 1);

		if(hfnUserRequest.getPageSize() == 0){
			hfnUserRequest.setPageSize(20);
		}

		hfnUserRequest.setPageOffset((hfnUserRequest.getPageIdx()-1)*hfnUserRequest.getPageSize());

		int totCnt = settingRepository.countHfnUserList(hfnUserRequest);
		List<HfnUserVO> list = settingRepository.selectHfnUserList(hfnUserRequest);

		HfnUserRsponsePaging hfnUserRsponsePaging = new HfnUserRsponsePaging();
		hfnUserRsponsePaging.setPageIdx(hfnUserRequest.getPageIdx());
		hfnUserRsponsePaging.setPageSize(hfnUserRequest.getPageSize());
		hfnUserRsponsePaging.setList(list);
		hfnUserRsponsePaging.setTotCnt(totCnt);
		hfnUserRsponsePaging.setSelCnt(list.size());

		return hfnUserRsponsePaging;
	}

	public void updateHfnUserStatCdChange(HfnUserRequest.HfnUserStatCdChangeRequest hfnUserStatCdChangeRequest){
		HfnUserRequest.HfnUserDetailRequest hfnUserDetailRequest = new HfnUserRequest.HfnUserDetailRequest();
		hfnUserDetailRequest.setUserKey(hfnUserStatCdChangeRequest.getUserKey());

		HfnUserVO hfnUserInfo = settingRepository.selectHfnUser(hfnUserDetailRequest);

		if(StringUtils.equals(hfnUserInfo.getUserStatCd(), "WAIT")){
			throw new BusinessException("E014",messageSource.getMessage("E014"));
		}

		if(StringUtils.equals(hfnUserInfo.getUserStatCd(), "Y")){
			hfnUserStatCdChangeRequest.setUserStatCd("N");
		}else if(StringUtils.equals(hfnUserInfo.getUserStatCd(), "N")){
			hfnUserStatCdChangeRequest.setUserStatCd("Y");
		}
		settingRepository.updateHfnUserStatCdChange(hfnUserStatCdChangeRequest);
	}

	public HfnUserRsponse selectHfnUserById(HfnUserRequest.HfnUserDetailRequest hfnUserDetailRequest){

        HfnUserVO hfnUserVO = settingRepository.selectHfnUser(hfnUserDetailRequest);

        HfnUserRsponse data = new HfnUserRsponse();
        data.setHfnUserVO(hfnUserVO);
        if(!CommonUtil.empty(hfnUserVO)) data.setSelCnt(1);

        return data;
	}

	// 관리포털 나의 정보
	public HfnUserRsponse selectHfnUser(HfnUserRequest.HfnUserDetailRequest hfnUserDetailRequest){

        HfnUserVO hfnUserVO = settingRepository.selectHfnUser(hfnUserDetailRequest);
        hfnUserVO.setRoleNm(RoleEnum.resolve(hfnUserVO.getRoleCd()).getKor());

        HfnUserRsponse data = new HfnUserRsponse();
        data.setHfnUserVO(hfnUserVO);

		if(!CommonUtil.empty(hfnUserVO)) data.setSelCnt(1);

		return data;
	}

	public HfnUserRsponse selectMyHfnMember(HfnInfoRequest hfnInfoRequest){

		List<HfnUserVO> list = settingRepository.selectMyHfnMember(hfnInfoRequest);
		HfnUserRsponse data = new HfnUserRsponse();
		data.setList(list);

		return data;
	}

	public void hfnUserPwdUpdate(HfnUserPwdUpdateRequest hfnUserPwdUpdateRequest){
		passwordCheck(hfnUserPwdUpdateRequest.getUserPwd());
		hfnUserPwdUpdateRequest.setUserPwd(passwordEncoder.encode(hfnUserPwdUpdateRequest.getUserPwd()));

		settingRepository.hfnUserPwdUpdate(hfnUserPwdUpdateRequest);
		settingRepository.hfnUserPwdHisUpdate(hfnUserPwdUpdateRequest);
	}

	public void hfnUserPwdAndTosUpdate(HfnUserRequest.HfnUserPwdAndTosUpdateRequest request){
		passwordCheck(request.getUserPwd());

		if(StringUtils.equals(request.getPortalTosYn(), "N")){
			throw new BusinessException("E015",messageSource.getMessage("E015"));
		}

		if(StringUtils.equals(request.getPrivacyTosYn(), "N")){
			throw new BusinessException("E015",messageSource.getMessage("E015"));
		}

        request.setUserPwd(passwordEncoder.encode(request.getUserPwd()));
		settingRepository.hfnUserPwdAndTosUpdate(request);

        HfnUserPwdUpdateRequest hRequest = new HfnUserPwdUpdateRequest();
        hRequest.setUserKey(request.getUserKey());
        hRequest.setUserPwd(request.getUserPwd());
        settingRepository.hfnUserPwdHisUpdate(hRequest);
	}

	/********************************승인********************************/

	public AplvRsponsePaging selectAplvListPaging(AplvRequest aplvRequest){
		if(aplvRequest.getPageIdx() == 0)
			aplvRequest.setPageIdx(aplvRequest.getPageIdx() + 1);

		if(aplvRequest.getPageSize() == 0){
			aplvRequest.setPageSize(20);
		}

		aplvRequest.setPageOffset((aplvRequest.getPageIdx()-1)*aplvRequest.getPageSize());

		int totCnt = settingRepository.countAplvList(aplvRequest);
		List<AplvVO> list = settingRepository.selectAplvList(aplvRequest);

		// 복호화: 이메일
		for(AplvVO vo : list) {
			try {
				if (vo.getUseorgUserEmail() != null && !"".equals(vo.getUseorgUserEmail())) {
					String decryptedUseorgUserEmail = cryptoUtil.decrypt(vo.getUseorgUserEmail());
					vo.setUseorgUserEmail(decryptedUseorgUserEmail);
					log.debug("복호화 - 이메일: {}", decryptedUseorgUserEmail);
				}
			} catch ( UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			}

            if (StringUtils.equals(vo.getUserNmEncrypted(), "Y")) {
                try {
                    if (vo.getRegUser() != null && !"".equals(vo.getRegUser())) {
                        String decryptedUserNm = cryptoUtil.decrypt(vo.getRegUser());
                        vo.setRegUser(decryptedUserNm);
                        log.debug("복호화 - 이름: {}", decryptedUserNm);
                    }
				} catch ( UnsupportedEncodingException e) {
					log.error(e.getMessage());
					throw new BusinessException("E026",messageSource.getMessage("E026"));
                } catch ( Exception e ) {
                    log.error(e.getMessage());
                    throw new BusinessException("E026",messageSource.getMessage("E026"));
                }
            }
		}

		// 승인권자 인지 확인 - 각 승인레벨 및 이전단계 승인정도 체크
		for(AplvVO vo : list) {
			AplvRequest.AplvDetailRequest req = new AplvRequest.AplvDetailRequest();
			req.setAplvSeqNo(vo.getAplvSeqNo());
			req.setProcUser(aplvRequest.getProcUser());
			req.setHfnCd(aplvRequest.getSearchHfnCd());
			req.setProcId(aplvRequest.getProcId());
			String s = settingRepository.selectAplvBtnYn(req);
			vo.setAplvBtnYn(s);
		}

		AplvRsponsePaging pagingData = new AplvRsponsePaging();
		pagingData.setPageIdx(aplvRequest.getPageIdx());
		pagingData.setPageSize(aplvRequest.getPageSize());
		pagingData.setList(list);
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	// 메시지허브 user parameter 생성
	public String userParamGen(String hfnCd, String hfnId, String userNm) {
		String pad = "";
		int len = 7 - hfnId.length();

		// 사번 7자리로 패딩
		for(int i=0; i<len; i++){
			pad += "0";
		}

		String msg = "";
		// 멤버스의 경우 카드 계정으로 허브메시지 발송
		if(hfnCd.equals("99")) {
			msg = "12" + pad + hfnId + "_" + userNm;
		} else {
			msg = hfnCd + pad + hfnId + "_" + userNm;
		}

		return msg;
	}

	/*******************************유효성 검사*******************************/
	public void passwordCheck(String password){

		if(!password.matches(".*[a-zA-Z].*")){
			throw new BusinessException("E006",messageSource.getMessage("E006"));
		}

		if(!password.matches(".*[0-9].*")){
			throw new BusinessException("E007",messageSource.getMessage("E007"));
		}

		if(password.length() < 8){
			throw new BusinessException("E008",messageSource.getMessage("E008"));
		}
	}

	/** 로그인 테이블 관련 **/
	public void updateUserLoginPwd(UserLoginRequest.UpdatePwdRequest request){
		settingRepository.updateUserLoginPwd(request);
		settingRepository.insertPwHisUser(request);
	}

    /*********** 개인회원 비밀번호변경 *************/
    public void setUserPwd(UserPwdUpdateRequest userPwdUpdateRequest){
        userPwdUpdateRequest.setUserPwd(passwordEncoder.encode(userPwdUpdateRequest.getUserPwd()));

        adminRepository.setUserPwd(userPwdUpdateRequest);
        adminRepository.setUserPwdLogin(userPwdUpdateRequest);
        adminRepository.setUserPwHis(userPwdUpdateRequest);

		// 비밀번호 변경시 로그인잠금 관련 세팅 초기화
		UserLoginLockCheckRequest userLoginLockCheckRequest = new UserLoginLockCheckRequest();
		userLoginLockCheckRequest.setUserId(userPwdUpdateRequest.getUserId());
		adminRepository.userLoginLockRelease(userLoginLockCheckRequest);
    }

	//------------------------------------------------------------------------------------------------------
	// 개발자공간-공지사항목록
	//------------------------------------------------------------------------------------------------------
	public NoticeResponse.NoticeListResponse selectNoticeList(NoticeRequest.NoticeListRequest noticeListRequest) {

    	if(noticeListRequest.getPageIdx() == 0) {
    		noticeListRequest.setPageIdx(1);
		}

    	if(noticeListRequest.getPageSize() == 0) {
    		noticeListRequest.setPageSize(20);
		}

    	noticeListRequest.setPageOffset((noticeListRequest.getPageIdx()-1) * noticeListRequest.getPageSize());

    	int totCnt = settingRepository.countNoticeList(noticeListRequest);
		List<NoticeVO> noticeList = settingRepository.selectNoticeList(noticeListRequest);
		NoticeResponse.NoticeListResponse response = new NoticeResponse.NoticeListResponse();
		response.setTotCnt(totCnt);
		response.setSelCnt(noticeList.size());
		response.setPageIdx(noticeListRequest.getPageIdx());
		response.setPageSize(noticeListRequest.getPageSize());
		response.setNoticeList(noticeList);

		return response;
	}

	public NoticeResponse detailNotice(String siteCd, NoticeRequest noticeRequest) {
    	NoticeVO notice = settingRepository.detailNotice(noticeRequest);

    	if(StringUtils.equals(siteCd, "userPortal") || StringUtils.equals(siteCd, "")) {
			notice.setViewCnt(notice.getViewCnt()+1);
			noticeRequest.setViewCnt(notice.getViewCnt());
			settingRepository.increaseNoticeViewCnt(noticeRequest);
		}

		NoticeResponse noticeResponse = new NoticeResponse();
		noticeResponse.setNotice(notice);

		return noticeResponse;
	}

	public FaqResponse.FaqListResponse selectFaqList(FaqRequest.FaqListRequest faqListRequest) {

    	if(faqListRequest.getPageIdx() == 0) {
    		faqListRequest.setPageIdx(1);
		}

    	if(faqListRequest.getPageSize() == 0) {
    		faqListRequest.setPageSize(20);
		}

    	faqListRequest.setPageOffset((faqListRequest.getPageIdx()-1) * faqListRequest.getPageSize());

    	int totCnt = settingRepository.countFaqList(faqListRequest);
		List<FaqVO> faqList = settingRepository.selectFaqList(faqListRequest);
		FaqResponse.FaqListResponse response = new FaqResponse.FaqListResponse();
		response.setTotCnt(totCnt);
		response.setSelCnt(faqList.size());
		response.setPageIdx(faqListRequest.getPageIdx());
		response.setPageSize(faqListRequest.getPageSize());
		response.setFaqList(faqList);

		return response;
	}

	public FaqResponse detailFaq(FaqRequest faqRequest) {
		FaqVO faq = settingRepository.detailFaq(faqRequest);
		FaqResponse faqResponse = new FaqResponse();
		faqResponse.setFaq(faq);
		return faqResponse;
	}

	/*** QNA 관련 service ***/
	public QnaResponse selectQnaList(QnaRequest qnaRequest) {

		if(qnaRequest.getPageIdx() == 0) {
			qnaRequest.setPageIdx(qnaRequest.getPageIdx()+1);
		}

		if(qnaRequest.getPageSize() == 0) {
			qnaRequest.setPageSize(20);
		}

		qnaRequest.setPageOffset((qnaRequest.getPageIdx()-1) * qnaRequest.getPageSize());

		int totCnt = adminRepository.countQna(qnaRequest);
		List<QnaVO> qnaList = adminRepository.selectQnaList(qnaRequest);

		// 복호화
		for (QnaVO qnaVO : qnaList) {
			try {
				// 이름
				if (qnaVO.getUserNm() != null && !"".equals(qnaVO.getUserNm())) {
					String decrypedUserNm = cryptoUtil.decrypt(qnaVO.getUserNm());
					qnaVO.setUserNm(decrypedUserNm);
					log.debug("복호화 - 이름: {}", decrypedUserNm);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.toString());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}

			try {
				// 전화번호
				if (qnaVO.getUserTel() != null && !"".equals(qnaVO.getUserTel())) {
					String decrypedUserTel = cryptoUtil.decrypt(qnaVO.getUserTel());
					qnaVO.setUserTel(decrypedUserTel);
					log.debug("복호화 - 전화번호: {}", decrypedUserTel);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.toString());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		QnaResponse qnaResponse = new QnaResponse();
		qnaResponse.setTotCnt(totCnt);
		qnaResponse.setSelCnt(qnaList.size());
		qnaResponse.setPageIdx(qnaRequest.getPageIdx());
		qnaResponse.setPageSize(qnaRequest.getPageSize());
		qnaResponse.setQnaList(qnaList);
		return qnaResponse;
	}
	
	/*** QNA 관련 service ***/
	public QnaResponse selectQnaListAuth(QnaRequest qnaRequest) {

		if(qnaRequest.getPageIdx() == 0) {
			qnaRequest.setPageIdx(qnaRequest.getPageIdx()+1);
		}

		if(qnaRequest.getPageSize() == 0) {
			qnaRequest.setPageSize(20);
		}

		qnaRequest.setPageOffset((qnaRequest.getPageIdx()-1) * qnaRequest.getPageSize());

		int totCnt = settingRepository.countQnaAuth(qnaRequest);
		List<QnaVO> qnaList = settingRepository.selectQnaListAuth(qnaRequest);

		// 복호화
		for (QnaVO qnaVO : qnaList) {
			try {
				// 이름
				if (qnaVO.getUserNm() != null && !"".equals(qnaVO.getUserNm())) {
					String decrypedUserNm = cryptoUtil.decrypt(qnaVO.getUserNm());
					qnaVO.setUserNm(decrypedUserNm);
					log.debug("복호화 - 이름: {}", decrypedUserNm);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.toString());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}

			try {
				// 전화번호
				if (qnaVO.getUserTel() != null && !"".equals(qnaVO.getUserTel())) {
					String decrypedUserTel = cryptoUtil.decrypt(qnaVO.getUserTel());
					qnaVO.setUserTel(decrypedUserTel);
					log.debug("복호화 - 전화번호: {}", decrypedUserTel);
				}
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				throw new BusinessException("E026",messageSource.getMessage("E026"));
			} catch ( Exception e ) {
				log.error(e.toString());
				throw new BusinessException("E026", messageSource.getMessage("E026"));
			}
		}

		QnaResponse qnaResponse = new QnaResponse();
		qnaResponse.setTotCnt(totCnt);
		qnaResponse.setSelCnt(qnaList.size());
		qnaResponse.setPageIdx(qnaRequest.getPageIdx());
		qnaResponse.setPageSize(qnaRequest.getPageSize());
		qnaResponse.setQnaList(qnaList);
		return qnaResponse;
	}

	public QnaResponse detailQna(QnaRequest qnaRequest) {
		QnaVO qna = settingRepository.detailQna(qnaRequest);

		try {
			if (qna.getUserNm() != null && !"".equals(qna.getUserNm())) {
				String decrypedUserNm = cryptoUtil.decrypt(qna.getUserNm());
				qna.setUserNm(decrypedUserNm);
				log.debug("복호화 - 이름: {}", decrypedUserNm);
			}
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.toString());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}

		try {
			if (qna.getUserTel() != null && !"".equals(qna.getUserTel())) {
				String decrypedUserTel = cryptoUtil.decrypt(qna.getUserTel());
				qna.setUserTel(decrypedUserTel);
				log.debug("복호화 - 전화번호: {}", decrypedUserTel);
			}
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			throw new BusinessException("E026",messageSource.getMessage("E026"));
		} catch ( Exception e ) {
			log.error(e.toString());
			throw new BusinessException("E026", messageSource.getMessage("E026"));
		}

		QnaResponse qnaResponse = new QnaResponse();
		qnaResponse.setQna(qna);
		return qnaResponse;
	}

    /** 인증메일 발송 조회 관련 **/
    public SndCertMgntResponse selectCertMailList(SndCertMgntRequest request) {

    	if(request.getPageIdx() == 0) {
    		request.setPageIdx(1);
		}
    	if(request.getPageSize() == 0) {
    		request.setPageSize(20);
		}

    	request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());

    	String userKey = "";
		if(request.getUserId() != null && !StringUtils.equals(request.getUserId(), "")) {
			userKey = settingRepository.selectUserkeyByUserid(request);
		}

    	int totCnt = settingRepository.countCertMailList(request);
		request.setUserKey(userKey);
		List<CertMailVO> certMailList = settingRepository.selectCertMailList(request);

		SndCertMgntResponse sndCertMgntResponse = new SndCertMgntResponse();
		sndCertMgntResponse.setCertMailList(certMailList);
		sndCertMgntResponse.setTotCnt(totCnt);
		sndCertMgntResponse.setSelCnt(certMailList.size());

		return sndCertMgntResponse;

	}

	// 배치 로그 리스트 페이징 조회
	public BatchLogListResponsePaging selectBatchLogListPaging(BatchLogListRequest request) {
		if(request.getPageIdx() == 0) {
			request.setPageIdx(request.getPageIdx() + 1);
		}

		if(request.getPageSize() == 0){
			request.setPageSize(20);
		}

		request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

		int totCnt = settingRepository.countBatchLogList(request);
		List<BatchLogVO> list = settingRepository.selectBatchLogList(request);

		BatchLogListResponsePaging pagingData = new BatchLogListResponsePaging();
		pagingData.setPageIdx(request.getPageIdx());
		pagingData.setPageSize(request.getPageSize());
		pagingData.setList(list);
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}

	/** 관리자 - 트랜잭션 관리 **/
	public TrxResponse selectTrx(TrxRequest request) {
		if(request.getPageIdx() == 0) {
			request.setPageIdx(request.getPageIdx() + 1);
		}

		if(request.getPageSize() == 0){
			request.setPageSize(20);
		}

		request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

		int totCnt = settingRepository.cntTrxList(request);
		List<TrxVO> list = settingRepository.selectTrx(request);

		TrxResponse response = new TrxResponse();
		response.setTrxList(list);
		response.setTotCnt(totCnt);
		response.setSelCnt(list.size());

		return response;
	}
	
    public UseorgRsponsePaging selectUseOrgListPaging(UseorgRequest request) {
		// pageIdx 0일때 1로 세팅
		if(request.getPageIdx() == 0)
			request.setPageIdx(request.getPageIdx() + 1);
		// 페이지사이즈 0일때 20으로 세팅
		if(request.getPageSize() == 0)
			request.setPageSize(20);
		// 페이지오프셋 세팅
		request.setPageOffset((request.getPageIdx()-1) * request.getPageSize());
		int totCnt = settingRepository.cntUseOrgList(request);

		UseorgRsponsePaging pagingData = new UseorgRsponsePaging();

		List<UseorgVO> list = settingRepository.selectUseOrgListPaging(request);
		pagingData.setList(list);
		pagingData.setPageIdx(request.getPageIdx());
		pagingData.setPageSize(request.getPageSize());
		pagingData.setTotCnt(totCnt);
		pagingData.setSelCnt(list.size());

		return pagingData;
	}
	public Integer chkUseorgDup(UseorgRequest.UseorgUpdateRequest request) {
		return settingRepository.chkUseorgDup(request);
	}
	public void insertUseOrg(UseorgRequest.UseorgUpdateRequest request, boolean sendBt) {
		ObjectMapper op = new ObjectMapper();
		UseorgRequest.UseorgChannelRequest channelRequest = new UseorgRequest.UseorgChannelRequest();
		UseorgRequest.UseorgDomainRequest domainRequest = new UseorgRequest.UseorgDomainRequest();
		UseorgRequest.UseorgNptimeRequest nptimeRequest = new UseorgRequest.UseorgNptimeRequest();
		// 1. PORTAL_USEORG_INFO 적재
		settingRepository.insertUseOrg(request);
		// 2. HIS 적재
		settingRepository.insertUseOrgHis(request);
		// 3.채널정보 PORTAL_USEORG_CHANNL_INFO
        for(int i = 0; i < request.getClientIPs().size(); i++) {
            channelRequest.setUserKey(request.getUseorgId());
            channelRequest.setCnlKey(request.getClientIPs().get(i).get("cnlKey"));
            channelRequest.setRegUser(request.getRegUser());
            settingRepository.useorgChannelReg(channelRequest);
        }
        // 4. 도메인IP 적재 PORTAL_USEORG_DOMAIN_INFO
		for(int i = 0; i < request.getUseorgDomainIPs().size(); i++) {
			domainRequest.setUserKey(request.getUseorgId());
			domainRequest.setDomainIp(request.getUseorgDomainIPs().get(i).get("domainIp"));
			domainRequest.setRegUser(request.getRegUser());
			settingRepository.useorgDomainReg(domainRequest);
		}
		// 5. NP TIME 적재 PORTAL_USEORG_DOMAIN_INFO
		for(int i = 0; i < request.getNptimes().size(); i++) {
			nptimeRequest.setUserKey(request.getUseorgId());
			nptimeRequest.setNpTime(request.getNptimes().get(i).get("npTime"));
			nptimeRequest.setRegUser(request.getRegUser());
			settingRepository.useorgNptimeReg(nptimeRequest);
		}
		
		LinkRequest dataRequst = new LinkRequest();
        dataRequst.setUserKey(request.getUseorgId());
        dataRequst.setOpType("I");

        Map<String, Object> sendData = settingRepository.useOrgDetailMap(dataRequst);
        List<Map<String, Object>> ipData = settingRepository.useOrgDetailIpMap(dataRequst);
        List<Map<String, Object>> domainIpData = settingRepository.useOrgDetailDomainIpMap(dataRequst);
        List<Map<String, Object>> nptimeData = settingRepository.useOrgDetailNptimeMap(dataRequst);

        sendData.put("ip_list", ipData);
        sendData.put("domain_ip_list", domainIpData);
        sendData.put("np_time_list", nptimeData);
//        sendData.replace("is_rcv_org", sendData.get("is_rcv_org").equals("Y"));
        if(sendBt) {
			innerTransUtil.sendBt(btTranOrgUrl, sendData);
		}
        
        // 5. 레디스에 APP IP등록
 		for(int i = 0; i < request.getClientIPs().size(); i++) {
 			RedisCommunicater.appIpRedisSet(request.getUseorgId(), request.getClientIPs().get(i).get("cnlKey"), "true");
 		}
        
        try {
        	Map<String, Object> certInfo = new HashMap<String, Object>();
            certInfo.put("useorgGb", request.getUseorgGb());
            certInfo.put("useorgNm", request.getUseorgNm());
            certInfo.put("useorgDomain", request.getUseorgDomain());
			RedisCommunicater.orgCertInfoRedisSet(request.getUserKey(), op.writeValueAsString(certInfo));
			
			if (request.getSerialNum() != null && !"".equals(request.getSerialNum())) {
				String orgJsonString = op.writeValueAsString(request);
				RedisCommunicater.orgInfoRedisSet(request.getSerialNum(), orgJsonString);
			}
		} catch (BusinessException | JsonProcessingException e) {
			log.error(e.getMessage());
		}
	}
	
	public void insertUseOrg(UseorgRequest.UseorgUpdateRequest request) {
		insertUseOrg(request, true);
	}
	
	public void useOrgStatusUpd(UseorgRequest.UseorgUpdateRequest request, boolean sendBt) {
		ObjectMapper op = new ObjectMapper();
		UseorgVO data = settingRepository.useOrgDetail(request);
		if(data != null && data.getUserKey() != null && "".equals(data.getUserKey())) {
			settingRepository.useOrgStatusUpd(request);
			
			settingRepository.insertUseOrgStatusHis(data);
			

			if(request.getUseorgStatCd().equals("DEL")) {
	        	LinkRequest dataRequst = new LinkRequest();
	            dataRequst.setUserKey(request.getUserKey());
	            dataRequst.setOpType("D");

	            Map<String, Object> sendData = settingRepository.useOrgDetailMap(dataRequst);
				if(sendBt) {
					innerTransUtil.sendBt(btTranOrgUrl, sendData);
				}
	        }
		}
		
		// 삭제, 중지시 Redis Del
		List<UseorgVO> dataVO = settingRepository.useorgIps(request);
		if(request.getUseorgStatCd().equals("DEL") || request.getUseorgStatCd().equals("OK")) {
			if (data.getSerialNum() != null && !"".equals(data.getSerialNum())) {
				try {
					RedisCommunicater.orgInfoRedisDel(data.getSerialNum());
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			for(int i = 0; i < dataVO.size(); i++) {
				RedisCommunicater.appIpRedisDel(request.getUserKey(), dataVO.get(i).getCnlKey());
			}
		} else { // 정상일 때 Redis Set
			
			try {
	        	Map<String, Object> certInfo = new HashMap<String, Object>();
	            certInfo.put("useorgGb", request.getUseorgGb());
	            certInfo.put("useorgNm", request.getUseorgNm());
	            certInfo.put("useorgDomain", request.getUseorgDomain());
				RedisCommunicater.orgCertInfoRedisSet(request.getUserKey(), op.writeValueAsString(certInfo));
				
				if (request.getSerialNum() != null && !"".equals(request.getSerialNum())) {
					String orgJsonString = op.writeValueAsString(request);
					RedisCommunicater.orgInfoRedisSet(request.getSerialNum(), orgJsonString);
				}
			} catch (BusinessException | JsonProcessingException e) {
				log.error(e.getMessage());
			}
		 
			for(int i = 0; i < dataVO.size(); i++) {
				RedisCommunicater.appIpRedisSet(request.getUserKey(), dataVO.get(i).getCnlKey(), "true");
			}
		}
	}
	// 수신자관리(기관) 상태 수정
	public void useOrgStatusUpd(UseorgRequest.UseorgUpdateRequest request) {
		useOrgStatusUpd(request, true);
	}
	public UseorgVO useOrgDetail(UseorgRequest.UseorgUpdateRequest request) {
		UseorgVO useorg = settingRepository.useOrgDetail(request);
		return useorg;
	}
	public List<HashMap> useorgIps(UseorgRequest.UseorgUpdateRequest request) {
		List<UseorgVO> dataVO = settingRepository.useorgIps(request);
		List<HashMap> data = new ArrayList<>();
		for(int i = 0; i < dataVO.size(); i++) {
			HashMap<String, String> hData = new HashMap<>();
			hData.put("cnlKey", dataVO.get(i).getCnlKey());
			data.add(i, hData);
		}
		return data;
	}
	// 수신기관 도메인 IP
	public List<HashMap> useorgDomainIps(UseorgRequest.UseorgUpdateRequest request) {
		List<UseorgVO> dataVO = settingRepository.useorgDomainIps(request);
		List<HashMap> data = new ArrayList<>();

		for(int i = 0; i < dataVO.size(); i++) {
			HashMap<String, String> hData = new HashMap<>();
			hData.put("domainIp", dataVO.get(i).getDomainIp());
			data.add(i, hData);
		}
		return data;
	}
	public void useOrgUpd(UseorgRequest.UseorgUpdateRequest request) {
		useOrgUpd(request, true);
	}
	
	public void useOrgUpd(UseorgRequest.UseorgUpdateRequest request, boolean sendBt) {
		ObjectMapper op = new ObjectMapper();
		// 기존 IP 정보 데이터 보유
		List<UseorgVO> dataVO = settingRepository.useorgIps(request);
		UseorgRequest.UseorgChannelRequest channelRequest = new UseorgRequest.UseorgChannelRequest();
		UseorgRequest.UseorgDomainRequest domainRequest = new UseorgRequest.UseorgDomainRequest();
		UseorgRequest.UseorgNptimeRequest nptimeRequest = new UseorgRequest.UseorgNptimeRequest();
		UseorgVO data = settingRepository.useOrgDetail(request);
		// 0.SerialNum Redis Del
		if (request.getUseorgStatCd().equals("OK")) {
			if (data != null && data.getSerialNum() != null && !"".equals(data.getSerialNum())) {
				try {
					RedisCommunicater.orgInfoRedisDel(data.getSerialNum());
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
		}
		
		if(data != null && data.getUserKey() != null && !"".equals(data.getUserKey())) {
			// 1.PORTAL_USEORG_INFO 정보 UPDATE
			settingRepository.useOrgUpd(request);
			
			// 2.PORTAL_USEORG_INFO_HIS 이력 UPDATE
			settingRepository.insertUseOrgHis(request);
			
			// 3. 기존 REDIS 정보 SELECT 해와서 DEL(redis, table)
			settingRepository.useorgIpsDel(request);
			settingRepository.useorgDomainIpsDel(request);
			settingRepository.useorgNptimesDel(request);
			for (int i = 0; i < dataVO.size(); i++) {
				RedisCommunicater.appIpRedisDel(request.getUserKey(), dataVO.get(i).getCnlKey());
			}
			
			// 4.변경된 IP로 SET (redis, table)
			for (int i = 0; i < request.getClientIPs().size(); i++) {
				channelRequest.setUserKey(request.getUseorgId());
				channelRequest.setCnlKey(request.getClientIPs().get(i).get("cnlKey"));
				channelRequest.setRegUser(request.getRegUser());
				settingRepository.useorgChannelReg(channelRequest);
				// 정상일때만 Redis Set
				if (request.getUseorgStatCd().equals("OK"))
					RedisCommunicater.appIpRedisSet(request.getUserKey(), channelRequest.getCnlKey(), "true");
			}
			for (int i = 0; i < request.getUseorgDomainIPs().size(); i++) {
				domainRequest.setUserKey(request.getUseorgId());
				domainRequest.setDomainIp(request.getUseorgDomainIPs().get(i).get("domainIp"));
				domainRequest.setRegUser(request.getRegUser());
				settingRepository.useorgDomainReg(domainRequest);
			}
			// 5. NP TIME 적재 PORTAL_USEORG_DOMAIN_INFO
			for(int i = 0; i < request.getNptimes().size(); i++) {
				nptimeRequest.setUserKey(request.getUseorgId());
				nptimeRequest.setNpTime(request.getNptimes().get(i).get("npTime"));
				nptimeRequest.setRegUser(request.getRegUser());
				settingRepository.useorgNptimeReg(nptimeRequest);
			}
			
			
			LinkRequest dataRequst = new LinkRequest();
	        dataRequst.setUserKey(request.getUserKey());
	        dataRequst.setOpType("M");

	        Map<String, Object> sendData = settingRepository.useOrgDetailMap(dataRequst);
	        List<Map<String, Object>> ipData = settingRepository.useOrgDetailIpMap(dataRequst);
	        List<Map<String, Object>> domainIpData = settingRepository.useOrgDetailDomainIpMap(dataRequst);
	        List<Map<String, Object>> nptimeData = settingRepository.useOrgDetailNptimeMap(dataRequst);
	        
	        sendData.put("ip_list", ipData);
	        sendData.put("domain_ip_list", domainIpData);
	        sendData.put("np_time_list", nptimeData);
//	        sendData.replace("is_rcv_org", sendData.get("is_rcv_org").equals("Y"));
	        
			if (sendBt) {
				innerTransUtil.sendBt(btTranOrgUrl, sendData);
			}
			
			// 5. SerialNum Redis Set
			if (request.getUseorgStatCd().equals("OK")) {
				
				try {
					
		        	Map<String, Object> certInfo = new HashMap<String, Object>();
		            certInfo.put("useorgGb", request.getUseorgGb());
		            certInfo.put("useorgNm", request.getUseorgNm());
		            certInfo.put("useorgDomain", request.getUseorgDomain());
					RedisCommunicater.orgCertInfoRedisSet(request.getUserKey(), op.writeValueAsString(certInfo));
					
					if (request.getSerialNum() != null && !"".equals(request.getSerialNum())) {
						String orgJsonString = op.writeValueAsString(request);
						RedisCommunicater.orgInfoRedisSet(request.getSerialNum(), orgJsonString);
					}
					
				} catch (BusinessException | JsonProcessingException e) {
					log.error(e.getMessage());
				}
			}
		}
	}
	public void insertUseOrgHis(UseorgRequest.UseorgUpdateRequest request) {
		settingRepository.insertUseOrgHis(request);
	}
	// 계정관리 계정등록
	public void addAccount(HfnUserRegistRequest request){
		HfnUserRequest.HfnUserDupCheckRequest userDupCheckRequest = new HfnUserRequest.HfnUserDupCheckRequest();
		userDupCheckRequest.setHfnId(request.getHfnId());

		UserRsponse.UserDupCheckResponse userDupCheckResponse = hfnIdDupCheck(userDupCheckRequest);
		if(StringUtils.equals(userDupCheckResponse.getHfnIdDupYn(), "Y")){
			throw new BusinessException("E028",messageSource.getMessage("E028"));
		}

		request.setRoleCd(RoleEnum.resolve(request.getRoleCd()).value());

		settingRepository.addAccount(request);
		settingRepository.addAccountRole(request);
	}
	// 계정관리 상태값 변경
	public void accountStatusUpd(HfnUserRequest.HfnUserStatCdChangeRequest request) {
		settingRepository.accountStatusUpd(request);
	}
	// 마스킹해제
	public HfnUserRsponse accountUnMasking(HfnUserRequest.HfnUserDetailRequest hfnUserDetailRequest){

		HfnUserVO hfnUserVO = settingRepository.selectHfnUser(hfnUserDetailRequest);

		HfnUserRsponse data = new HfnUserRsponse();
		data.setHfnUserVO(hfnUserVO);
		if(!CommonUtil.empty(hfnUserVO)) data.setSelCnt(1);
		return data;
	}
	public void accountRoleUpd (HfnUserUpdateRequest request){
		request.setRoleCd(RoleEnum.resolve(request.getRoleCd()).value());

		settingRepository.accountRoleUpd(request);
	}
	
	
	
	public void updateSearchTime(ApiTimesampRequest request) {
		int searchTimeCnt = settingRepository.selectSearchTimeCount(request);
		
		if(searchTimeCnt > 0) {
			settingRepository.insertSearchTime(request);
		}else {
			settingRepository.updateSearchTime(request);
		}
		
	}
	
	public TokenResponse selectTokenInfo(LinkRequest.TokenRequest request) {
		TokenResponse result = new TokenResponse();
		result.setList(settingRepository.selectTokenInfo(request));
		return result;
	}

}
