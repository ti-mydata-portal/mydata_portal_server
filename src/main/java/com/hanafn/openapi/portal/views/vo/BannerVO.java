package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("banner")
public class BannerVO {

	private int seqNo;
	private String subject;
	private String ctnt;
	private int orderNo;
	private String dispYn;
	private String bkImg;
	private String regUser;
	private String regId;
	private String regDttm;
	private String modUser;
	private String modId;
	private String modDttm;
	private String url;
	private String bannerGb;
}
