package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.CertMailVO;

import lombok.Data;

@Data
public class SndCertMgntResponse {
    private CertMailVO certMail;
    private List<CertMailVO> certMailList;
    private int totCnt;
    private int selCnt;
}
