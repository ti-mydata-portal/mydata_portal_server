package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.PlanVO;

import lombok.Data;

@Data
public class PlanResponse {

    private int pageIdx;
    private int pageSize;
    private int totCnt;
    private int selCnt;
    List<PlanVO> planList;

}
