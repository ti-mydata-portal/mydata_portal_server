package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

@Data
public class NicePayResponse {

    private int price;
    private String goodsName;
    private String buyerName;
    private String buyerTel;
    private String buyerEmail;
    private String hashString;
	private String moid;
	private String merchantID;
	private String ediDate;
	private String returnURL;
	
	@Data
    public static class NicePayResultResponse {
		private String resultMsg;
		private String resultCode;
		private String payMethod;
		private String goodsName;
		private String amt;
		private String tID;
	}

}
