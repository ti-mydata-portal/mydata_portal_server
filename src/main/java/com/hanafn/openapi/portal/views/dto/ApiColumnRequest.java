package com.hanafn.openapi.portal.views.dto;

import lombok.Data;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;

@Data
public class ApiColumnRequest {
    private String clmSel;
    private String clmCd;
    private String apiId;
    private String clmNm;
    private String clmReqDiv;
    private Long clmOrd;
    private String clmType;
    private String clmCtnt;
    private String clmDefRes;
    private String clmLength;
    private String clmValue;
    private String required;
    private String masking;
    private String logVisible;
    private String sqlInjection;
    private String nonidYn;
    private String hideYn;
    private String regUserId;
    private List<AdminRequest.ApiColumnListRequest> apiColumnList;
}
