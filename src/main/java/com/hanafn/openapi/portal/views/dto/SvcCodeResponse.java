package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.views.vo.SvcCodeVO;

import lombok.Data;

@Data
public class SvcCodeResponse {
    private int pageIdx;
    private int pageSize;
    private int totCnt;
    private int selCnt;
    List<SvcCodeVO> svcCodeList;
}
