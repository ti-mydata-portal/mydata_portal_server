package com.hanafn.openapi.portal.views.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeRequest;
import com.hanafn.openapi.portal.admin.views.dto.CommCodeResponse;
import com.hanafn.openapi.portal.admin.views.repository.AdminRepository;
import com.hanafn.openapi.portal.admin.views.vo.ProductApiInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.CommonResponse;
import com.hanafn.openapi.portal.cmct.RedisCommunicater;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.ExcelUtil;
import com.hanafn.openapi.portal.views.dto.ApiRsponsePaging;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.dto.SvcCodeResponse;
import com.hanafn.openapi.portal.views.repository.ProductRepository;
import com.hanafn.openapi.portal.views.service.ProductService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;
    private final ProductRepository repository;

    @Autowired
    MessageSourceAccessor messageSource;
    @Autowired
    FileService fileService;
    @Autowired
    CommonUtil commonUtil;
    @Autowired
    ExcelUtil excelUtil;
    @Autowired
    AdminRepository adminRepository;

    @PostMapping("/selectPrdCommList")
    public ResponseEntity<?> selectPrdCommList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody CommCodeRequest commCodeRequests) {
        CommCodeResponse data = service.selectPrdCommList(commCodeRequests);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/selectPrdList")
    public ResponseEntity<?> selectPrdList(@CurrentUser UserPrincipal currentUser, @RequestBody ProductRequest request) {
        ProductResponse.ProductListResponse data = service.selectPrdList(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/updatePrdOrder")
    public ResponseEntity<?> updatePrdOrder(@CurrentUser UserPrincipal currentUser, @RequestBody ProductRequest request) {

        int data = service.updatePrdOrder(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/selectPrdDetail")
    public ResponseEntity<?> selectPrdDetail(@CurrentUser UserPrincipal currentUser, @RequestBody ProductRequest request) {
        ProductResponse data = service.selectPrdDetail(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/selectPrdApiList")
    public ResponseEntity<?> selectPrdApiList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiRequest request) {

//        if(StringUtils.equals(currentUser.getSiteCd(),"adminPortal") ||
//                StringUtils.equals(currentUser.getSiteCd(),"pvBatch") ||
//                StringUtils.equals(currentUser.getSiteCd(),"backoffice")){
//            Collection<? extends GrantedAuthority> authorities = currentUser.getAuthorities();
//            for(GrantedAuthority ga : authorities) {
//                if(ga.getAuthority() != "ROLE_SYS_ADMIN") {
////                    if(StringUtils.isNotBlank(request.getSearchHfnCd()) && !StringUtils.equals(currentUser.getHfnCd(), request.getSearchHfnCd())) {
//                        throw new BusinessException("C001",messageSource.getMessage("잘못된 조작입니다."));
//                    }
//                }
//            }
//        }

        ApiRsponsePaging data = service.selectPrdApiList(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/selectSvcCodeList")
    public ResponseEntity<?> selectSvcCodeList(@CurrentUser UserPrincipal currentUser, @RequestBody ProductRequest request, HttpServletRequest httpRequest) throws Exception {

        SvcCodeResponse data = service.selectSvcCodeList(request, httpRequest);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/insertProduct")
    public ResponseEntity<?> insertProduct(
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "prdNm", required = false) String prdNm,
            @RequestParam(value = "prdDesc", required = false) String prdDesc,
            @RequestParam(value = "prdCtgy", required = false) String prdCtgy,
            @RequestParam(value = "prdKind", required = false) String prdKind,
            @RequestParam(value = "iconPath", required = false) MultipartFile iconPath,
            @RequestParam(value = "filePath", required = false) MultipartFile filePath,
            @RequestParam(value = "persYn", required = false) String persYn,
            @RequestParam(value = "openYn", required = false) String openYn,
            @RequestParam(value = "scope", required = false) String scope,
            @RequestParam(value = "grantType", required = false) String grantType,
            @RequestParam(value = "devHtmlCtnt", required = false) String devHtmlCtnt,
            @RequestParam(value = "serviceCd", required = false) String serviceCd,
            @RequestParam(value = "htmlCtnt", required = false) String htmlCtnt,
            @RequestParam(value = "feeAmount", required = false, defaultValue = "0") int feeAmount,
            @RequestParam(value = "limitYn", required = false) String limitYn,
            @RequestParam(value = "applDoc", required = false) String applDoc,
            @RequestParam(value = "prdMemo", required = false) String prdMemo,
            @RequestParam(value = "batchMailYn", required = false) String batchMailYn,
            @RequestPart(value = "apiList", required = false) List<ProductApiInfoVO> apiList
    ) {
        ProductRequest request = new ProductRequest();

        if (iconPath != null && !iconPath.equals("")) {
            String data = "";
            try {
                data = fileService.filePhotoSave(iconPath);
            } catch (IOException e) {
                log.info("파일 저장하기 에러 : " + e.toString());
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            } catch (Exception e) {
                log.info("파일 저장하기 에러 : " + e.toString());
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            }

            if (data != null && data != "" && !data.equals("")) {
                request.setIconPath(data);
            } else {
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            }
        }

        if (filePath != null && !filePath.equals("")) {
            String data = "";
            try {
                data = fileService.filePhotoSave(filePath);
            } catch (IOException e) {
                log.info("파일 저장하기 에러 : " + e.toString());
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            } catch (Exception e) {
                log.info("파일 저장하기 에러 : " + e.toString());
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            }
            if (data != null && data != "" && !data.equals("")) {
                request.setFilePath(data);
            } else {
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            }
        }

        request.setPrdNm(prdNm);
        request.setPrdCtgy(prdCtgy);
        request.setPrdDesc(prdDesc);
        request.setHtmlCtnt(htmlCtnt);
        request.setPersYn(persYn);
        request.setOpenYn(openYn);
        request.setScope(scope);
        request.setGrantType(grantType);
        request.setDevHtmlCtnt(devHtmlCtnt);
        request.setPrdKind(prdKind);
        request.setServiceCd(serviceCd);
        request.setFeeAmount(feeAmount);
        request.setApplDoc(applDoc);
        request.setLimitYn(limitYn);
        request.setPrdMemo(prdMemo);
        request.setBatchMailYn(batchMailYn);
        request.setRegUser(currentUser.getUserKey());
        repository.insertProduct(request);

        request.setPrdId(request.getPrdId());
        request.setApiList(apiList);
        insertPrdApiInfo(request, "Y");

        ProductRequest productRequest = new ProductRequest();
        productRequest.setPrdId(request.getPrdId());
        ProductVO productVO = adminRepository.selectPrdDetail(productRequest);

        RedisCommunicater.appPrdSvcCdRedisSet(prdCtgy, productVO.getPrdCd(), serviceCd);

        return ResponseEntity.ok(new SignUpResponse(true, "Product Insert Successfully"));
    }

    @PostMapping("/updateProduct")
    public ResponseEntity<?> updateProduct(
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "prdId", required = false) int prdId,
            @RequestParam(value = "prdCd", required = false) String prdCd,
            @RequestParam(value = "prdNm", required = false) String prdNm,
            @RequestParam(value = "prdDesc", required = false) String prdDesc,
            @RequestParam(value = "prdCtgy", required = false) String prdCtgy,
            @RequestParam(value = "prdKind", required = false) String prdKind,
            @RequestParam(value = "iconPath", required = false) MultipartFile iconPath,
            @RequestParam(value = "filePath", required = false) MultipartFile filePath,
            @RequestParam(value = "exsIconPath", required = false) String exsIconPath,
            @RequestParam(value = "exsFilePath", required = false) String exsFilePath,
            @RequestParam(value = "iconChange", defaultValue = "false") Boolean iconChange,
            @RequestParam(value = "fileChange", defaultValue = "false") Boolean fileChange,
            @RequestParam(value = "persYn", required = false) String persYn,
            @RequestParam(value = "openYn", required = false) String openYn,
            @RequestParam(value = "scope", required = false) String scope,
            @RequestParam(value = "grantType", required = false) String grantType,
            @RequestParam(value = "devHtmlCtnt", required = false) String devHtmlCtnt,
            @RequestParam(value = "serviceCd", required = false) String serviceCd,
            @RequestParam(value = "htmlCtnt", required = false) String htmlCtnt,
            @RequestParam(value = "feeAmount", required = false, defaultValue = "0") int feeAmount,
            @RequestParam(value = "limitYn", required = false) String limitYn,
            @RequestParam(value = "applDoc", required = false) String applDoc,
            @RequestParam(value = "prdMemo", required = false) String prdMemo,
            @RequestParam(value = "batchMailYn", required = false) String batchMailYn,
            @RequestPart(value = "apiList", required = false) List<ProductApiInfoVO> apiList,
            @RequestPart(value = "exstnApiList", required = false) List<ProductApiInfoVO> exstnApiList
    ) {
        ProductRequest request = new ProductRequest();
        if (iconChange) {
            String data = "";
            try {
                fileService.fileDelete(exsIconPath);
                data = fileService.filePhotoSave(iconPath);
                request.setIconPath(data);
            } catch (IOException e) {
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            } catch (Exception e) {
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            }
        }

        if (fileChange) {
            String data = "";
            try {
                fileService.fileDelete(exsFilePath);
                data = fileService.filePhotoSave(filePath);
                request.setFilePath(data);
            } catch (IOException e) {
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            } catch (Exception e) {
                throw new BusinessException("CR01", messageSource.getMessage("CR01"));
            }
        }

        request.setPrdId(prdId);
        request.setPrdCd(prdCd);
        request.setPrdNm(prdNm);
        request.setPrdCtgy(prdCtgy);
        request.setPrdDesc(prdDesc);
        request.setHtmlCtnt(htmlCtnt);
        request.setPersYn(persYn);
        request.setOpenYn(openYn);
        request.setScope(scope);
        request.setGrantType(grantType);
        request.setDevHtmlCtnt(devHtmlCtnt);
        request.setPrdKind(prdKind);
        request.setServiceCd(serviceCd);
        request.setFeeAmount(feeAmount);
        request.setApplDoc(applDoc);
        request.setLimitYn(limitYn);
        request.setPrdMemo(prdMemo);
        request.setModUser(currentUser.getUserKey());
        request.setIconChange(iconChange);
        request.setFileChange(fileChange);
        request.setBatchMailYn(batchMailYn);
        repository.updateProduct(request);

        request.setPrdId(request.getPrdId());
        request.setApiList(apiList);
        request.setExstnApiList(exstnApiList);
        request.setRegUser(currentUser.getUserKey());
        updatePrdApiInfo(request, "N");

        ProductRequest productRequest = new ProductRequest();
        productRequest.setPrdId(request.getPrdId());
        ProductVO productVO = adminRepository.selectPrdDetail(productRequest);
        RedisCommunicater.appPrdSvcCdRedisSet(prdCtgy, productVO.getPrdCd(), serviceCd);

        return ResponseEntity.ok(new SignUpResponse(true, "Product Update Successfully"));
    }

    public void insertPrdApiInfo(ProductRequest request, String useFL) {
        for (ProductApiInfoVO vo : request.getApiList()) {
            vo.setPrdId(request.getPrdId());
            vo.setApiId(vo.getApiId());
            vo.setUseFl(useFL);
            vo.setRegUser(request.getRegUser());
            repository.insertPrdApiInfo(vo);
//            RedisCommunicater.productCodeRedisSet(vo.getApiUrl(), request.getPrdId());
        }
    }

    public void updatePrdApiInfo(ProductRequest request, String useFL) {
        for (ProductApiInfoVO vo : request.getExstnApiList()) {
            vo.setPrdId(request.getPrdId());
            vo.setUseFl(useFL);
            vo.setModUser(request.getModUser());
            repository.updatePrdApiInfo(vo);
//            RedisCommunicater.productCodeRedisDel(vo.getApiUrl());
        }
        insertPrdApiInfo(request, "Y");
    }

    @PostMapping("/selectDown")
    public ResponseEntity<Resource> selectDown(HttpServletRequest httpServletRequest, HttpServletResponse response, @RequestParam int prdId, String gubun) throws FileNotFoundException {

        ProductRequest request = new ProductRequest();
        request.setPrdId(prdId);
        ProductVO prd = service.selectPrdDetail(request).getProd();

        String filePath = "";
        String contentType = null;

        if (gubun.equals("icon")) {
            filePath = prd.getIconPath();
        } else {
            filePath = prd.getFilePath();
        }

        Resource resource = fileService.loadFileAsResource(filePath);

        try {
            contentType = httpServletRequest.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.debug("Could not determine file type.");
        }

        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    /* NICE 상품목록 엑셀 다운로드 */
    @PostMapping("/excelPrd")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelDownload(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "prdCtgy", required = false) String prdCtgy,
            @RequestParam(value = "prdNm", required = false) String prdNm
    ) throws Exception {
        ProductRequest request = new ProductRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setPrdCtgy(prdCtgy);
        request.setPrdNm(prdNm);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());

        excelUtil.excelPrd(request, response);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }
}
