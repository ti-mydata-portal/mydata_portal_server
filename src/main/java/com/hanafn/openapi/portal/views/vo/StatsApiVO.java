package com.hanafn.openapi.portal.views.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("statsApi")
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatsApiVO {
    private String day;
    private String tm;
    private String hfnCd;
    private String apiId;
    private String apiNm;
    private String apiUrl;
    private String appKey;
    private String appNm;
    private String ctgrNm;
    private String userId;
    private String userNm;
    // 기관코드
    private String userKey;
    // 기관명
    private String useorgNm;
    // 분류
    private String apiType;
    // 분류 한글명
    private String apiTypeNm;
    // 일자
    private String statDate;
    // API 총 이용건수
    private String maxApiCnt;
    // 오류건수
    private String failApiCnt;
    // 완료건수
    private String successApiCnt;
    private String apiError;
    // 통계코드
    private String tmSlot;
}
