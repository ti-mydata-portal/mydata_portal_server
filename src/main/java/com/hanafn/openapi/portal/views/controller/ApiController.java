package com.hanafn.openapi.portal.views.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.SwaggerRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserDetailRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.apiCodeListRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.apiRealTimeRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.viewsv2.dto.CommonResponse;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.AuthServiceProxy;
import com.hanafn.openapi.portal.util.CommExcelUtil;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.util.ExcelUtil;
import com.hanafn.openapi.portal.views.dto.ApiColumnRequest;
import com.hanafn.openapi.portal.views.dto.ApiDetailRsponse;
import com.hanafn.openapi.portal.views.dto.ApiRegistRequest;
import com.hanafn.openapi.portal.views.dto.ApiResponse;
import com.hanafn.openapi.portal.views.dto.ApiRsponsePaging;
import com.hanafn.openapi.portal.views.dto.ApiServiceRequest;
import com.hanafn.openapi.portal.views.dto.ApiServiceResponse;
import com.hanafn.openapi.portal.views.dto.ApiStatModRequest;
import com.hanafn.openapi.portal.views.dto.EchoRequest;
import com.hanafn.openapi.portal.views.dto.EchoResponse;
import com.hanafn.openapi.portal.views.dto.StatsRsponse;
import com.hanafn.openapi.portal.views.repository.ApiRepository;
import com.hanafn.openapi.portal.views.service.ApiService;
import com.hanafn.openapi.portal.views.vo.ApiColumnVO;
import com.hanafn.openapi.portal.views.vo.EchoVO;
import com.hanafn.openapi.portal.views.vo.StatsRealTimeVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Slf4j
@RequiredArgsConstructor
public class ApiController {

    private final MessageSourceAccessor messageSource;
    private final ApiService apiService;
    private final ApiRepository apiRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    CommonUtil commonUtil;
    @Autowired
    ExcelUtil excelUtil;
    
    @Autowired
    CommExcelUtil excelExtUtil;
    
    @Autowired
	AuthServiceProxy authServiceProxy;

    @Value("${spring.profiles.active}")
    private String thisServer;

    /*******************************API*********************************/

    @PostMapping("/api")
    public ResponseEntity<?> api(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.ApiRequest request) throws Exception {
        UserDetailRequest userDetailRequest = new UserDetailRequest();
        userDetailRequest.setUserKey(currentUser.getUserKey());
        ApiDetailRsponse data = apiService.selectApi(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/apis")
    public ResponseEntity<?> apiList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiRequest request) {
    	
        ApiRsponsePaging data = apiService.selectApiListPaging(request);
        return ResponseEntity.ok(data);
    }

    @PostMapping("/apiListNoPaging")
    public ResponseEntity<?> apisListNoPaging(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody AdminRequest.ApiRequest request) {

        ApiRsponsePaging data = apiService.selectApiListNoPaging(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/apiRegist")
    public ResponseEntity<?> apiRegist(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiRegistRequest request) throws Exception {

        String apiVer = StringUtils.replaceAll(StringUtils.trim(request.getApiVer()), " ", "");
        String apiVerYn = StringUtils.trim(request.getApiVerYn());
        String apiIndustry = StringUtils.replaceAll(StringUtils.trim(request.getApiIndustry()), " ", "");
        String apiUri = StringUtils.replaceAll(StringUtils.trim(request.getApiUri()), " ", "");
        String ctgrCd = request.getCtgrCd();
        String url = "/" + request.getApiVer() + "/";
        
        if(apiVerYn != null && !"".equals(apiVerYn) && !"Y".equals(apiVerYn)) {
        	url = "/";
        }
        
        if(ctgrCd != null && !"".equals(ctgrCd) && "01".equals(ctgrCd)) {
        	url += apiUri;
        }else if(ctgrCd != null && !"".equals(ctgrCd) && "02".equals(ctgrCd)) {
        	url += apiIndustry + "/" + apiUri;
        }else {
        	url = "/" +apiUri;
        }
        
        url = StringUtils.replace(url, "-", "_");

        request.setRegUserId(currentUser.getUserKey());
        request.setRegUserName(currentUser.getUsername());
        request.setApiIndustry(apiIndustry);
        request.setApiVer(apiVer);
        request.setApiUri(apiUri);
        request.setApiUrl(url);

        //API 중복 체크
        apiService.apiDupCheck(request);
        apiService.insertApi(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Api registered successfully"));
    }

    @PostMapping("/apiUpdate")
    public ResponseEntity<?> apiUpdate(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiRegistRequest request)  throws Exception {
        request.setRegUserId(currentUser.getUserKey());
        request.setRegUserName(currentUser.getUsername());
        apiService.updateApi(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "Api Update successfully"));
    }

    @PostMapping("/apiStatCdChange")
    public ResponseEntity<?> apiStatCdChange(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiStatModRequest request) {

        request.setRegUserId(currentUser.getUserKey());
        request.setRegUserName(currentUser.getUsername());
        apiService.apiStatCdChange(request);

        return ResponseEntity.ok(new SignUpResponse(true, "Api_Stat_Cd Change successfully"));
    }

    @PostMapping("/apiDelete")
    public ResponseEntity<?> apiDelete(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiStatModRequest request) {
        request.setRegUserId(currentUser.getUserKey());
        request.setRegUserName(currentUser.getUsername());
        apiService.apiDelete(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }

    @PostMapping("/swaggerList")
    public ResponseEntity<?> swaggerList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody SwaggerRequest request) {

        ApiResponse data = apiService.swaggerList(request);

        return ResponseEntity.ok(data);
    }

    /** 대응답 정보용 : 대응답정보 리스트 불러오기 **/
    @PostMapping("/selectApiEcho")
    public ResponseEntity<?> selectApiEcho(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest request) {
        EchoResponse.EchoListResponse response = new EchoResponse.EchoListResponse();
        
        if(request.getPageIdx() == 0)
        	request.setPageIdx(request.getPageIdx() + 1);

		if(request.getPageSize() == 0){
			request.setPageSize(20);
		}

		request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
		
        try {
        	response.setTotCnt(apiRepository.selectApiEchoCount(request));
            response.setEchoList(apiRepository.selectApiEcho(request));
        } catch (BusinessException e) {
            log.error("selectApiEcho error :" + e.toString());
            throw new BusinessException("E182", messageSource.getMessage("E182"));
        }catch(Exception e) {
            log.error("selectApiEcho error :" + e.toString());
            throw new BusinessException("E182", messageSource.getMessage("E182"));
        }
        return ResponseEntity.ok(response);
    }

    /** 대응답 정보용 : api 리스트 불러오기 **/
    @PostMapping("/selectApisForEcho")
    public ResponseEntity<?> selectApisForEcho(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.ApisRequest request) {
        EchoResponse.ApisResponse response = new EchoResponse.ApisResponse();

        try {
            List<ApiVO> apis = apiRepository.selectApisForEcho(request);
            response.setApis(apis);
        } catch (BusinessException e) {
            log.error("selectApisForEcho :" + e.toString());
            throw new BusinessException("E183", messageSource.getMessage("E183"));
        }catch(Exception e) {
            log.error("selectApisForEcho :" + e.toString());
            throw new BusinessException("E183", messageSource.getMessage("E183"));
        }
        return ResponseEntity.ok(response);
    }

    /** 대응답 정보용 : 해당 api의 응답정보 가져오기 **/
    @PostMapping("/getApiResponseSetting")
    public ResponseEntity<?> getApiResponseSetting(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.ApiDetailRequest request) {
        EchoResponse.ApiDetailResponse response = new EchoResponse.ApiDetailResponse();

        try {
            ApiColumnRequest apiColumnRequest = new ApiColumnRequest();
            apiColumnRequest.setApiId(request.getApiId());
            apiColumnRequest.setClmReqDiv("RESPONSE");
            List<ApiColumnVO> apiColumnResponseList = apiRepository.selectApiColumnList(apiColumnRequest);
            apiService.settingApiColumn(apiColumnResponseList); //컬럼 타입이 LIST일 경우 하위 리스트 셋팅

            response.setColumns(apiColumnResponseList);
        } catch (BusinessException e) {
            log.error("getApiResponseSetting :" + e.toString());
            throw new BusinessException("E183", messageSource.getMessage("E183"));
        }catch(Exception e) {
            log.error("getApiResponseSetting :" + e.toString());
            throw new BusinessException("E183", messageSource.getMessage("E183"));
        }
        return ResponseEntity.ok(response);
    }

    /** 대응답 정보용 : 해당 api의 요청정보(Parameter) 가져오기 **/
    @PostMapping("/getApiRequestSetting")
    public ResponseEntity<?> getApiRequestSetting(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.ApiDetailRequest request) {
        EchoResponse.ApiDetailResponse response = new EchoResponse.ApiDetailResponse();

        try {
            ApiColumnRequest apiColumnRequest = new ApiColumnRequest();
            apiColumnRequest.setApiId(request.getApiId());
            apiColumnRequest.setClmReqDiv("REQUEST");
            List<ApiColumnVO> apiColumnRequestList = apiRepository.selectApiColumnList(apiColumnRequest);

            response.setColumns(apiColumnRequestList);
        } catch (BusinessException e) {
            log.error("getApiRequestSetting :" + e.toString());
            throw new BusinessException("E183", messageSource.getMessage("E183"));
        }catch(Exception e) {
            log.error("getApiRequestSetting :" + e.toString());
            throw new BusinessException("E183", messageSource.getMessage("E183"));
        }
        return ResponseEntity.ok(response);
    }

    /** 대응답 정보용 : 대응답 정보 등록 **/
    @PostMapping("/regApiEcho")
    public ResponseEntity<?> regApiEcho(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegEchoRequest request) {

        int cnt = apiRepository.checkApiEchoDup(request);

        if (cnt > 0) {
            log.error(messageSource.getMessage("E184"));
            throw new BusinessException("E184", messageSource.getMessage("E184"));
        }

        try {
            ApiVO api = apiRepository.getApiInfo(request);

            //String url = "/" + api.getGwType() + "/" + api.getHfnCd() + "/api/" + api.getApiSvc() + "/" + api.getApiVer() + "/" + api.getApiUri();
            request.setApiUrl(api.getApiUrl());

            request.setRegUser(currentUser.getUserKey());
            apiRepository.regApiEcho(request);
        } catch (BusinessException e) {
            log.error("regApiEcho :" + e.toString());
            throw new BusinessException("E185", messageSource.getMessage("E185"));
        } catch (Exception e) {
            log.error("regApiEcho :" + e.toString());
            throw new BusinessException("E185", messageSource.getMessage("E185"));
        }

        return ResponseEntity.ok(new SignUpResponse(true, "대응답정보를 성공적으로 등록하였습니다."));
    }

    /** 대응답 정보용 : 대응답 정보 업데이트 **/
    @PostMapping("/updateApiEcho")
    public ResponseEntity<?> updateApiEcho(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegEchoRequest request) {
        try {
            request.setModUser(currentUser.getUserKey());
            apiRepository.updateApiEcho(request);
        } catch (BusinessException e) {
            log.error("updateApiEcho :" + e.toString());
            throw new BusinessException("E186", messageSource.getMessage("E186"));
        } catch (Exception e) {
            log.error("updateApiEcho :" + e.toString());
            throw new BusinessException("E186", messageSource.getMessage("E186"));
        }

        return ResponseEntity.ok(new SignUpResponse(true, "대응답정보를 성공적으로 수정하였습니다."));
    }

    /** 대응답 정보용 : 대응답 상세 정보 불러오기 **/
    @PostMapping("/detailApiEcho")
    public ResponseEntity<?> detailApiEcho(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegEchoRequest request) {

        EchoResponse response = new EchoResponse();

        try {
            EchoVO echo = apiRepository.detailApiEcho(request);
            response.setEcho(echo);
        } catch (BusinessException e) {
            log.error("detailApiEcho :" + e.toString());
            throw new BusinessException("E187", messageSource.getMessage("E187"));
        } catch (Exception e) {
            log.error("detailApiEcho :" + e.toString());
            throw new BusinessException("E187", messageSource.getMessage("E187"));
        }

        return ResponseEntity.ok(response);
    }

    /** 대응답 정보용 : 해당 API의 대응답정보 리스트 불러오기 **/
    @PostMapping("/apiEchos")
    public ResponseEntity<?> apiEchos(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegEchoRequest request) {

        EchoResponse.EchoListResponse response = new EchoResponse.EchoListResponse();

        try {
            response.setEchoList(apiRepository.apiEchos(request));
        } catch (BusinessException e) {
            log.error("apiEchos :" + e.toString());
            throw new BusinessException("E187", messageSource.getMessage("E187"));
        } catch (Exception e) {
            log.error("apiEchos :" + e.toString());
            throw new BusinessException("E187", messageSource.getMessage("E187"));
        }

        return ResponseEntity.ok(response);
    }

    /** 대응답 정보용 : 대응답 상세 정보 삭제 **/
    @PostMapping("/deleteApiEcho")
    public ResponseEntity<?> deleteApiEcho(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegEchoRequest request) {

        try {
            request.setModUser(currentUser.getUserKey());
            apiRepository.deleteApiEcho(request);
        } catch (BusinessException e) {
            log.error("deleteApiEcho :" + e.toString());
            throw new BusinessException("E188", messageSource.getMessage("E188"));
        } catch (Exception e) {
            log.error("deleteApiEcho :" + e.toString());
            throw new BusinessException("E188", messageSource.getMessage("E188"));
        }

        return ResponseEntity.ok(new SignUpResponse(true, "대응답정보를 성공적으로 삭제하였습니다."));
    }

    /** 대응답 정보용 : API 검색 키 등록 **/
    @PostMapping("/regApiSearchKey")
    public ResponseEntity<?> regApiSearchKey(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegSearchKeyRequest request) {

        try {
            request.setRegUser(currentUser.getUserKey());
            apiRepository.regApiSearchKey(request);
        } catch (BusinessException e) {
            log.error("regApiSearchKey :" + e.toString());
            throw new BusinessException("E189", messageSource.getMessage("E189"));
        } catch (Exception e) {
            log.error("regApiSearchKey :" + e.toString());
            throw new BusinessException("E189", messageSource.getMessage("E189"));
        }

        return ResponseEntity.ok(new SignUpResponse(true, "API 검색키를 성공적으로 등록하였습니다."));
    }

    /** 대응답 정보용 : API 검색 키 업데이트 **/
    @PostMapping("/updateApiSearchKey")
    public ResponseEntity<?> updateApiSearchKey(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegSearchKeyRequest request) {

        try {
            request.setModUser(currentUser.getUserKey());

            if(StringUtils.equals(request.getSearchKey(), "NULL") || request.getSearchKey() == "NULL") {
                apiRepository.deleteApiEchoAll(request);
            }
            apiRepository.updateApiSearchKey(request);
        } catch (BusinessException e) {
            log.error("updateApiSearchKey :" + e.toString());
            throw new BusinessException("E190", messageSource.getMessage("E190"));
        } catch (Exception e) {
            log.error("updateApiSearchKey :" + e.toString());
            throw new BusinessException("E190", messageSource.getMessage("E190"));
        }

        return ResponseEntity.ok(new SignUpResponse(true, "API 검색키를 성공적으로 변경하였습니다."));
    }

    /** 대응답 정보용 : API 검색 키 가져오기 **/
    @PostMapping("/getApiSearchKey")
    public ResponseEntity<?> getApiSearchKey(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody EchoRequest.RegSearchKeyRequest request) {

        EchoResponse echo = new EchoResponse();
        try {
            echo.setEcho(apiRepository.getApiSearchKey(request));
        } catch (BusinessException e) {
            log.error("getApiSearchKey :" + e.toString());
            throw new BusinessException("E191", messageSource.getMessage("E191"));
        } catch (Exception e) {
            log.error("getApiSearchKey :" + e.toString());
            throw new BusinessException("E191", messageSource.getMessage("E191"));
        }

        return ResponseEntity.ok(echo);
    }

    @RequestMapping(value= "/cont/pricePlanList", method=RequestMethod.POST, consumes="application/json", produces="application/json")
    @ResponseBody
    public ResponseEntity<?> pricePlanList(HttpServletRequest httpRequest, HttpServletResponse response) {
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	ObjectMapper mapper = new ObjectMapper();

    	String parameter = null;
    	StringBuilder stringBuilder = new StringBuilder();
    	BufferedReader bufferedReader = null;
    	try {
    	    InputStream inputStream = httpRequest.getInputStream();
    	    if (inputStream != null) {
    	        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    	        char[] charBuffer = new char[128];
    	        int bytesRead = -1;
    	        while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
    	            stringBuilder.append(charBuffer, 0, bytesRead);
    	        }
    	    }
    	    
    	    parameter = stringBuilder.toString();
    	    
    		Map<String, Object> map = mapper.readValue(parameter, Map.class);
    		
    		result.put("resultCode", "0000");
    		result.put("message", "");

    		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
    		Map<String, Object> listInfo = new HashMap<String, Object>();
    		listInfo.put("term", "1");
    		listInfo.put("price", 1000);
    		listInfo.put("planNm", "계좌확인 선납");
    		listInfo.put("count", 5000);
    		listInfo.put("planCd", "NN01104022020082561000220");
    		list.add(listInfo);
    		result.put("list", list);
    		
    	} catch (IOException e) {
    		log.error(e.getMessage());
    	} finally {
    	    if (bufferedReader != null) {
    	        try {
    	            bufferedReader.close();
    	        } catch (IOException e) {
    	        	log.error(e.getMessage());
    	        }
    	    }
    	}
    	
    	String resultString = "";
    	
    	try {
			resultString = mapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
		}
    	
    	// 응답헤더 지정
        HttpHeaders resHeaders = new HttpHeaders();
        resHeaders.add("Content-Type", "application/json;charset=UTF-8");
    	
    	return new ResponseEntity<String>(resultString, resHeaders, HttpStatus.OK);
    }

    /* NICE API목록 엑셀 다운로드 */
    @PostMapping("/excelApi")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> excelDownload(
            @CurrentUser UserPrincipal currentUser,
            HttpServletResponse response,
            @RequestParam int pageIdx,
            @RequestParam int pageSize,
            @RequestParam(value = "ctgrCd", required = false) String ctgrCd,
            @RequestParam(value = "apiStatCd", required = false) String apiStatCd,
            @RequestParam(value = "apiNm", required = false) String apiNm
    ) throws Exception {
    	String[] sheetNm = null;
    	String[] header = null;
    	String[] exportColumn = null;
    	String fileNm = "";
    	
        ApiRequest request = new ApiRequest();
        request.setPageIdx(pageIdx);
        request.setPageSize(pageSize);
        request.setPageOffset((request.getPageIdx()-1)*request.getPageSize());
        request.setCtgrCd(ctgrCd);
        request.setApiStatCd(apiStatCd);
        request.setApiNm(apiNm);
        
        sheetNm = new String[] {"마이데이터  API목록"};
        header = new String[] {"API분류", "API명", "URL", "메서드", "상태"};
        exportColumn = new String[] {"ctgrNm", "apiNm", "apiUrl", "apiMthd", "apiStatCd"};
        fileNm = "마이데이터_API목록";
        
        List<ApiVO> vo = apiRepository.selectApiList(request);
        List<String[]> headers = new ArrayList<>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<>();
        bodys.add(vo);
        
        List<String[]> exportColumns = new ArrayList<>();
        exportColumns.add(exportColumn);
        excelExtUtil.excelDown(1, sheetNm, headers, bodys, exportColumns, response, fileNm);

        return ResponseEntity.ok(new CommonResponse("0000", "정상처리되었습니다."));
    }
    
    /*******************************API********************************/

    @PostMapping("/selectApiCodeList")
    public ResponseEntity<?> selectApiCodeList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiCodeListRequest request) {
        ApiResponse data = new ApiResponse();
        data.setList(apiService.selectApiCodeList(request));

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectRealTimeList")
    public ResponseEntity<?> selectRealTimeList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) throws Exception {

        return ResponseEntity.ok(apiService.selectRealTimeList(request));
    }
    
    @PostMapping("/selectRealTimeDetailList")
    public ResponseEntity<?> selectRealTimeDetailList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) {

        return ResponseEntity.ok(apiService.selectRealTimeDetailList(request));
    }
    
    // 실시간API사용통계 엑셀다운로드
    @PostMapping("/realTimeExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimeExcelDown( @CurrentUser UserPrincipal currentUser,
                                                HttpServletResponse response,
                                                @RequestParam String searchType,
                                                @RequestParam String prdCtgy,
                                                @RequestParam String prdId,
                                                @RequestParam String apiId
    ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
    	request.setSearchType(searchType);
    	request.setPrdCtgy(prdCtgy);
    	request.setPrdId(prdId);
    	request.setApiId(apiId);
        
        String[] header = {"상품명", "API명","URL", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectRealTimeList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 API 사용통계"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"prdNm", "apiNm", "apiUrl", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간API사용통계");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    // 실시간API사용통계상세 엑셀다운로드
    @PostMapping("/realTimeDetailExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimeDetailExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String apiUrl
                                            ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
    	request.setApiUrl(apiUrl);
        
        String[] header = {"시간", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectRealTimeDetailList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 API 사용통계 상세"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"time", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 API 사용통계 상세");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    @PostMapping("/selectUserRealTimeList")
    public ResponseEntity<?> selectUserRealTimeList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) throws Exception {

        StatsRsponse.RealTimeStatsResponse res = apiService.selectUserRealTimeList(request);

        return ResponseEntity.ok(res);
    }
    
    @PostMapping("/selectUserRealTimeDetailList")
    public ResponseEntity<?> selectUserRealTimeDetailList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) {

        return ResponseEntity.ok(apiService.selectUserRealTimeDetailList(request));
    }
    
    // 실시간API사용통계 엑셀다운로드
    @PostMapping("/realTimeUserExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimeUserExcelDown( @CurrentUser UserPrincipal currentUser, HttpServletResponse response,
                                                   @RequestParam String searchType,
                                                   @RequestParam String prdCtgy,
                                                   @RequestParam String userId
    ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
        request.setSearchType(searchType);
    	request.setPrdCtgy(prdCtgy);
    	request.setUserId(userId);
        
        String[] header = {"아이디", "앱명","상품명", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectUserRealTimeList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 이용자 사용통계"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"userId", "appNm", "prdNm", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 이용자 사용통계");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    // 실시간API사용통계상세 엑셀다운로드
    @PostMapping("/realTimeUserDetailExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimeUserDetailExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String userId,
                                           @RequestParam String appKey,
                                           @RequestParam String prdCd
                                            ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
    	request.setAppKey(appKey);
    	request.setPrdCd(prdCd);
    	request.setUserId(userId);
        
        String[] header = {"시간", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectUserRealTimeDetailList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 이용자 사용통계 상세"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"time", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 이용자 사용통계 상세");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    @PostMapping("/selectPrdRealTimeList")
    public ResponseEntity<?> selectPrdRealTimeList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) {

        return ResponseEntity.ok(apiService.selectPrdRealTimeList(request));
    }
    
    @PostMapping("/selectPrdRealTimeDetailList")
    public ResponseEntity<?> selectPrdRealTimeDetailList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) {

        return ResponseEntity.ok(apiService.selectPrdRealTimeDetailList(request));
    }
    
    // 실시간API사용통계 엑셀다운로드
    @PostMapping("/realTimePrdExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimePrdExcelDown( @CurrentUser UserPrincipal currentUser, HttpServletResponse response,
                                                   @RequestParam String searchType,
                                                   @RequestParam String prdCtgy,
                                                   @RequestParam String prdId
    ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
        request.setSearchType(searchType);
    	request.setPrdCtgy(prdCtgy);
    	request.setPrdId(prdId);
        
        String[] header = {"상품ID", "상품명", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectPrdRealTimeList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 상품 사용통계"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"prdId", "prdNm", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 상품 사용통계");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    // 실시간API사용통계상세 엑셀다운로드
    @PostMapping("/realTimePrdDetailExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimePrdDetailExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String prdCd
                                            ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
    	request.setPrdCd(prdCd);
        
        String[] header = {"시간", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectPrdRealTimeDetailList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 상품 사용통계 상세"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"time", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 상품 사용통계 상세");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    @PostMapping("/selectBizRealTimeList")
    public ResponseEntity<?> selectBizRealTimeList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) throws Exception {

        return ResponseEntity.ok(apiService.selectBizRealTimeList(request));
    }
    
    @PostMapping("/selectBizRealTimeDetailList")
    public ResponseEntity<?> selectBizRealTimeDetailList(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody apiRealTimeRequest request) {

        return ResponseEntity.ok(apiService.selectBizRealTimeDetailList(request));
    }
    
    // 실시간API사용통계 엑셀다운로드
    @PostMapping("/realTimeBizExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimeBizExcelDown( @CurrentUser UserPrincipal currentUser, HttpServletResponse response,
                                                   @RequestParam String searchType,
                                                   @RequestParam String prdCtgy,
                                                   @RequestParam String copRegNo,
                                                   @RequestParam String appKey
    ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
        request.setSearchType(searchType);
    	request.setPrdCtgy(prdCtgy);
    	request.setAppKey(appKey);
    	request.setCopRegNo(copRegNo);
        
        String[] header = {"사업자등록번호", "아이디","앱명", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectBizRealTimeList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 사업자 사용통계"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"copRegNo", "userId", "appKey", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 사업자 사용통계");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    // 실시간API사용통계상세 엑셀다운로드
    @PostMapping("/realTimeBizDetailExcelDown")
    @CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<?> realTimeBizDetailExcelDown(
                                            @CurrentUser UserPrincipal currentUser,
                                            HttpServletResponse response,
                                           @RequestParam String copRegNo,
                                           @RequestParam String userId,
                                           @RequestParam String prdCd,
                                           @RequestParam String appKey
                                            ) throws Exception {
    	apiRealTimeRequest request = new apiRealTimeRequest();
    	request.setPrdCd(prdCd);
    	request.setAppKey(appKey);
    	request.setCopRegNo(copRegNo);
    	request.setUserId(userId);
    	
        String[] header = {"시간", "건수", "평균응답시간(ms)", "최고응답시간(ms)", "에러율(%)"};
        
        StatsRsponse.RealTimeStatsResponse res = apiService.selectBizRealTimeDetailList(request);
        
        List<StatsRealTimeVO> realTimeVo = res.getDayList();
        
        String[] sheetNms = {"실시간 사업자 사용통계 상세"};
        List<String[]> headers = new ArrayList<String[]>();
        headers.add(header);
        List<List<?>> bodys = new ArrayList<List<?>>();
        bodys.add(realTimeVo);
        
        String[] exportColumn1 = {"time", "apiTrxCnt", "avgProcTerm", "maxProcTerm", "apiError"};
        
        List<String[]> exportColumns = new ArrayList<String[]>();
        
        exportColumns.add(exportColumn1);
        
        excelExtUtil.excelDown(1, sheetNms, headers, bodys, exportColumns, response, "실시간 사업자 사용통계 상세");
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Deleted successfully"));
    }
    
    @PostMapping("/selectApiSvcInfo")
    public ResponseEntity<?> selectApiSvcInfo(@CurrentUser UserPrincipal currentUser) throws Exception {
    	
        ApiServiceResponse data = apiService.selectApiSvcInfo();
        
        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/updateApiSvcInfo")
    public ResponseEntity<?> updateApiSvcInfo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody ApiServiceRequest request) {
    	request.setModUser(currentUser.getUserKey());
        apiService.updateApiSvcInfo(request);
        
        return ResponseEntity.ok(new SignUpResponse(true, "API Service Info updated successfully"));
    }
}