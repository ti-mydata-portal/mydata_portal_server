package com.hanafn.openapi.portal.views.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TermsRequest {
    private String termsCd;
    private String searchTerms;
    private String seqNo;
    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;
    
    @Data
    public static class TermsDetailRequest{
        @NotBlank
        private String seqNo;
    }
    
    @Data
    public static class TermsUserRequest{
        @NotBlank
        private String termsCd;
        private String instDttm;
    }
    
    @Data
    public static class TermsInsertRequest{
        private int seqNo;
        private String subject;
        private String ctnt;
        private String instDttm;
        private String termsCd;
        private String regId;
        private String regDttm;
    }
    
    @Data
    public static class TermsUpdateRequest{
        private int seqNo;
        private String subject;
        private String ctnt;
        private String instDttm;
        private String termsCd;
        private String modId;
        private String modDttm;
    }

}
