package com.hanafn.openapi.portal.views.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class BannerRequest {
    private String searchBanner;
    private String bannerGb;
    private String seqNo;
    private int pageIdx = 0;
    private int pageSize = 20;
    private int pageOffset = 0;
    
    @Data
    public static class BannerDetailRequest{
        @NotBlank
        private String seqNo;
    }
    
    @Data
    public static class BannerInsertRequest{
        private int seqNo;
        private String subject;
        private String ctnt;
        private int orderNo;
        private String url;
        private String bkImg;
        private String dispYn;
        private String regId;
        private String regDttm;
        private String bannerGb;
    }
    
    @Data
    public static class BannerUpdateRequest{
        private int seqNo;
        private String subject;
        private String ctnt;
        private int orderNo;
        private String url;
        private String bkImg;
        private String dispYn;
        private String modId;
        private String modDttm;
        private String bannerGb;
    }

}
