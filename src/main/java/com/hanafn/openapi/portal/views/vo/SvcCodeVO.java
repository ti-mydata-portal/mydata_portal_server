package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("svcCode")
public class SvcCodeVO {
	private String serviceNm;
    private String serviceCd;
}
