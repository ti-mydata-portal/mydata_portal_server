package com.hanafn.openapi.portal.views.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.AppsAllRequest;
import com.hanafn.openapi.portal.views.dto.AppsAllRsponse;
import com.hanafn.openapi.portal.views.dto.BillingRequest;
import com.hanafn.openapi.portal.views.dto.BillingResponse;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.dto.SettlementRequest;
import com.hanafn.openapi.portal.views.dto.SettlementResponse;
import com.hanafn.openapi.portal.views.repository.BillingRepository;
import com.hanafn.openapi.portal.views.vo.AppPrdUseVO;
import com.hanafn.openapi.portal.views.vo.AppUseVO;
import com.hanafn.openapi.portal.views.vo.BillingVO;
import com.hanafn.openapi.portal.views.vo.PayInfoVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class BillingService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BillingService.class);

    @Autowired
    BillingRepository repository;
    @Autowired
    MessageSourceAccessor messageSource;
    
    @Autowired
    @Qualifier("Crypto")
    ISecurity cryptoUtil;

    public SettlementResponse.PayInfoResponse selectPayInfoNice(BillingRequest.PaymentRequest request) {

        if(request.getPageIdx() == 0) request.setPageIdx(request.getPageIdx() + 1);
        if(request.getPageSize() == 0) request.setPageSize(20);
        request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

        int totCnt = repository.countPayInfoNice(request);
        List<PayInfoVO> payInfoList = repository.selectPayInfoNice(request);

        SettlementResponse.PayInfoResponse data = new SettlementResponse.PayInfoResponse();
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
        data.setTotCnt(totCnt);
        data.setSelCnt(payInfoList.size());
        data.setPayInfoList(payInfoList);

        return data;
    }

    public SettlementResponse.PayInfoResponse selectPayDetailInfoNice(SettlementRequest.PayDetailRequest request) {

        List<PayInfoVO> payInfoList = repository.selectPayDetailInfoNice(request);

        SettlementResponse.PayInfoResponse data = new SettlementResponse.PayInfoResponse();
        data.setPayInfoList(payInfoList);

        return data;
    }

    public BillingResponse.BillingListResponse selectBillingList(BillingRequest request) {

        if(request.getPageIdx() == 0) request.setPageIdx(request.getPageIdx() + 1);
        if(request.getPageSize() == 0) request.setPageSize(20);
        request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

        int totCnt = repository.countBillingList(request);
        List<BillingVO> list = repository.selectBillingList(request);

        BillingResponse.BillingListResponse data  = new BillingResponse.BillingListResponse();
        data.setList(list);
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
        data.setTotCnt(totCnt);
        data.setSelCnt(list.size());

        return data;
    }

    public BillingResponse selectBillingDetail(BillingRequest request) {
        BillingVO vo = repository.selectBillingDetail(request);

        BillingResponse res = new BillingResponse();
        res.setVo(vo);

        return res;
    }

    public BillingResponse.BillingListResponse selectBillingAppList(BillingRequest request) {

        if(request.getPageIdx() == 0) request.setPageIdx(request.getPageIdx() + 1);
        if(request.getPageSize() == 0) request.setPageSize(20);
        request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

        if (!StringUtils.isEmpty(request.getSearchNm())) {
            try {
                request.setSearchUserNm(cryptoUtil.encrypt(request.getSearchNm()));
            } catch (UnsupportedEncodingException e) {
                log.error("검색조건 암호화 오류.", e);
            } catch (Exception e) {
                log.error("검색조건 암호화 오류.", e);
            }
        }

        int totCnt = repository.countBillingAppList(request);
        List<BillingVO> list = repository.selectBillingAppList(request);

        for (BillingVO vo: list) {
            try {
                if (vo.getUserNm() != null && !"".equals(vo.getUserNm())) {
                    String decryptedUserNm = cryptoUtil.decrypt(vo.getUserNm());
                    vo.setUserNm(decryptedUserNm);
                    log.debug("복호화 - 이름: {}", decryptedUserNm);
                }
            } catch ( UnsupportedEncodingException e ) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            } catch ( Exception e ) {
                log.error(e.getMessage());
                throw new BusinessException("E026",messageSource.getMessage("E026"));
            }
        }

        BillingResponse.BillingListResponse resp = new BillingResponse.BillingListResponse();
        resp.setList(list);
        resp.setPageIdx(request.getPageIdx());
        resp.setPageSize(request.getPageSize());
        resp.setTotCnt(totCnt);
        resp.setSelCnt(list.size());

        return resp;
    }

    public void insertBillingInfo(BillingRequest request) {
        int dupCheck = repository.selectBillingInfoDup(request);

        if (dupCheck > 0) {
            throw new BusinessException("E805", messageSource.getMessage("E805"));
        } else {
            repository.insertBillingInfo(request);
        }
    }

    public AppsAllRsponse selectAppsBeforeNice(AppsAllRequest request){

        List<AppsVO> appsList = repository.selectAppsAllNice(request);
        AppsAllRsponse appsAllRsponse = new AppsAllRsponse();
        appsAllRsponse.setList(appsList);

        return appsAllRsponse;
    }

    public ProductResponse.ProductListResponse selectProdList(AdminRequest.PrdSettReqeust request){

        List<ProductVO> prdList = repository.selectProdList(request);
        ProductResponse.ProductListResponse response = new ProductResponse.ProductListResponse();
        response.setProdList(prdList);

        return response;
    }

    public SettlementResponse selectUseInfoNice(BillingRequest.SettlementRequest request) {

        if(request.getPageIdx() == 0) request.setPageIdx(request.getPageIdx() + 1);
        if(request.getPageSize() == 0) request.setPageSize(20);
        request.setPageOffset((request.getPageIdx() - 1) * request.getPageSize());

        int totCnt = repository.countAppUseInfo(request);
        List<AppUseVO> appUseList = repository.selectAppUseInfo(request);

        SettlementResponse data = new SettlementResponse();
        data.setAppUseList(appUseList);
        data.setPageIdx(request.getPageIdx());
        data.setPageSize(request.getPageSize());
        data.setTotCnt(totCnt);
        data.setSelCnt(appUseList.size());

        return data;
    }

    public Object selectUseDetailNice(BillingRequest.AppUseInfoRequest request) {
        SettlementResponse data = new SettlementResponse();

        List<AppPrdUseVO> appPrdUseList = repository.getAppPrdUseInfo(request);
        data.setAppPrdUseList(appPrdUseList);

        return data;
    }
}
