package com.hanafn.openapi.portal.views.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.file.FileService;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.security.dto.SignUpResponse;
import com.hanafn.openapi.portal.util.CommonUtil;
import com.hanafn.openapi.portal.views.dto.DocRequest;
import com.hanafn.openapi.portal.views.dto.DocResponse;
import com.hanafn.openapi.portal.views.dto.UseorgRequest;
import com.hanafn.openapi.portal.views.repository.DocRepository;
import com.hanafn.openapi.portal.views.service.DocService;
import com.hanafn.openapi.portal.views.vo.DocVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequestMapping("/doc")
@Slf4j
@RequiredArgsConstructor
public class DocController {

    private final DocService service;
    private final DocRepository repository;
    
    @Autowired
    CommonUtil commonUtil;
    
    @Autowired
    FileService fileService;
    
    @Autowired
    MessageSourceAccessor messageSource;

    @PostMapping("/selectDocList")
    public ResponseEntity<?> selectDocList(@Valid @RequestBody DocRequest request, @CurrentUser UserPrincipal currentUser) {
        log.debug("###################### 개발자공간-약LIST ######################");

        DocResponse.DocListResponse data = service.selectDocList(request);

        return ResponseEntity.ok(data);
    }
    
    @PostMapping("/selectDocDetail")
    public ResponseEntity<?> selectDocDetail(@Valid @RequestBody DocRequest.DocDetailRequest request, @CurrentUser UserPrincipal currentUser) {
        log.debug("###################### 개발자공간-약LIST ######################");

        DocResponse data = service.selectDoc(request);

        return ResponseEntity.ok(data);
    }

    @PostMapping("/insertDoc")
    public ResponseEntity<?> insertDoc (
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "docCd", required = false) String docCd,
            @RequestParam(value = "docNm", required = false) String docNm,
            @RequestParam(value = "fileNm", required = false) String fileNm,
            @RequestParam(value = "file", required = false) MultipartFile file
            ) {
        if(commonUtil.superAdminCheck(currentUser)) {
        	
        	DocRequest.DocInsertRequest request = new DocRequest.DocInsertRequest();
        	
            if( file != null && !file.isEmpty() ) {

                String data = "";
                try {
                    data = fileService.fileSave(file);
                } catch (IOException e) {
                    log.info("파일 저장하기 에러 : " + e.toString());
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                } catch (Exception e) {
                    log.info("파일 저장하기 에러 : " + e.toString());
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }

                if(data != null && data != "" && !data.equals("")) {
                    request.setDocPath(data);
                } else {
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }
            }
            request.setDocCd(docCd);
            request.setDocNm(docNm);
            request.setRegId(currentUser.getUserKey());
            
            repository.insertDoc(request);

            return ResponseEntity.ok(new SignUpResponse(true,"Document Insert Successfully"));
            
        } else {
            throw new BusinessException("C003", messageSource.getMessage("C003"));
        }
    }

    @PostMapping("/deleteDoc")
    public ResponseEntity<?> deleteDoc(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody DocRequest.DocDetailRequest request) {

        try {
            request.setModId(currentUser.getUserKey());
            repository.deleteDoc(request);
            return ResponseEntity.ok(new SignUpResponse(true, "Document Delete successfully"));
        } catch (BusinessException e) {
            return ResponseEntity.ok(new SignUpResponse(false, "Document Delete Fail"));
        }catch(Exception e){
            return ResponseEntity.ok(new SignUpResponse(false, "Document Delete Fail"));
        }
    }

    @PostMapping("/updateDoc")
    public ResponseEntity<?> updateDoc
    (
            @CurrentUser UserPrincipal currentUser,
            @RequestParam(value = "docCd", required = false) String docCd,
            @RequestParam(value = "docNm", required = false) String docNm,
            @RequestParam(value = "fileNm", required = false) String fileNm,
            @RequestParam(value = "docPath", required = false) String filePath,
            @RequestParam(value = "file", required = false) MultipartFile file,
            @RequestParam(value = "seqNo", required = false) int seqNo
    ) {
    	if(commonUtil.superAdminCheck(currentUser)) {
        	
        	DocRequest.DocUpdateRequest request = new DocRequest.DocUpdateRequest();
        	
            if( file != null && !file.isEmpty() ) {

                String data = "";
                try {
                    fileService.fileDelete(filePath);
                    data = fileService.fileSave(file);
                } catch (IOException e) {
                    log.info("파일 저장하기 에러 : " + e.toString());
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                } catch (Exception e) {
                    log.info("파일 저장하기 에러 : " + e.toString());
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }

                if(data != null && data != "" && !data.equals("")) {
                    request.setDocPath(data);
                } else {
                    throw new BusinessException("CR01", messageSource.getMessage("CR01"));
                }
            }
            request.setDocCd(docCd);
            request.setDocNm(docNm);
            request.setModId(currentUser.getUserKey());
            request.setSeqNo(seqNo);
            
            repository.updateDoc(request);

            return ResponseEntity.ok(new SignUpResponse(true,"Document Insert Successfully"));
            
        } else {
            throw new BusinessException("C003", messageSource.getMessage("C003"));
        }
    }
    
    @PostMapping("/selectDown")
    //@CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
    public ResponseEntity<StreamingResponseBody> docFileDownload(HttpServletRequest httpServletRequest, HttpServletResponse response, @RequestParam String seqNo) throws IOException {
        DocRequest.DocDetailRequest request = new DocRequest.DocDetailRequest();
        request.setSeqNo(seqNo);
        DocVO doc = service.selectDoc(request).getDoc();
        String filePath = doc.getDocPath();
        		
        File file = fileService.loadFile(filePath);

        // Try to determine file's content type
        String contentType = null;
        if (file != null)
            contentType = httpServletRequest.getServletContext().getMimeType(file.getAbsolutePath());

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = MediaType.APPLICATION_OCTET_STREAM.toString();
        }

        contentType = MediaType.APPLICATION_OCTET_STREAM.toString();

        //InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        String fileName = "Unknown";
        long fileLength = -1;
        if (file != null) {
            fileName = file.getName();
            fileLength = file.length();
        }

        StreamingResponseBody responseBody = outputStream -> {
           Files.copy(file.toPath(), outputStream);
        } ;

//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(contentType))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
//                .header(HttpHeaders.CONTENT_LENGTH, String.valueOf(fileLength))
//                .body(resource);


        return ResponseEntity.ok().contentLength(fileLength).body(responseBody);
    }

}
