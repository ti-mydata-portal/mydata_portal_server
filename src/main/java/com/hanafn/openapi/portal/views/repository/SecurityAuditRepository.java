package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.PortalLogRequest;
import com.hanafn.openapi.portal.admin.views.vo.HfnUserVO;
import com.hanafn.openapi.portal.admin.views.vo.PortalLogVO;
import com.hanafn.openapi.portal.views.dto.HfnUserRequest;

@Mapper
public interface SecurityAuditRepository {

    // 관리포털 계정 조회
    int countAdminAccountList(HfnUserRequest hfnUserRequest);
    List<HfnUserVO> selectAdminAccountList(HfnUserRequest hfnUserRequest);

    // 관리포털 접속이력 조회
    int countConnectionHistory(PortalLogRequest portalLogRequest);
    List<PortalLogVO> selectConnectionHistory(PortalLogRequest portalLogRequest);

    // 관리자 행위이력 목록 조회
    int countActivityHistory(PortalLogRequest portalLogRequest);
    List<PortalLogVO> selectActivityHistory(PortalLogRequest portalLogRequest);
}
