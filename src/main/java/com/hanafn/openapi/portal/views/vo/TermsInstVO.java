package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("termsInst")
public class TermsInstVO {
	private String instDttm;
}
