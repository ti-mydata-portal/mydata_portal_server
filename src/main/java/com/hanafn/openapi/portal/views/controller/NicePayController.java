
package com.hanafn.openapi.portal.views.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.UserDetailRequest;
import com.hanafn.openapi.portal.admin.views.vo.UserVO;
import com.hanafn.openapi.portal.exception.BusinessException;
import com.hanafn.openapi.portal.security.CurrentUser;
import com.hanafn.openapi.portal.security.UserPrincipal;
import com.hanafn.openapi.portal.util.DataEncrypt;
import com.hanafn.openapi.portal.util.NicePayUtil;
import com.hanafn.openapi.portal.util.service.ISecurity;
import com.hanafn.openapi.portal.views.dto.NicePayRequest;
import com.hanafn.openapi.portal.views.dto.NicePayResponse;
import com.hanafn.openapi.portal.views.repository.IpinRepository;
import com.hanafn.openapi.portal.views.repository.SettingRepository;
import com.hanafn.openapi.portal.views.service.NicePayService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Controller
@RequestMapping("/nicePay")
@Slf4j
@RequiredArgsConstructor
@SessionAttributes("cert_info")
public class NicePayController {

	@Value("${pay.merchant.key}")
	private String merchantKey;
	@Value("${pay.merchant.id}")
	private String merchantID;
	@Value("${pay.return.url}")
	private String returnURL;
//
//	private final String merchantKey = "bhWRJJg9W9hcN/i48VNaNkXsjtsJSreYAQuRgps/E/s4ZwW7HVgbsiALyaU+/jQbeqYwl2f1nWCJ0VAzjxJJRw==";
//	private final String merchantID = "niceapi10m";
//	private final String merchantKey = "EYzu8jGGMfqaDEp76gSckuvnaHHu+bC4opsSN6lHv3b2lurNYkVXrZ7Z1AoqQnXI3eLuaUFyoRNC6FkrzVjceg==";
//	private final String merchantID = "nicepay00m";
//	private final String returnURL = "/nicePay/payResult";
	
	@Autowired
    MessageSourceAccessor messageSource;
	
	@Autowired
	IpinRepository repository;
	
	@Autowired
	SettingRepository settingRepository;
	
	@Autowired
	NicePayService service;
	
	@Autowired
	NicePayUtil nicePayUtil;
	
	@Autowired
	@Qualifier("Crypto")
	ISecurity cryptoUtil;
	
	/********************************NICE PAY CATEGORY*******************************/
	@PostMapping("/payReqeust")
    public ResponseEntity<?> payReqeust(@CurrentUser UserPrincipal currentUser, HttpServletResponse response, @Valid @RequestBody NicePayRequest request) throws Exception {
    	DataEncrypt sha256Enc 	= new DataEncrypt();
    	String ediDate 			= nicePayUtil.getyyyyMMddHHmmss();	
    	
    	UserDetailRequest udRequest = new UserDetailRequest();
    	udRequest.setUserKey(currentUser.getUserKey());
    	UserVO userVo = settingRepository.selectUser(udRequest);
    	
    	int price = request.getPrice();
        String goodsName = request.getGoodsName();
        String buyerName = userVo.getUserNm();
        String buyerTel = userVo.getUserTel();
        String buyerEmail = userVo.getUserEmail();
        String hashString = sha256Enc.encrypt(ediDate + merchantID + price + merchantKey);

        // SELECT CONCAT('MOID', DATE_FORMAT(CURRENT_TIMESTAMP(6), '%Y%m%d%H%i%s%f'))
    	String moid = repository.selectMoid();
    	
    	request.setBuyerEmail(buyerEmail);
    	request.setBuyerName(buyerName);
    	request.setBuyerTel(buyerTel);
    	request.setEdiDate(ediDate);
    	request.setMoid(moid);
    	request.setSignData(hashString);
    	request.setMid(merchantID);
    	request.setRegUser(currentUser.getUserKey());

    	// INSERT PORTAL_PRD_PAY_INFO, PORTAL_PAY_HIS
    	service.createAppPrdPay(request);
    	
    	NicePayResponse data = new NicePayResponse();
    	
    	// 이름
 		if (buyerName != null && !"".equals(buyerName)) {
 			String decryptedData = cryptoUtil.decrypt(buyerName);
 			buyerName = decryptedData;
 			log.debug("복호화 - 구매 이름: {}", decryptedData);
 		}
 		
 		// 연락처
 		if (buyerTel != null && !"".equals(buyerTel)) {
 			String decryptedData = cryptoUtil.decrypt(buyerTel);
 			buyerTel = decryptedData;
 			log.debug("복호화 - 구매 연락처: {}", decryptedData);
 		}
		
		// 이메일
 		if (buyerEmail != null && !"".equals(buyerEmail)) {
 			String decryptedData = cryptoUtil.decrypt(buyerEmail);
 			buyerEmail = decryptedData;
 			log.debug("복호화 - 구매 이메일: {}", decryptedData);
 		}
    	
    	data.setPrice(price);
    	data.setGoodsName(goodsName);
    	data.setBuyerName(buyerName);
    	data.setBuyerEmail(buyerEmail);
    	data.setBuyerTel(buyerTel);
    	data.setHashString(hashString);
    	data.setMoid(moid);
    	data.setMerchantID(merchantID);
    	data.setEdiDate(ediDate);
    	data.setReturnURL(returnURL);
    	
        return ResponseEntity.ok(data);
    }
    
    
    /*
     * ******************************IPIN CATEGORY******************************
    */
	@PostMapping("/payResult")
    public ResponseEntity<?> payResult(@CurrentUser UserPrincipal currentUser, HttpServletResponse response, @Valid @RequestBody NicePayRequest.NicePayReusltRequest request, HttpServletRequest httpRequest) throws Exception {
		NicePayResponse.NicePayResultResponse data = new NicePayResponse.NicePayResultResponse();
		
    	String authResultCode 	= request.getAuthResultCode(); 	// 인증결과 : 0000(성공)
    	String authResultMsg 	= request.getAuthResultMsg(); 	// 인증결과 메시지
    	String nextAppURL 		= request.getNextAppURL(); 		// 승인 요청 URL
    	String txTid 			= request.getTxTid(); 			// 거래 ID
    	String authToken 		= request.getAuthToken(); 		// 인증 TOKEN
    	String mid 				= merchantID;
    	String amt 				= request.getAmt(); 			// 결제 금액
    	String netCancelURL 	= request.getNetCancelURL(); 	// 망취소 요청 URLß
    	
    	request.setModUser(currentUser.getUserKey());
    	
    	/*
    	****************************************************************************************
    	* <승인 결과 파라미터 정의>
    	* 샘플페이지에서는 승인 결과 파라미터 중 일부만 예시되어 있으며, 
    	* 추가적으로 사용하실 파라미터는 연동메뉴얼을 참고하세요.
    	****************************************************************************************
    	*/
    	String rResultCode 	= "";
    	String rResultMsg 	= "";
    	String rPayMethod 	= "";
    	String rGoodsName 	= "";
    	String rAmt 		= "";
    	String rTID 		= "";
    	
    	/*
    	****************************************************************************************
    	* <인증 결과 성공시 승인 진행>
    	****************************************************************************************
    	*/
    	String resultJsonStr = "";
    	
    	if(authResultCode.equals("0000")){
    		/*
    		****************************************************************************************
    		* <해쉬암호화> (수정하지 마세요)
    		* SHA-256 해쉬암호화는 거래 위변조를 막기위한 방법입니다. 
    		****************************************************************************************
    		*/
    		DataEncrypt sha256Enc 	= new DataEncrypt();
    		String ediDate			= nicePayUtil.getyyyyMMddHHmmss();
    		String signData 		= sha256Enc.encrypt(authToken + mid + amt + ediDate + merchantKey);

    		/*
    		****************************************************************************************
    		* <승인 요청>
    		* 승인에 필요한 데이터 생성 후 server to server 통신을 통해 승인 처리 합니다.
    		****************************************************************************************
    		*/
    		StringBuffer requestData = new StringBuffer();
    		requestData.append("TID=").append(txTid).append("&");
    		requestData.append("AuthToken=").append(authToken).append("&");
    		requestData.append("MID=").append(mid).append("&");
    		requestData.append("Amt=").append(amt).append("&");
    		requestData.append("EdiDate=").append(ediDate).append("&");
    		requestData.append("CharSet=").append("utf-8").append("&");
    		requestData.append("SignData=").append(signData);

    		resultJsonStr = nicePayUtil.connectToServer(requestData.toString(), nextAppURL);

    		HashMap resultData = new HashMap();
    		boolean paySuccess = false;
    		if("9999".equals(resultJsonStr)){
    			/*
    			*************************************************************************************
    			* <망취소 요청>
    			* 승인 통신중에 Exception 발생시 망취소 처리를 권고합니다.
    			*************************************************************************************
    			*/
    			requestData.append("&").append("NetCancel=").append("1");
    			String cancelResultJsonStr = nicePayUtil.connectToServer(requestData.toString(), netCancelURL);
    			
    			HashMap cancelResultData = nicePayUtil.jsonStringToHashMap(cancelResultJsonStr);
				rResultCode = (String)cancelResultData.get("ResultCode");
				rResultMsg = (String)cancelResultData.get("ResultMsg");
    			
    			request.setResultCode(rResultCode);
    			request.setResultMsg(rResultMsg);
    			
    			throw new BusinessException(rResultCode, rResultMsg);
    		}else{
    			resultData = nicePayUtil.jsonStringToHashMap(resultJsonStr);
				rResultCode 	= (String)resultData.get("ResultCode");	// 결과코드 (정상 결과코드:3001)
				rResultMsg 	= (String)resultData.get("ResultMsg");	// 결과메시지
				rPayMethod 	= (String)resultData.get("PayMethod");	// 결제수단
				rGoodsName   = (String)resultData.get("GoodsName");	// 상품명
				rAmt       	= (String)resultData.get("Amt");		// 결제 금액
				rTID       	= (String)resultData.get("TID");		// 거래번호
    			
    			String authCode = (String)resultData.get("AuthCode");
    			String authDate = (String)resultData.get("AuthDate");
    			String cardCode = (String)resultData.get("CardCode");
    			String cardName = (String)resultData.get("CardName");
    			String cardNo = (String)resultData.get("CardNo");
    			String cardCl = (String)resultData.get("CardCl");
    			String cardQuota = (String)resultData.get("CardQuota");
    			String vbankCode = (String)resultData.get("VbankBankCode");
    			String vbankName = (String)resultData.get("VbankBankName");
    			String vbankNum = (String)resultData.get("VbankNum");
    			String vbankExpDate = (String)resultData.get("VbankExpDate");
    			String vbankExpTime = (String)resultData.get("VbankExpTime");
    			
    			request.setPayMethod(rPayMethod);
    			request.setAuthCode(authCode);
    			request.setAuthDate(authDate);
    			request.setResultCode(rResultCode);
    			request.setResultMsg(rResultMsg);
    			request.setAmt(rAmt);
    			request.setCardCode(cardCode);
    			request.setCardName(cardName);
    			request.setCardNo(cardNo);
    			request.setCardQuota(cardQuota);
    			request.setCardCl(cardCl);
    			request.setVbankCode(vbankCode);
    			request.setVbankName(vbankName);
    			request.setVbankNum(vbankNum);
    			request.setVbankExpDate(vbankExpDate);
    			request.setVbankExpTime(vbankExpTime);
    			/*
    			*************************************************************************************
    			* <결제 성공 여부 확인>
    			*************************************************************************************
    			*/
    			if(rPayMethod != null){
    				if(rPayMethod.equals("CARD")){
    					if(rResultCode.equals("3001")) paySuccess = true; // 신용카드(정상 결과코드:3001)
    				}else if(rPayMethod.equals("BANK")){
    					if(rResultCode.equals("4000")) paySuccess = true; // 계좌이체(정상 결과코드:4000)
    				}else if(rPayMethod.equals("CELLPHONE")){
    					if(rResultCode.equals("A000")) paySuccess = true; // 휴대폰(정상 결과코드:A000)
    				}else if(rPayMethod.equals("VBANK")){
    					if(rResultCode.equals("4100")) paySuccess = true; // 가상계좌(정상 결과코드:4100)
    				}else if(rPayMethod.equals("SSG_BANK")){
    					if(rResultCode.equals("0000")) paySuccess = true; // SSG은행계좌(정상 결과코드:0000)
    				}else if(rPayMethod.equals("CMS_BANK")){
    					if(rResultCode.equals("0000")) paySuccess = true; // 계좌간편결제(정상 결과코드:0000)
    				}
    			}
    			
    			if(paySuccess) {
					rResultCode = "0000";
    	    	}
    			
        		data.setPayMethod(rPayMethod);
        		data.setGoodsName(rGoodsName);
        		data.setAmt(rAmt);
        		data.setTID(rTID);
    		}
    	}else{
			rResultCode 	= authResultCode;
			rResultMsg 	= authResultMsg;
    	}
    	
    	data.setResultMsg(rResultMsg);
		data.setResultCode(rResultCode);
		
		service.updateAppPrdPay(request, httpRequest);
    	
    	return ResponseEntity.ok(data);
    }
}