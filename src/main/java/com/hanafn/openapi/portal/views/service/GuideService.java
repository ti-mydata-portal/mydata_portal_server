package com.hanafn.openapi.portal.views.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ApiRequest;
import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.PrdSettReqeust;
import com.hanafn.openapi.portal.admin.views.vo.ApiTagVO;
import com.hanafn.openapi.portal.admin.views.vo.AppsVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.views.dto.ApiCtgrRsponse;
import com.hanafn.openapi.portal.views.dto.ApiDevGuideRequest;
import com.hanafn.openapi.portal.views.dto.AppsAllRequest;
import com.hanafn.openapi.portal.views.dto.AppsAllRsponse;
import com.hanafn.openapi.portal.views.dto.ProductResponse;
import com.hanafn.openapi.portal.views.repository.ApiRepository;
import com.hanafn.openapi.portal.views.repository.GuideRepository;
import com.hanafn.openapi.portal.views.vo.ApiCtgrVO;
import com.hanafn.openapi.portal.views.vo.ApiDevGuideVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor
public class GuideService {

    private final GuideRepository guideRepository;
    private final ApiRepository apiRepository;

    public List<ApiDevGuideVO> selectApiDevGuideList(ApiDevGuideRequest apiDevGuideRequest){

        List<ApiDevGuideVO> apiDevGuideList = guideRepository.selectApiDevGuideList(apiDevGuideRequest);

        for(ApiDevGuideVO apiDevGuideInfo : apiDevGuideList){
            ApiRequest apiRequest = new ApiRequest();
            apiRequest.setApiId(apiDevGuideInfo.getApiId());

            List<ApiTagVO> apiTagList = apiRepository.selectApiTagList(apiRequest);

            apiDevGuideInfo.setApiTagList(apiTagList);
        }

        return apiDevGuideList;
    }
    public AppsAllRsponse selectAppsAll(AppsAllRequest appsAllRequest){

        List<AppsVO> appsList = guideRepository.selectAppsAll(appsAllRequest);

        AppsAllRsponse appsAllRsponse = new AppsAllRsponse();
        appsAllRsponse.setList(appsList);

        return appsAllRsponse;
    }
    
    public AppsAllRsponse selectAppsAllNice(AppsAllRequest appsAllRequest){

        List<AppsVO> appsList = guideRepository.selectAppsAllNice(appsAllRequest);

        AppsAllRsponse appsAllRsponse = new AppsAllRsponse();
        appsAllRsponse.setList(appsList);

        return appsAllRsponse;
    }
    
    public AppsAllRsponse selectAppsAfterNice(AppsAllRequest appsAllRequest){
    	String [] prdKinds = {"20","21"};
    	
    	 
    	appsAllRequest.setSearchPrdKind(prdKinds);
    	
        List<AppsVO> appsList = guideRepository.selectAppsAllNice(appsAllRequest);

        AppsAllRsponse appsAllRsponse = new AppsAllRsponse();
        appsAllRsponse.setList(appsList);

        return appsAllRsponse;
    }
    
    public AppsAllRsponse selectAppsBeforeNice(AppsAllRequest appsAllRequest){
    	
		String [] prdKinds = {"10","11"};
    	 
    	appsAllRequest.setSearchPrdKind(prdKinds);
    	
        List<AppsVO> appsList = guideRepository.selectAppsAllNice(appsAllRequest);

        AppsAllRsponse appsAllRsponse = new AppsAllRsponse();
        appsAllRsponse.setList(appsList);

        return appsAllRsponse;
    }
    
    public ProductResponse.ProductListResponse selectProdList(PrdSettReqeust request){

        List<ProductVO> prdList = guideRepository.selectProdList(request);

        ProductResponse.ProductListResponse response = new ProductResponse.ProductListResponse();
        response.setProdList(prdList);

        return response;
    }

    public List<ApiDevGuideVO> selectApiDevGuideAllList(ApiDevGuideRequest apiDevGuideRequest){

        List<ApiDevGuideVO> apiDevGuideList = guideRepository.selectApiDevGuideAllList(apiDevGuideRequest);

        for(ApiDevGuideVO apiDevGuideInfo : apiDevGuideList){
            ApiRequest apiRequest = new ApiRequest();
            apiRequest.setApiId(apiDevGuideInfo.getApiId());

            List<ApiTagVO> apiTagList = apiRepository.selectApiTagList(apiRequest);

            apiDevGuideInfo.setApiTagList(apiTagList);
        }

        return apiDevGuideList;
    }

    public ApiCtgrRsponse selectDevGuidesApiCtgrAll(ApiDevGuideRequest apiDevGuideRequest){

        List<ApiCtgrVO> ctgrList = guideRepository.selectDevGuidesApiCtgrAll(apiDevGuideRequest);

        ApiCtgrRsponse apiCtgrRsponse = new ApiCtgrRsponse();
        apiCtgrRsponse.setList(ctgrList);

        return apiCtgrRsponse;
    }

    // TODO: 필터링 값 추가 입력 필요
    public String ctgrCdReplace(String ctgrCd) {
        //String buf = ctgrCd;
        String buf;

        buf = ctgrCd.replaceAll("'", "");
        buf = buf.replaceAll("--", "");
        buf = buf.replaceAll("=", "");
        buf = buf.replaceAll(">", "");
        buf = buf.replaceAll("<", "");
        buf = buf.replaceAll("/*", "");
        //buf = buf.replaceAll("*/", "");
        //buf = buf.replaceAll("\\", "");
        //buf = buf.replaceAll("+", "");
        buf = buf.replaceAll("user_", "");
        buf = buf.replaceAll("table", "");
        buf = buf.replaceAll("tables", "");
        buf = buf.replaceAll("name", "");
        buf = buf.replaceAll("column", "");
        buf = buf.replaceAll("sysolums", "");
        buf = buf.replaceAll("union", "");
        buf = buf.replaceAll("select", "");
        buf = buf.replaceAll("insert", "");
        buf = buf.replaceAll("drop", "");
        buf = buf.replaceAll("update", "");
        buf = buf.replaceAll("and", "");
        buf = buf.replaceAll("or", "");
        buf = buf.replaceAll("join", "");
        buf = buf.replaceAll("substring", "");
        buf = buf.replaceAll("from", "");
        buf = buf.replaceAll("where", "");
        buf = buf.replaceAll("declare", "");
        buf = buf.replaceAll("substr", "");
        buf = buf.replaceAll("openrowset", "");
        buf = buf.replaceAll("xp_", "");
        buf = buf.replaceAll("sysobjects", "");

        return buf;
    }

    public String settingSearchApiCtgrs(List<ApiCtgrVO> searchApiCtgrList){
        //검색 API 카테고리 설정 EX) ('ctgr1','ctgr2')
        String qApiCtgrs = "";
        if(searchApiCtgrList != null && searchApiCtgrList.size() > 0){
            if(searchApiCtgrList.size() == 1){
                String ctgrCd = ctgrCdReplace(searchApiCtgrList.get(0).getCtgrCd());
                qApiCtgrs = "('" +ctgrCd + "')";
            }
            else{
                for(int i=0; i<searchApiCtgrList.size(); i++){
                    String ctgrCd = ctgrCdReplace(searchApiCtgrList.get(i).getCtgrCd());
                    if(i == 0){
                        qApiCtgrs += "('" + ctgrCd + "'";
                    }
                    else if((i+1) == searchApiCtgrList.size()){
                        qApiCtgrs += ",'" + ctgrCd + "')";
                    }
                    else{
                        qApiCtgrs += ",'" + ctgrCd + "'";
                    }
                }
            }
        }else{
            qApiCtgrs = "('')";
        }

        return qApiCtgrs;
    }

    public String selectEnckeyByEntrCd(String entrCd){
        return guideRepository.selectEnckeyByEntrCd(entrCd);
    }
}
