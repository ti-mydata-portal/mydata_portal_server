package com.hanafn.openapi.portal.views.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;

@Data
@Alias("apiTest")
public class ApiTestVO {

	private String clientId;
	private String scr;
}
