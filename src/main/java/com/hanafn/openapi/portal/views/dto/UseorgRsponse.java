package com.hanafn.openapi.portal.views.dto;

import com.hanafn.openapi.portal.views.vo.UseorgVO;
import lombok.Data;

import java.util.List;

@Data
public class UseorgRsponse {
    private List<UseorgVO> list;
}
