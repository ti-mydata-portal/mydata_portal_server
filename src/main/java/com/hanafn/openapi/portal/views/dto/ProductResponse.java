package com.hanafn.openapi.portal.views.dto;

import java.util.List;

import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;

import lombok.Data;

@Data
public class ProductResponse {

	private ProductVO prod;
    private List<ApiVO> prdApiList;

    @Data
    public static class ProductListResponse{
        private List<ProductVO> prodList;
        private int totCnt;
        private int selCnt;
        private int pageIdx;
        private int pageSize;
    }
}
