package com.hanafn.openapi.portal.views.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.hanafn.openapi.portal.admin.views.dto.AdminRequest.ProductRequest;
import com.hanafn.openapi.portal.admin.views.vo.ApiVO;
import com.hanafn.openapi.portal.admin.views.vo.CommCodeVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductApiInfoVO;
import com.hanafn.openapi.portal.admin.views.vo.ProductVO;
import com.hanafn.openapi.portal.views.dto.MailRequest;

@Mapper
public interface ProductRepository {
	List<CommCodeVO> selectPrdCommList();

	/** 상품목록조회 **/
    int countPrdList(ProductRequest request);
	List<ProductVO> selectPrdList(ProductRequest request);

	List<ApiVO> selectPrdApiInfo(ProductRequest request);

	Integer insertProduct(ProductRequest request);
	void insertPrdApiInfo(ProductApiInfoVO vo);

    void updateProduct(ProductRequest request);
	void updatePrdApiInfo(ProductApiInfoVO vo);

	int countPrdOrder(ProductRequest request);
    void updatePrdOrder(ProductRequest request);
    
    List<String> selectPrdUseUser(MailRequest request);
    List<String> selectPrdUseUserSms(MailRequest request);
}