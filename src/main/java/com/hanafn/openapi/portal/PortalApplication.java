package com.hanafn.openapi.portal;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import com.hanafn.openapi.portal.file.FileUploadProperties;

@SpringBootApplication
@PropertySource("classpath:application.properties")
@EnableConfigurationProperties({
		FileUploadProperties.class
})
@EntityScan(basePackageClasses = {
		PortalApplication.class,
		Jsr310JpaConverters.class
})
@EnableFeignClients
public class PortalApplication { 
	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
	}

	public static void main(String[] args) {
		SpringApplication.run(PortalApplication.class, args);
	}
}

/*
/api/realTimeBizDetailExcelDown
실시간사업자사용통계 엑셀다운로드

/api/realTimeBizExcelDown
실시간사업자사용통계조회 엑셀다운로드

/api/realTimeDetailExcelDown
실시간API사용통계 엑셀다운로드

/api/realTimeExcelDown
실시간API사용통계조회 엑셀다운로드

/api/realTimePrdDetailExcelDown
실시간상품사용통계 엑셀다운로드

/api/realTimePrdExcelDown
실시간상품사용통계조회 엑셀다운로드

/api/realTimeUserDetailExcelDown
실시간이용자사용통계 엑셀다운로드

/api/realTimeUserExcelDown
실시간이용자사용통계조회 엑셀다운로드

/api/selectBizRealTimeDetailList
실시간사업자사용통계조회상세

/api/selectBizRealTimeList
실시간사업자사용통계조회(일자)

/api/selectPrdRealTimeDetailList
실시간상품사용통계조회상세

/api/selectPrdRealTimeList
실시간상품사용통계조회(일자)

/api/selectRealTimeDetailList
실시간API사용통계조회상세

/api/selectRealTimeList
실시간API사용통계조회(일자)

/api/selectUserRealTimeDetailList
실시간이용자사용통계조회상세

/api/selectUserRealTimeList
실시간이용자사용통계조회(일자)

*/
